-- MySQL dump 10.13  Distrib 5.5.57, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: crownId
-- ------------------------------------------------------
-- Server version	5.5.57-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_password_resets`
--

DROP TABLE IF EXISTS `admin_password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `admin_password_resets_email_index` (`email`),
  KEY `admin_password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_password_resets`
--

LOCK TABLES `admin_password_resets` WRITE;
/*!40000 ALTER TABLE `admin_password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_role`
--

DROP TABLE IF EXISTS `admin_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_role` (
  `role_id` int(10) unsigned NOT NULL,
  `admin_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`admin_id`),
  KEY `admin_role_admin_id_foreign` (`admin_id`),
  CONSTRAINT `admin_role_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE,
  CONSTRAINT `admin_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_role`
--

LOCK TABLES `admin_role` WRITE;
/*!40000 ALTER TABLE `admin_role` DISABLE KEYS */;
INSERT INTO `admin_role` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `admin_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'Openxcell','nirali@gmail.com','$2y$10$49NdPjGOJWoUw7tHWoumeOTw.jWTmIYTFqO8oUGqWu75EUt5fh4iS','avatar.png','active','zZui4wQljoqe2lHNXQIzGEwPrhJ0CF2mx0FAyqbtavsNKASs1nqd31EsUHmC','2017-09-18 07:02:52','2017-09-18 07:02:52'),(2,'Admin','crownidadmin@gmail.com','$2y$10$49NdPjGOJWoUw7tHWoumeOTw.jWTmIYTFqO8oUGqWu75EUt5fh4iS','avatar.png','active','GDhTJr75FSuGJREseS6KMxgjZ7fw65eg7FPfNj30jOcSgZBpdbqmdzH26Lgq','2017-09-19 07:36:23','2017-09-19 07:36:23');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `case_bite_registration`
--

DROP TABLE IF EXISTS `case_bite_registration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_bite_registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `en_name` text CHARACTER SET utf8 NOT NULL,
  `es_name` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `case_bite_registration`
--

LOCK TABLES `case_bite_registration` WRITE;
/*!40000 ALTER TABLE `case_bite_registration` DISABLE KEYS */;
INSERT INTO `case_bite_registration` VALUES (1,'No it is a bite tray and the bite is correct','No it is a bite tray and the bite is correct','No it is a bite tray and the bite is correct'),(2,'Yes it is a bite tray but we include a registration record for accuracy, Do not use the tray position ','Yes it is a bite tray but we include a registration record for accuracy, Do not use the tray position ','Yes it is a bite tray but we include a registration record for accuracy, Do not use the tray position '),(3,'Yes we include a specific registration bite for the case','Yes we include a specific registration bite for the case','Yes we include a specific registration bite for the case'),(4,'No we need a bite registration plate to be sent to us since we have no vertical dimension','No we need a bite registration plate to be sent to us since we have no vertical dimension','No we need a bite registration plate to be sent to us since we have no vertical dimension'),(5,'N/A','N/A','N/A');
/*!40000 ALTER TABLE `case_bite_registration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `case_ceramic`
--

DROP TABLE IF EXISTS `case_ceramic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_ceramic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `en_name` text CHARACTER SET utf8 NOT NULL,
  `es_name` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `case_ceramic`
--

LOCK TABLES `case_ceramic` WRITE;
/*!40000 ALTER TABLE `case_ceramic` DISABLE KEYS */;
INSERT INTO `case_ceramic` VALUES (1,'Ceramic Facial Margin','Ceramic Facial Margin','Ceramic Facial Margin');
/*!40000 ALTER TABLE `case_ceramic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `case_color`
--

DROP TABLE IF EXISTS `case_color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `en_name` text CHARACTER SET utf8 NOT NULL,
  `es_name` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `case_color`
--

LOCK TABLES `case_color` WRITE;
/*!40000 ALTER TABLE `case_color` DISABLE KEYS */;
INSERT INTO `case_color` VALUES (1,'Neck','Neck','Neck'),(2,'Body','Body','Body'),(3,'Special Instruction and Shade guide used if not vita','Special Instruction and Shade guide used if not vita','Special Instruction and Shade guide used if not vita');
/*!40000 ALTER TABLE `case_color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `case_color_body_option`
--

DROP TABLE IF EXISTS `case_color_body_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_color_body_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `en_name` text CHARACTER SET utf8 NOT NULL,
  `es_name` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `case_color_body_option`
--

LOCK TABLES `case_color_body_option` WRITE;
/*!40000 ALTER TABLE `case_color_body_option` DISABLE KEYS */;
INSERT INTO `case_color_body_option` VALUES (1,'A1','A1','A1'),(2,'A2','A2','A2'),(3,'A3','A3','A3'),(4,'A3,5','A3,5','A3,5'),(5,'A4','A4','A4'),(7,'B1','B1','B1'),(8,'B2','B2','B2'),(9,'B3','B3','B3'),(10,'B4','B4','B4'),(11,'','',''),(12,'C1','C1','C1'),(13,'C2','C2','C2'),(14,'C3','C3','C3'),(15,'C4','C4','C4'),(16,'','',''),(17,'D2','D2','D2'),(18,'D3','D3','D3'),(19,'D4','D4','D4'),(20,'','','');
/*!40000 ALTER TABLE `case_color_body_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `case_color_neck_option`
--

DROP TABLE IF EXISTS `case_color_neck_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_color_neck_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `en_name` text CHARACTER SET utf8 NOT NULL,
  `es_name` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `case_color_neck_option`
--

LOCK TABLES `case_color_neck_option` WRITE;
/*!40000 ALTER TABLE `case_color_neck_option` DISABLE KEYS */;
INSERT INTO `case_color_neck_option` VALUES (1,'A1','A1','A1'),(2,'A2','A2','A2'),(3,'A3','A3','A3'),(4,'A3,5','A3,5','A3,5'),(5,'A4','A4','A4'),(7,'B1','B1','B1'),(8,'B2','B2','B2'),(9,'B3','B3','B3'),(10,'B4','B4','B4'),(11,'','',''),(12,'C1','C1','C1'),(13,'C2','C2','C2'),(14,'C3','C3','C3'),(15,'C4','C4','C4'),(16,'','',''),(17,'D2','D2','D2'),(18,'D3','D3','D3'),(19,'D4','D4','D4'),(20,'','','');
/*!40000 ALTER TABLE `case_color_neck_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `case_delivery`
--

DROP TABLE IF EXISTS `case_delivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `en_name` text CHARACTER SET utf8 NOT NULL,
  `es_name` text CHARACTER SET utf8 NOT NULL,
  `setting` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `case_delivery`
--

LOCK TABLES `case_delivery` WRITE;
/*!40000 ALTER TABLE `case_delivery` DISABLE KEYS */;
INSERT INTO `case_delivery` VALUES (1,'Metal try-in','Metal try-in','Metal try-in',1),(2,'Procelain 1st bake-biscuit try-in','Procelain 1st bake-biscuit try-in','Procelain 1st bake-biscuit try-in',1),(3,'Glazed-finished','Glazed-finished','Glazed-finished',1),(4,'Other','Other','Other',2),(5,'Date limit if any','Date limit if any','Date limit if any',2);
/*!40000 ALTER TABLE `case_delivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `case_image`
--

DROP TABLE IF EXISTS `case_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_image` (
  `case_id` int(10) unsigned NOT NULL,
  `image_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`case_id`,`image_id`),
  KEY `case_image_image_id_foreign` (`image_id`),
  CONSTRAINT `case_image_case_id_foreign` FOREIGN KEY (`case_id`) REFERENCES `cases` (`id`) ON DELETE CASCADE,
  CONSTRAINT `case_image_image_id_foreign` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `case_image`
--

LOCK TABLES `case_image` WRITE;
/*!40000 ALTER TABLE `case_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `case_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `case_material`
--

DROP TABLE IF EXISTS `case_material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `en_name` text CHARACTER SET utf8 NOT NULL,
  `es_name` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `case_material`
--

LOCK TABLES `case_material` WRITE;
/*!40000 ALTER TABLE `case_material` DISABLE KEYS */;
INSERT INTO `case_material` VALUES (1,'Metal Ceramic','Metal Ceramic','Metal Ceramic'),(2,'All Ceramic','All Ceramic','All Ceramic'),(3,'Zirconia','Zirconia','Zirconia'),(4,'Resin-Composite','Resin-Composite','Resin-Composite'),(5,'All Metal','All Metal','All Metal');
/*!40000 ALTER TABLE `case_material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `case_metal_ceramic`
--

DROP TABLE IF EXISTS `case_metal_ceramic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_metal_ceramic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `en_name` text CHARACTER SET utf8 NOT NULL,
  `es_name` text CHARACTER SET utf8 NOT NULL,
  `setting` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `case_metal_ceramic`
--

LOCK TABLES `case_metal_ceramic` WRITE;
/*!40000 ALTER TABLE `case_metal_ceramic` DISABLE KEYS */;
INSERT INTO `case_metal_ceramic` VALUES (1,'Precious-Gold','Precious-Gold','Precious-Gold',2),(2,'Semi Precious','Semi Precious','Semi Precious',1),(3,'Nickel Chrome','Nickel Chrome','Nickel Chrome',1),(4,'Cabalt Chrome','Cabalt Chrome','Cabalt Chrome',1),(5,'Titanium','Titanium','Titanium',1),(6,'Other','Other','Other',1);
/*!40000 ALTER TABLE `case_metal_ceramic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `case_prosthesis`
--

DROP TABLE IF EXISTS `case_prosthesis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_prosthesis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `en_name` text CHARACTER SET utf8 NOT NULL,
  `es_name` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `case_prosthesis`
--

LOCK TABLES `case_prosthesis` WRITE;
/*!40000 ALTER TABLE `case_prosthesis` DISABLE KEYS */;
INSERT INTO `case_prosthesis` VALUES (1,'Separate Crowns','Separate Crowns','Separate Crowns'),(2,'Bridge/Splinted Crowns','Bridge/Splinted Crowns','Bridge/Splinted Crowns'),(3,'Onlay/Inlay','Onlay/Inlay','Onlay/Inlay'),(4,'Porcelain Veneers','Porcelain Veneers','Porcelain Veneers');
/*!40000 ALTER TABLE `case_prosthesis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `case_prosthetic_margins`
--

DROP TABLE IF EXISTS `case_prosthetic_margins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_prosthetic_margins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `en_name` text CHARACTER SET utf8 NOT NULL,
  `es_name` text CHARACTER SET utf8 NOT NULL,
  `setting` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `case_prosthetic_margins`
--

LOCK TABLES `case_prosthetic_margins` WRITE;
/*!40000 ALTER TABLE `case_prosthetic_margins` DISABLE KEYS */;
INSERT INTO `case_prosthetic_margins` VALUES (1,'No Metal visible','No Metal visible','No Metal visible',1),(2,'Metal lingual/palatal','Metal lingual/palatal','Metal lingual/palatal',1),(3,'1 mm metal all around (perio)','1 mm metal all around (perio)','1 mm metal all around (perio)',1),(4,'1 mm metal furcation (perio)','1 mm metal furcation (perio)','1 mm metal furcation (perio)',1);
/*!40000 ALTER TABLE `case_prosthetic_margins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cases`
--

DROP TABLE IF EXISTS `cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `case_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `have_child` tinyint(1) DEFAULT '0',
  `clinic_id` int(11) NOT NULL,
  `lab_id` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prosthesis_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `problematic_teeth` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `missing_teeth` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `bite_registration_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `material_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `material_other` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metal_ceramic_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `metal_ceramic_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prosthetic_margins` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `ceramic_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `ceramic_facial_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `color_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color_neck_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `color_body_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `delivery_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `delivery_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_selected` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `delivery_date_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `end_date` date DEFAULT NULL,
  `qr_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `bar_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `status` enum('Pending','Completed') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Pending',
  `created_by_id` int(11) NOT NULL DEFAULT '0',
  `created_by_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `retake_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `solder_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tryin_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tryin_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `process_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'DD/MM/YYYY',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cases`
--

LOCK TABLES `cases` WRITE;
/*!40000 ALTER TABLE `cases` DISABLE KEYS */;
INSERT INTO `cases` VALUES (1,'CLIDMY K0001',0,0,5,7,1,'Kai','Ghj','1','28','31','4','2',NULL,'0',NULL,'0','0',NULL,'1',NULL,'1,2',NULL,'3',NULL,NULL,NULL,NULL,NULL,'crownid/cases/qr_images/lhIe9JZo1HOSVFCdje7TgLCJCUAedOD4Lc70Lfty.jpeg','0293275739','Pending',5,'Clinic',NULL,NULL,NULL,NULL,'2017-09-19 06:28:38','2017-09-19 06:28:38',NULL,'2017-09-19'),(2,'CLIDMY A0002',0,0,5,7,1,'Aer','Ert','1','18,28','48,38','2','1',NULL,'6','hello','1','1','hi','1,3','hello my','3,9,14,19',NULL,'4','Hello','1','Hello my I',NULL,NULL,'crownid/cases/qr_images/TKH2IuKtPrykLQfTcMEz2u8MLc7QLPB8df4qjuaI.jpeg','0293275741','Pending',5,'Clinic',NULL,'hello there',NULL,NULL,'2017-09-19 13:44:12','2017-09-19 13:44:48',NULL,'2017-09-19');
/*!40000 ALTER TABLE `cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cases_image`
--

DROP TABLE IF EXISTS `cases_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases_image` (
  `cases_id` int(10) unsigned NOT NULL,
  `image_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`cases_id`,`image_id`),
  KEY `cases_image_image_id_foreign` (`image_id`),
  CONSTRAINT `cases_image_cases_id_foreign` FOREIGN KEY (`cases_id`) REFERENCES `cases` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cases_image_image_id_foreign` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cases_image`
--

LOCK TABLES `cases_image` WRITE;
/*!40000 ALTER TABLE `cases_image` DISABLE KEYS */;
INSERT INTO `cases_image` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `cases_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinic_details`
--

DROP TABLE IF EXISTS `clinic_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinic_details` (
  `user_id` int(10) unsigned NOT NULL,
  `clinic_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `city` text COLLATE utf8_unicode_ci,
  `country` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prosthetic_id` int(11) NOT NULL DEFAULT '0',
  `metal_id` int(11) NOT NULL DEFAULT '0',
  `delivery_id` int(11) NOT NULL DEFAULT '0',
  `total_credits` int(11) NOT NULL DEFAULT '0',
  `used_credits` int(11) NOT NULL DEFAULT '0',
  `is_purchased` tinyint(1) NOT NULL DEFAULT '0',
  KEY `clinic_details_user_id_foreign` (`user_id`),
  CONSTRAINT `clinic_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinic_details`
--

LOCK TABLES `clinic_details` WRITE;
/*!40000 ALTER TABLE `clinic_details` DISABLE KEYS */;
INSERT INTO `clinic_details` VALUES (5,'vs clinic','Bodakdev','Ahmedabad','India',NULL,NULL,0,0,0,0,0,0),(6,'pn','Ahmedabad','Ahmedabad','India',NULL,NULL,0,0,0,0,0,0),(8,'abc','ahmedabad','ahmedabad','india',NULL,NULL,0,0,0,0,0,0);
/*!40000 ALTER TABLE `clinic_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinic_doctor`
--

DROP TABLE IF EXISTS `clinic_doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinic_doctor` (
  `clinic_id` int(10) unsigned NOT NULL,
  `doctor_id` int(10) unsigned NOT NULL,
  `is_purchased` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`clinic_id`,`doctor_id`),
  KEY `clinic_doctor_doctor_id_foreign` (`doctor_id`),
  CONSTRAINT `clinic_doctor_clinic_id_foreign` FOREIGN KEY (`clinic_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `clinic_doctor_doctor_id_foreign` FOREIGN KEY (`doctor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinic_doctor`
--

LOCK TABLES `clinic_doctor` WRITE;
/*!40000 ALTER TABLE `clinic_doctor` DISABLE KEYS */;
INSERT INTO `clinic_doctor` VALUES (5,1,1),(5,3,1),(5,4,1),(6,4,1);
/*!40000 ALTER TABLE `clinic_doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinic_labs`
--

DROP TABLE IF EXISTS `clinic_labs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinic_labs` (
  `clinic_id` int(10) unsigned NOT NULL,
  `lab_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`clinic_id`,`lab_id`),
  KEY `clinic_labs_lab_id_foreign` (`lab_id`),
  CONSTRAINT `clinic_labs_clinic_id_foreign` FOREIGN KEY (`clinic_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `clinic_labs_lab_id_foreign` FOREIGN KEY (`lab_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinic_labs`
--

LOCK TABLES `clinic_labs` WRITE;
/*!40000 ALTER TABLE `clinic_labs` DISABLE KEYS */;
INSERT INTO `clinic_labs` VALUES (5,7),(3,9),(6,10);
/*!40000 ALTER TABLE `clinic_labs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinic_password_resets`
--

DROP TABLE IF EXISTS `clinic_password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinic_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `clinic_password_resets_email_index` (`email`),
  KEY `clinic_password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinic_password_resets`
--

LOCK TABLES `clinic_password_resets` WRITE;
/*!40000 ALTER TABLE `clinic_password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `clinic_password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinics`
--

DROP TABLE IF EXISTS `clinics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `clinics_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinics`
--

LOCK TABLES `clinics` WRITE;
/*!40000 ALTER TABLE `clinics` DISABLE KEYS */;
/*!40000 ALTER TABLE `clinics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,'crownid/cases/images/F5TE1LJIJPv2Mho6kR56AfDgBII8014ek9Nyn3ru.jpeg',1,'2017-09-19 06:28:38','2017-09-19 06:28:38'),(2,'crownid/cases/images/lYrXMm0fGgR6G2b5ORIYADNoCBZKO8M6cpshuZz5.jpeg',1,'2017-09-19 13:44:13','2017-09-19 13:44:13');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2017_07_03_102039_create_admins_table',1),(4,'2017_07_03_102040_create_admin_password_resets_table',1),(5,'2017_07_03_120245_create_role_table',1),(6,'2017_07_17_110241_page_table_add',1),(7,'2017_08_11_114808_create_clinics_table',1),(8,'2017_08_11_114809_create_clinic_password_resets_table',1),(9,'2017_08_16_054931_create_cases_table',2),(10,'2017_08_17_122001_add_setting_to_users',2),(11,'2017_09_18_070056_add_cases_fileds',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `description_es` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(2,2),(3,2),(4,2),(5,2),(6,2),(7,2),(8,2),(10,2),(11,2),(12,2),(13,2),(14,2),(15,2),(16,2),(17,2),(18,2);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'developerOnly','developerOnly',NULL,NULL),(2,'admin.create','Admin Create',NULL,NULL),(3,'admin.list','Admin List',NULL,NULL),(4,'admin.remove','Admin Remove',NULL,NULL),(5,'admin.update','Admin Update',NULL,NULL),(6,'conversion','Conversion',NULL,NULL),(7,'currency','Currency',NULL,NULL),(8,'pages','pages',NULL,NULL),(9,'role','Role',NULL,NULL),(10,'user.create','User Create',NULL,NULL),(11,'user.delete','User Delete',NULL,NULL),(12,'user.edit','User Edit',NULL,NULL),(13,'user.list','User List',NULL,NULL),(14,'user.update','User Update',NULL,NULL),(15,'clinic','clinic','2017-09-19 07:39:15','2017-09-19 07:39:15'),(16,'doctor','doctor','2017-09-19 07:39:35','2017-09-19 07:39:35'),(17,'lab','lab','2017-09-19 07:39:41','2017-09-19 07:39:41'),(18,'case','case','2017-09-19 07:39:57','2017-09-19 07:39:57');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (4,1),(4,3),(4,4),(3,5),(3,6),(5,7),(3,8),(5,9),(5,10);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'developer','Developer',NULL,NULL),(2,'Super','Super',NULL,NULL),(3,'Clinic','Clinic',NULL,NULL),(4,'Doctor','Doctor',NULL,NULL),(5,'Lab','Lab',NULL,NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_number` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referral_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` enum('en','es') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en' COMMENT 'en = English, es = Spanish',
  `date_format` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'dd-mm-yyyy',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `is_purchased` tinyint(1) NOT NULL DEFAULT '0',
  `is_registered` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'clinic is fully register or not',
  `verified` tinyint(1) NOT NULL DEFAULT '1',
  `api_token` text COLLATE utf8_unicode_ci,
  `otp` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `device_type` enum('ios','android') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ios',
  `device_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `preferred_metal` int(11) DEFAULT '0',
  `prosthetic_margins` int(11) DEFAULT '0',
  `delivery` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_referral_code_unique` (`referral_code`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Doctor One','Doctor','One','doctorone@gamil.com','doctorone','$2y$10$mUFXl7p/mjKaY4JcZn7sxe9j/tFpd2o1Hn7FSg7EWhZz0hV6XEmU.','+917878787878','','','123','en','dd-mm-yy',1,1,0,1,'igNwk7Jfd1pHP3rz5du9dwwYCeKwXOdJ7VqUpRWAPFd8NVg4KdESO6g1XL95',NULL,'ios',NULL,'2017-09-19 11:46:48','2017-09-21 05:32:24',0,0,0),(3,'Doctor Two','Doctor','Two','doctortwo@gamil.com','doctortwo','$2y$10$mUFXl7p/mjKaY4JcZn7sxe9j/tFpd2o1Hn7FSg7EWhZz0hV6XEmU.','+917878787878','','','456','en','dd-mm-yyyy',1,1,0,1,NULL,NULL,'ios',NULL,'2017-09-19 11:46:48','2017-09-19 07:18:40',0,0,0),(4,'Doctor Three','Doctor','Three','doctorthree@gamil.com','doctorthree','$2y$10$mUFXl7p/mjKaY4JcZn7sxe9j/tFpd2o1Hn7FSg7EWhZz0hV6XEmU.','+917878787878','','','789','en','dd-mm-yyyy',1,1,0,1,NULL,NULL,'ios',NULL,'2017-09-19 11:46:48','2017-09-19 07:18:40',0,0,0),(5,NULL,'Virag','Shah','clinic2@gmail.com','clinic2','$2y$10$mUFXl7p/mjKaY4JcZn7sxe9j/tFpd2o1Hn7FSg7EWhZz0hV6XEmU.','7990461464','crownid/users/profile_images/fkGl7XejkZf2E0saxmnsiJVdtx0WEbCRMfNxhb9c.jpeg',NULL,'VTT9Zq0hcI','en','yyyy-mm-dd',1,1,0,1,'QtC2DVsC0NGxcxWJaQoIOtbKpNd3VP5R3tm9SX7EYTgzJKwNwiZ4wRE1EkMq',NULL,'ios',NULL,'2017-09-19 06:16:48','2017-09-21 05:50:24',0,0,0),(6,NULL,'clinic1','pn','clinic1@gmail.com','clinic1','$2y$10$M1ZqFEvNOohxOCNEwi6vbuG6xfbdwzigbPHY1A2qbayOxcgdG9tVe','2578965','crownid/users/profile_images/I0hv7g5IBJbxvDv8vowgQXBcgsFX9tANwWEs3C4q.png',NULL,'ZlAlrecv4u','en','dd-mm-yyyy',1,1,0,1,'5Rw61h17kcH2Bi7UAZmPSUgH2U3URQ9ToOqY89FHZSw2Y2uQvjgeVLtFlcHM',NULL,'ios','12345645645645','2017-09-19 06:19:35','2017-09-21 06:06:29',0,0,0),(7,'My Lab','lab',NULL,'lab@gmail.com',NULL,NULL,'653325','crownid/users/profile_images/DK4Hi8qCHWGVvHiyxrFtB1SLOBaF0R2mYInkhc5Y.jpeg',NULL,'MbuswXmCu5','en','dd-mm-yyyy',1,0,0,1,'qjRjlsie2RrzkJjK8HbZINtjO2OPO2DFFQhqOnZvcjvSqO75CWffP5Ilmeeo',NULL,'ios',NULL,'2017-09-19 06:27:29','2017-09-19 06:27:29',0,0,0),(8,NULL,'chirag','darji','chirag.darji@openxcell.info','chirag','$2y$10$k0modXoEhW5po/Q5t/vazO2dnIsrLG.cL1ZmLIwAB42hvZIpWmupG','9998037772',NULL,NULL,'juSD7TKLjI','en','dd-mm-yyyy',1,1,0,1,'jPvHC62PfPWLYjchZLZwO2HIq0OG8hPETGVe8V4dpnvebBajY9Kf0jBW5k90',NULL,'ios','12345645645645','2017-09-19 07:15:56','2017-09-19 07:16:59',0,0,0),(9,'khushbu bhadani','Khushi',NULL,'khushbu1@gmail.com',NULL,NULL,'+917878787878',NULL,NULL,'ip72z9uhhs','en','dd-mm-yyyy',1,0,0,1,'uz28VTbzapdjbw4C2kQVSjL9Ygqr1tOx8LoSTmeNeE1JuoVHDLXNWlMcnSMW',NULL,'ios',NULL,'2017-09-20 09:12:25','2017-09-20 09:12:25',0,0,0),(10,'clinic4','clinic1@gmail.com',NULL,'clinic1',NULL,NULL,'9898989898',NULL,NULL,'tNWpzC519I','en','dd-mm-yyyy',1,0,0,1,'giOpkhN7rjnBVsYhKJD790PGMh4Xgin4am7mzWnP3EzqjzjbpPojRgLUeB5T',NULL,'ios',NULL,'2017-09-20 10:58:25','2017-09-20 10:58:25',0,0,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-21  7:36:55
