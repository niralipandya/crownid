<?php

Route::get('/home', "lab\DashController@index")->name('home');
Route::get('/pendingCaseList', "lab\DashController@pendingCaseList")->name('pendingCaseList');


Route::get('/chat/cases/{id}',"lab\ChatController@cases");
Route::get('/chat/{id?}',"lab\ChatController@index")->name('chatlist');
Route::post('/chat/list-ajax',"lab\ChatController@listAjax");
Route::resource('/chat',"lab\ChatController");


Route::get('/caseshow/{id}',"lab\LabController@caseshow");
Route::post('/casefilter',"lab\LabController@casefilter");
Route::post('/updateStatusFromDelay',"lab\LabController@updateStatusFromDelay");
Route::post('/updateStatus',"lab\LabController@updateStatus");
Route::post('/updateTrying',"lab\LabController@updateTrying");
Route::post('/delayed',"lab\LabController@delayed");
Route::post('/case/receive',"lab\LabController@receiveStatus");
Route::get('/cliniccaselibrary/{id}',"lab\LabController@cliniccaselibrary")->name('cliniccaselibrary');
Route::get('/caselibrary',"lab\LabController@caselibrary")->name('caselibrary');
Route::get('/caselogs/{id}',"lab\LabController@caselogs")->name('caselogs');
Route::get('/profile',"lab\LabController@profile")->name('profile');
Route::resource('/labs',"lab\LabController");

Route::get('/delayBoxCase',"lab\DelayboxController@delayBoxCase")->name('delayBoxCase');
Route::resource('/delaybox',"lab\DelayboxController");
Route::resource('/report',"lab\ReportController");

Route::post('/settings/checkpassword',"lab\SettingController@checkpassword");
Route::get('/changepassword',"lab\SettingController@changepasseordpage")->name('changepasseordpage');
Route::post('/settings/updatepassword',"lab\SettingController@changePassword");
Route::get('/settings/getProfileData',"lab\SettingController@getProfileData");
Route::put('/settings/setProfileData',"lab\SettingController@setProfileData");
Route::get('/settings/getData',"lab\SettingController@getData");
Route::put('/settings/setDateFormat',"lab\SettingController@setDateFormat");
Route::get('/settings/getDefaultData',"lab\SettingController@getDefaultData");
Route::put('/settings/setData',"lab\SettingController@setData")->name('setData');
Route::get('/settings/aboutus',"lab\SettingController@aboutus")->name('aboutus');
Route::get('/settings/contactus',"lab\SettingController@contactus")->name('contactus');

Route::resource('/settings',"lab\SettingController");


Route::get('/cliniccaselibrary/{id}',"lab\ClinicController@cliniccaselibrary")->name('cliniccaselibrary');
Route::resource('/clinics',"lab\ClinicController");


Route::get('/calender',"lab\DashController@calender")->name('calender');
Route::get('/calendercaselist',"lab\DashController@calendercaselist")->name('calendercaselist');
#Route::resource('/home',"lab\DashController");

Route::get('/badgeUpdate',"lab\NotificationController@badgeUpdate")->name('badgeUpdate');
Route::resource('/notifications', 'lab\NotificationController');