<?php

/*Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('clinic')->user();

    //dd($users);

    return view('clinic.home');
})->name('home');
*/
Route::get('/home', "Clinic\DashController@index")->name('home');


Route::post('/token/buy',  'Clinic\DoctorController@buy_token');
Route::get('/token/purchase',"Clinic\DoctorController@purchase_token");
Route::post('/doctor/buy_doctor',  'Clinic\DoctorController@buy_doctor');
Route::get('/doctor/image',			"Clinic\DoctorController@image");
Route::put('/doctor/status',		"Clinic\DoctorController@switchStatus");
Route::post('/doctor/image',		"Clinic\DoctorController@image");
Route::post('/doctor/statusUpdate', 'Clinic\DoctorController@statusUpdate');
Route::post('/doctor/checkdoctorexist',  'Clinic\DoctorController@checkEmail');
Route::post('/doctor/checkdoctorexistedit',  'Clinic\DoctorController@checkEmailEdit');
Route::post('/doctor/checkUsernameexist',  'Clinic\DoctorController@checkUsernameexist');
Route::post('/doctor/linkdoctor',  'Clinic\DoctorController@linkdoctor');
Route::get('/doctor/show/{id}',"Clinic\DoctorController@show");
Route::get('/doctor/purchase',"Clinic\DoctorController@purchase");
Route::resource('/doctor',"Clinic\DoctorController");

Route::get('/chat/cases/{id}',"Clinic\ChatController@cases");
Route::get('/chat/{id?}',"Clinic\ChatController@index")->name('chatlist');
Route::post('/chat/list-ajax',"Clinic\ChatController@listAjax");
Route::resource('/chat',"Clinic\ChatController");

Route::get('/caseshow/{id}',"Clinic\LabController@caseshow");
Route::post('/casefilter',"Clinic\LabController@casefilter");
Route::get('/labcaselibrary/{id}',"Clinic\LabController@labcaselibrary")->name('labcaselibrary');
Route::get('/caselibrary',"Clinic\LabController@caselibrary")->name('caselibrary');
Route::post('/updateStatus',"Clinic\LabController@updateStatus");
Route::resource('/labs',"Clinic\LabController");

Route::get('/report/labs',"Clinic\ReportController@labs");
Route::get('/report/doctor',"Clinic\ReportController@doctor");
Route::resource('/report',"Clinic\ReportController");

Route::resource('/support',"Clinic\SupportController");

Route::get('/profile',"Clinic\SettingsController@profile")->name('profile');
Route::post('/settings/checkpassword',"Clinic\SettingsController@checkpassword");
Route::get('/changepassword',"Clinic\SettingsController@changepasseordpage")->name('changepasseordpage');
Route::post('/settings/updatepassword',"Clinic\SettingsController@changePassword");
Route::get('/settings/getProfileData',"Clinic\SettingsController@getProfileData");
Route::put('/settings/setProfileData',"Clinic\SettingsController@setProfileData");

Route::get('/settings/getData',"Clinic\SettingsController@getData");
Route::put('/settings/setDateFormat',"Clinic\SettingsController@setDateFormat");
Route::get('/settings/getDefaultData',"Clinic\SettingsController@getDefaultData");
Route::put('/settings/setData',"Clinic\SettingsController@setData")->name('setData');
Route::get('/settings/aboutus',"Clinic\SettingsController@aboutus")->name('aboutus');
Route::get('/settings/contactus',"Clinic\SettingsController@contactus")->name('contactus');
Route::get('/settings/default',"Clinic\SettingsController@defaultSettings")->name('defaultSettings');
Route::resource('/settings',"Clinic\SettingsController");

Route::post('/delayBoxCaseFilter',"Clinic\CaseController@delayBoxCaseFilter");
Route::post('/addtodelaybox',"Clinic\CaseController@addtodelaybox");
Route::get('/delayBoxCase',"Clinic\CaseController@delayBoxCase")->name('delayBoxCase');
Route::get('/doctorcaselibrary/{id}',"Clinic\CaseController@doctorcaselibrary")->name('doctorcaselibrary');
Route::get('/caselogs/{id}',"Clinic\CaseController@caselogs")->name('caselogs');
Route::resource('/cases',"Clinic\CaseController");

Route::get('/checkislogin', 'Clinic\DashController@checkislogin');
Route::get('/calender',"Clinic\DashController@calender")->name('calender');
Route::get('/calendercaselist',"Clinic\DashController@calendercaselist")->name('calendercaselist');

Route::get('/badgeUpdate',"Clinic\NotificationController@badgeUpdate")->name('badgeUpdate');
Route::resource('/notifications', 'Clinic\NotificationController');	

Route::post('/subscription/upgrade',"Clinic\SubscriptionController@upgrade");
Route::get('/subscription/current',"Clinic\SubscriptionController@current")->name('current');
Route::resource('/subscription', 'Clinic\SubscriptionController');	

