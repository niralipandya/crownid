<?php

Route::get('/home', function (App\ClinicDetail $ClinicDetail) {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();
	
    $pass['cliniccount'] = $ClinicDetail->count(); 
    return view('admin.home')->with($pass);
})->name('home');



//Administrator
Route::get('/profile',"Admin\AdminController@profileEdit");
Route::put('/profile/{admin}',"Admin\AdminController@profileUpdate");
Route::put('/changepassword/{admin}',"Admin\AdminController@changePassword");
Route::put('/administrator/status',"Admin\AdminController@switchStatus");
Route::post('/administrator/removeBulk',"Admin\AdminController@destroyBulk");
Route::put('/administrator/statusBulk',"Admin\AdminController@switchStatusBulk");
Route::resource('/administrator',"Admin\AdminController");


/**
 * ROLES
 */
Route::get('/role/{role}/permissions',"Admin\RoleController@permissions");
Route::get('/rolePermissions',"Admin\RoleController@rolePermissions")->name('myrolepermission');
Route::get('/roles/all',"Admin\RoleController@all");
Route::post('/assignPermission','Admin\RoleController@attachPermission');
Route::post('/detachPermission','Admin\RoleController@detachPermission');
Route::resource('/roles',"Admin\RoleController");


/**
 * PERMISSIONs
 */
Route::get('/permissions/all',"Admin\PermissionController@all");
Route::resource('/permissions',"Admin\PermissionController");

//clinics

Route::put('/clinics/verify',"Admin\ClinicController@switchVerification");
Route::put('/clinics/status',"Admin\ClinicController@switchStatus");
Route::get('/clinics/fetchData/{id}',"Admin\ClinicController@fetchData");
Route::post('/clinics/removeBulk',"Admin\ClinicController@destroyBulk");
Route::put('/clinics/statusBulk',"Admin\ClinicController@switchStatusBulk");
Route::get('/clinics/all',"Admin\ClinicController@all");
Route::resource('/clinics',"Admin\ClinicController");


Route::put('/doctors/verify',"Admin\DoctorController@switchVerification");
Route::put('/doctors/status',"Admin\DoctorController@switchStatus");
Route::get('/doctors/fetchData/{id}',"Admin\DoctorController@fetchData");
Route::post('/doctors/removeBulk',"Admin\DoctorController@destroyBulk");
Route::put('/doctors/statusBulk',"Admin\DoctorController@switchStatusBulk");
Route::get('/doctors/clinic/{id}',"Admin\DoctorController@byClinic");
Route::get('/doctors/all',"Admin\DoctorController@all");
Route::resource('/doctors',"Admin\DoctorController");


Route::put('/labs/verify',"Admin\LabController@switchVerification");
Route::put('/labs/status',"Admin\LabController@switchStatus");
Route::get('/labs/fetchData/{id}',"Admin\LabController@fetchData");
Route::post('/labs/removeBulk',"Admin\LabController@destroyBulk");
Route::put('/labs/statusBulk',"Admin\LabController@switchStatusBulk");
Route::get('/labs/all',"Admin\LabController@all");
Route::resource('/labs',"Admin\LabController");

Route::put('/cases/verify',"Admin\CaseController@switchVerification");
Route::put('/cases/status',"Admin\CaseController@switchStatus");
Route::get('/cases/fetchData/{id}',"Admin\CaseController@fetchData");
Route::get('/cases/{id}/detail',"Admin\CaseController@fetchData");
Route::post('/cases/removeBulk',"Admin\CaseController@destroyBulk");
Route::put('/cases/statusBulk',"Admin\CaseController@switchStatusBulk");
Route::get('/cases/clinic/{id}',"Admin\CaseController@byClinic");
Route::get('/cases/log/{id}',"Admin\CaseController@logesDetail");
Route::get('/cases/search/{id}',"Admin\CaseController@byClinicajax");
Route::resource('/cases',"Admin\CaseController");

Route::put('/pages/status',"Admin\PagesController@switchStatus");
Route::post('/pages/removeBulk',"Admin\PagesController@destroyBulk");
Route::put('/pages/statusBulk',"Admin\PagesController@switchStatusBulk");
Route::resource('/pages',"Admin\PagesController");
