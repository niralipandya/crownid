<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/testing', function () {
	App::setLocale('es');
    return  trans('api.clinic_authenticated');
});

// Clinic and Doctor user related common apis
Route::post('/login'			, 'Api\UsersController@login');
Route::post('/check-email'		, 'Api\UsersController@checkEmail');
Route::post('/purchase'			, 'Api\UsersController@purchase');
Route::post('/forgot/password'	, 'Api\UsersController@forgotPassword');
Route::post('/reset/password'	, 'Api\UsersController@resetPassword');

// With Auth API
Route::get('/logout'			,"Api\UsersController@logout");
Route::post('/change/password'			, 'Api\UsersController@changePassword');
Route::post('/match/password'			, 'Api\UsersController@matchPassword');
Route::post('/chagne/language-and-date'	, 'Api\UsersController@languageAndDate');
Route::post('/chagne/saveMetal'	, 'Api\UsersController@saveMetal');
Route::post('/chagne/saveProsthetic'	, 'Api\UsersController@saveProsthetic');
Route::post('/chagne/saveDelivery'	, 'Api\UsersController@saveDelivery');
Route::get('/setting/getMetalOption'	, 'Api\UsersController@getMetalOption');
Route::get('/setting/getDeliveryOption'	, 'Api\UsersController@getDeliveryOption');
Route::get('/setting/getProstheticOption'	, 'Api\UsersController@getProstheticOption');
// get user details
Route::get('/users/{id}'	, 'Api\UsersController@show');

// For store clinic details
Route::post('clinics/{id}'	, 'Api\ClinicsController@update');
Route::resource('/clinics'	, 'Api\ClinicsController', [ 'only' => ['store'] ]);
Route::get('clinics/labs'	, 'Api\ClinicsController@labs');


Route::post('cases/sendToLab', 'Api\CasesController@sendToLab');
Route::post('cases/pageDetail', 'Api\CasesController@pageDetail');
Route::post('cases/delayBoxCase', 'Api\CasesController@delayBoxCase');
Route::post('cases/search', 'Api\CasesController@search');
Route::post('cases/edit', 'Api\CasesController@update');
Route::post('cases/list', 'Api\CasesController@index');
Route::post('cases/doctorCaseList', 'Api\CasesController@doctorCaseList');
Route::post('cases/{id}', 'Api\CasesController@show');
Route::post('checkBarCode', 'Api\CasesController@checkBarCode');
Route::get('cases/scan/{id}', 'Api\CasesController@scan');
Route::resource('/cases'	, 'Api\CasesController');

Route::get('/pages/{id}'	, 'Api\PagesController@show');
Route::resource('/pages'	, 'Api\PagesController');

Route::post('labs/generateLabLink', 'Api\LabsController@generateLabLink');
Route::post('labs/search', 'Api\LabsController@search');
Route::post('doctor/search', 'Api\LabsController@doctorSearch');
Route::get('clinics/doctors', 'Api\LabsController@doctors');
Route::post('labs/list', 'Api\LabsController@index');
Route::resource('/labs'	, 'Api\LabsController');


Route::get('/planview'	, 'Api\PlanController@planview');
Route::get('/creditview'	, 'Api\PlanController@creditview');
Route::get('/tokensummary'	, 'Api\PlanController@tokensummary');
Route::resource('/plan'	, 'Api\PlanController');

Route::post('/doctor/list'		, 'Api\DoctorController@index');
Route::post('/doctor/statusUpdate'		, 'Api\DoctorController@statusUpdate');
Route::post('/check-doctor-exist'		, 'Api\DoctorController@checkEmail');
Route::resource('/doctor'	, 'Api\DoctorController');


Route::post('/purchase/token'		, 'Api\PurchaseController@token');
Route::post('/purchase/doctor'		, 'Api\PurchaseController@doctor');
Route::post('/purchase/subscription', 'Api\PurchaseController@subscription');


Route::group(['prefix' => 'v1'], function() {
		// Clinic and Doctor user related common apis
		Route::post('/login'			, 'Api\V1\UsersController@login');
		Route::post('/check-email'		, 'Api\V1\UsersController@checkEmail');
		Route::post('/purchase'			, 'Api\V1\UsersController@purchase');
		Route::post('/forgot/password'	, 'Api\V1\UsersController@forgotPassword');
		Route::post('/reset/password'	, 'Api\V1\UsersController@resetPassword');

		// With Auth API
		Route::get('/logout'			,"Api\V1\UsersController@logout");
		Route::post('/change/password'			, 'Api\V1\UsersController@changePassword');
		Route::post('/match/password'			, 'Api\V1\UsersController@matchPassword');
		Route::post('/chagne/language-and-date'	, 'Api\V1\UsersController@languageAndDate');
		Route::post('/chagne/saveMetal'	, 'Api\V1\UsersController@saveMetal');
		Route::post('/chagne/saveProsthetic'	, 'Api\V1\UsersController@saveProsthetic');
		Route::post('/chagne/saveDelivery'	, 'Api\V1\UsersController@saveDelivery');
		Route::get('/setting/getMetalOption'	, 'Api\V1\UsersController@getMetalOption');
		Route::get('/setting/getDeliveryOption'	, 'Api\V1\UsersController@getDeliveryOption');
		Route::get('/setting/getProstheticOption'	, 'Api\V1\UsersController@getProstheticOption');
		// get user details
		Route::get('/users/{id}'	, 'Api\V1\UsersController@show');

		// For store clinic details
		Route::post('clinics/{id}'	, 'Api\V1\ClinicsController@update');
		Route::resource('/clinics'	, 'Api\V1\ClinicsController', [ 'only' => ['store'] ]);
		Route::post('currentdoctor'	, 'Api\V1\ClinicsController@currentdoctor');
		Route::get('clinics/labs'	, 'Api\V1\ClinicsController@labs');


		Route::post('cases/sendToLab', 'Api\V1\CasesController@sendToLab');
		Route::post('cases/pageDetail', 'Api\V1\CasesController@pageDetail');
		Route::post('cases/delayBoxCase', 'Api\V1\CasesController@delayBoxCase');
		Route::post('cases/search', 'Api\V1\CasesController@search');
		Route::post('cases/edit', 'Api\V1\CasesController@update');
		Route::post('cases/tryingstatus', 'Api\V1\CasesController@tryingstatus');
		Route::post('cases/list', 'Api\V1\CasesController@index');
		Route::post('cases/doctorCaseList', 'Api\V1\CasesController@doctorCaseList');
		Route::post('cases/{id}', 'Api\V1\CasesController@show');
		Route::post('checkBarCode', 'Api\V1\CasesController@checkBarCode');
		Route::get('cases/scan/{id}', 'Api\V1\CasesController@scan');
		Route::get('cases/detail/{id}', 'Api\V1\CasesController@detail');
		Route::resource('/cases'	, 'Api\V1\CasesController');

		Route::post('/verifyReceipt','Api\V1\PagesController@verifyReceipt');
		Route::get('/pages/{id}'	, 'Api\V1\PagesController@show');
		Route::resource('/pages'	, 'Api\V1\PagesController');

		Route::post('labs/generateLabLink', 'Api\V1\LabsController@generateLabLink');
		Route::post('labs/search', 'Api\V1\LabsController@search');
		Route::post('doctor/search', 'Api\V1\LabsController@doctorSearch');
		Route::get('clinics/doctors', 'Api\V1\LabsController@doctors');
		Route::post('labs/list', 'Api\V1\LabsController@index');
		Route::resource('/labs'	, 'Api\V1\LabsController');

		Route::any('/chatNotification'	, 'Api\V1\PlanController@chatNotification');
		Route::get('/planview'	, 'Api\V1\PlanController@planview');
		Route::get('/creditview'	, 'Api\V1\PlanController@creditview');
		Route::get('/tokensummary'	, 'Api\V1\PlanController@tokensummary');
		Route::resource('/plan'	, 'Api\V1\PlanController');

		Route::post('/doctor/list'		, 'Api\V1\DoctorController@index');
		Route::get('/doctor/clinics', 'Api\V1\DoctorController@cliniclist');
		Route::post('/doctor/statusUpdate'		, 'Api\V1\DoctorController@statusUpdate');
		Route::post('/check-doctor-exist'		, 'Api\V1\DoctorController@checkEmail');
		Route::resource('/doctor'	, 'Api\V1\DoctorController');


		Route::post('/purchase/token'		, 'Api\V1\PurchaseController@token');
		Route::post('/purchase/doctor'		, 'Api\V1\PurchaseController@doctor');
		Route::post('/purchase/subscription', 'Api\V1\PurchaseController@subscription');

		Route::get('/getBadgeCount', 'Api\V1\NotificationController@getBadgeCount');
		Route::post('/notificationlist', 'Api\V1\NotificationController@index');	
		Route::resource('/notifications', 'Api\V1\NotificationController');

});