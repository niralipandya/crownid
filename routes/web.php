<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin'], function () {
  Route::get('/', 'AdminAuth\LoginController@showLoginForm')->name('login');
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout');

  Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'AdminAuth\RegisterController@register');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'clinic'], function () {
  Route::get('/', 'Clinic\HomeController@showHomePage');
  Route::get('/login', 'Clinic\HomeController@showHomePage');
  Route::get('/logout', 'Clinic\HomeController@showHomePage')->name('logout');
  Route::post('/reset/password' , 'Clinic\LoginController@resetPassword');
  Route::post('/check_username', 'Clinic\HomeController@check_username');
  Route::post('/check_email', 'Clinic\HomeController@check_email');
  Route::post('/create_clinic', 'Clinic\HomeController@create_clinic');
  Route::get('/success','Clinic\HomeController@success');
  Route::get('/plans/doctor/pro', "Clinic\HomeController@doctor_pro")->name('plans');
  Route::get('/upgreadplans/{doctor}/{plan}/{type}', "Clinic\HomeController@upgreadplans")->name('upgreadplans');
  Route::get('/upgreaddoctorplan/{doctor}/{plan}/{type}', "Clinic\HomeController@upgreaddoctorplan")->name('upgreaddoctorplan');
  Route::get('/plans/{doctor}/{plan}/{type}', "Clinic\HomeController@doctor_pro")->name('plans');
  Route::get('/getPriceDetail', "Clinic\HomeController@getPriceDetail")->name('getPriceDetail');
  
  Route::get('/passwordresetabc', 'Clinic\HomeController@passwordresetabc');

  Route::post('/loginabc', 'ClinicAuth\LoginController@login');
  Route::post('/logout', 'ClinicAuth\LoginController@logout');
  
  Route::get('/register', 'ClinicAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'ClinicAuth\RegisterController@register');

  Route::post('/password/email', 'ClinicAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'ClinicAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'ClinicAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'ClinicAuth\ResetPasswordController@showResetForm');
  
});

Route::group(['prefix' => 'lab'], function () {

  Route::get('/', 'Clinic\HomeController@showHomePage');
  Route::get('/login', 'Clinic\HomeController@showHomePage');
  Route::get('/login', 'LabAuth\LoginController@showLoginForm');
  Route::post('/login', 'LabAuth\LoginController@login')->middleware('CheckRole:Lab');
  Route::post('/logout', 'LabAuth\LoginController@logout');

  Route::get('/register', 'LabAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'LabAuth\RegisterController@register');

  Route::post('/password/email', 'LabAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'LabAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'LabAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'LabAuth\ResetPasswordController@showResetForm');
  
});
