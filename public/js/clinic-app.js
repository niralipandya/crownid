/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 428);
/******/ })
/************************************************************************/
/******/ ({

/***/ 428:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(429);


/***/ }),

/***/ 429:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*!
 * Start Bootstrap - Clean Blog v4.0.0-beta (https://startbootstrap.com/template-overviews/clean-blog)
 * Copyright 2013-2017 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap-clean-blog/blob/master/LICENSE)
 */
!function (i) {
  "use strict";
  i("body").on("input propertychange", ".floating-label-form-group", function (o) {
    i(this).toggleClass("floating-label-form-group-with-value", !!i(o.target).val());
  }).on("focus", ".floating-label-form-group", function () {
    i(this).addClass("floating-label-form-group-with-focus");
  }).on("blur", ".floating-label-form-group", function () {
    i(this).removeClass("floating-label-form-group-with-focus");
  });if (i(window).width() > 1170) {
    var o = i("#mainNav").height();i(window).on("scroll", { previousTop: 0 }, function () {
      var s = i(window).scrollTop();s < this.previousTop ? s > 0 && i("#mainNav").hasClass("is-fixed") ? i("#mainNav").addClass("is-visible") : i("#mainNav").removeClass("is-visible is-fixed") : s > this.previousTop && (i("#mainNav").removeClass("is-visible"), s > o && !i("#mainNav").hasClass("is-fixed") && i("#mainNav").addClass("is-fixed")), this.previousTop = s;
    });
  }
}(jQuery);

/***/ })

/******/ });