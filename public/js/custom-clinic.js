/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 430);
/******/ })
/************************************************************************/
/******/ ({

/***/ 430:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(431);


/***/ }),

/***/ 431:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


//$('.help-block').hide();
$("#labs a").click(function (e) {
    e.preventDefault();
    $("a.active").removeClass("active");
    $(this).addClass("active");
    $("#companionbox").css({ display: "none" });
    $("#practicebox").css({ display: "none" });
    $("#labsbox").css({ display: "" });
});

$("#practice a").click(function (e) {
    e.preventDefault();
    $("a.active").removeClass("active");
    $(this).addClass("active");
    $("#labsbox").css({ display: "none" });
    $("#companionbox").css({ display: "none" });
    $("#practicebox").css({ display: "" });
});

$("#companion a").click(function (e) {
    e.preventDefault();
    $("a.active").removeClass("active");
    $(this).addClass("active");
    $("#practicebox").css({ display: "none" });
    $("#labsbox").css({ display: "none" });
    $("#companionbox").css({ display: "" });
});

$("#monthlyPlan").click(function (e) {
    e.preventDefault();
    $("#annualPlan").removeClass("btn btn-selected");
    $("#monthlyPlan").removeClass("btn btn-deselected");
    $("#monthlyPlan").addClass("btn btn-selected");
    $("#annualPlan").addClass("btn btn-deselected");
});

$("#annualPlan").click(function (e) {
    e.preventDefault();
    $("#monthlyPlan").removeClass("btn btn-selected");
    $("#annualPlan").removeClass("btn btn-deselected");
    $("#annualPlan").addClass("btn btn-selected");
    $("#monthlyPlan").addClass("btn btn-deselected");
});

$("#monthlypro").click(function (e) {
    e.preventDefault();
    $("#annualpro").removeClass("active-plan");
    $("#monthlypro").addClass("active-plan");
    $("#monthlyproamount").removeClass("hide");
    $("#annualproamount").removeClass("show");
    $("#annualproamount").addClass("hide");
    $("#monthlyproamount").addClass("show");
});

$("#annualpro").click(function (e) {
    e.preventDefault();
    $("#monthlypro").removeClass("active-plan");
    $("#annualpro").addClass("active-plan");
    $("#annualproamount").removeClass("hide");
    $("#monthlyproamount").removeClass("show");
    $("#monthlyproamount").addClass("hide");
    $("#annualproamount").addClass("show");
});

$("#monthlyPlanPremium").click(function (e) {
    e.preventDefault();
    $("#annualPlanPremium").removeClass("active-plan");
    $("#monthlyPlanPremium").addClass("active-plan");

    $("#premimummonthly").show();
    $("#premimumyearly").hide();

    $("#annualPlanPremiumAmount").removeClass("show");
    $("#annualPlanPremiumAmount").addClass("invisible");
});

$("#annualPlanPremium").click(function (e) {
    e.preventDefault();
    $("#monthlyPlanPremium").removeClass("active-plan");
    $("#annualPlanPremium").addClass("active-plan");
    $("#premimumdetail").removeClass("invisible");
    $("#annualPlanPremiumAmount").removeClass("invisible");
    $("#annualPlanPremiumAmount").removeClass("hide");

    $("#premimumdetail").show();

    $("#premimumyearly").show();
    $("#premimummonthly").hide();

    $("#annualPlanPremiumAmount").addClass("show");
});

$("#doctormonthlyPlanPremium").click(function (e) {
    e.preventDefault();
    $("#doctorannualPlanPremium").removeClass("active-plan");
    $("#doctormonthlyPlanPremium").addClass("active-plan");
    $("#doctorpremimumdetail").addClass("invisible");
    $("#doctormonthlyPlanPremiumAmount").removeClass("hide");
    $("#doctorannualPlanPremiumAmount").removeClass("show");
    $("#doctorannualPlanPremiumAmount").addClass("hide");
    $("#doctormonthlyPlanPremiumAmount").addClass("show");
});

$("#doctorannualPlanPremium").click(function (e) {
    e.preventDefault();
    $("#doctormonthlyPlanPremium").removeClass("active-plan");
    $("#doctorannualPlanPremium").addClass("active-plan");
    $("#doctorpremimumdetail").removeClass("invisible");
    $("#doctorannualPlanPremiumAmount").removeClass("hide");
    $("#doctormonthlyPlanPremiumAmount").removeClass("show");
    $("#doctormonthlyPlanPremiumAmount").addClass("hide");
    $("#doctorpremimumdetail").show();
    $("#doctorannualPlanPremiumAmount").addClass("show");
});

/***/ })

/******/ });