      //$('.help-block').hide();
      $("#labs a").click(function(e) {
          e.preventDefault();
          $("a.active").removeClass("active");
          $(this).addClass("active");
          $("#companionbox").css({ display: "none" });
          $("#practicebox").css({ display: "none" });
          $("#labsbox").css({ display: "" });
      });

      $("#practice a").click(function(e) {
          e.preventDefault();
          $("a.active").removeClass("active");
          $(this).addClass("active");
          $("#labsbox").css({ display: "none" });
          $("#companionbox").css({ display: "none" });
          $("#practicebox").css({ display: "" });
      });

      $("#companion a").click(function(e) {
          e.preventDefault();
          $("a.active").removeClass("active");
          $(this).addClass("active");
          $("#practicebox").css({ display: "none" });
          $("#labsbox").css({ display: "none" });
          $("#companionbox").css({ display: "" });
      });

      $("#monthlyPlan").click(function(e) {
          e.preventDefault();
          $("#annualPlan").removeClass("btn btn-selected");
          $("#monthlyPlan").removeClass("btn btn-deselected");
          $("#monthlyPlan").addClass("btn btn-selected");
          $("#annualPlan").addClass("btn btn-deselected");
      });

      $("#annualPlan").click(function(e) {
          e.preventDefault();
          $("#monthlyPlan").removeClass("btn btn-selected");
          $("#annualPlan").removeClass("btn btn-deselected");
          $("#annualPlan").addClass("btn btn-selected");
          $("#monthlyPlan").addClass("btn btn-deselected");
      });

      $("#monthlypro").click(function (e) {
          e.preventDefault();
          $("#annualpro").removeClass("active-plan");
          $("#monthlypro").addClass("active-plan");
          $("#monthlyproamount").removeClass("hide");
          $("#annualproamount").removeClass("show");
          $("#annualproamount").addClass("hide");
          $("#monthlyproamount").addClass("show");
      });

      $("#annualpro").click(function (e) {
          e.preventDefault();
          $("#monthlypro").removeClass("active-plan");
          $("#annualpro").addClass("active-plan");
          $("#annualproamount").removeClass("hide");
          $("#monthlyproamount").removeClass("show");
          $("#monthlyproamount").addClass("hide");
          $("#annualproamount").addClass("show");
      });    

      $("#monthlyPlanPremium").click(function (e) { 
          e.preventDefault();
          $("#annualPlanPremium").removeClass("active-plan");
          $("#monthlyPlanPremium").addClass("active-plan");

          $("#premimummonthly").show();
          $("#premimumyearly").hide();

          $("#annualPlanPremiumAmount").removeClass("show");
          $("#annualPlanPremiumAmount").addClass("invisible");
      });

      $("#annualPlanPremium").click(function (e) { 
          e.preventDefault();
          $("#monthlyPlanPremium").removeClass("active-plan");
          $("#annualPlanPremium").addClass("active-plan");
          $("#premimumdetail").removeClass("invisible");
          $("#annualPlanPremiumAmount").removeClass("invisible");
          $("#annualPlanPremiumAmount").removeClass("hide");
          
          $("#premimumdetail").show();

          $("#premimumyearly").show();
          $("#premimummonthly").hide();

          $("#annualPlanPremiumAmount").addClass("show");
      });

      $("#doctormonthlyPlanPremium").click(function (e) { 
          e.preventDefault();
          $("#doctorannualPlanPremium").removeClass("active-plan");
          $("#doctormonthlyPlanPremium").addClass("active-plan");
          $("#doctorpremimumdetail").addClass("invisible");
          $("#doctormonthlyPlanPremiumAmount").removeClass("hide");
          $("#doctorannualPlanPremiumAmount").removeClass("show");
          $("#doctorannualPlanPremiumAmount").addClass("hide");
          $("#doctormonthlyPlanPremiumAmount").addClass("show");
      });

      $("#doctorannualPlanPremium").click(function (e) { 
          e.preventDefault();
          $("#doctormonthlyPlanPremium").removeClass("active-plan");
          $("#doctorannualPlanPremium").addClass("active-plan");
          $("#doctorpremimumdetail").removeClass("invisible");
          $("#doctorannualPlanPremiumAmount").removeClass("hide");
          $("#doctormonthlyPlanPremiumAmount").removeClass("show");
          $("#doctormonthlyPlanPremiumAmount").addClass("hide");
          $("#doctorpremimumdetail").show();
          $("#doctorannualPlanPremiumAmount").addClass("show");
      });