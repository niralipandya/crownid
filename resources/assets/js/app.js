
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vuelidate from 'vuelidate';

window.Vue.use(Vuelidate);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('showlink', require('./components/Link.vue'));
Vue.component('alert', require('./components/Alert.vue'));
Vue.component('AuthValidate', require('./components/AuthValidation.vue'));
Vue.component('ImgFileinput', require('./components/ImgFileinput.vue'));
Vue.component('caselog', require('./components/Caselog.vue'));

Vue.component('roles', require('./components/admin/Roles.vue'));
Vue.component('permissions', require('./components/admin/Permissions.vue'));
Vue.component('assign-permission', require('./components/admin/AssignPermission.vue'));
Vue.component('administrator', require('./components/admin/Administrator.vue'));
Vue.component('Clinic', require('./components/admin/Clinic.vue'));
Vue.component('cliniccase', require('./components/admin/Cliniccase.vue'));
Vue.component('AddClinic', require('./components/admin/AddClinic.vue'));
Vue.component('Clinic', require('./components/admin/Clinic.vue'));
Vue.component('Clinicdoctor', require('./components/admin/Clinicdoctor.vue'));
Vue.component('Doctor', require('./components/admin/Doctor.vue'));
Vue.component('Lab', require('./components/admin/Lab.vue'));
Vue.component('Case', require('./components/admin/Cases.vue'));
Vue.component('CsmPages', require('./components/admin/CmsPages.vue'));
Vue.component('Contact', require('./components/clinic/Contact.vue'));
Vue.component('AuthLogin', require('./components/clinic/AuthLogin.vue'));
Vue.component('forgetpassword', require('./components/clinic/AuthForgetpassword.vue'));
Vue.component('changepasswords', require('./components/clinic/Authchangepassword.vue'));
Vue.component('AuthRegister', require('./components/clinic/AuthRegister.vue'));
Vue.component('doctors', require('./components/clinic/doctors.vue'));
Vue.component('doctoradd', require('./components/clinic/doctorAdd.vue'));
Vue.component('doctoredit', require('./components/clinic/doctorEdit.vue'));
Vue.component('purchase', require('./components/clinic/purchase.vue'));
Vue.component('chat', require('./components/clinic/Chat.vue'));
Vue.component('caselibrary', require('./components/clinic/Caselibrary.vue'));
Vue.component('clinicuploder', require('./components/clinic/ImgFileinput.vue'));
Vue.component('cliniclabs', require('./components/clinic/Labs.vue'));
Vue.component('labcaselibrary', require('./components/clinic/Labcaselibrary.vue'));
Vue.component('setting', require('./components/clinic/Setting.vue'));
Vue.component('settingdefault', require('./components/clinic/Settingdefault.vue'));
Vue.component('delaylibrary', require('./components/clinic/Delaylibrary.vue'));
Vue.component('doctorcaselibrary', require('./components/clinic/Doctorcaselibrary.vue'));
Vue.component('chatstatus', require('./components/clinic/Chatstatus.vue'));
Vue.component('clinic-profile', require('./components/clinic/Clinicprofile.vue'));
Vue.component('clinic-password', require('./components/clinic/Clinicpassword.vue'));
Vue.component('notificationclinic', require('./components/clinic/Notificationclinic.vue'));
Vue.component('plan', require('./components/clinic/Plan.vue'));

Vue.component('caselibrarylabs', require('./components/lab/Caselibrary.vue'));
Vue.component('delaylibrarylabs', require('./components/lab/Delaylibrary.vue'));
Vue.component('labclinics', require('./components/lab/Clinics.vue'));
Vue.component('cliniccaselibrary', require('./components/lab/Cliniccaselibrary.vue'));
Vue.component('labchat', require('./components/lab/Chat.vue'));
Vue.component('labclinic', require('./components/lab/Labclinic.vue'));
Vue.component('receivecasepopup', require('./components/lab/Receivecasepopup.vue'));
Vue.component('addDelay', require('./components/lab/AddDelay.vue'));
Vue.component('labsetting', require('./components/lab/Setting.vue'));
Vue.component('lab-profile', require('./components/lab/Labprofile.vue'));
Vue.component('lab-password', require('./components/lab/Labpassword.vue'));
Vue.component('notificationlab', require('./components/lab/Notificationlab.vue'));


Vue.prototype.$http = axios;



window.Event= new class{
    constructor(){
        this.vue= new Vue();
    }

    fire(event, data=null){
        this.vue.$emit(event,data);
    }

    listen(event, callback){
        this.vue.$on(event,callback);
    }
}

const app = new Vue({
    el: '#app',

    data: {
        chat_data_parent: {
            chat_caselist: [],
            flag_load_chat: 1
        }
    },

    mounted() {
		this.LoadChatCaselist();
	},

    methods: {

		LoadChatCaselist() {
            const vm = this;

			firebase.database().ref("RECENTCHAT/" + window.Laravel.login_user_id)
                .on("value", function(snapshot) {

                    let tmp_chat_caselist = [];
                    let chat_badgeCount = 0;

                    if (snapshot.val()) {
                        let datalist = snapshot.val();

                        _.each(datalist, function(element, key) {
                            tmp_chat_caselist.push({
                                'badgeCount': element.badgeCount,
                                'groupID': element.groupID,
                                'imageURL': element.imageURL,
                                'senderName': element.senderName,
                                'messageType': element.messageType,
                                'imageThumbURL': element.imageThumbURL,
                                'messageText': element.messageText,
                                'timeStamp': element.timeStamp,
                                'isTyping': element.isTyping,
                                'typingText': element.typingText
                            });

                            chat_badgeCount += element.badgeCount;
                        });
                    }
                    if (chat_badgeCount > 0) {
                        $("#chat_badgeCount").show();
                    } else {
                        $("#chat_badgeCount").hide();
                    }
                    $("#chat_badgeCount").html(chat_badgeCount);

                    vm.chat_data_parent.chat_caselist = tmp_chat_caselist.sort(function(a, b) {
                        if (a.timeStamp < b.timeStamp)
                            return 1;
                        if (a.timeStamp > b.timeStamp)
                            return -1;
                        return 0;
                    });

                    vm.chat_data_parent.flag_load_chat = 0;
                });
        }
    }
});
