@extends('admin.layout.master')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('/plugins/datepicker/datepicker3.css') }}">
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Clinic Add
            <small>Manage Clinics</small>
        </h1>
       <!--  <ol class="breadcrumb">
           <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
           <li><a href="{{ url('/admin/clinics') }}">Clinics</a></li>
           <li class="active">Add Clinic</li>
       </ol> -->
    </section>
    <section class="content" id="app">
    <component is='add-clinic' userids = 0 imgsrc = {{ $defaultImg }} inline-template> 
        <div class="row">
            <div class="col-12 mx-auto">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Clinic</h3>
                    </div>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/clinics') }}" enctype="multipart/form-data"  @submit.prevent='onSubmit(0)' @keydown = 'errors.clear($event.target.name)'>
                        <div class="box-body">
                        {{ csrf_field() }}
                        <div v-if='showAlert'>
                             <alert :type="alertType">@{{ alertText }}</alert>
                        </div>

                        <div class="form-group" v-bind:class="errors.has('clinic_name') ? ' has-error' : ''">
                                <label for="clinic_name" class="col-md-3 control-label">Clinic Name<span class="red">*</span></label>

                                <div class="col-md-7">
                                    <input id="clinic_name" type="text" class="form-control" name="clinic_name" value="{{ old('clinic_name') }}" v-model="clinic_name" placeholder="clinic name">

                                    
                                        <span class="help-block" v-text="errors.get('clinic_name')"></span>
                                    
                                </div>
                        </div>

                        <div class="form-group" v-bind:class="errors.has('first_name') ? ' has-error' : ''">
                                <label for="first_name" class="col-md-3 control-label">First Name<span class="red">*</span></label>

                                <div class="col-md-7">
                                    <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" v-model="first_name" placeholder="firstname">

                                    
                                        <span class="help-block" v-text="errors.get('first_name')"></span>
                                    
                                </div>
                        </div>
                            {{-- Last Name --}}
                        <div class="form-group" v-bind:class="errors.has('last_name') ? ' has-error' : ''">
                                <label for="last_name" class="col-md-3 control-label">Last Name<span class="red">*</span></label>

                                <div class="col-md-7">
                                    <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" v-model="last_name" placeholder="lastname">

                                    
                                      <span class="help-block" v-text="errors.get('last_name')"></span>
                                    
                                </div>
                        </div>
                        {{-- Email --}}
                        <div class="form-group" v-bind:class="errors.has('email') ? ' has-error' : ''">
                            <label for="email" class="col-md-3 control-label">Email<span class="red">*</span></label>

                            <div class="col-md-7">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" v-model="email" placeholder="email">

                                    <span class="help-block" v-text="errors.get('email')"></span>
                            </div>
                        </div>

                        <div class="form-group" v-bind:class="errors.has('username') ? ' has-error' : ''">
                            <label for="username" class="col-md-3 control-label">Username<span class="red">*</span></label>

                            <div class="col-md-7">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" v-model="username" placeholder="username">

                                    <span class="help-block" v-text="errors.get('username')"></span>
                            </div>
                        </div>


                        <div class="form-group" v-bind:class="errors.has('password') ? ' has-error' : ''">
                            <label for="password" class="col-md-3 control-label">Password<span class="red">*</span></label>

                            <div class="col-md-7">
                                <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}" v-model="password" placeholder="password">

                                    <span class="help-block" v-text="errors.get('password')"></span>
                            </div>
                        </div>


                        <div class="form-group" v-bind:class="errors.has('contact_number') ? ' has-error' : ''">
                            <label for="contact_number" class="col-md-3 control-label">Phone<span class="red">*</span></label>

                            <div class="col-md-7">

                                <input id="contact_number" name="contact_number" type="tel" class="form-control" v-model="contact_number"> 

                                    <span class="help-block" v-text="errors.get('contact_number')"></span>
                               
                            </div>
                        </div>

                        <div class="form-group" v-bind:class="errors.has('address') ? ' has-error' : ''">
                            <label for="address" class="col-md-3 control-label">Address<span class="red">*</span></label>

                            <div class="col-md-7">

                                <input id="address" name="address" type="text" class="form-control" v-model="address" placeholder="address"> 

                                    <span class="help-block" v-text="errors.get('address')"></span>
                               
                            </div>
                        </div>

                        <div class="form-group" v-bind:class="errors.has('city') ? ' has-error' : ''">
                            <label for="city" class="col-md-3 control-label">City<span class="red">*</span></label>

                            <div class="col-md-7">

                                <input id="city" name="city" type="text" class="form-control" v-model="city" placeholder="city"> 

                                    <span class="help-block" v-text="errors.get('city')"></span>
                               
                            </div>
                        </div>


                        <div class="form-group" v-bind:class="errors.has('country') ? ' has-error' : ''">
                            <label for="country" class="col-md-3 control-label">Country<span class="red">*</span></label>

                            <div class="col-md-7">

                                <input id="country" name="country" type="text" class="form-control" v-model="country" placeholder="country"> 

                                <span class="help-block" v-text="errors.get('country')"></span>
                               
                            </div>
                        </div>
                        {{-- PIC --}}
                        <div class="form-group" v-bind:class="errors.has('pic') ? ' has-error' : ''">
                            <label for="pic" class="col-md-3 control-label">Picture</label>

                            <div class="col-md-7">
                                    <div class="img-input">
                                            <img :src="pic" class="img-responsive img-rounded">
                                            <input @change="onFileChange" type="file" name="pic" class="form-control" readonly>
                                    </div>
                                    
                                    <span class="help-block" v-text="errors.get('pic')"></span>
                              
                            </div>
                        </div>
                     
                        <div class="clear-fix"></div>
                        <div class="form-group">
                            <div class="col-md-7 col-md-offset-3">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                                <a type="button" href="{{ url('/admin/clinics') }}" class="btn btn-danger">
                                    Cancel
                                </a>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </component>
    </section>
@endsection
