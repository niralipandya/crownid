@extends('admin.layout.master')
@section('content')
	<section class="content-header">
		<h1>
			Clinics
			<small>Manage Clinics</small>
		</h1>
		<!-- <ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Clinics</li>
		</ol> -->
	</section>

	<section class="content" id="app">
		@include('flash')
        <clinic headline='Clinic'></clinic>
	</section>
@endsection
