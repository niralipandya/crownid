@extends('admin.layout.master')
@section('content')
	<section class="content-header">
		<h1>
			Permissions
			<small>Manage Permissions</small>
		</h1>
		
	</section>

	<section class="content" id="app">
	    <permissions headline='Permission'></permissions>
	</section>
@endsection
