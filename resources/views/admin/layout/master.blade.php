<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'CrownId') }}</title>


    <!-- Styles -->
    <link href="/css/admin-app.css" rel="stylesheet">
    
    <link rel="stylesheet" href="{{ asset('/plugins/iCheck/square/blue.css') }}">
    <link rel="stylesheet" href="{{ asset('/plugins/intl-tel-input/build/css/intlTelInput.css') }}">
    <style type="text/css">
        .skin .sidebar-menu > li.active > a {
            background: #ECF0F5;
        }
    </style>
    @yield('css')

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>

    </script>
</head>
<body class="skin sidebar-mini">
    <div class="wrapper">
        @include('admin.includes.headerATL')
        @include('admin.includes.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>
    </div>
    
    <!-- Scripts -->
    
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="/js/app.js"></script>
    <script src="/js/admin-app.js"></script>
    <script src="/js/admin-main.js"></script>
   
    @yield('js')
</body>
</html>
