@extends('admin.layout.master')
@section('content')
	<section class="content-header">
		<h1>
			{{$clinic_name}} Clinic Doctors
			<small>List Doctors</small>
		</h1>
		
	</section>

	<section class="content" id="app">
		@include('flash') 
        <clinicdoctor headline="Doctor" name='doctors' usrid='{{ $clinic_id }}'></clinicdoctor>
	</section>
@endsection
