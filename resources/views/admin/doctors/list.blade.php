@extends('admin.layout.master')
@section('content')
	<section class="content-header">
		<h1>
			Doctors
			<small>Manage Doctors</small>
		</h1>
		
	</section>

	<section class="content" id="app">
		@include('flash')
        <doctor headline='Doctor'></doctor>
	</section>
@endsection
