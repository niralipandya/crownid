@extends('admin.layout.master')

@section('content')


<div id="app">
   <section class="content">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class="fa fa-user"></i></span>

                <div class="info-box-content"><a href="{{ url('/admin/clinics') }}">
                  <span class="info-box-text">Clinics</span>
                   <span class="info-box-number">{{$cliniccount}}</span></a>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
        </div>
    </section>
</div>
@endsection
