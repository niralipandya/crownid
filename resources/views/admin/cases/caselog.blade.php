@extends('admin.layout.master')
@section('css')
    <style id="jsbin-css">
    @media (min-width: 768px) {
      .modal-xl {
        width: 90%;
       max-width:900px;
      }
    }

</style>
@endsection
@section('content')
	<section class="content-header">
		<h1>
			Case Logs
			<small>View Logs</small>
		</h1>
		
	</section>
	
	<section class="content" id="app">
		@include('flash')
       	<!-- row -->
      <div class="row">
        <div class="col-md-12">

        	<div class="box" style="min-height: 250px;">
        		<div class="box-header">
	        		 <div class="box-title col-6">
	        		 	<h3 style="padding: 15px;">{{$cases->case_code}}</h3>
	        		 </div> 
	        		 <div class="box-tools col-6" >
	        		 	<a class="btn btn-default pull-right" href='/admin/cases' style="display: block;" >Back</a>
	        		 </div>
        		</div>	
        		<div class="box-body">
        			<ul class="timeline" style="margin: 20px 20px 10px 20px;">		 
			            @forelse ($caseLog as $element) 
							<!-- timeline item -->
				            <li>
				              <i class="{{$element->class}}"></i>

				              <div class="timeline-item">
				                <span class="time"><i class="fa fa-clock-o"></i> {{$element->display_date}}</span>

				                <h3 class="timeline-header">{{$element->type_display}}</h3>

				                <div class="timeline-body">
				                  {{$element->text}}
				                </div>
				              </div>
				            </li>
				            <!-- END timeline item -->	
						@empty
						    <h4>No logs found</h4>
						@endforelse               
					</ul>
        		</div>



		          
		          
        	</div>

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
	</section>
@endsection
