@extends('admin.layout.master')
@section('css')
    <style id="jsbin-css">
    @media (min-width: 768px) {
      .modal-xl {
        width: 90%;
       max-width:900px;
      }
    }

</style>
@endsection
@section('content')
	<section class="content-header">
		<h1>
			{{$clinic_name}} clinic Cases
			<small>View Cases</small>
		</h1>
		
	</section>

	<section class="content" id="app">
		@include('flash')
        <cliniccase headline='Case' usrid='{{ $clinic_id }}'></cliniccase>
	</section>
@endsection
