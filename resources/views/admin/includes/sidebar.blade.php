<style type="text/css">
 .sidebar-mini.sidebar-collapse .main-header .logo > .logo-lg {
    display: block;
  }

</style>

<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
  
 
  <ul class="sidebar-menu">
      
    
    <li>
      <a href="{{ url('/admin') }}">
        <i class="fa fa-th"></i> <span>Dashboard</span>
        <span class="pull-right-container">
        </span>
      </a>
    </li>
   
    @if( auth()->user()->can('developerOnly') || 
    auth()->user()->can('role') || 
    auth()->user()->can('role_permissions') || 
    auth()->user()->can('admin') 
    )
      <li class="header">ADMINISTRATION</li>
    @endif
     @if(auth()->user()->can('developerOnly') || auth()->user()->can('role'))
      <li class="treeview {{ in_array($current_route_name,['admin.roles.index','admin.permissions.index','admin.myrolepermission'])?'active':'' }}">
        <a href="javascript:void(0);">
          <i class="fa fa-shield"></i> <span>Role Manager</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>

        <ul class="treeview-menu">
          <li class="{{ $current_route_name=='admin.roles.index'?'active':'' }}">
            <a href="{{ url('/admin/roles') }}"><i class="fa fa-circle-o"></i> Roles</a>
          </li>
          @can('developerOnly')
          <li class="{{ $current_route_name=='admin.permissions.index'?'active':'' }}">
            <a href="{{ url('/admin/permissions') }}"><i class="fa fa-circle-o"></i> Permissions</a>
          </li>
          @endcan
          
        </ul>
      </li>
      @endif
      {{-- ADMIN USERS --}}
       @if(auth()->user()->can('developerOnly') || auth()->user()->can('role_permissions'))
      <li class=" {{ in_array($current_route_name,['admin.roles.index','admin.permissions.index','admin.myrolepermission'])?'active':'' }}">
        <a href="{{ url('/admin/rolePermissions') }}" >
          <i class="fa fa-shield"></i> <span>Role Permissions</span>
          <span class="pull-right-container">
            
          </span>
        </a>
      </li>
      @endif


      @if(auth()->user()->can('developerOnly') || auth()->user()->can('admin'))
        <li class="{{ $current_route_name=='admin.administrator.index'?'active':'' }}">
          <a href="{{ url('/admin/administrator') }}">
            <i class="fa  fa-user-secret"></i> <span>Admin Users</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
      @endif

      <li class="header">Management</li>

      @if(auth()->user()->can('developerOnly') || auth()->user()->can('clinic'))
        <li class="{{ $current_route_name=='admin.clinics.index'?'active':'' }}">
          <a href="{{ url('/admin/clinics') }}">
            <i class="fa  fa-hospital-o"></i> <span>Clinics</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
      @endif

      @if(auth()->user()->can('developerOnly') || auth()->user()->can('doctor'))
        <li class="{{ $current_route_name=='admin.doctors.index'?'active':'' }}">
          <a href="{{ url('/admin/doctors') }}">
            <i class="fa  fa-user-md"></i> <span>Doctors </span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
      @endif

      @if(auth()->user()->can('developerOnly') || auth()->user()->can('lab'))
        <li class="{{ $current_route_name=='admin.labs.index'?'active':'' }}">
          <a href="{{ url('/admin/labs') }}">
            <i class="fa  fa-medkit"></i> <span>Labs </span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
      @endif

      @if(auth()->user()->can('developerOnly') || auth()->user()->can('case'))
        <li class="{{ $current_route_name=='admin.cases.index'?'active':'' }}">
          <a href="{{ url('/admin/cases') }}">
            <i class="fa  fa-list"></i> <span>Cases </span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
      @endif

      
        
      @if(auth()->user()->can('developerOnly') || auth()->user()->can('pages'))
      <li class="header">OTHER MODULES</li>
        <li class="{{ $current_route_name=='admin.pages.index'?'active':'' }}">
          <a href="{{ url('/admin/pages') }}">
            <i class="fa  fa-file-text"></i> <span>Pages</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
      @endif  


  
  </ul>
  </section>
  <!-- /.sidebar -->
</aside>