@extends('admin.layout.master')
@section('content')
	<section class="content-header">
		<h1>
			Role Permissions
			<small>Manage Role's Permission</small>
		</h1>
		<small>Please select role to view given permission list.</small>
	</section>
	<section class="content" id="app">
	    <assign-permission></assign-permission>
	</section>
@endsection
