@extends('admin.layout.master')
@section('content')
	<section class="content-header">
		<h1>
			Labs
			<small>Manage Labs</small>
		</h1>
		
	</section>

	<section class="content" id="app">
		@include('flash')
        <lab headline='Lab'></lab>
	</section>
@endsection
