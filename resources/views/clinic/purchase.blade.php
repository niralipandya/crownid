@extends('clinic.layout.buy')


@section('content')
   

    <!-- Main Content -->
  <div class="container">
      <div class="row" id="feature">
        <div class="col-lg-8 col-md-10 mx-auto">
          <h4 class="section-heading prizehead">Your Free 1-month Trial is just moments away!</h4>
        </div>
      </div>
      <form class="form" id="planform" name="planform" method="POST" action="{{ url('/clinic/create_clinic') }}" enctype="multipart/form-data"  >
        <div class="row">
            <div class="col-lg-7 col-md-7 mx-auto ">
              <div  class="checkout-form">
                  <div name="alert"></div> 
                  @include('flash')            
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">    
                  <input type="hidden" name="plan" value="{{ $plan }}">
                  <input type="hidden" name="user_type" value="{{ $who }}">
                  <input type="hidden" name="type" value="{{ $typesend }}"> 
                  <input type="hidden" name="pricetype" value="{{ $type }}"> 
                  <input type="hidden" name="receipt" value=""> 
                  <div class="form__section">
                      <h4 class="section__title">Account Information</h4>
                      <div class=" form__field-container">
                        <div class="form__field-wrapper col-xs-12">
                          <label for="email" class="form__label">Email</label>
                          <input required="" type="email"  name="email" value="{{ old('email') }}" class="form-control" aria-required="true">
                        </div>
                      </div>
                      <div class=" form__field-container">
                        <div class="form__field-wrapper col-xs-12">
                          <label for="clinic_name" class="form__label">Clinic Name</label>
                          <input required="" type="text"  name="clinic_name" value="{{ old('clinic_name') }}" minlength="2" maxlength="50" class="form-control" aria-required="true">
                        </div>
                      </div>
                      <div class="row form__field-container inrowtwo ma-l-0">
                        <div class="form__field-wrapper col-xs-5 width50">
                          <label for="first_name" class="form__label">Clinic Admin First Name</label>
                          <input required="" type="text" name="first_name" value="{{ old('first_name') }}"   minlength="1" maxlength="50" class="form-control " aria-required="true">
                        </div>
                        <div class="form__field-wrapper ma-l-15 width50 col-xs-5">
                          <label for="last_name" class="form__label">Clinic Admin Last Name</label>
                          <input required="" type="text" name="last_name" value="{{ old('last_name') }}"  minlength="1" maxlength="50" class="form-control " aria-required="true">
                        </div>
                      </div>
                      <div class=" form__field-container">
                        <div class="form__field-wrapper col-xs-12">
                          <label for="username" class="form__label">User Name (For Login)</label>
                          <input required="" type="text"  name="username" value="{{ old('username') }}"   minlength="5" maxlength="20"  class="form-control" aria-required="true">
                        </div>
                      </div>
                      <div class="row form__field-container inrowtwo ma-l-0">
                        <div class="form__field-wrapper col-xs-5 width50">
                          <label for="password" class="form__label">Password</label>
                          <input required="" type="password" name="password"   id="password" class="form-control " aria-required="true">
                        </div>
                        <div class="form__field-wrapper ma-l-15 width50 col-xs-5">
                          <label for="cpassword" class="form__label">Conform Password</label>
                          <input required="" type="password" name="cpassword"  class="form-control " aria-required="true">
                        </div>
                      </div>
                  </div>
                  <div class="form__section">
                    <h4 class="section__title">Billing Information</h4>
                    <div class=" form__field-container">
                      <div class="form__field-wrapper col-xs-12">
                        <label for="address" class="form__label">Address</label>
                        <input required="" type="text" minlength="5" maxlength="100"  value="{{ old('address') }}" name="address" class="form-control" aria-required="true">
                      </div>
                    </div>
                    
                    <div class="row form__field-container inrowtwo ma-l-0">
                      <div class="form__field-wrapper col-xs-5 width50">
                        <label for="city" class="form__label">City</label>
                        <input required="" type="text" name="city" minlength="2" maxlength="20" value="{{ old('city') }}" class="form-control " aria-required="true">
                      </div>
                      <div class="form__field-wrapper ma-l-15 width50 col-xs-5">
                        <label for="country" class="form__label">Country</label>
                        <input required="" type="text" name="country" minlength="2" maxlength="20" value="{{ old('country') }}"   class="form-control " aria-required="true">
                      </div>
                    </div>
                    <div class=" form__field-container">
                      <div class="form__field-wrapper col-xs-12">
                        <label for="contact_number" class="form__label">Contact Number</label>
                        <input required="" type="text"  name="contact_number" value="{{ old('contact_number') }}"  class="form-control" aria-required="true">
                      </div>
                    </div>

                    <div class=" form__field-container">
                      <div class="form__field-wrapper col-xs-12">
                        <input required="" type="text" disabled="" name="plan" value="{{ old('contact_number') }}"  placeholder="{{$fullPlanname}}" class="form-control" aria-required="true">
                      </div>
                    </div>
                    @if($type != 'free')

                   
                    <div class="row form__field-container">
                      <div class="form__field-wrapper cc-number col-sm-6 col-xs-12">
                        <label for="cc_number" class="form__label">Credit Card</label>
                        <input required="" type="text" name="cc_number" autocomplete="payment cc-number" class="form-control" aria-required="true">
                        <span class="card-icon amex"></span>
                        <span class="card-icon master"></span>
                        <span class="card-icon visa"></span>
                        <span class="card-icon discover"></span>
                      </div>
                      <div class="form__field-wrapper col-sm-2 col-xs-12">
                        <div class="select select--full-width">
                          <label for="cc_exp_month" class="form__label">Exp Month</label>
                          <select name="cc_exp_month" class="selectpicker" data-placeholder="" required="" tabindex="-1">
                            <option value="" selected=""></option>
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                          </select>
                        </div>
                      </div>
                      <div class="form__field-wrapper col-sm-2 col-xs-12">
                        <div class="select select--full-width">
                          <label for="cc_exp_year" class="form__label">Exp Year</label>
                          <select name="cc_exp_year" class="selectpicker" data-placeholder="" required="" tabindex="-1">
                            <option value="" selected=""></option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option><option value="2026">2026</option><option value="2027">2027</option><option value="2028">2028</option><option value="2029">2029</option><option value="2030">2030</option><option value="2031">2031</option><option value="2032">2032</option><option value="2033">2033</option><option value="2034">2034</option><option value="2035">2035</option><option value="2036">2036</option><option value="2037">2037</option></select>
                        </div>
                      </div>
                      <div class="form__field-wrapper col-sm-2 col-xs-12">
                        <label for="cc_cvc" class="form__label">
                          <span>CVC</span>
                          <span class="tip tip--left">
                            <i class="tip__label lp-icon lp-icon-help_circle cvc"></i>
                            
                          </span>
                        </label>
                        <input required="" type="text" name="cc_cvc" class="form-control" aria-required="true">
                      </div>
                    </div> 
                     @endif
                  <span id="package_type" class="hide">{{$type}}</span>   
                </div>
              </div>
              
            </div>
            <div class="col-lg-5 col-md-5 mx-auto order-summary">
                <div class="order-summary-column col-lg-11 col-md-11">
                    <p class="order-summary-title">Order Summary</p>
                        <div class="order-summary-box">

                          <div class="order-summary-table">

                            
                              <div class="order-summary-table__row--header text-center">
                                {{$planname}}
                              </div>

                              <div class="divider"></div>
                            
                              @foreach ($facilityArray as $key => $value)
                            
                              <div class="order-summary-table__row">
                                <div class="{{$key}}">
                                    {{$value}}
                                </div>                                
                              </div>
                              
                              @endforeach

                              <div class="divider"></div>

                            
                            <div class="order-summary-table__row">
                              <div>Billing Cycle</div>
                              <div>Every {{$duration}} Months</div>
                            </div>
                            

                            <div class="divider"></div>

                            
                              <div class="order-summary-table__row order-summary-table__row--header pushoff-top">
                                <div>Today's charge:</div>
                                <div class="order-total">
                                  <!-- <span class="order-total__dollar"></span> -->
                                  <span class="order-total__price">
                                    $0
                                  </span>
                                </div>
                              </div>

                              
                              <div class="order-summary-table__row order-summary-table__row--header order-summary-table__total-future pushoff-top">
                                <div>Charged on {{$trialEnd}}:</div>
                                <div class="order-total order-total__future">
                                  <!-- <span class="order-total__dollar">$</span> -->
                                  <span class="order-total__price">
                                    ${{$amount}}
                                  </span>
                                </div>
                              </div>
                              
                            

                          </div>
                        </div>

                        <div class="consent-summary">
                        
                          <p class="order-summary-terms">If you don't cancel your subscription before your trial ends on {{$trialEnd}} , you agree that you will automatically be charged the subscription fee (of ${{$amount}}) every {{$duration}} months.</p>
                        


                            <!-- <div class="form__field-wrapper">
                              <label class="consent-email-label">
                                <input type="checkbox" id="consent-email-status" name="consent-email-status" value="granted">
                                I agree to receive emails with essential updates and awesome offers
                              </label>
                            </div>
                            <input id="consent-email-message" name="consent-email-message" type="hidden" value="I agree to receive emails with essential updates and awesome offers"> -->
                        </div>
                        <button type="submit" name="planformsubmit" class="lego-btn submit-order btn btn-custom" id="submit">Start my FREE 30-day Trial</button>


                        <p class="order-summary-terms">By clicking the button above I acknowledge that I've read and agree with the <a href="#" target="_blank">Terms of Service</a> and
                          
                            want to start my 30-day free trial.
                          
                        </p>

                        <!-- <p class="order-summary-braintree-badge">
                          <a href="#" target="_blank">
                            <img src="https://s3.amazonaws.com/braintree-badges/braintree-badge-wide-light.png" width="280" height="44" border="0">
                          </a>
                        </p> -->
                </div>

              </div>
        </div>       
        <hr class="hr">
      </form>
  </div>

 

   

@endsection


@section('js')



<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
   <script>

    $.validator.addMethod("blankSpace", function (value, element) {
        return (value.length == 0) ? true : (value.trim().length > 0 || value == "");
    }, "Please enter a valid detail");

    $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var check = false;
            return this.optional(element) || regexp.test(value);
        },
        "Please enter a valid phone number."
    );

  $(document).ready(function () {




    $('#planform').validate({ // initialize the plugin
        rules: {
            clinic_name: {
                required: true,
                minlength: 2,
                maxlength:50,
                blankSpace:true,
            },
            email: {
                required: true,
                email: true,
                remote: {
                  url: "/clinic/check_email",
                  type: "post"
                },
                blankSpace:true,
            },
            first_name: {
                required: true,
                minlength: 1,
                maxlength:50,
                blankSpace:true,
            },
            last_name: {
                required: true,
                minlength: 1,
                maxlength:50,
                blankSpace:true,
            },
            username: {
                required: true,                
                minlength: 5,
                maxlength:20,
                remote: {
                  url: "/clinic/check_username",
                  type: "post"
                },
                blankSpace:true,
            },
            password: {
                required: true,
                minlength: 5,
                blankSpace:true,
            },
            cpassword: {
                required: true,
                blankSpace:true,
                minlength: 5,
               // equalTo: "#password"
            },
            address: {
                required: true,
                minlength: 5,
                maxlength:100,
                blankSpace:true,
            },
            city: {
                required: true,
                minlength: 2,
                maxlength:20,
                blankSpace:true,
            },
            country: {
                required: true,
                minlength: 2,
                maxlength:20,
                blankSpace:true,
            },
            contact_number: {
                required: true,
                blankSpace:true,
                regex :  /^[0-9\s+()-]*[0-9]$/,
                minlength: 10,
                maxlength: 16,
            },
            cc_number: {
                required: function(element) {
                  if($('#package_type').html() != 'free'){
                    return true
                  }
                  else{
                    return false
                  }
                },

                blankSpace:true,

            },
            cc_exp_month: {
                required: function(element) {
                  if($('#package_type').html() != 'free'){
                    return true
                  }
                  else{
                    return false
                  }
                },
                blankSpace:true,
            },
            cc_exp_year: {
                required: function(element) {
                  if($('#package_type').html() != 'free'){
                    return true
                  }
                  else{
                    return false
                  }
                },
                blankSpace:true,
            },
            cc_cvc: {
                required: function(element) {
                  if($('#package_type').html() != 'free'){
                    return true
                  }
                  else{
                    return false
                  }
                },
                blankSpace:true,
            }
        },
        messages: {
            password: {
                required: "Please enter a password",
                minlength: "Your password must be at least 5 characters long"
            },
            cpassword: {
                required: "Please enter a confirm password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the confirm password same as password "
            },
            email: {
                required: "Please enter a email",
                email:  "Please enter a valid email address",
                remote:  "Email address is already registered.",
            },
            first_name: {
                required:  "Please enter a first name",
                minlength: 'Frist name should be atleast 1 digits long',
                maxlength: 'Frist name should be maximum 50 characters long',
            },
            last_name: {
                required:  "Please enter a last name",
                minlength: 'Last name should be atleast 1 digits long',
                maxlength: 'Last name should be maximum 50 characters long',
            },
            clinic_name: {
                required:  "Please enter a clinic name",
                minlength: 'Clinic name should be atleast 5 digits long',
                maxlength: 'Clinic name should be maximum 20 characters long',
            },
            username: {
                required:  "Please enter a username",
                minlength: 'Username should be atleast 5 digits long',
                maxlength: 'Username should be maximum 20 characters long',
            },
            address: {
                required:  "Please enter a address",
                minlength: 'Address should be atleast 5 digits long',
                maxlength: 'Address should be maximum 100 characters long',
            },
            city: {
                required:  "Please enter a city",
                minlength: 'City should be atleast 2 digits long',
                maxlength: 'City should be maximum 20 characters long',
            },
            country: {
                required:  "Please enter a country",
                minlength: 'Country should be atleast 2 digits long',
                maxlength: 'Country should be maximum 20 characters long',
            },
            contact_number: {
                required:  "Please enter a contact number",
                minlength: 'contact number should be atleast 10 digits long',
                maxlength: 'contact number should be maximum 16 characters long',
            },
           
            cc_number: {
                required:  "Please enter a card number",
                blankSpace:true,

            },
            cc_exp_month: {
                required:  "Please enter an expiry month",
                blankSpace:true,
            },
            cc_exp_year: {
                required:  "Please enter an expiry year",
                blankSpace:true,
            },
            cc_cvc: {
                required:  "Please enter a CVV number",
                blankSpace:true,
            }

        },
        errorPlacement: function(error, element) {
            switch (element.attr("name")) {                    
                default:
                    $('.help-block').hide();
                    $('.form-group').removeClass("has-error");
                    error.insertAfter(element);
            }
        }
    });
  });
</script>
@endsection