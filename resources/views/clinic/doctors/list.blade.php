@extends('clinic.layout.auth')
@section('content')

	<section class="content doctors" id="app">
	    <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Doctors</li>
          </ol>
        </div>

      
        
          <!-- <form action="UploadImages"
                              class="dropzone"
                              id="my-awesome-dropzone" enctype="multipart/form-data"> {{ csrf_field() }}
                        </form> -->

	     
        <doctors headline='doctor'></doctors>
     </section>
     

@endsection
@section('js')

<script type="text/javascript">
   Dropzone.options.myAwesomeDropzone = {
            paramName: "file",
            maxFilesize: 10,
            url: 'doctor/image',
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            maxfilesexceeded: function(file) {
                this.removeAllFiles();
                this.addFile(file);
              },
            init: function() {
                var cd;
                
                this.on("success", function(file, response) {

                    $('.dz-progress').hide();
                    $('.dz-size').hide();
                    $('.dz-error-mark').hide();
                    //$('#image_id').html(response.data);

                    var textbox = document.getElementById('image_id');
                    textbox.value = response.data;

                    /*console.log(response.data);
                    console.log(file);*/
                    cd = response;
                });
                this.on("addedfile", function(file) {
                    var removeButton = Dropzone.createElement("<a href=\"#\">Remove file</a>");
                    var _this = this;
                    removeButton.addEventListener("click", function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                        _this.removeFile(file);
                        var name = "largeFileName=" + cd.pi.largePicPath + "&smallFileName=" + cd.pi.smallPicPath;
                        $.ajax({
                            type: 'POST',
                            url: 'DeleteImage',
                            data: name,
                            dataType: 'json'
                        });
                    });
                    file.previewElement.appendChild(removeButton);
                });

                
            }
        };
</script> 
@endsection