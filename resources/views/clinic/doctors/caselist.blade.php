@extends('clinic.layout.auth')
@section('content')
	   <section class="content" id="app">
	      <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/clinic/home">Home</a></li>
            <li class="breadcrumb-item"><a href="/clinic/doctor">Doctors</a></li>
            <li class="breadcrumb-item active">{{$lab_name}}</li>
            <!-- <li class="breadcrumb-item active" style="float: right;"><a href="/clinic/labs"> Back </a></li> -->
          </ol>
        </div>

       <doctorcaselibrary doctor_id="{{$id}}"></doctorcaselibrary>
     </section>
  <style type="text/css">
    
    .nodataDiv{
        font-size: 22px;
        color: #909796;
        padding: 20px 10px 10px 28px;  
    } 
  </style>
@endsection
@section('js')
  <script src="/js/common-clinic.js"></script>
@endsection
