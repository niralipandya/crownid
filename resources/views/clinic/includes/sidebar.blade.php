<aside class="main-sidebar">
  	<!-- sidebar: style can be found in sidebar.less -->
  	<section class="sidebar">
    	<ul class="sidebar-menu">
			<li class="sidebar-li {{ in_array($current_route_name, ['clinic.home', 'clinic.calender']) ? 'active' : '' }}">
				<a href="{{ url('/clinic/home') }}">
					<div class="menu-home"></div>
					<span>HOME</span>
				</a>
			</li>
			<li class="sidebar-li {{ in_array($current_route_name , ['clinic.doctor.index','clinic.doctorcaselibrary']) ? 'active' : '' }}">
				<a href="{{ url('clinic/doctor') }}">
					<div class="menu-doctors"></div>
					<span>DOCTORS</span>
				</a>
			</li>
			<li class="sidebar-li {{ $current_route_name == 'clinic.chatlist' ? 'active' : '' }}">
				<a href="{{ url('clinic/chat') }}">
					<div class="menu-chats"></div>
					<span>CHATS
						<span class="label label-danger display-none" id="chat_badgeCount"></span>
					</span>
				</a>
			</li>
			<li class="sidebar-li {{ in_array($current_route_name, ['clinic.labs.index', 'clinic.labcaselibrary']) ? 'active' : '' }}">
				<a href="{{ url('clinic/labs') }}">
					<div class="menu-labs"></div>
					<span>LABS</span>
				</a>
			</li>
			<li class="sidebar-li {{ $current_route_name == 'clinic.caselibrary' ? 'active' : '' }}">
				<a href="{{ url('clinic/caselibrary') }}">
					<div class="menu-library"></div>
					<span>CASE LIBRARY</span>
				</a>
			</li>
			<li class="sidebar-li {{ $current_route_name == 'clinic.delayBoxCase' ? 'active' : '' }}">
				<a href="{{ url('clinic/delayBoxCase') }}">
					<div class="menu-clock"></div>
					<span>DELAY BOX</span>
				</a>
			</li>
			<li class="sidebar-li">
				<a href="#">
					<div class="menu-reports"></div>
					<span>REPORTS</span>
				</a>
			</li>
			<!-- <li class="treeview sidebar-li {{ in_array($current_route_name, ['clinic.report.index', 'clinic.report.labs']) ? 'active' : '' }}">
				<a href="{{ url('clinic/report') }}">
					<div class="menu-reports"></div>
					<span>REPORTS</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="sidebar-li {{ $current_route_name == 'clinic.report.index' ? 'active' : '' }}">
						<a href="{{ url('clinic/report') }}">CASE REPORTS</a>
					</li>
					<li class="sidebar-li {{ $current_route_name == 'clinic.report.labs' ? 'active' : '' }}">
						<a href="{{ url('clinic/report/labs') }}">LABS REPORTS</a>
					</li>
					<li class="sidebar-li {{ $current_route_name == 'clinic.report.doctor' ? 'active' : '' }}">
						<a href="{{ url('clinic/report/doctor') }}">DOCTORS REPORTS</a>
					</li>
				</ul>
			</li> -->
			<!-- <li class="sidebar-li {{ $current_route_name == 'clinic.support.index' ? 'active' : '' }}">
				<a href="{{ url('clinic/support') }}">
					<div class="menu-support"></div>
					<span>SUPPORT</span>
				</a>
			</li> -->
			<!-- <li class="treeview sidebar-li {{ in_array($current_route_name, ['clinic.support.index', 'clinic.support.faq', 'clinic.support.contactus']) ? 'active' : '' }}">
				<a href="{{ url('/') }}">
					<i class="fa fa-question-circle fa-lg"></i>
					<span>SUPPORT</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="sidebar-li {{ $current_route_name == 'clinic.support.index' ? 'active' : '' }}">
						<a href="{{ url('clinic/support') }}">SUPPORT TICKET</a>
					</li>
					<li class="sidebar-li {{ $current_route_name == 'clinic.support.faq' ? 'active' : '' }}">
						<a href="#">FAQ</a>
					</li>
					<li class="sidebar-li {{ $current_route_name == 'clinic.support.contactus' ? 'active' : '' }}">
						<a href="#">CONTACT US</a>
					</li>
				</ul>
			</li> -->
			<li class="treeview sidebar-li {{ in_array($current_route_name, ['clinic.settings.index', 'clinic.defaultSettings','clinic.profile','clinic.changepasseordpage']) ? 'active' : '' }}">
				<a href="{{ url('/clinic/home') }}">
					<div class="menu-settings"></div>
					<span>SETTINGS</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="sidebar-li {{ $current_route_name == 'clinic.settings.index' ? 'active' : '' }}">
						<a href="{{ url('clinic/settings') }}">GENERAL</a>
					</li>
					<li class="sidebar-li {{ $current_route_name == 'clinic.defaultSettings' ? 'active' : '' }}">
						<a href="{{ url('clinic/settings/default') }}">DEFAULT</a>
					</li>
					<li class="sidebar-li {{ $current_route_name == 'clinic.profile' ? 'active' : '' }}">
						<a href="{{ url('clinic/profile') }}">profile</a>
					</li>
					<li class="sidebar-li {{ $current_route_name == 'clinic.changepasseordpage' ? 'active' : '' }}">
						<a href="{{ url('clinic/changepassword') }}">Change Password</a>
					</li>
					<li class="sidebar-li ">
						<a href="#">REFERRALS</a>
					</li>
				</ul>
			</li>
    	</ul>
  	</section>
  	<!-- /.sidebar -->
</aside>