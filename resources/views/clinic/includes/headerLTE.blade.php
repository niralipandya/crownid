<header class="main-header">

    <!-- Logo -->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
   
      <a href="{{ url('/clinic/home') }}" class="logo">
        <!-- <span class="logo-lg" style="padding: 8px 0 0 0;">CrownID</span> -->
        <span class="logo-lg"><img src="{{ asset('images/logo.png') }}" width="120px"/></span>
      </a>
    

  
    <nav class="navbar navbar-light bg-faded">
       <a class="navbar-brand" href="#"></a>
      <ul class="nav justify-content-end">
        <!-- <li class="header-search-box">
          <i class="fa fa-search"></i>
          <input type="text">
        </li> -->
        <notificationclinic></notificationclinic>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          @if(Auth::user()->pic=='')
            <img src="{{ asset('storage/avatar.png') }}" class="user-image" alt="User Image">
          @else
            <img src="{{ asset('storage/'.Auth::user()->pic) }}" class="user-image" alt="User Image">
          @endif
          
          <span class="hidden-xs">{{ ucwords(Auth::user()->username) }}</span>
          </a>
          <div class="dropdown-menu">
           
            <a class="dropdown-item bggreen" href="{{ url('/clinic/logout') }}" 
            onclick="event.preventDefault();document.getElementById('logout-form').submit();" ><strong><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;&nbsp;Logout</strong>
            <form id="logout-form" action="{{ url('/clinic/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            </a>
            <div class="dropdown-divider"></div>
          </div>
        </li>
      </ul>

    </nav>
</header>
