@extends('clinic.layout.auth')
@section('content')
	<section class="content" id="app">
	      <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Reports</a></li>
            <li class="breadcrumb-item active">Doctor Reports</li>
          </ol>
        </div>
          <div class="col-12">
              <div class="dashboard-box report-doctor">
                  <div class="row bs-example-report-detail">
                     <div class=" col-11">
                        <h4 class="text-center">Doctors Reports</h4>
                      </div>
                      <div class="col-1">
                        <button class="btn btn-yellow btn-report">Filter</button>
                      </div>

                </div>
              </div>
          </div>
          
          <div class="col-12">
                <div class="dashboard-box-no-margin ongoingcase">
                  <div class="row bs-example-report-table">
                    <div class="box-body content-table">
                      <table class="table table-bordered table-responsive">
                        <tbody>
                            <tr>
                              <th>Doctors Name</th>
                              <th>Email</th>
                              <th>Phone</th>
                              <th>Ongoing Cases</th>
                              <th>Total Cases</th>
                            </tr>
                            <tr>
                              <td>Dr. James Smith</td>
                              <td>dentechlabs@gmail.com</td>
                              <td>+91 123 456 7890</td>
                              <td>15</td>
                              <td>20</td>
                            </tr>
                            <tr>
                              <td>Dr. James Smith</td>
                              <td>dentechlabs@gmail.com</td>
                              <td>+91 123 456 7890</td>
                              <td>15</td>
                              <td>20</td>
                            </tr>
                            <tr>
                              <td>Dr. James Smith</td>
                              <td>dentechlabs@gmail.com</td>
                              <td>+91 123 456 7890</td>
                              <td>15</td>
                              <td>20</td>
                            </tr>
                            <tr>
                              <td>Dr. James Smith</td>
                              <td>dentechlabs@gmail.com</td>
                              <td>+91 123 456 7890</td>
                              <td>15</td>
                              <td>20</td>
                            </tr>
                            <tr>
                              <td>Dr. James Smith</td>
                              <td>dentechlabs@gmail.com</td>
                              <td>+91 123 456 7890</td>
                              <td>15</td>
                              <td>20</td>
                            </tr>
                            <tr>
                              <td>Dr. James Smith</td>
                              <td>dentechlabs@gmail.com</td>
                              <td>+91 123 456 7890</td>
                              <td>15</td>
                              <td>20</td>
                            </tr>
                            <tr>
                              <td>Dr. James Smith</td>
                              <td>dentechlabs@gmail.com</td>
                              <td>+91 123 456 7890</td>
                              <td>15</td>
                              <td>20</td>
                            </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="col-12 box-footer clearfix">
                      <ul class="pagination pagination-sm center-pagination bordered pull-right">
                        <li><a href="#">Previous</a></li>
                        <li class=""><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li class=""><a href="#">3</a></li>
                        <li><a href="#">Next</a></li>
                      </ul>
                    </div>
                     
                  </div>
                </div>
                <div class="dashboard-box-no-margin labs" style="display: none">
                  <div class="row bs-example-report-table">
                    <div class="box-body content-table">
                      <table class="table table-bordered table-responsive">
                        <tbody>
                          <tr>
                            <th>Labe Name</th>
                            <th>Case Id</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>EDD</th>
                            <th>Current Status</th>
                          </tr>
                          <tr>
                            <td>Dentech Labs</td>
                            <td>#jospau1234</td>
                            <td>dentechlabs@gmail.com</td>
                            <td>+91 1234567890</td>
                            <td>May 3,2017</td>
                            <td class="status-green">Pickup Pending</td>
                          </tr>
                          <tr>
                            <td>Dentech Labs</td>
                            <td>#jospau1234</td>
                            <td>dentechlabs@gmail.com</td>
                            <td>+91 1234567890</td>
                            <td>May 3,2017</td>
                            <td class="">No status</td>
                          </tr>
                          
                         
                          
                        
                        </tbody>
                      </table>
                    </div>
                    <div class="col-12 box-footer clearfix">
                      <ul class="pagination pagination-sm center-pagination bordered pull-right">
                        <li><a href="#">Previous</a></li>
                        <li class=""><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li class=""><a href="#">3</a></li>
                        <li><a href="#">Next</a></li>
                      </ul>
                    </div>
                     
                  </div>
                </div>
                <div class="dashboard-box-no-margin companion" style="display: none">
                  <div class="row bs-example-report-table">
                    <div class="box-body content-table">
                    <table class="table table-bordered table-responsive">
                    <tbody>
                      <tr>
                        <th>Labe Name</th>
                        <th>Case Id</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>EDD</th>
                        <th>Current Status</th>
                      </tr>
                      <tr>
                        <td>Dentech Labs</td>
                        <td>#jospau1234</td>
                        <td>dentechlabs@gmail.com</td>
                        <td>+91 1234567890</td>
                        <td>May 3,2017</td>
                        <td class="status-green">Pickup Pending</td>
                      </tr>
                      <tr>
                        <td>Dentech Labs</td>
                        <td>#jospau1234</td>
                        <td>dentechlabs@gmail.com</td>
                        <td>+91 1234567890</td>
                        <td>May 3,2017</td>
                        <td class="">No status</td>
                      </tr>
                      <tr>
                        <td>Dentech Labs</td>
                        <td>#jospau1234</td>
                        <td>dentechlabs@gmail.com</td>
                        <td>+91 1234567890</td>
                        <td>May 3,2017</td>
                        <td class="status-green">Pickup Pending</td>
                      </tr>
                      <tr>
                        <td>Dentech Labs</td>
                        <td>#jospau1234</td>
                        <td>dentechlabs@gmail.com</td>
                        <td>+91 1234567890</td>
                        <td>May 3,2017</td>
                        <td class="">No status</td>
                      </tr>
                      <tr>
                        <td>Dentech Labs</td>
                        <td>#jospau1234</td>
                        <td>dentechlabs@gmail.com</td>
                        <td>+91 1234567890</td>
                        <td>May 3,2017</td>
                        <td class="status-green">Pickup Pending</td>
                      </tr>
                      <tr>
                        <td>Dentech Labs</td>
                        <td>#jospau1234</td>
                        <td>dentechlabs@gmail.com</td>
                        <td>+91 1234567890</td>
                        <td>May 3,2017</td>
                        <td class="">No status</td>
                      </tr>
                      
                     
                      
                    
                    </tbody>
                  </table>
                    </div>
                    <div class="col-12 box-footer clearfix">
                      <ul class="pagination pagination-sm center-pagination bordered pull-right">
                        <li><a href="#">Previous</a></li>
                        <li class=""><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li class=""><a href="#">3</a></li>
                        <li><a href="#">Next</a></li>
                      </ul>
                    </div>
                     
                  </div>
                </div>
                
          </div>

	       
        
     </section>
@endsection
@section('js')
  <script src="/js/reports.js"></script>
@endsection