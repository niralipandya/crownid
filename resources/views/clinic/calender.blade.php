@extends('clinic.layout.auth')

@section('js')
 <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="/css/fullcalendar/fullcalendar.min.js"></script>
<script type="text/javascript">

$(function () {



    var obj = {};
    $.ajax({
        url: '/clinic/calendercaselist',
        type: 'GET',
        success: function(doc) {
            obj = doc.data;
            ini_events();
        }
    });
   
   //console.log(obj);
    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    /*var date = new Date();
    var d = date.getDate(),
        m = date.getMonth(),
        y = date.getFullYear();*/

    function ini_events() {
      console.log(obj);
        $('#calendar').fullCalendar({
            header: {
              left: 'prev,next today',
              center: 'title',
              right: 'month,agendaWeek,agendaDay'
            },
            buttonText: {
              today: 'today'
            },
            //Random default events
            events: obj,
            displayEventTime: false,
            editable: false,
            droppable: false
          });

    }    
    
  });

</script>
@endsection

@section('css')
  <link rel="stylesheet" href="/css/fullcalendar/fullcalendar.min.css">
@endsection

@section('content')


<div id="app">
   <section class="content">
       
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/clinic/home') }}"  >Home</a></li>
            <li class="breadcrumb-item active">Calender</li>
          </ol>
        </div>
        
        <div class="col-sm-8 col-md-12">
            <div class="box color4">
                <div class="row">
                  <div class="col-md-2 p-l-0">
                    <div class=" box-solid casestatuslist">
                      <div class="box-header with-border">
                        <h4 class="box-title">Case Status</h4>
                      </div>
                      <div class="box-body">
                        <!-- the events -->
                        <div id="external-events">
                          <div class="colorboxdisplay">
                            <span   class="external-event bg-teal label label-default">&nbsp;</span> 
                            <label>Pick-up pending</label>  
                          </div>
                          <div class="colorboxdisplay">
                            <span   class="external-event bg-blue label label-default">&nbsp;</span> 
                            <label>Received By Lab</label>  
                          </div>
                          <div class="colorboxdisplay">
                            <span   class="external-event bg-aqua label label-default">&nbsp;</span> 
                            <label>In Progress</label>  
                          </div>
                          <div class="colorboxdisplay">
                            <span  class="external-event bg-red label label-default">&nbsp;</span> 
                            <label>Delayed</label>  
                          </div>
                          <div class="colorboxdisplay">
                            <span class="external-event label label-default bg-light-blue">&nbsp;</span> 
                            <label>Ready for delivery</label>  
                          </div>
                          <div class="colorboxdisplay">
                            <span  class="external-event label label-default bg-olive">&nbsp;</span> 
                            <label>Delivered to patient</label>  
                          </div>
                          <!-- <div class="external-event bg-teal">Pick-up pending</div>
                          <div class="external-event bg-blue">Received</div>
                          <div class="external-event bg-aqua">In Process</div>
                          <div class="external-event bg-red">Delayed</div>
                          <div class="external-event bg-light-blue">Ready for delivery</div>  
                          <div class="external-event bg-olive">Delivered to patient</div>               -->  
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="dashboard-box col-md-10">
                      <div id="calendar"></div>
                  </div>
                </div>
                
            </div>
        </div>


        

        


    </section>
</div>
@endsection
