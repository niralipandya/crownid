@extends('clinic.layout.auth')
@section('content')
	   <section class="content">
	      <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Labs</a></li>
            <li class="breadcrumb-item active">Dentech Lab</li>
          </ol>
        </div>

        <div class="row">
          <div class="col-xl-4 col-lg-12 col-md-12">
             <div class="top" style="">
                <form id="custom-search-form" class="form-search form-horizontal">
                  <div class="input-append span12">
                      <i class="fa fa-search"></i>
                      <input type="text" class="search-query mac-style" placeholder="Search">
                  </div>
                </form>
                <span class="pull-left">All Cases</span>
                <div class="pull-right">
                  <div class="dropdown show">Filter :
                    <a class="dropdown-toggle filter-dropdown-text" href="https://example.com" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Pick-up pending
                    </a>

                    <div class="dropdown-menu filter-dropdown" aria-labelledby="dropdownMenuLink">
                      <a class="dropdown-item" href="#">Pick-up pending</a>
                      <a class="dropdown-item" href="#">In progress</a>
                      <a class="dropdown-item" href="#">Delayed cases</a>
                      <a class="dropdown-item" href="#">All labs</a>
                    </div>
                  </div>
                </div>
             </div>
             <div class="scroll-area">
               
                     <div class="dashboard-box-lab">
                          <a href="#">
                              <div class="bs-example-lab">
                                  <span class="inner-box-lab">#jospau1234</span><br/>
                                  <span class="clinic-name">Dentech Clinic</span><br/>
                                  <span class="date-class">EDD- 05 July,17</span>
                                  <span class="inner-box-right-lab-pickup">On Pickup</span>
                              </div>
                          </a>
                      </div>

                      <div class="dashboard-box-lab">
                           <a href="#">
                              <div class="bs-example-lab">
                                  <span class="inner-box-lab">#donamarsh123</span><br/>
                                  <span class="clinic-name">Dentech Clinic</span><br/>
                                  <span class="date-class">EDD- 05 July,17</span>
                                  <span class="inner-box-right-lab-pickup-close">Pickup Close</span>
                              </div>
                           </a>
                      </div>


                      <div class="dashboard-box-lab">
                         <a href="#">
                              <div class="bs-example-lab">
                                  <span class="inner-box-lab">#jospau1234</span><br/>
                                  <span class="clinic-name">Dentech Clinic</span><br/>
                                  <span class="date-class">EDD- 05 July,17</span>
                                  <span class="inner-box-right-lab-process">In Process</span>
                              </div>
                         </a>
                      </div>

                      <div class="dashboard-box-lab">
                         <a href="#">
                              <div class="bs-example-lab">
                                  <span class="inner-box-lab">#matthkelly1986</span><br/>
                                  <span class="clinic-name">Dentech Clinic</span><br/>
                                  <span class="date-class">EDD- 05 July,17</span>
                                  <span class="inner-box-right-lab-completed">Completed</span>
                              </div>
                         </a>
                      </div>

                      <div class="dashboard-box-lab">
                          <a href="#">
                              <div class="bs-example-lab">
                                  <span class="inner-box-lab">#jospau1234</span><br/>
                                  <span class="clinic-name">Dentech Clinic</span><br/>
                                  <span class="date-class">EDD- 05 July,17</span>
                                  <span class="inner-box-right-lab-pickup">On Pickup</span>
                              </div>
                          </a>
                      </div>
                </div>
            
          </div>   

          <div class="col-xl-8 col-lg-12 col-md-12">
              <div class="dashboard-box">
                <div class="row bs-example-lab-detail">
                 <div class="col-2">
                  <span class="clinic-name">Case ID:</span>
                 </div>
                 <div class="col-10">
                  <span class="inner-box-lab">#jospau1234</span>
                  <a href="/clinic/chat"><span class="inner-box-right-lab-detail"><img class="chat-icon" src="/images/chat-icon.png"></img></a>
                  </span>
                 </div>
                </div>
                <div class="row bs-example-lab-detail-nobackground">
                 <div class="col-4">
                  <span class="doctor-name">Doctor</span><br/>
                  <span class="padding-detail">Dr. James Smith</span>
                 </div> 
                 <div class="col-4">
                  <span class="doctor-name">EOD</span><br/>
                  <span class="padding-detail">05-07-2017</span>
                 </div> 
                 <div class="col-4">
                  <span class="doctor-name">Status</span><br/>
                  <span class="padding-detail status-completed">Completed</span>
                 </div> 
                </div>
                <div class="row bs-example-lab-detail">
                 <div class="col-2">
                    <img src="http://52.77.117.85:8004/images/qrcode.jpg" class="qr-code" /> 
                 </div>
                 <div class="col-10">
                    <span class="inner-box-lab-detail">Technical Detail</span>
                     <div class="row detail-margin">
                       <div class="col-4">
                        <span class="clinic-name">Process Date:</span><br/>
                        <span class="">DD/MM/YYYY</span>
                       </div> 
                       <div class="col-4">
                        <span class="clinic-name">Fixed Prosthesis:</span><br/>
                        <span class="">Separate Crowns</span>
                       </div> 
                       <div class="col-4">
                        <span class="clinic-name">Teeth:</span><br/>
                        <span class="">15,17</span>
                       </div> 
                    </div> 

                    <div class="row detail-margin">
                       <div class="col-4">
                        <span class="clinic-name">Missing Teeth:</span><br/>
                        <span class="">21,27,31,37</span>
                       </div> 
                       <div class="col-4">
                        <span class="clinic-name">Materials used:</span><br/>
                        <span class="">All Ceramic</span>
                       </div> 
                       <div class="col-4">
                        <span class="clinic-name">Metal Ceramic:</span><br/>
                        <span class="">Precious-Gold</span>
                       </div> 
                    </div> 

                     <div class="row detail-margin">
                       <div class="col-12">
                        <span class="clinic-name">Bite Registration:</span><br/>
                        <span class="">Yes it is a bite tray but we include a registration record for accuracy, Do not use the tray position.</span>
                       </div> 
                    </div> 

                     <span class="inner-box-lab-detail">Color</span> <hr class="hrclassdetail"/>  

                     <div class="row detail-margin">
                       <div class="col-6">
                        <span class="clinic-name">Neck:</span><br/>
                        <span class="">A3,C2</span>
                       </div> 
                       <div class="col-6">
                        <span class="clinic-name">Body:</span><br/>
                        <span class="">A3,C2</span>
                       </div> 
                    </div> 

                    <div class="row detail-margin">
                       <div class="col-12">
                        <span class="clinic-name">Special Instrauctions and Shade guide used if not Vita.</span><br/>
                        <span class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</span>
                       </div> 
                    </div> 

                     <div class="row detail-margin">
                       <div class="col-12">
                        <span class="clinic-name">Try-Ins/Delivery:</span><br/>
                        <span class="">Metal try-in</span>
                       </div> 
                    </div> 

                    <div class="row detail-margin">
                       <div class="col-12">
                          <img src="http://52.77.117.85:8004/images/barcode.png" class="barcode" width="188px" height="70px">
                       </div> 
                    </div> 

                </div> 


                </div>
              </div>

          </div>
        </div>
     </section>
   
@endsection
@section('js')
  <script src="/js/common-clinic.js"></script>
@endsection
