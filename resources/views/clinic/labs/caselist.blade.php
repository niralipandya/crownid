@extends('clinic.layout.auth')

@section('content')
	   <section class="content" id="app">
	      <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Case Library</li>
          </ol>
        </div>
        <caselibrary></caselibrary>
       
     </section>

@endsection
@section('js')
  <script src="/js/common-clinic.js"></script>
@endsection