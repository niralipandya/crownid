<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'CrownId') }}</title>

    <!-- Styles -->
    <link href="/css/clinic-inner.css" rel="stylesheet">
    
    <link rel="stylesheet" href="{{ asset('/plugins/iCheck/square/blue.css') }}">
    @yield('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
    <!-- Scripts -->
    <script>
        window.Laravel = <?php 
        echo json_encode([
            'csrfToken' => csrf_token(),
            'login_user_id' => auth()->user()->id
        ]); ?>;

    </script>
</head>
<body class="skin sidebar-mini"> 
    <section id="app">
        @include('clinic.includes.headerLTE')
            @include('clinic.includes.sidebar')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                @yield('content')
            </div>

            @include('clinic.includes.footerLTE')
    </section>
        
    
    <!-- Scripts -->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    
    <script src="https://www.gstatic.com/firebasejs/4.13.0/firebase.js"></script>

    <script>
        // Initialize Firebase //console.log('call');
        var config = {
          apiKey: "AIzaSyAy6utZcdm4sqe1QZjnKu_1mPpAdfvC6H4",
          authDomain: "crownid-e6f21.firebaseapp.com",
          databaseURL: "https://crownid-e6f21.firebaseio.com",
          projectId: "crownid-e6f21",
          storageBucket: "crownid-e6f21.appspot.com",
          messagingSenderId: "975358842203"
        };
        firebase.initializeApp(config);

        var database = firebase.database();




    </script>
    <script src="/js/app.js"></script>
    <script src="/js/admin-app.js"></script>
    <script src="/js/admin-main.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>      


    <script type="text/javascript">
    $(document).ready(function () {
       
        $().fancybox({
            selector : '[data-fancybox="image_gallery"]',
            loop    : true
        });
    });
    </script>  
    @yield('js')
</body>
</html>
