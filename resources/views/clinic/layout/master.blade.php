<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'CrownID') }}</title>

    <!-- Styles -->
   
    <link href="/css/clinic-app.css" rel="stylesheet">
    

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>
<body>
    
       
       <div id="app">

        @include('clinic.includes.header')
        @include('clinic.includes.scroll')
        @yield('content')

        @include('clinic.includes.footer')
       
       </div>
    
    <!-- Scripts -->

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="/js/app.js"></script>
    <script src="/js/clinic-app.js"></script>
    <script src="/js/custom-clinic.js"></script>
    @yield('js')

    <script type="text/javascript">
        function openLoginModal(){

          $('#register').modal('hide');
          //$('#xyz').modal('show');
        }

        function openLoginRModal(){
          //$('#xyz').modal('hide');
          $('#forgatpassword').modal('hide');
          $('#changepassword').modal('hide');
          $('#register').modal('show');
        }
        

        function openForgetPModal(){
          $('#register').modal('hide');
          $('#changepassword').modal('hide');
          $('#forgatpassword').modal('show');
        }

        function openChangePasswordModal(){
          $('#register').modal('hide');
          $('#forgatpassword').modal('hide');
          $('#componentReadyDataModal').modal('show');
        }
       
        function openPurchaseModal(){
          $('#purchase').modal('show');
        }
    </script>

</body>
</html>
