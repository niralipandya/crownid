@extends('clinic.layout.auth')
@section('content')

	<section class="content doctors" id="app">
	    <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item "><a href="{{ url('clinic/doctor') }}">Doctors</a></li>
            <li class="breadcrumb-item active">Purchase Token</li>
          </ol>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="box-cover">

                        <div class="box-header">
                          <!-- ADD EVENT -->
                          <div class="box-title">
                              <div class="boxtext">
                                  <form class="form" id="planform" name="planform" method="POST" action="{{ url('/clinic/token/buy') }}" enctype="multipart/form-data"  >
                                  <span id="is_card" class="hide">{{$is_card}}</span>   
                                  <div name="alert"></div> 
                                  @include('flash')            
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                  @if($is_card == 'no')
                                    <div class="row form__field-container">
                                          <div class="form__field-wrapper cc-number col-sm-12 col-xs-12">
                                            <label for="cc_number" class="form__label">Credit Card</label>
                                            <input required="" type="text" name="cc_number" autocomplete="payment cc-number" class="form-control" aria-required="true">
                                            <span class="card-icon amex"></span>
                                            <span class="card-icon master"></span>
                                            <span class="card-icon visa"></span>
                                            <span class="card-icon discover"></span>
                                          </div>
                                    </div>
                                    <div class="row form__field-container">
                                          <div class="form__field-wrapper col-sm-12 col-xs-12">
                                            <div class="select select--full-width">
                                              <label for="cc_exp_month" class="form__label">Exp Month</label>
                                              <select name="cc_exp_month" class="selectpicker form-control" data-placeholder="" required="" tabindex="-1">
                                                <option value="" selected=""></option>
                                                <option value="01">01</option>
                                                <option value="02">02</option>
                                                <option value="03">03</option>
                                                <option value="04">04</option>
                                                <option value="05">05</option>
                                                <option value="06">06</option>
                                                <option value="07">07</option>
                                                <option value="08">08</option>
                                                <option value="09">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                              </select>
                                            </div>
                                          </div>
                                    </div>
                                    <div class="row form__field-container">
                                          <div class="form__field-wrapper col-sm-12 col-xs-12">
                                            <div class="select select--full-width">
                                              <label for="cc_exp_year" class="form__label">Exp Year</label>
                                              <select name="cc_exp_year" class="selectpicker form-control" data-placeholder="" required="" tabindex="-1">
                                                <option value="" selected=""></option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option><option value="2026">2026</option><option value="2027">2027</option><option value="2028">2028</option><option value="2029">2029</option><option value="2030">2030</option><option value="2031">2031</option><option value="2032">2032</option><option value="2033">2033</option><option value="2034">2034</option><option value="2035">2035</option><option value="2036">2036</option><option value="2037">2037</option></select>
                                            </div>
                                          </div>
                                    </div>
                                    <div class="row form__field-container">
                                          <div class="form__field-wrapper col-sm-12 col-xs-12">
                                            <label for="cc_cvc" class="form__label">
                                              <span>CVC</span>
                                              <span class="tip tip--left">
                                                <i class="tip__label lp-icon lp-icon-help_circle cvc"></i>
                                                
                                              </span>
                                            </label>
                                            <input required="" type="text" name="cc_cvc" class="form-control" aria-required="true">
                                          </div>
                                    </div> 
                                    @endif
                                    <div class="row form__field-container col-lg-11 col-md-11">

                                      <div class="order-summary-box">
                                      
                                         $10 | 2 Doctor
                                      </div> 
                                      <div class="divider"></div>
                                    </div>

                                     <button type="submit" name="planformsubmit" class="lego-btn submit-order btn btn-custom" id="submit">Buy </button>

                                  
                              </div>
                          </div>
                          
                        </div>
                          <!-- /.box-header -->
                        <div class="box-body table-responsive table-fixed">
                          
                        </div>
                        
                </div>       
            </div>
        </div>
     </section>
     

@endsection

@section('js')



<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
   <script>


  $(document).ready(function () {




    $('#planform').validate({ // initialize the plugin
        rules: {
           cc_number: {
                required: function(element) {
                  if($('#is_card').html() != 'yes'){
                    return true
                  }
                  else{
                    return false
                  }
                },

                blankSpace:true,

            },
            cc_exp_month: {
                required: function(element) {
                  if($('#is_card').html() != 'yes'){
                    return true
                  }
                  else{
                    return false
                  }
                },
                blankSpace:true,
            },
            cc_exp_year: {
                required: function(element) {
                  if($('#is_card').html() != 'yes'){
                    return true
                  }
                  else{
                    return false
                  }
                },
                blankSpace:true,
            },
            cc_cvc: {
                required: function(element) {
                  if($('#is_card').html() != 'free'){
                    return true
                  }
                  else{
                    return false
                  }
                },
                blankSpace:true,
            }
        },
        messages: {
            cc_number: {
                required:  "Please enter a card number",
                blankSpace:true,

            },
            cc_exp_month: {
                required:  "Please enter an expiry month",
                blankSpace:true,
            },
            cc_exp_year: {
                required:  "Please enter an expiry year",
                blankSpace:true,
            },
            cc_cvc: {
                required:  "Please enter a CVV number",
                blankSpace:true,
            }

        },
        errorPlacement: function(error, element) {
            switch (element.attr("name")) {                    
                default:
                    $('.help-block').hide();
                    $('.form-group').removeClass("has-error");
                    error.insertAfter(element);
            }
        }
    });
  });
</script>
@endsection