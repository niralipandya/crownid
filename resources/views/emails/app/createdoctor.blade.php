@component('mail::message')
Hi {{ $name }},

{{$clinicname}} create your doctor account in {{ config('app.name') }}

Other Detais:

Name  :	{{$name}} <br>
Email : {{$email}}<br>
Username  :	{{$username}} <br>
Password : {{$password}}<br>
Number: {{$phonenumber}} <br>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
