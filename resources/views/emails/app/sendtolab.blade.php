@component('mail::message')
Hi {{ $name }},

{{$clinicname}} send you reference code and request you to create account with "{{ config('app.name') }}".Use below reference code to create account.


Reference code is <b>{{ $code }}</b>



Thanks,<br>
{{ config('app.name') }}
@endcomponent
