@extends('layouts.mail')
@section('content')
	<table>
	<tbody>
		<tr>
			<td>&nbsp;</td>
			<td><!-- content -->
			<table>
				<tbody>
					<tr>
						<td>
							<h5>Hello {{ ucwords($admin->name)}},</h5><br/>
							<p>Your password is : {{$pwd}}</p>
						</td>
						<td>
							<p>Kindly please change password after you logged in!!</p>
						</td>
					</tr>
				</tbody>
			</table>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>

@stop