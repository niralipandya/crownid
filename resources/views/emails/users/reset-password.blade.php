@component('mail::message')
Hi {{ $name }},

You recently requested to reset password for your "{{ config('app.name') }}" account.
Use below otp to reset your password.

@component('mail::button', ['url' => ''])
	{{ $otp }}
@endcomponent

If you did not request a password reset, please ignore this email.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
