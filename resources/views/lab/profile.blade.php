@extends('lab.layout.auth')


@section('content')

<section class="content" id="app">
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><a href="#">Profile</a></li>
          </ol>
        </div>

          <div class="col-12">
              <div class="dashboard-box">
                  <div class="row bs-example-support-detail">
                      <div class="col-7">
                        <span class="inner-box-support">Profile</span>
                      </div>
                  </div>
              </div>
          </div>
          
          <lab-profile></lab-profile>

         
        
     </section>
@endsection
