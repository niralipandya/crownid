@extends('lab.layout.auth')

@section('js')

<script type="text/javascript">


  Highcharts.chart('container', {

    chart: {
        marginBottom: 80,

    },
    title:{
        text:''
    },
    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        labels: {
            rotation: 90
        },
        title: {
            enabled: false
        }
    },
    yAxis: [
      {
        title: {
          text: ""
        },
        plotLines: [
        {
          value: 0,
          width: 0,
          color: "#808080"
        }]
      }
    ],

    series: [{
        data: [15, 16, 17, 20, 25, 20, 35, 40, 50, 60, 85, 100],
        showInLegend:false
    }],

    responsive: {
        rules: [{
            condition: {
            },
            chartOptions: {
            }
        }]
    },
    exporting: { enabled: false }

});
</script>
@endsection

@section('content')


<div id="app">
   <section class="content">
       
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
        </div>

       
        
        <div class="dashboard-box">
          <div class="row justify-content-between removeRow">
            <div class="col-sm-6">
                <div class="bs-example">
                  <img src="{{ asset('/images/doctorbag.png') }}" alt="Responsive image" />
                  <span class="inner-box">TOTAL CASES</span> 
                  <span class="inner-box-right">55</span>
                  <hr class="hrclass1" />           
                </div>
            </div>
            <div class="col-sm-6">
                <div class="bs-example">
                  <img src="{{ asset('/images/building.png') }}" alt="Responsive image" />
                  <span class="inner-box">TOTAL CLINICS</span> 
                  <span class="inner-box-right">16</span>
                  <hr class="hrclass2"/>           
                </div>
            </div>
          </div>
        </div>

        <div class="dashboard-box">
           <div class="row justify-content-between removeRow">
            <div class="col-sm-6">
                <div class="bs-example">
                  <img src="{{ asset('/images/Layer-12_0003_Shape-14-copy.png') }}" alt="Responsive image" />
                  <span class="inner-box">CASE LIBRARY</span> 
                  <span class="inner-box-right">17</span>
                  <hr class="hrclass3"/>           
                </div>
            </div>
            <div class="col-sm-6">
                <div class="bs-example">
                  <img src="{{ asset('/images/Layer-12_0002_Shape-24.png') }}" alt="Responsive image" />
                  <span class="inner-box">DELAY BOX</span> 
                  <span class="inner-box-right">11</span>
                  <hr class="hrclass4"/>           
                </div>
            </div>
          </div>
        </div>       
       
        @if((url()->previous() == url('/clinic')) && $pendingcasecount != 0 )            
              <receivecasepopup></receivecasepopup>          
        @endif

        <div class="dashboard-box">
           <div class="row justify-content-between removeRow">
             

              <div class="col-sm-6 col-md-12">
                  <div class="box color4">
                      <img src="{{ asset('/images/Layer-12_0001_Shape-80.png') }}" alt="Responsive image" />
                      <span class="inner-box1">CALENDAR</span> 
                      <span class="inner-box-right1">
                        <!-- <button type="button" class="btn btn-default padding-class">Upcoming</button> -->
                        <a href="{{ url('/lab/calender') }}"  class="btn btn-pitch">View All</a>
                      </span>
                     <div class="box-body table-responsive no-padding table-custom">
                        <table class="table table-hover">
                          <tbody>
                              @foreach ($allcase as $key => $value)
                                <tr>
                                  <td><img src="{{ asset('images/Image_from_Skype.png') }}" width="50px"/></td>
                                  <td>#{{$value['case_code']}}</td>
                                  <td>{{$value['clinic_name']}}</td>
                                  <td>{{$value['status']}}</td>
                                  <td>{{$value['end_date']}}</td>
                               </tr>
                                
                              @endforeach
                          </tbody>
                        </table>
                     </div>  
                  </div>
              </div>
           
           
              <div class="col-sm-6 col-md-12">
                  <div class="box color5">
                      <img src="{{ asset('/images/Layer-12_0000_Shape-78-copy.png') }}" alt="Responsive image" />
                      <span class="inner-box1">REPORTS</span> 
                      <span class="inner-box-right1">
                        <button type="button" class="btn btn-pitch">View Detail</button>
                      </span>  
                      <div class="box-body chart-responsive">
                        <div id="container"></div>
                      </div>
                  </div>
              </div>
           
          </div>
        </div>


    </section>
</div>
@endsection
