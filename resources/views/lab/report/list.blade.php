@extends('lab.layout.auth')
@section('content')
	<section class="content" id="app">
	      <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Reports</li>
          </ol>
        </div>

          <div class="col-12">
                <div class="row bs-example-report-detail">
                     <div class="col-xl-8 col-md-12 col-sm-12">
                        <span class="titleJob">Job Report</span>
                      </div>
                      <div class="col-xl-4 col-md-12 col-sm-12">
                        
                        <button class="btn btn-yellow btn-report">Filter</button>
                        <a href="#"><i class="fa fa-print print-report" aria-hidden="true"></i></a>
                      </div>
                </div>
          </div>
          <div class="col-12">
             <div class="dashboard-box">
                  <div class="row bs-example-report-detail-part">
                    <div class="col-xl-2 col-md-12 col-sm-12">
                      <span class="inner-box-lab-report">TOTAL CASES: 20</span>
                    </div>
                    <div class="col-xl-3 col-md-12 col-sm-12">
                      <span class="inner-box-lab-report">ONGOING CASES: 15</span>
                    </div>
                    <div class="col-xl-7 col-md-12 col-sm-12">
                      <select class="form-control select-option"><option selected="selected">Select Clinic</option><option>Clinic 1</option><<option>Clinic 2</option></select><label class="clinic-options">Clinic Name: </label>
                    </div>
                  </div>
              </div>
          </div>
          
          <div class="col-12">
                <div class="dashboard-box-no-margin" id="ongoingcase">
                  <div class="row bs-example-report-table">
                    <div class="box-body content-table">
                      <table class="table table-bordered">
                        <tbody>
                          <tr>
                            <th>Doctor Name</th>
                            <th>Patient Id</th>
                            <th>Cases Id</th>
                            <th>Current Status</th>
                          </tr>
                          <tr>
                            <td>Dr.James Smith</td>
                            <td>#jospau1234</td>
                            <td>#jospau1234</td>
                            <td class="bggreen">Pickup Pending</td>
                          </tr>
                          <tr>
                            <td>Dr.Andrew Culver</td>
                            <td>#jospau1234</td>
                            <td>#jospau1234</td>
                            <td class="bggray">No status</td>
                          </tr>
                           <tr>
                            <td>Dr.James Smith</td>
                            <td>#jospau1234</td>
                            <td>#jospau1234</td>
                            <td class="bggreen">Pickup Pending</td>
                          </tr>
                          <tr>
                          <td>Dr.James Smith</td>
                            <td>#jospau1234</td>
                            <td>#jospau1234</td>
                            <td class="bggray">No status</td>
                          </tr>
                           <tr>
                            <td>Dr.James Smith</td>
                            <td>#jospau1234</td>
                            <td>#jospau1234</td>
                            <td class="bggreen">Pickup Pending</td>
                          </tr>
                          <tr>
                           <td>Dr.James Smith</td>
                            <td>#jospau1234</td>
                            <td>#jospau1234</td>
                            <td class="bggray">No status</td>
                          </tr> 
                          <tr>
                           <td>Dr.James Smith</td>
                            <td>#jospau1234</td>
                            <td>#jospau1234</td>
                            <td class="bggreen">Pickup Pending</td>
                          </tr>
                          <tr>
                           <td>Dr.James Smith</td>
                            <td>#jospau1234</td>
                            <td>#jospau1234</td>
                            <td class="bggray">No status</td>
                          </tr>
                        
                        </tbody>
                      </table>
                    </div>
                    <div class="col-12 box-footer clearfix">
                      <ul class="pagination pagination-sm center-pagination pull-right">
                        <li><a href="#">Previous</a></li>
                        <li class="bordered"><a href="#">1</a></li>
                        <li class="bordered"><a href="#">2</a></li>
                        <li class="bordered active"><a href="#">3</a></li>
                        <li><a href="#">Next</a></li>
                      </ul>
                    </div>
                     
                  </div>
                </div>
          </div>

	       
        
     </section>
@endsection
