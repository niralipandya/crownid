@extends('lab.layout.auth')
@section('content')
    <div class="login-box" id="app">
        <!-- /.login-logo -->
         <div class="login-logo">
            <a href="#"><img src="{{ asset('/images/logo1.png') }}" style="height: 80px"></a>
        </div>
        <div class="login-box-body">
         <p class="login-box-msg">LAB INTERFACE</p>
         <component is='auth-login' inline-template>
         <form id="loginform" name="loginform" method="POST" enctype="multipart/form-data">
                      
                <div class="col-lg-12 mx-auto form-group loginformgrp" v-bind:class="{ 'has-error': $v.email.$error }">
                  
                  <input type="email" class="form-control loginfrm" name="email" id="email" value="" v-model="email" @input="$v.email.$touch()" placeholder="Email">
                  <span class="form-error-message" v-bind:class="{ 'help-block': $v.email.$error }" v-if="!$v.email.required">Email Field is required</span>
                </div>
                <div class="col-lg-12 mx-auto form-group loginformgrp" v-bind:class="{ 'has-error': $v.password.$error }">
                  <input type="password" class="form-control loginfrm" id="password" name="password" value="" v-model = "password"  @input="$v.password.$touch()" placeholder="Password">
                    <span class="form-error-message" v-bind:class="{ 'help-block': $v.password.$error }">Password Field is required</span>
                </div>

                <div class="col-lg-12 mx-auto form-group loginformgrp" v-bind:class="{ 'has-error': $v.cpassword.$error }">
                  <input type="password" class="form-control loginfrm" id="cpassword" name="cpassword" value="" v-model = "cpassword"  @input="$v.cpassword.$touch()" placeholder="Confirm Password">
                    <span class="form-error-message" v-bind:class="{ 'help-block': $v.password.$error }">Confirm Password Field is required</span>
                    <span class="help-block" v-if="!$v.cpassword.sameAsPassword">Passwords must be identical.</span>
                </div>

               
                <div class="col-lg-12 mx-auto">
                  <button type="submit" class="btn btn-custom3" @click="submitform">SUBMIT</button>
                </div>
                     
         </form>
         </component>
        </div>
        <!-- /.login-box-body -->
    </div>
@endsection
