@extends('lab.layout.auth')
@section('content')
	   <section class="content" id="app">
	      <div class="col-sm-12">
          <ol class="breadcrumb">
           <li class="breadcrumb-item"><a href="/lab/home">Home</a></li>
            <li class="breadcrumb-item"><a href="/lab/clinics">Clinic</a></li>
            <li class="breadcrumb-item active">{{$clinic_name}}</li>
          </ol>
        </div>

        <cliniccaselibrary clinic_id="{{$id}}" ></cliniccaselibrary>
     </section>

@endsection
@section('js')
  <script src="/js/common-lab.js"></script>
@endsection