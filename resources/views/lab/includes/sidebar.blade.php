<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">

            <li class="sidebar-li {{ in_array($current_route_name, ['lab.home', 'lab.calender'])?'active':'' }}">
                <a href="{{ url('/lab/home') }}">
                    <div class="menu-home"></div>
                    <span>HOME</span>
                </a>
            </li>

            <li class="sidebar-li {{ $current_route_name=='lab.clinics.index' || $current_route_name == 'lab.cliniccaselibrary' ?'active':'' }}">
                <a href="{{ url('lab/clinics') }}">
                    <div class="menu-clinic"></div>
                    <span>CLINIC</span>
                </a>
            </li>

            <li class="sidebar-li {{ $current_route_name=='lab.caselibrary'?'active':'' }}">
                <a href="{{ url('lab/caselibrary') }}">
                    <div class="menu-library"></div>
                    <span>CASE LIBRARY</span>
                </a>
            </li>

            <li class="sidebar-li {{ $current_route_name=='lab.chatlist'?'active':'' }}">
                <a href="{{ url('lab/chat') }}">
                    <div class="menu-chats"></div>
                    <span>CHATS
                        <span class="label label-danger display-none" id="chat_badgeCount"></span>
                    </span>
                </a>
            </li>

            <li class="sidebar-li {{ $current_route_name=='lab.delayBoxCase'?'active':'' }}">
                <a href="{{ url('lab/delayBoxCase') }}">
                    <div class="menu-delay"></div>
                    <span>DELAY BOX</span>
                </a>
            </li>

            <li class="sidebar-li {{ $current_route_name=='lab.report.index'?'active':'' }}">
                <a href="{{ url('lab/report') }}">
                    <div class="menu-reports"></div>
                    <span>REPORTS</span>
                </a>
            </li>

            <li class="treeview sidebar-li {{ in_array($current_route_name, ['lab.settings.index', 'lab.profile' , 'lab.changepasseordpage']) ? 'active' : '' }}">
                <a href="{{ url('/clinic/home') }}">
                    <div class="menu-settings"></div>
                    <span>SETTINGS</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="sidebar-li {{ $current_route_name == 'lab.settings.index' ? 'active' : '' }}">
                        <a href="{{ url('lab/settings') }}">GENERAL</a>
                    </li>
                    <li class="sidebar-li {{ $current_route_name == 'lab.profile' ? 'active' : '' }}">
                        <a href="{{ url('lab/profile') }}">profile</a>
                    </li>
                    <li class="sidebar-li {{ $current_route_name == 'lab.changepasseordpage' ? 'active' : '' }}">
                        <a href="{{ url('lab/changepassword') }}">Change Password</a>
                    </li>
                    <li class="sidebar-li ">
                        <a href="#">REFERRALS</a>
                    </li>
                </ul>
            </li>

            <!-- <li class="sidebar-li {{ $current_route_name=='lab.settings.index'?'active':'' }}">
                <a href="{{ url('lab/settings') }}">
                    <div class="menu-settings"></div>
                    <span>SETTINGS</span>
                </a>
            </li>

            <li class="sidebar-li {{ $current_route_name=='lab.profile'?'active':'' }}">
                <a href="{{ url('lab/profile') }}">
                    <div class="menu-settings"></div>
                    <span>Profile</span>
                </a>
            </li>

            <li class="sidebar-li {{ $current_route_name=='lab.changepasseordpage'?'active':'' }}">
                <a href="{{ url('lab/changepassword') }}">
                    <div class="menu-settings"></div>
                    <span>Change Password</span>
                </a>
            </li> -->



            <!--  <li class="treeview sidebar-li {{ in_array($current_route_name,['lab.settings.index','lab.settings.default'])?'active':'' }}">
      <a href="#">
        <i class="fa fa-cog fa-lg"></i> <span>SETTINGS</span>
         <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
          </span>
      </a>

      <ul class="treeview-menu">
            <li class="sidebar-li {{ $current_route_name=='lab.settings.index'?'active':'' }}">
              <a href="{{ url('lab/settings') }}">GENERAL</a>
            </li>
            <li class="sidebar-li {{ $current_route_name=='lab.settings.default'?'active':'' }}">
              <a href="#">DEFAULT</a>
            </li>
      </ul>
    </li> -->

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>