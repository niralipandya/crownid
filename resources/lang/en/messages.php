<?php 

return [
    "not-found"                  =>"Not found.",
    "internal-error"             =>"Internal server error.",
    "unauthorized-access"        =>"Unathorized access",
    "unauthorized-access-role"   =>"Unathorized role access",
    "account-inactive"           =>"Restricted! You account is inactive.",
    "parameters-fail-validation" =>"Fail to pass validation",
    
    
	"api-add"        =>"Api added successfully",
	"category-add"   =>"Category added successfully",
	
	'admin-profile-update' =>"Your profile has successfully been updated!",

    "role-add"    =>"Role added successfully",
    "role-update" =>"Role updated successfully",
    "role-distroy" =>"Role removed successfully",

    "permission-add"    =>"Permission added successfully",
    "permission-update" =>"Permission updated successfully",
    "permission-distroy" =>"Permission removed successfully",

    "admin-add"         =>"Administrator added successfully",
    "admin-update"      =>"Administrator updated successfully",
    "admin-distroy"     =>"Administrator removed successfully",
    "admin-status"      =>"Administrator's account (:status)status updated successfully",
    'admin-update-fail' =>'Fail to update the record!',


    "clinic-add"            =>"clinic added successfully",
    "clinic-update"         =>"clinic updated successfully",
    "clinic-distroy"        =>"clinic removed successfully",
    "clinic-status"         =>"Account status updated successfully",
    "clinic-verifed-status" =>"Account set as :status.",
    'clinic-update-fail'    =>'Fail to update the record!',
    'clinic-not-found'      =>'No clinic found with this ID!',
    'clinic-profile-update' =>'Your profile has successfully been updated!',

    "lab-status"         => "Account status updated successfully",
    "lab-update-fail"    => "Fail to update the record!",

    "doctor-add"            =>"Doctor added successfully",
    "doctor-update"         =>"Doctor updated successfully",
    "doctor-distroy"        =>"Doctor removed successfully",
    "doctor-status"         =>"Doctor's account status updated successfully",
    "doctor-verifed-status" =>"Doctor's account set as :status.",
    'doctor-update-fail'    =>'Fail to update the record!',
    'doctor-not-found'      =>'No Doctor found with this ID!',
    'doctor-profile-update' =>'Your profile has successfully been updated!',

    "REGISTRATION_AND_VERIFICATION" =>"Check your inbox for verification link!",
    "REGISTRATION_SUCCESS"          =>"You will receive OTP shortly to verify the account",
    "REGISTRATION_DONE"             =>"Registration done successfully",
    "REGISTRATION_FAIL"             =>"Fail to register your account!",
    "ACCOUNT_VERFIED"               =>"Your account has been verifed! Please login.",
    "LOGIN_SUCCESSFULL"             =>"Login successfully done",
    "ACCOUNT_VERFIED_FAIL"          =>"Opps, your account is not verified!",
    "NO_EMAIL_FOUND"                =>"This email is not available",
    "FORGOT_PASSWORD"               =>"Password reset instructions have been sent to the email address entered during registration",
    "RESET_PASSWORD"                =>"Password reset successfully",
    "NO_MATCH_FOUND"                =>"Email OR Password does not match",
    "NO_DATA_FOUND"                 =>"No Data Found",

    "currency-add"     =>"Currency added successfully",
    "currency-update"  =>"Currency updated successfully",
    "currency-distroy" =>"Currency removed successfully",

    "currency-failTo_fetch"               =>"Opp, Fail to fetch currency while cheching for conversion",
    "currency-conversion-add"             =>"Currency conversion (:from -> :to) added successfully",
    "currency-conversion-update"          =>"Currency conversion (:from -> :to) updated successfully",
    "currency-conversion-distroy"         =>"Currency conversion (:from -> :to) removed successfully",
    'toCurrency-not-available'            =>"No curruncy avaialble for :name(currency) to transform.You might already added conversion for :name(currency)",
    "currency-conversion-fail"            =>"Currency conversion fail due to unavaibility of either currency.",
    "currency-conversion-updaterate-fail" =>"Fail to update rate for :from to :to.",


    "page-add"            =>"Page added successfully",
    "page-update"         =>"Page updated successfully",
    "page-distroy"        =>"Page removed successfully",
    "page-status"         =>"Page's account status updated successfully",
    "page-verifed-status" =>"Page's account status updated successfully",
    'page-update-fail'    =>'Fail to update the record!',
    'page-not-found'      =>'No page found with this ID!',



    "category-add"            =>"Category added successfully",
    "category-update"         =>"Category updated successfully",
    "category-distroy"        =>"Category removed successfully",
    "category-status"         =>"Category status updated successfully",
    "category-verifed-status" =>"Category status updated successfully",
    'category-update-fail'    =>'Fail to update the record!',
    'category-not-found'      =>'No page found with this ID!',
    'category-subcategory'      =>'This category has one or more subcategories, Please delete subcategories first befor deleting this category!',

    'contactus-message'      =>'Thank you for contacting us, We will get back to you soon!',

    "CURR_NOT_EXISTS"=> "Currency :name is not available for transaction!",
    "CONVERSION_NOT_AVAIALBLE"=> "Conversion (:from -> :to) is not avaialble.",
    "TRANSACTION_NOT_FOUND"=> "Transaction reference not found!",
    "TRANSACTION_SUCCESS"=> "Your transaction is completed successfully",

    "company-add"            =>"Company added successfully",
    "company-update"         =>"Company updated successfully",
    "company-distroy"        =>"Company removed successfully",
    "company-status"         =>"Company's status updated successfully",
    "company-verifed-status" =>"Company's account set as :status.",
    'company-update-fail'    =>'Fail to update the record!',
    'company-not-found'      =>'No company found with this ID!',

    
    "car-add"            =>"Car added successfully",
    "car-update"         =>"Car updated successfully",
    "car-distroy"        =>"Car removed successfully",
    "car-status"         =>"Car's status updated successfully",
    "car-verifed-status" =>"Car's account set as :status.",
    'car-update-fail'    =>'Fail to update the record!',
    'car-not-found'      =>'No car found with this ID!',

];