<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriptionhistory extends Model
{
    protected $table = 'subscriptionhistory';
    protected $fillable = [
        'user_id', 'user_type', 'plan', 'type', 'amount', 'currency', 'bill_date', 'payment_date', 'next_bill_date', 'next_token_date', 'paid_from', 'receipt', 'payment_id',
    ];
}
