<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referencecode extends Model
{
    protected $table = 'referencecode';
    protected $fillable = [
        "name","email","phonenumber",'code','used','for','sendBy','sendByType'
    ];
}
