<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClinicDetail extends Model
{
    protected $table = 'clinic_details';
    
	/**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    public $timestamps = false;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public static function boot()
    {
    	parent::boot();
        //Clinic::observe(ClinicObserver::class);
    }

    // Relationship with table
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
