<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctortokenhistory extends Model
{
    protected $table = 'doctortokenhistory';
    protected $fillable = [
        'id', 'user_id', 'amount', 'currency', 'remark', 'message', 'doctor_count', 'user_type', 'type', 'paid_from', 'payment_id', 'created_at', 'updated_at'
    ];
}
