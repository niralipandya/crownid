<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfLab
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = 'lab')
	{
		//mprd($request->user);
	    if (Auth::guard($guard)->check()) {
	    	//mprd(Auth::guard($guard)->user());
	    	 if (Auth::guard($guard)->user()->hasRole('Lab')){

	        	return redirect('lab/home');
	    	 }else{

	    	 }
	    }

	    return $next($request);
	}
}