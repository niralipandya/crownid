<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role)
    {
      /*  mpr($request->password);
        mprd($request->email);*/

        $users = User::where('username',$request->username)->with('roles')->first()->toArray();
        //mprd($users);
        if($users['roles'][0]['name']==$role){
            return $next($request);
        }else{
            return response([
                "message"        => 'You are not authorized to use this service',
                "status_code"    => 401
            ],401);
        }  

        
    }
}
