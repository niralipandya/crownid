<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotLab
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = 'lab')
	{
	    if (!Auth::guard($guard)->check()) {
	    	//mprd(2);
	        return redirect('lab/login');
	    }

	    return $next($request);
	}
}