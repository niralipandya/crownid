<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Pages;
use App\Http\Controllers\Controller;

class PagesController extends ApiController
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function show($id)
    {
    	$user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);

    	$name = 'title_'.$user->language.' as title , description_'.$user->language.' as detail' ;
        $sql = "SELECT id , $name FROM pages WHERE id = $id";
		$pages = collect( \DB::select($sql))->first();
       
        if($pages)
        {
            return $this->respond([
                "data" => [
                    "cases" => $pages,
                ],
                "message" => trans('api.pages'),
                "status" => $this->getStatuscode(),
            ]);
        }
    }
}
