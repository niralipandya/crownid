<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications;
use App\Transformers\NotificationTransformer;

class NotificationController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:api');
        
        $this->notificationTransformer = new NotificationTransformer;
    }

    public function index(Request $request)
    {
        $clinic = auth()->user();

        $validate= validate_parameter($request->all(),['page']);

        if(!$validate){
           return $this->respondValidationError(trans('api.insufficientdata'));
        }
   
        $language = $clinic->language != '' ? $clinic->language : 'en';
        app()->setLocale($language);

        $filedname  = "message_".$language;
        
        $list = Notifications::where('user_id',$clinic->id)
        ->select('user_id','case_id','case_code','push_type',"$filedname as message",'id','sender_type','reciver_type','sender_id','send_from','created_at')->orderBy('id','desc')
        ->paginate(20);
        #mprd($list->toArray());
        $paginator=[
                'total_count'  => $list->total(),
                'total_pages'  => $list->lastPage(),
                'current_page' => $list->currentPage(),
                'limit'        => $list->perPage()
            ];

        $clinic->badge_count = 0;
        $clinic->save();

        return $this->respond([
            "data" => [
                "list" => $this->notificationTransformer->transformCollection($list->all()),
                "paginator" => $paginator
            ],
            "message" => trans('api.notification_list'),
            "status" => $this->getStatuscode(),
        ]);
    }

      public function getBadgeCount(){ 
        $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);

        $badge_count = $user->badge_count;
        
        return $this->respond([
                    "data"    => [
                        "badge_count" => $badge_count,
                    ],
                    "message" => 'badge_count',
                    "status"  => $this->getStatuscode(),
                ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

   
}
