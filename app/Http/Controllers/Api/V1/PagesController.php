<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Pages;
use App\Subscription;
use App\Http\Controllers\Controller;
use App\Traits\Subscriptiontrait;
class PagesController extends ApiController
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    use Subscriptiontrait;

    public function __construct()
    {
        
    }

    public function show($id)
    {
    	$user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);

    	$name = 'title_'.$user->language.' as title , description_'.$user->language.' as detail' ;
        $sql = "SELECT id , $name FROM pages WHERE id = $id";
		$pages = collect( \DB::select($sql))->first();
       
        if($pages)
        {
            return $this->respond([
                "data" => [
                    "cases" => $pages,
                ],
                "message" => trans('api.pages'),
                "status" => $this->getStatuscode(),
            ]);
        }
    }

    public function verifyReceipt_first(Request $request){
       # mprd($request->all());
        $arr = array(
            'receipt-data'  => $request->receipt,
            'password'      => "7df9570b7d954500aeb28082f0f0148d" );

        $data_string = json_encode($arr);
        $ch = curl_init('https://sandbox.itunes.apple.com/verifyReceipt');        
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                      
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                          
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                               
            'Content-Type: application/json',      
            'Content-Length: ' . strlen($data_string))                  
        );
        $result = curl_exec($ch);
        $data = json_decode($result);
        #mprd($data);
        $receiptArray = (array)$data->latest_receipt_info;
        $total_count = count($receiptArray) ;
        
        $latest_receipt = $receiptArray[$total_count - 1];
        $pending_Array = (array)$data->pending_renewal_info;

        $returnData['transaction_id']           = @$latest_receipt->transaction_id;
        $returnData['original_transaction_id']  = @$latest_receipt->original_transaction_id;
        $returnData['is_trial_period']          = @$latest_receipt->is_trial_period;
        $returnData['expires_date']             = @$latest_receipt->expires_date;
        $returnData['product_id']               = @$latest_receipt->product_id;
        #mprd($latest_receipt);

        return $returnData;

        /*stdClass Object
(
    [quantity] => 1
    [product_id] => premium.annual.CrownID.com
    [transaction_id] => 1000000410323482
    [original_transaction_id] => 1000000410323368
    [purchase_date] => 2018-06-23 04:54:50 Etc/GMT
    [purchase_date_ms] => 1529729690000
    [purchase_date_pst] => 2018-06-22 21:54:50 America/Los_Angeles
    [original_purchase_date] => 2018-06-23 04:49:50 Etc/GMT
    [original_purchase_date_ms] => 1529729390000
    [original_purchase_date_pst] => 2018-06-22 21:49:50 America/Los_Angeles
    [expires_date] => 2018-06-23 05:54:50 Etc/GMT
    [expires_date_ms] => 1529733290000
    [expires_date_pst] => 2018-06-22 22:54:50 America/Los_Angeles
    [web_order_line_item_id] => 1000000039225318
    [is_trial_period] => false
    [is_in_intro_offer_period] => false
)*/
    }

    public function verifyReceipt(Request $request){
       # mprd($request->all());


        $currentDate   = date("Y-m-d H:i:00");
        $subscription  = Subscription::where('expires_date','<',$currentDate)->get();
        if($subscription){
           foreach ($subscription->toArray() as $key => $value) { 
               $expires_date       = $value['expires_date'];
               $is_trial_period    = $value['is_trial_period'];

               $arr = array(
                    'receipt-data'  => $value['receipt'],
                    'password'      => "7df9570b7d954500aeb28082f0f0148d" );
               }

               $data_string = json_encode($arr);
                $ch = curl_init('https://sandbox.itunes.apple.com/verifyReceipt');        
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                      
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                  
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                          
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(                               
                    'Content-Type: application/json',      
                    'Content-Length: ' . strlen($data_string))                  
                );
                $result = curl_exec($ch);
                $data   = json_decode($result); 
                if($data->status != 0){
                    return 0;
                }
                $receiptArray = (array)$data->latest_receipt_info;
                $pending_Array = (array)$data->pending_renewal_info;
                $subscriptionUpdate       = Subscription::where('id',$value['id'])->first();
                $subscriptionUpdate->auto_renew_status        = @$pending_Array[0]->auto_renew_status;
                foreach ($receiptArray as $key => $value) {
                   $purchase_date = date("Y-m-d H:i:00", strtotime($value->purchase_date));
                   $current_expires_date = date("Y-m-d H:i:00", strtotime($value->expires_date));
                   #mprd($is_trial_period);
                   if($purchase_date == $expires_date){

                        if($is_trial_period == 'true'){ 
                             $subscriptionUpdate->payment_date   =    date("Y-m-d H:i:s", strtotime(@$value->purchase_date) );
                        }

                        $returnData['purchase_date']            = @$value->purchase_date;
                        $returnData['transaction_id']           = @$value->transaction_id;
                        $returnData['original_transaction_id']  = @$value->original_transaction_id;
                        $returnData['is_trial_period']          = @$value->is_trial_period;
                        $returnData['expires_date']             = @$value->expires_date;
                        $returnData['product_id']               = @$value->product_id;
                        
                        $subscriptionUpdate->expires_date = date("Y-m-d H:i:s", strtotime(@$value->expires_date) );
                        $subscriptionUpdate->transaction_id = $returnData['transaction_id'] ;
                        $subscriptionUpdate->is_trial_period  = @$value->is_trial_period;
                   }
                }
                $subscriptionUpdate->save();
                mpr(date("Y-m-d H:i:s"));mprd($data);

        }

        mprd($currentDate);


        $subscription       = Subscription::where('user_id',173)->first();
        
        if($subscription){
            $expires_date       = $subscription->expires_date;
            $is_trial_period    = $subscription->is_trial_period; #mprd($is_trial_period);
          // $getData = $this->verifyReceiptCron($subscription->receipt,$subscription->expires_date);
           $arr = array(
            'receipt-data'  => $subscription->receipt,
            'password'      => "7df9570b7d954500aeb28082f0f0148d" );

                $data_string = json_encode($arr);
                $ch = curl_init('https://sandbox.itunes.apple.com/verifyReceipt');        
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                      
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                  
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                          
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(                               
                    'Content-Type: application/json',      
                    'Content-Length: ' . strlen($data_string))                  
                );
                $result = curl_exec($ch);
                $data   = json_decode($result); 
                if($data->status != 0){
                    return 0;
                }
                $receiptArray = (array)$data->latest_receipt_info;
                $pending_Array = (array)$data->pending_renewal_info;
                
                $subscription->auto_renew_status        = @$pending_Array[0]->auto_renew_status;
                
                foreach ($receiptArray as $key => $value) {
                   $purchase_date = date("Y-m-d H:i:00", strtotime($value->purchase_date));
                   $current_expires_date = date("Y-m-d H:i:00", strtotime($value->expires_date));
                   #mprd($is_trial_period);
                   if($purchase_date == $expires_date){

                        if($is_trial_period == 'true'){ mprd(45);
                             $subscription->payment_date   =    date("Y-m-d H:i:s", strtotime(@$value->purchase_date) );
                        }

                        $returnData['purchase_date']            = @$value->purchase_date;
                        $returnData['transaction_id']           = @$value->transaction_id;
                        $returnData['original_transaction_id']  = @$value->original_transaction_id;
                        $returnData['is_trial_period']          = @$value->is_trial_period;
                        $returnData['expires_date']             = @$value->expires_date;
                        $returnData['product_id']               = @$value->product_id;
                        
                        $subscription->expires_date = date("Y-m-d H:i:s", strtotime(@$value->expires_date) );
                        $subscription->transaction_id = $returnData['transaction_id'] ;
                        $subscription->is_trial_period  = @$value->is_trial_period;
                   }
                }
                $subscription->save();
                mpr(date("Y-m-d H:i:s"));mprd($data);
        }
        else{
            return 'not found';
        }

    }
}
