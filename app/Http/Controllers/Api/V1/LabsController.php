<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Transformers\Api\UsersTransformer;
use App\Transformers\Api\LabsTransformer;
use App\Transformers\Api\DoctorTransformer;
use App\User;
use App\ClinicDetail;
use App\Referencecode;
use App\Traits\FileManipulationTrait;
use App\Http\Controllers\Controller;

use App\Mail\LabReferenceLink;
use Illuminate\Support\Facades\Mail;

class LabsController extends ApiController
{
    protected $usersTransformer;
    use FileManipulationTrait;
    
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->usersTransformer  = new UsersTransformer;
        $this->labsTransformer   = new LabsTransformer;
        $this->doctorTransformer = new DoctorTransformer;
    }

    public function generateLabLink(Request $request){ 

        $login_user = auth()->user();
        $language = $login_user->language != '' ? $login_user->language : 'en';
        app()->setLocale($language);
        $validate= validate_parameter($request->all(),['clinic_id','name','email','phonenumber']);

        if(!$validate){
           return $this->respondValidationError(trans('api.insufficientdata',[
                    'first_name'=>$login_user->first_name
                    ]));
        }
        extract($request->all());
        $code = rand(111111, 999999);
        $clinic =  ClinicDetail::where('user_id', $request->clinic_id)->first();
        $user =  (object)[
        'name' => $name,
        'clinicname' => $clinic->clinic_name,
        'code' => $code,
        'email' => $email,
        'phonenumber' => $phonenumber
        ];

        Mail::to($email)->send(new LabReferenceLink($user));

        $referencecode = Referencecode::create([
                "name"         => $request->name,
                "email"        => $request->email,
                "phonenumber"  => $request->phonenumber,
                "email"        => $request->email,
                "code"         => $code,
                "for"          => 'lab',
                "sendBy"       => $request->clinic_id,
                "sendByType"   => 'clinic',
            ]);
            
            return $this->respond([
                "message" => trans('api.link_share'),
                "status"  => $this->getStatuscode(),
                
            ]);
    }


    /**
     * Display a listing of the resource. (Get all labs from clinic id)
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        

        $login = auth()->user();
        $language = $login->language != '' ? $login->language : 'en';
        app()->setLocale($language);

        $labs =\DB::table('clinic_labs')
        ->select('clinic_labs.*','users.*')
        ->where('clinic_labs.clinic_id','=',$request->clinic_id)
        ->leftJoin('users', 'users.id', '=', 'clinic_labs.lab_id')
        ->orderBy('created_at', 'desc')
        ->paginate(10);

        $paginator =[
                'total_count'  =>$labs->total(),
                'total_pages'  => $labs->lastPage(),
                'current_page' => $labs->currentPage(),
                'limit'        => $labs->perPage()
            ];

        if($labs)
        {
            return $this->respond([
                "data" => [
                    "labs" => $this->labsTransformer->transformCollection($labs->all()),
                    "paginator" => $paginator
                ],
                "message" => trans('api.lab_list'),
                "status" => $this->getStatuscode(),
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $login = auth()->user();
        $language = $login->language != '' ? $login->language : 'en';
        app()->setLocale($language);
        $search = $request->keyword;
        $labs =\DB::table('clinic_labs')
        ->select('clinic_labs.*','users.*')
        ->where('clinic_labs.clinic_id','=',$request->clinic_id)
        ->where('users.full_name','like',"%$search%")
        ->leftJoin('users', 'users.id', '=', 'clinic_labs.lab_id')
        ->orderBy('created_at', 'desc')
        ->paginate(10);
           
        if($labs)
        {
            $paginator =[
                'total_count'  =>$labs->total(),
                'total_pages'  => $labs->lastPage(),
                'current_page' => $labs->currentPage(),
                'limit'        => $labs->perPage()
            ];
            return $this->respond([
                "data" => [
                    "labs" => $this->labsTransformer->transformCollection($labs->all()) ,
                    "paginator" => $paginator
                ],
                "message" => trans('api.search_data'),
                "status" => $this->getStatuscode(),
            ]);
        }
        else{
            return $this->respond([
                "message" => trans('api.noData'),
                "status" => 500,
            ]);
        }
    }

    /**
     * Display a record.
     *
     * @return \Illuminate\Http\Response
     */
    public function doctors()
    {
        $clinic = auth()->user();
        $language = $clinic->language != '' ? $clinic->language : 'en';
        app()->setLocale($language);
        $doctor =\DB::table('clinic_doctor')
        ->select('clinic_doctor.*','users.*','clinic_doctor.status as doctorstatus')
        ->where('clinic_doctor.clinic_id','=',$clinic->id)
        ->where('clinic_doctor.status','=','1')
        ->leftJoin('users', 'users.id', '=', 'clinic_doctor.doctor_id')
        ->orderBy('created_at', 'desc')
        ->paginate(10);
           
        if($doctor->all())
        {
            $paginator =[
                'total_count'  =>$doctor->total(),
                'total_pages'  => $doctor->lastPage(),
                'current_page' => $doctor->currentPage(),
                'limit'        => $doctor->perPage()
            ];
            return $this->respond([
                "data" => [
                    "doctor" => $this->doctorTransformer->transformCollection($doctor->all()) ,
                    "paginator" => $paginator
                ],
                "message" => trans('api.doctor_list'),
                "status" => $this->getStatuscode(),
            ]);
        }
        else{
            return $this->respond([
                "message" => trans('api.noData'),
                "status" => 500,
            ]);
        }
        
    }

     public function doctorSearch(Request $request)
    {
        $login = auth()->user();
        $language = $login->language != '' ? $login->language : 'en';
        app()->setLocale($language);

        $search = $request->keyword;
        $doctor =\DB::table('clinic_doctor')
        ->select('clinic_doctor.*','users.*')
        ->where('clinic_doctor.clinic_id','=',$request->clinic_id)
        ->where('users.first_name','like',"%$search%")
        ->OrWhere('users.last_name','like',"%$search%")
        ->leftJoin('users', 'users.id', '=', 'clinic_doctor.doctor_id')
        ->orderBy('created_at', 'desc')
        ->paginate(10);
           
        if($doctor)
        {
            $paginator =[
                'total_count'  =>$doctor->total(),
                'total_pages'  => $doctor->lastPage(),
                'current_page' => $doctor->currentPage(),
                'limit'        => $doctor->perPage()
            ];
            return $this->respond([
                "data" => [
                    "doctor" => $this->doctorTransformer->transformCollection($doctor->all()) ,
                    "paginator" => $paginator
                ],
                "message" => trans('api.search_data'),
                "status" => $this->getStatuscode(),
            ]);
        }
        else{
            return $this->respond([
                "message" => trans('api.noData'),
                "status" => 500,
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $clinic = auth()->user();
        $language = $clinic->language != '' ? $clinic->language : 'en';
        app()->setLocale($language);

        $validate= validate_parameter($request->all(),['full_name','contact_name','email','contact_number','clinic_id']);
        if(!$validate){
           return $this->respondValidationError(trans('api.insufficientdata',[
                    'first_name'=>$clinic->first_name
                    ]));
        }

        // check email is registered or not
        $exist = User::where('email', $request->email)->first();

        if($exist)
        {
            return $this->respond([
                    "message" => trans('api.email_registered_login'),
                    "status"  => 500,
                ]);
            
        }

        $clinic =  User::where('id', $request->clinic_id)->first();
        $user = User::create([
            "full_name"         => $request->full_name,
            "first_name"        => $request->contact_name,
            "email"             => $request->email,
            "contact_number"    => $request->contact_number
        ]);
        if ($request->hasFile('image')) {
            // save image in local
            $image = $this->quickUpload($request->image, USER_PROFILE_PATH);
            // transfer image to s3
            $this->transferFileToS3($image);
            // delete image from local
            unlink(storage_path('app/public/'.$image));

            // save image in database
            $user->image = $image;
            $user->save();
        }
        $user->assignRole('Lab');
        $clinic->givePermissionToLab($user);
        return $this->respond([
                    "data"    => [
                        "detail" => $this->labsTransformer->transform($user) ,
                    ],
                    "message" => trans('api.lab_created'),
                    "status"  => $this->getStatuscode(),
                ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
