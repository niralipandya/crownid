<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Plan;
use App\Clinicplan;
use App\Credittype;
use App\Traits\FcmTrait;
use App\Cases;
use App\User;

class PlanController extends ApiController
{
    use FcmTrait;

    public function __construct()
    {
        $this->middleware('auth:api', [
            'except' => [ 'creditview', 'planview','chatNotification']
        ]);
    }

    function chatNotification(Request $request){
        
         $notificationData = file_get_contents("php://input"); 
         parse_str($notificationData, $notificationData);
        
       $notificationData =   array(
        'receiverId' => 0,
    'messageText' => 'Hhhhb',
    'groupID' => 'JACVLABA0172',
    'senderId' => 'undefined',
    'memberID' => '136,145,134',
    'messageType' => 'text',
    'senderName' => 'Jack',
    );


         $case_code         = $notificationData['groupID'];
         $message           = $notificationData['messageText'];
         
         $cases             = Cases::where('case_code',$case_code)->first();
         $cases_id          = $cases->id;

         if($notificationData['messageType'] == 'image'){
            $message = 'Image';
         }

         if($notificationData['messageType'] == 'video'){
            $message = 'Video';
         }

         $memberID  = $notificationData['memberID']; 
         if($memberID != ''){
            $memberList = explode(',', $memberID); 
            foreach ($memberList as $key => $value) {
                $userData       = User::where('id',$value)->first();
                if($userData){
                    $to             = $userData->device_token; 
                    if($to != ''){
                        $device_type    = $userData->device_type;
                        $data           = [
                            'type' => 'chat_message',
                            'case_code' => $case_code ,
                            'case_id' => $cases_id,
                            "body" => $message,
                            'title' =>  'CrownId'
                            ];
                        $notification   = [ "body" => $message ];
                        $this->notify($to, $notification, $data ,0,$device_type, $isSilent = false);
                    }                    
                }            
            }
         }
         exit;

    }

   function creditview(){
        $credit = Credittype::get();
        $creditArray = $credit->toArray();

        return $this->respond([
                "data" => [
                    "creditArray" => $creditArray,
                ],
                "message" => trans('api.creditview'),
                "status" => $this->getStatuscode(),
            ]);

        $i=0;
        foreach ($creditArray as $key => $value) {
            $returnData[$i] = $value;
            $returnData[$i]['text'] = $value;
            $i++;
        }
   }

   function planview(){
        $plan = Clinicplan::get();
        $planArray = $plan->toArray();

        return $this->respond([
                "data" => [
                    "planArray" => $planArray,
                ],
                "message" => trans('api.planview'),
                "status" => $this->getStatuscode(),
            ]);
        
        $i=0;
        foreach ($planArray as $key => $value) {
            $returnData[$i] = $value;
            $returnData[$i]['text'] = $value;
            $i++;
        }
   }


   function tokensummary(){
        $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);

        $tokensummary = [
            'case_token_free'       => $user->case_token_free,
            'case_token_purchased'  => $user->case_token_purchased,
            'case_token_bylab'      => $user->case_token_bylab,
            'case_token_total'      => $user->case_token_total,
            'case_token_used'       => $user->case_token_used,
            'case_token_pending'    => $user->case_token_total - $user->case_token_used
        ];

        $doctortokensummary = [
            'doctor_token_free'       => $user->doctor_token_free,
            'doctor_token_purchased'  => $user->doctor_token_purchased,
            'doctor_token_total'      => $user->doctor_token_total,
            'doctor_token_used'       => $user->doctor_token_used,
            'doctor_token_pending'    => $user->doctor_token_total - $user->doctor_token_used
        ];

        return $this->respond([
                "data" => [
                    'case_token_pending'    => $user->case_token_total - $user->case_token_used,
                    'doctor_token_pending'  => $user->doctor_token_total - $user->doctor_token_used,
                    "doctortokensummary"    => $doctortokensummary,
                    "tokensummary"          => $tokensummary,
                    "extratoken_amount"     => 10,
                    "extratoken_count"      => 2
                ],
                "message" => trans('api.creditview'),
                "status" => $this->getStatuscode(),
            ]);
   }
}
