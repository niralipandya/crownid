<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Cases;
use App\Delaybox;
use App\ClinicDetail;
use App\User;
use App\Image;
use App\Http\Controllers\Controller;
use App\Traits\FileManipulationTrait;
use App\Traits\CaseLogT;
use App\Transformers\Api\CasesTransformer;
use App\Transformers\Api\DelaycasesTransformer;
use App\Mail\SendToLab;
use Illuminate\Support\Facades\Mail;
use App\Traits\Purchasetrait;

class CasesController extends ApiController
{
    use FileManipulationTrait,CaseLogT,Purchasetrait;

    protected $casesTransformer;
    protected $delaycasesTransformer;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->casesTransformer = new CasesTransformer;
        $this->delaycasesTransformer =  new DelaycasesTransformer;
    }

    public function scan($id)
    {
        $clinic = auth()->user();
        $language = $clinic->language != '' ? $clinic->language : 'en';
        app()->setLocale($language);
        $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
                ->with('images')
                ->where('cases.bar_code', $id)
                ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
                ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
                ->orderBy('id','desc')
                ->first();
       
        if($cases)
        {
            if($cases->created_by_id != $clinic->id){
                return $this->respond([
                  "message" => trans('api.invalidcaseidotherclinic'),
                  "status" => 500,
              ]);
            }

            return $this->respond([
                "data" => [
                    "cases" => $this->casesTransformer->transform($cases),
                ],
                "message" => trans('api.scan_sucess'),
                "status" => $this->getStatuscode(),
            ]);
        }
        else{
          return $this->respond([
                "message" => trans('api.scan_fail'),
                "status" => 500,
            ]);
        }
    }
    
      public function sendToLab(Request $request){ 

        $login_user = auth()->user();
        $language = $login_user->language != '' ? $login_user->language : 'en';
        app()->setLocale($language);
        $validate= validate_parameter($request->all(),['case_id','clinic_id','case_code','lab_id']);

        if(!$validate){
           return $this->respondValidationError(trans('api.insufficientdata',[
                    'first_name'=>$login_user->first_name
                    ]));
        }
        extract($request->all());

        $clinic =  ClinicDetail::where('user_id', $request->clinic_id)->first();
        $lab =  User::where('id', $request->lab_id)->first();
        /*if($clinic)
        {
          return $this->respondValidationError('Invalid clinic');
        }
        
        if($lab)
        {
          return $this->respondValidationError('Invalid lab');
        }*/
        $user =  (object)['name' => $lab->full_name,'case_code' => $case_code,'clinicname' => $clinic->clinic_name];
        Mail::to($lab->email)->send(new SendToLab($user));

        return $this->respond([
            "message" => trans('api.send_lab'),
            "status"  => $this->getStatuscode(),
            
        ]);
    }

    public function index(Request $request)
    { 

        $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);
        $item = User::find($user->id);
        $userRole = $item->roles()->first();
        $login_user_role = $userRole->name;
        
        extract($request->all());

        if($login_user_role == 'Doctor'){
          $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
                ->with('images')
                ->where('clinic_id', $request->clinic_id)
                ->where('doctor_id', $user->id)
                ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
                ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
                ->orderBy('id','desc')->paginate(15);
        }
        else{
            $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
                ->with('images')
                ->where('clinic_id', $request->clinic_id)
                ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
                ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
                ->orderBy('id','desc')->paginate(15);
        }


        
        $paginator =[
                'total_count'  =>$cases->total(),
                'total_pages'  => $cases->lastPage(),
                'current_page' => $cases->currentPage(),
                'limit'        => $cases->perPage()
            ];
        if($cases)
        {
            return $this->respond([
                "data" => [
                    "cases" => $this->casesTransformer->transformCollection($cases->all()),
                    "paginator" => $paginator
                ],
                "message" => trans('api.case_list'),
                "status" => $this->getStatuscode(),
            ]);
        }
        else{
          return $this->respond([
                "message" => trans('api.no_Data'),
                "status" => 500,
            ]);
        }
    }


    public function doctorCaseList(Request $request)
    { 

        $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);
        $item = User::find($user->id);
        $userRole = $item->roles()->first();
        $login_user_role = $userRole->name;
        
        extract($request->all());

        $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
                ->with('images')
                ->where('clinic_id', $request->clinic_id)
                ->where('doctor_id', $request->doctor_id)
                ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
                ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
                ->orderBy('id','desc')->paginate(15);


        
        $paginator =[
                'total_count'  =>$cases->total(),
                'total_pages'  => $cases->lastPage(),
                'current_page' => $cases->currentPage(),
                'limit'        => $cases->perPage()
            ];
        if($cases)
        {
            return $this->respond([
                "data" => [
                    "cases" => $this->casesTransformer->transformCollection($cases->all()),
                    "paginator" => $paginator
                ],
                "message" => trans('api.case_list'),
                "status" => $this->getStatuscode(),
            ]);
        }
        else{
          return $this->respond([
                "message" => trans('api.no_Data'),
                "status" => 500,
            ]);
        }
    }

    public function delayBoxCase(Request $request)
    { 

        $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);
        $item = User::find($user->id);
        $userRole = $item->roles()->first();
        $login_user_role = $userRole->name;
        
        extract($request->all());

        if($login_user_role == 'Doctor'){
          $cases = Delaybox::select('c.*','delayboxes.due_date','delayboxes.expeted_date','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
                //->where('delayboxes.clinic_id', $request->clinic_id)
                ->where('delayboxes.doctor_id', $user->id)
                ->leftJoin('cases as c', 'c.case_code', '=', 'delayboxes.case_code')
                ->leftJoin('users as d', 'c.doctor_id', '=', 'd.id')
                ->leftJoin('users as l', 'c.lab_id', '=', 'l.id')
                ->orderBy('id','desc')->paginate(15);
        }
        else{
            $cases = Delaybox::select('c.*','delayboxes.due_date','delayboxes.expeted_date','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
                ->where('delayboxes.clinic_id', $user->id)
                ->leftJoin('cases as c', 'c.case_code', '=', 'delayboxes.case_code')
                ->leftJoin('users as d', 'c.doctor_id', '=', 'd.id')
                ->leftJoin('users as l', 'c.lab_id', '=', 'l.id')
                ->orderBy('id','desc')->paginate(15);
        }

        #mprd($cases->toArray());
        
        $paginator =[
                'total_count'  => $cases->total(),
                'total_pages'  => $cases->lastPage(),
                'current_page' => $cases->currentPage(),
                'limit'        => $cases->perPage()
            ];
        if($cases)
        {
            return $this->respond([
                "data" => [
                    "cases" => $this->delaycasesTransformer->transformCollection($cases->all()),
                    "paginator" => $paginator
                ],
                "message" => trans('api.case_list'),
                "status" => $this->getStatuscode(),
            ]);
        }
        else{
          return $this->respond([
                "message" => trans('api.no_Data'),
                "status" => 500,
            ]);
        }
    }

     public function search(Request $request)
    { 
      $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);
        #mprd($request->all());
        extract($request->all());
        
        $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
                ->where('clinic_id', $request->clinic_id)
                ->where(function ($query) use($keyword){
                    $query->where('cases.first_name','like',"$keyword%")
                          ->orWhere('cases.last_name','like',"$keyword%")
                          ->orWhere('cases.bar_code','like',"$keyword%")
                          ->orWhere('cases.case_code','like',"$keyword%")
                          ->orWhere('l.full_name','like',"$keyword%")
                          ->orWhere('d.first_name','like',"$keyword%")
                          ->orWhere('d.full_name','like',"$keyword%")
                          ->orWhere('d.last_name','like',"$keyword%");
                })
                ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
                ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
                ->orderBy('id','desc')->paginate(15 );

        $paginator =[
                'total_count'  =>$cases->total(),
                'total_pages'  => $cases->lastPage(),
                'current_page' => $cases->currentPage(),
                'limit'        => $cases->perPage()
            ];
        if($cases)
        {
            return $this->respond([
                "data" => [
                    "cases" => $this->casesTransformer->transformCollection($cases->all()),
                    "paginator" => $paginator
                ],
                "message" => trans('api.search_data'),
                "status" => $this->getStatuscode(),
            ]);
        }
        else{
          return $this->respond([
                "message" => trans('api.noData'),
                "status" => 500,
            ]);
        }
    }

    
    public function pageDetail(Request $request)
    {

       $user = auth()->user();
       $language = $user->language != '' ? $user->language : 'en';
       app()->setLocale($language);
       $name = $user->language.'_name';

       $select = " id , $name as name ";
      
       $cases = Cases::find($request->id);
       if(empty($cases)){
           $prosthetic_margins = $user->prosthetic_margins;
           $preferred_metal = $user->preferred_metal;
           $delivery  = $user->delivery ;
           $bite_registration = 0;
           $color = 0;
           $color_body = 0;
           $color_neck = 0 ;
           $material = 0;
           $prosthesis = 0;
           $ceramic = 0;
       }
       else{
           $prosthetic_margins = $cases->prosthetic_margins;
           $preferred_metal = $cases->metal_ceramic_id;
           $delivery  = $cases->delivery_id;
           $bite_registration = $cases->bite_registration_id;
           $color = $cases->color_id;
           $color_body = $cases->color_body_id;
           $color_neck = $cases->color_neck_id ;
           $material = $cases->material_id;
           $prosthesis = $cases->prosthesis_id;
           $ceramic = $cases->ceramic_id;
       }
       
       $case_bite_registration = \DB::select("SELECT  $select , IF( id =  '$bite_registration',  'true',  'false' ) AS is_selected FROM case_bite_registration");
           
           $case_color = \DB::select("SELECT $select , IF( id in($color),  'true',  'false' ) AS is_selected FROM case_color");
           
           $case_color_body_option = \DB::select("SELECT $select , IF( id in  ($color_body),  'true',  'false' ) AS is_selected  FROM case_color_body_option");
           
           $case_color_neck_option = \DB::select("SELECT $select , IF( id in  ($color_neck),  'true',  'false' ) AS is_selected  FROM case_color_neck_option");
           
           $case_delivery = \DB::select("SELECT $select , IF( id in  ($delivery),  'true',  'false' ) AS is_selected FROM case_delivery ");
           
           $case_material = \DB::select("SELECT $select , IF( id =  '$material',  'true',  'false' ) AS is_selected FROM case_material");
           
           $case_prosthesis = \DB::select("SELECT $select, IF( id =  '$prosthesis',  'true',  'false' ) AS is_selected FROM case_prosthesis");
           
           $case_metal_ceramic = \DB::select("SELECT $select , IF( id =  '$preferred_metal',  'true',  'false' ) AS is_selected FROM case_metal_ceramic");
           
           $case_prosthetic_margins = \DB::select("SELECT $select , IF( id =  '$prosthetic_margins',  'true',  'false' ) AS is_selected FROM case_prosthetic_margins");
           
           $case_ceramic = \DB::select("SELECT $select , IF( id =  '$ceramic',  'true',  'false' ) AS is_selected FROM case_ceramic"); 

       $returnData['bite_registration'] =  $case_bite_registration;
       
       $returnData['delivery'] =  $case_delivery;
       $returnData['material'] =  $case_material;
       $returnData['prosthesis'] =  $case_prosthesis;
       $returnData['metal_ceramic']['metal'] =  $case_metal_ceramic;
       $returnData['metal_ceramic']['prosthetic_margins']  =  $case_prosthetic_margins;
       $returnData['metal_ceramic']['ceramic'] = $case_ceramic;

       
       $i=0;
       foreach ($case_color as $value){
          $returnData['color'][$i]['id'] = $value->id;
          $returnData['color'][$i]['name'] = $value->name;
          $returnData['color'][$i]['is_selected'] = $value->is_selected;

          if($value->name == 'Neck'){
            $returnData['color'][$i]['option']   =  $case_color_neck_option;
          }
          if($value->name == 'Body'){
              $returnData['color'][$i]['option']   =  $case_color_body_option;
          }
          $i++;
       }
       
       return $this->respond([
                    "data"    => [
                        "detail" => $returnData,
                    ],
                    "message" => trans('api.case_static'),
                    "status"  => $this->getStatuscode(),
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        mprd(2);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
        $clinic = auth()->user();
        
        if($clinic->case_token_total == $clinic->case_token_used ){
            return $this->respondValidationError(trans('api.notoken',[
                    'first_name'=>$clinic->first_name
                    ]));
        }

        $language = $clinic->language != '' ? $clinic->language : 'en';
        app()->setLocale($language);
        $validate= validate_parameter($request->all(),['lab_id','doctor_id','first_name','last_name','prosthesis_id','problematic_teeth','bite_registration_id','missing_teeth','material_id','material_other','metal_ceramic_id','metal_ceramic_text','prosthetic_margins','color_id','color_text','color_neck_id','color_body_id','delivery_id','delivery_text','parent_id','ceramic_id','bar_code','retake_text','ceramic_facial_text','delivery_date_text','date_selected']);

        if(!$validate){
           return $this->respondValidationError(trans('api.insufficientdata',[
                    'first_name'=>$clinic->first_name
                    ]));
        }
        extract($request->all());
        // check email is registered or not
        if($request->parent_id == 0 ){
            $exist = Cases::where('bar_code', $request->bar_code)->first();

            if($exist)
            {
                return $this->respond([
                        "message" => trans('api.bar_code_exist'),
                        "status"  => 500,
                    ]);
                
            }
        }
        else{
            $parentclinic = Cases::where('id', $request->parent_id)->first();

            $parent_bar_code = $parentclinic->bar_code;

           /* if($parent_bar_code != $bar_code )
            {
                return $this->respond([
                        "message" => trans('api.bar_code_exist'),
                        "status"  => 500,
                    ]);
                
            }*/
        }
        
        $doctor = User::where('id', $request->doctor_id)->first();
        if(empty($doctor)){
          return $this->respondValidationError(trans('api.invalid_doctor',[
                    'first_name'=>$clinic->first_name
                    ]));
        }
        else{
          $doctorName = strtoupper(substr($doctor->first_name, 0, 1));
        }
        
        $lab = User::where('id', $request->lab_id)->first();
        if(empty($lab)){
          return $this->respondValidationError(trans('api.invalid_lab',[
                    'first_name'=>$clinic->first_name
                    ]));
        }
        else{
          $labName = strtoupper(substr($lab->full_name, 0, 3));
        }
        $clinicName = strtoupper(substr($clinic->first_name, 0, 3));
        $first_name = strtoupper(substr($request->first_name, 0, 1));

        // clinic basic details
        $input['parent_id']          = $request->parent_id;
        $input['first_name']         = $request->first_name;
        $input['last_name']          = $request->last_name;
        $input['lab_id']             = $request->lab_id;
        $input['doctor_id']          = $request->doctor_id;
        
        $input['clinic_id']          = $clinic->id;
        $input['prosthesis_id']      = $request->prosthesis_id;
        $input['problematic_teeth']  = $request->problematic_teeth;
        $input['missing_teeth']      = $request->missing_teeth;
        $input['bite_registration_id'] = $request->bite_registration_id;        
        $input['material_id']        = $request->material_id;
        $input['material_other']     = $request->material_other;        
        $input['metal_ceramic_id']   = $request->metal_ceramic_id;
        $input['metal_ceramic_text'] = $request->metal_ceramic_text;
        $input['prosthetic_margins'] = $request->prosthetic_margins;
        $input['ceramic_facial_text'] = $request->ceramic_facial_text;
        $input['ceramic_id']         = $request->ceramic_id;
        $input['process_date']       = $request->process_date;
        $input['color_id']           = $request->color_id;
        $input['color_text']         = $request->color_text;
        $input['color_neck_id']      = $request->color_neck_id;
        $input['color_body_id']      = $request->color_body_id;
        $input['delivery_id']        = $request->delivery_id;
        $input['delivery_text']      = $request->delivery_text;  
        $input['date_selected']      = $request->date_selected;    
        $input['delivery_date_text'] = $request->delivery_date_text;      
        $input['created_by_id']      = auth()->user()->id;
        $input['created_by_name']    = $clinic->roles()->first()->name;
        $input['bar_code']           = $bar_code;
        $input['bar_code']           = $bar_code;
       
        $cases = Cases::create($input);

        $this->lessToken($clinic,'case_token_used',1);


        $invID = str_pad($cases->id, 4, '0', STR_PAD_LEFT);
        $case_code = $clinicName.$doctorName.$labName.$first_name.$invID;
        if($request->parent_id != 0)
        {

         
            $parentclinic->status = 'Repetition';
            $parentclinic->have_child = 1;
            $parentclinic->save();

            $this->addCaseLog($request->parent_id,3,'user',$clinic->first_name,'',$case_code,$retake_text);
        }

        $this->addCaseLog($cases->id,1,'user',$clinic->first_name);
        $this->addCaseLog($cases->id,2,'user',$clinic->first_name,$labName);

        if(isset($delivery_id) && $delivery_id != 3){
          $cases->is_trying    = 1;
          $cases->trying_by    = 'Clinic';
          $cases->trying_status = 'Pending';
          $cases->save();
          $this->addCaseLog($cases->id,10,'user',$clinic->first_name,$labName);
        }

        if(!empty($cases))
        {  
            if ($request->hasFile('image')) {
                // save image in local
                $image = $this->quickUpload($request->image, CASE_PATH);
                // transfer image to s3
                $this->transferFileToS3($image);
                // delete image from local
                unlink(storage_path('app/public/'.$image));

                // save image in database
                $imagei['name'] = $image;
                $image = Image::create($imagei);
                $cases->givePermissionTo($image);
            }
            if ($request->hasFile('qr_image')) {
                // save image in local
                $image = $this->quickUpload($request->qr_image, CASE_QR_PATH);
                // transfer image to s3
                $this->transferFileToS3($image);
                // delete image from local
                unlink(storage_path('app/public/'.$image));

                // save image in database
                $cases['qr_code'] = $image;
                $cases->save();
                
            }
            
            $cases['case_code'] = (string)$case_code;
            $cases->save();
            // Create Bar Code
            /*3 Clinic 
              1 Dr 
              3 lab
              1 Firstname
              4 code
              The first 3 digits  identify  the clinic  4 TH  identifies  the Dr  Chosen  from  the menu– the 3 following 
              are lab identifiers auto  numbers given by  the system  to  avoid repetition  the following 5 digits  
              link  to  the patient and unique  case  number
              */
            

            return $this->respond([
                "data" => [
                    "user" => $this->casesTransformer->transform($cases),
                ],
                "message" => trans('api.case_add'),
                "status" => $this->getStatuscode(),
            ]);
        }
        else
        {
            return $this->respond([
                "message" => trans('api.case_error'),
                "status" => 500,
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { 
        $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);
        $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
                ->with('images')
                ->where('cases.id', $id)
                ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
                ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
                ->first();
       
        if($cases)
        {
            return $this->respond([
                "data" => [
                    "cases" => $this->casesTransformer->transform($cases),
                ],
                "message" => trans('api.case_detail'),
                "status" => $this->getStatuscode(),
            ]);
        }
        else{
          return $this->respond([
                "message" => trans('api.invalidcaseid'),
                "status" => 500,
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function checkBarCode(Request $request)
    { 
      $user = auth()->user();
      $language = $user->language != '' ? $user->language : 'en';
      app()->setLocale($language);
       $validate= validate_parameter($request->all(),['bar_code']);
      if(!$validate){
         return $this->respondValidationError(trans('api.insufficientdata'));
      }
     /* if(isset($request->id) && $request->id != 0 ){
        $count = Cases::where('id', $request->id)->where('bar_code', $request->bar_code)->get();

      }
      else{
        $count = Cases::where('bar_code', $request->bar_code)->count();
      }*/
      $count = Cases::where('bar_code', $request->bar_code)->count();
       $is_used = 0;
       if($count == 0)
       {
          $is_used = 1;
          return $this->respond([
                "data" => [
                    "is_used" => $is_used,
                ],
                "message" => trans('api.notUsed'),
                "status" => $this->getStatuscode(),
            ]);
       }
       return $this->respond([
                "data" => [
                    "is_used" => $is_used,
                ],
                "message" => trans('api.case_exist'),
                "status" => $this->getStatuscode(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function tryingstatus(Request $request){
      $clinic = auth()->user();
      $language = $clinic->language != '' ? $clinic->language : 'en';
      app()->setLocale($language);
      $validate= validate_parameter($request->all(),['tryin_type','tryin_text','tryin_date','case_code','id','status_flag']);
      if(!$validate){
         return $this->respondValidationError(trans('api.insufficientdata',[
                  'first_name'=>$clinic->first_name
                  ]));
      }
      extract($request->all());
      $userRole = $clinic->roles()->first();
      $rolename = $userRole->name;
      
      $cases = Cases::where('case_code', $case_code)
      ->where('id', $id)
      ->where('created_by_name', $rolename)
      ->with('images')
      ->first();

      if(empty($cases)){
          return $this->respond([
                "message" => trans('api.invalidcaseid'),
                "status" => 500,
            ]);
      }else{

          $lab = User::where('id', $cases->lab_id)->first();
          $labName = $lab->full_name;

          if($cases->created_by_id != $clinic->id){
              return $this->respond([
                "message" => trans('api.invalidcaseidotherclinic'),
                "status" => 500,
            ]);
          }

          if($cases->is_trying != 1){
              $cases->is_trying       =  1;
              $cases->trying_by       = 'Clinic';
              $cases->trying_status   = 'Pending';
              $cases->save();
              $this->addCaseLog($cases->id,10,'user',$clinic->first_name,$labName);
          }


          if($cases->trying_status == 'Finish'){
              return $this->respond([
                "message" => trans('api.tryingFinish'),
                "status" => 500,
            ]);
          }

          if($cases->trying_status == 'Pending'){
              return $this->respond([
                "message" => trans('api.tryingPending'),
                "status" => 500,
            ]);
          }

          

          $trying_status         = $status_flag == 'yes'? 'Finish' : 'Re-trying' ;
          $cases->trying_status  = $trying_status;
          $cases->tryin_type     = $tryin_type;
          $cases->tryin_text     = $tryin_text;
          $cases->tryin_date     = $tryin_date;
          $cases->save();
          
          $this->addCaseLog($cases->id,11,'user',$clinic->first_name,$labName,$case_code,$tryin_type);
          return $this->respond([
                "data" => [
                    "user" => $this->casesTransformer->transform($cases),
                ],
                "message" => trans('api.case_edit'),
                "status" => $this->getStatuscode(),
            ]);
      }

    }


    public function update(Request $request)
    {
      $clinic = auth()->user();
      $language = $clinic->language != '' ? $clinic->language : 'en';
      app()->setLocale($language);
      $validate= validate_parameter($request->all(),['bar_code','lab_id','doctor_id','first_name','last_name','prosthesis_id','problematic_teeth','bite_registration_id','missing_teeth','material_id','material_other','metal_ceramic_id','metal_ceramic_text','prosthetic_margins','color_id','color_text','color_neck_id','color_body_id','delivery_id','delivery_text','solder_text','tryin_type','tryin_text','ceramic_facial_text','delivery_date_text','date_selected']);
      if(!$validate){
         return $this->respondValidationError(trans('api.insufficientdata',[
                  'first_name'=>$clinic->first_name
                  ]));
      }

      extract($request->all());

      $clinic = auth()->user();
      $userRole = $clinic->roles()->first();
      $rolename = $userRole->name;
      
      $cases = Cases::where('bar_code', $bar_code)
      ->where('created_by_name', $rolename)
      ->with('images')
      ->first();
      if(empty($cases)){
          return $this->respond([
                "message" => trans('api.invalidcaseid'),
                "status" => 500,
            ]);
      }else{
          if($cases->created_by_id != $clinic->id){
              return $this->respond([
                "message" => trans('api.invalidcaseidotherclinic'),
                "status" => 500,
            ]);
          }
          $cases->lab_id  = $lab_id;
          $cases->doctor_id  = $doctor_id;
          $cases->first_name  = $first_name;
          $cases->last_name  = $last_name;
          $cases->prosthesis_id  = $prosthesis_id;
          $cases->problematic_teeth  = $problematic_teeth;
          $cases->bite_registration_id  = $bite_registration_id;
          $cases->missing_teeth  = $missing_teeth;
          $cases->material_id  = $material_id;
          $cases->material_other  = $material_other;
          $cases->metal_ceramic_id  = $metal_ceramic_id;
          $cases->metal_ceramic_text  = $metal_ceramic_text;
          $cases->prosthetic_margins  = $prosthetic_margins;
          $cases->color_id  = $color_id;
          $cases->color_text  = $color_text;
          $cases->color_neck_id  = $color_neck_id;
          $cases->color_body_id  = $color_body_id;
          $cases->delivery_id  = $delivery_id;
          $cases->delivery_text  = $delivery_text;
          $cases->solder_text     = $solder_text;
          $cases->tryin_type     = $tryin_type;
          $cases->tryin_text     = $tryin_text;
          $cases->ceramic_id     = $ceramic_id;
          $cases->ceramic_facial_text     = $ceramic_facial_text;
          $cases->delivery_date_text     = $delivery_date_text;
          $cases->date_selected     = $date_selected;


          if(isset($delivery_id) && $delivery_id != 3){
            $cases->is_trying    = 1;
            $cases->trying_by    = 'Clinic';
            $cases->trying_status = 'Pending';
            $cases->save();
            $this->addCaseLog($cases->id,10,'user',$clinic->first_name,$labName);
          }

          
          $cases->save();
          $this->addCaseLog($cases->id,4,'user',$clinic->first_name);
          return $this->respond([
                "data" => [
                    "user" => $this->casesTransformer->transform($cases),
                ],
                "message" => trans('api.case_edit'),
                "status" => $this->getStatuscode(),
            ]);
      }
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($case_code)
    {
        $clinic = auth()->user();
        $language = $clinic->language != '' ? $clinic->language : 'en';
        app()->setLocale($language);
        $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
                ->with('images')
                ->where('cases.case_code', $case_code)
                ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
                ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
                ->orderBy('id','desc')
                ->first();
       
        if($cases)
        {
            return $this->respond([
                "data" => [
                    "detail" => $this->casesTransformer->transform($cases),
                ],
                "message" => trans('api.scan_sucess'),
                "status" => $this->getStatuscode(),
            ]);
        }
        else{
          return $this->respond([
                "message" => trans('api.scan_fail'),
                "status" => 500,
            ]);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        mprd(7);
    }
}
