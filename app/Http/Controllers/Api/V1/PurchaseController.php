<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Subscription;

use App\Traits\Subscriptiontrait;
use App\Traits\Purchasetrait;


class PurchaseController extends ApiController
{
    use Purchasetrait,Subscriptiontrait;
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function token(Request $request)
    {

        $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);

        $item = User::find($user->id);
        $userRole = strtolower($item->roles()->first()->name);

        $validate= validate_parameter($request->all(),['token_count','amount','device_type','remark']);

        if(!$validate){
           return $this->respondValidationError(trans('api.insufficientdata'));
        }

        
        $message = 'Purchased';
        
        $this->addToken($user,$request->token_count,$userRole,'paid',$message,$request->device_type,0,$request->remark,$request->amount,"$");

        $this->updateToken($user,'case_token_purchased',$request->token_count);

        $tokensummary = [
            'case_token_free'       => $user->case_token_free,
            'case_token_purchased'  => $user->case_token_purchased,
            'case_token_bylab'      => $user->case_token_bylab,
            'case_token_total'      => $user->case_token_total,
            'case_token_used'       => $user->case_token_used,
        ];

        return $this->respond([
                "message" => trans('api.purchase_successfully'),
                "status"  => $this->getStatuscode(),
                "data" => [
                'case_token_pending'    => $user->case_token_total - $user->case_token_used,
                'token_count' => $request->token_count,
                'amount' => $request->amount,
                    "tokensummary" => $tokensummary,
                ],
            ]);


    }

    
    public function doctor(Request $request)
    {
        $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);

        $item = User::find($user->id);
        $userRole = strtolower($item->roles()->first()->name);

        $validate= validate_parameter($request->all(),['token_count','amount','device_type','remark']);

        if(!$validate){
           return $this->respondValidationError(trans('api.insufficientdata'));
        }

        
        $message = 'Purchased';
        
        $this->addDoctor($user,$request->token_count,$userRole,'paid',$message,$request->device_type,0,$request->remark,$request->amount,"$");

        $this->updateDoctor($user,'doctor_token_purchased',$request->token_count);

        $tokensummary = [
            'doctor_token_free'       => $user->doctor_token_free,
            'doctor_token_purchased'  => $user->doctor_token_purchased,
            'doctor_token_total'      => $user->doctor_token_total,
            'doctor_token_used'       => $user->doctor_token_used,
        ];

        return $this->respond([
                "message" => trans('api.purchase_successfully'),
                "status"  => $this->getStatuscode(),
                "data" => [
                    'doctor_token_pending'  => $user->doctor_token_total - $user->doctor_token_used,
                    "tokensummary" => $tokensummary,
                ],
            ]);

    }

    public function subscription(Request $request)
    {
        $user    = auth()->user();
        $current = $this->getSubescriptionDetail($user->id);
        if($current['is_subscribe'] == 'yes'){

        }
        else{
            $validate= validate_parameter($request->all(),['plan','amount','device_type','receipt','type']);

            if(!$validate){
               return $this->respondValidationError(trans('api.insufficientdata'));
            }

            $user = auth()->user();
            $language = $user->language != '' ? $user->language : 'en';
            app()->setLocale($language);

            $item = User::find($user->id);
            $userRole  = strtolower($item->roles()->first()->name);

            $renew_id  = 0;
            $subscription  = Subscription::where('user_id',$clinic_id)->first();
            if($subscription){
                $renew_id = $subscription->history_id;
                $subscription->delete();
            }


            $this->newUser($user->id,$request->plan,$userRole,@$request->type,$request->amount,'$',$request->device_type,0,$request->receipt,$renew_id);
            $token_count = $this->get_free_token($request->plan);
            $message = ' Trial';
            $remark  = 'Re subscription Trial';
            $paid_from = $request->device_type;
            $this->addToken($clinic,$token_count,$request->user_type,'free',$message,$paid_from,0,$remark,0,"$");


           // as not sure we will give doctor as well $this->addDoctor($clinic,$token_count,$request->user_type,'free',$message,$paid_from,0,$remark,0,"$");
        }
    }
}
