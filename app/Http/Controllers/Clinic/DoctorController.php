<?php

namespace App\Http\Controllers\Clinic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Transformers\DoctorWebTransformer;
use App\Transformers\UsersTransformer;
use App\ClinicDetail;
use App\Traits\FileManipulationTrait;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\Traits\Purchasetrait;
use Image;
use Cartalyst\Stripe\Laravel\Facades\Stripe;

use App\Mail\DoctorCreate;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use FileManipulationTrait, Purchasetrait;
    protected $user;

    public function __construct(User $user)
    {
       // echo "gfgewr"; exit;
        $this->user = $user;
        $this->doctorsTransformer = new DoctorWebTransformer;
        $this->usersTransformer = new UsersTransformer;
    }

    public function index()
    {
        $user = auth()->user();

          if(request()->ajax() || request()->wantsJson()){
            $clinicId = auth()->user()->id;
            $sort= request()->has('sort')?request()->get('sort'):'first_name';
            $order= request()->has('order')?request()->get('order'):'asc';
            $search= request()->has('searchQuery')?request()->get('searchQuery'):'';

           
            if(request()->has('sort') && request()->get('sort') == 'Name'){
                $sort = "full_name";
            }

            if(request()->has('sort') && request()->get('sort') == 'Phone'){
                $sort = "contact_number";
            }
            
            $clinic=
                $this->user->where('r.role_id','4')
                ->where('d.clinic_id',$clinicId)
                ->join('role_user as r', 'users.id' ,'=','r.user_id')
                ->join('clinic_doctor as d', 'users.id' ,'=','d.doctor_id')

                ->whereHas('roles',function($query) use ($search)
                {
                    $query->where('name','Doctor');
                    if ($search) {
                        $query->where('users.first_name','like',"$search%")
                            ->orWhere('users.last_name','like',"$search%")
                            ->orWhere('users.email','like',"$search%")
                            ->orWhere('users.full_name','like',"$search%");
                    }
                    
                })

                ->select('users.*','d.status as doctorstatus','d.is_selected as is_selected')->orderBy("$sort", "$order")->paginate(10);

                
       
            $paginator=[
                'total_count'  => $clinic->total(),
                'total_pages'  => $clinic->lastPage(),
                'current_page' => $clinic->currentPage(),
                'limit'        => $clinic->perPage()
            ];
            return response([
                'doctor_token_pending'  => $user->doctor_token_total - $user->doctor_token_used,
                "data"        => $this->doctorsTransformer->transformCollection($clinic->all()),
                "paginator"   => $paginator,
                "status_code" => 200
            ],200);

            
            }
        
        return view('clinic.doctors.list');
    }

    public function linkdoctor(Request $request){
        
        $clinicId = auth()->user()->id;
        $doctor =\DB::table('clinic_doctor')
        ->where('clinic_doctor.clinic_id','=',$clinicId)
        ->where('clinic_doctor.doctor_id','=',$request->doctor_id)
        ->count();

        if($doctor == 1){
            return response([
                "message" => 'Doctor already linked',
                "status_code"  => 201,
            ],201);
        }
        $clinic     =  User::where('id', $clinicId)->first();

        if($clinic->doctor_token_total == $clinic->doctor_token_used ){
            return $this->respondValidationError(trans('api.nodoctor',[
                    'first_name'=>$clinic->first_name
                    ]));
        }


        $user       =  User::where('id', $request->doctor_id)->first();
        $clinic->givePermissionToDoctor($user);
        $this->updateDoctor($clinic,'doctor_token_used',1);
        return response([
            "message" => 'Doctor linked successfully',
            "status_code"  => 200,
        ],200);


    }


    public function checkEmail(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'email' => 'required|email',
        ]);

        // if error than response error with its message
        if($validator->fails()) 
        {
            return response([
                "message"     => 'Invalid email.',
                "status_code" => 201
            ],201);
            #return $this->respondValidationError(trans('api.fail_validation'), $validator->messages());
        }

        $clinicId = auth()->user()->id;
        // check email is registered or not
        $doctor = User::where('email', $request->email)->first();
    

        if($doctor)
        {           
            $role       = \DB::table('role_user')->where('role_user.user_id', $doctor->id)->leftJoin('roles as r', 'r.id', '=', 'role_user.role_id')->first();
            $roleName   =  $role->name;
            if($roleName == 'Doctor'){


                $doctorcount =\DB::table('clinic_doctor')
                ->where('clinic_doctor.clinic_id','=',$clinicId)
                ->where('clinic_doctor.doctor_id','=',$doctor->id)
                ->count();

                if($doctorcount == 1){
                    return response([
                        "message"        => 'Doctor already linked' ,
                        "status_code" => 201,
                        "data"    => [
                            "can_link" => false,
                            "doctor_id" => $doctor->id,
                            
                        ],
                    ],201);
                }

                return response([
                    "message"        =>trans('api.email_registered').' Please click on link button to link this doctor with your clinic.',
                    "status_code" => 202,
                    "data"    => [
                        "can_link" => true,
                        "doctor_id" => $doctor->id,
                        
                    ],
                ],202);
            }

            return response([
                "message"        =>trans('api.email_registered_other',['name'=>$roleName]),
                "status_code" => 202,
                "data"    => [
                    "id" => $doctor->id,
                    "can_link" => false
                ]
            ],202);

        }
        else
        {
            return response([
                "message"        =>trans('api.email_available'),
                "status_code" => 200
            ],200);
        }
    }


    public function checkEmailEdit(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'email' => 'required|email',
        ]);

        // if error than response error with its message
        if($validator->fails()) 
        {
            return response([
                "message"     => 'Invalid email.',
                "status_code" => 201
            ],201);
            #return $this->respondValidationError(trans('api.fail_validation'), $validator->messages());
        }

        $clinicId = auth()->user()->id;
        // check email is registered or not
        $doctor = User::where('email', $request->email)->where('id','!=',$request->doctor_id)->first();
    

        if($doctor)
        {           
            $role       = \DB::table('role_user')->where('role_user.user_id', $doctor->id)->leftJoin('roles as r', 'r.id', '=', 'role_user.role_id')->first();
            $roleName   =  $role->name;
            if($roleName == 'Doctor'){

                $doctorcount =\DB::table('clinic_doctor')
                ->where('clinic_doctor.clinic_id','=',$clinicId)
                ->where('clinic_doctor.doctor_id','=',$doctor->id)
                ->count();

                if($doctorcount == 1){
                    return response([
                        "message"        => 'Doctor already linked' ,
                        "status_code" => 201,
                        "data"    => [
                            "can_link" => false,
                            "doctor_id" => $doctor->id,
                            
                        ],
                    ],201);
                }

                return response([
                    "message"        =>trans('api.email_registered').' Please click on link button to link this doctor with your clinic.',
                    "status_code" => 202,
                    "data"    => [
                        "can_link" => true,
                        "doctor_id" => $doctor->id,
                        
                    ],
                ],202);
            }

            return response([
                "message"        =>trans('api.email_registered_other',['name'=>$roleName]),
                "status_code" => 202,
                "data"    => [
                    "id" => $doctor->id,
                    "can_link" => false
                ]
            ],202);

        }
        else
        {
            return response([
                "message"        =>trans('api.email_available'),
                "status_code" => 200
            ],200);
        }
    }

    public function checkUsernameexist(Request $request)
    {
        if(isset($request->doctor_id)) {
            $validator = validator()->make($request->all(), [
                'username'      => 'required|unique:users,username,'.$request->doctor_id,
            ]);
        }
        else{
            $validator = validator()->make($request->all(), [
                'username'      => 'required|unique:users,username'
            ]);
        }
        

        // if error than response error with its message
        if($validator->fails()) {
            return response([
                "message" => trans('api.username_exist'),
                "status"  => 201,
            ],201);
        }

        return response([
            "message"        =>trans('api.email_available'),
            "status_code" => 200
        ],200);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        

        return view('clinic.doctors.add',['defaultImg'=>$this->getFileUrl('user/avatar.png')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function exist(Request $request)
    {
        // check email is registered or not
        $exist = User::where('email', $request->email)->first();

        if($exist)
        {
            return $this->respond([
                    "message" => trans('api.email_registered_login'),
                    "status"  => 500,
                ]);
            
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        $validate= validate_parameter($request->all(),['first_name','last_name','username','email','phone']);
        if(!$validate){
           return response([
                "status_code" => 401
            ],401);
        }

        $password = rand(111111, 999999);
        //$hashed = \Hash::make($password);
        $clinicId = auth()->user()->id;

        $clinic =  User::where('id', $clinicId)->first();

        if($clinic->doctor_token_total == $clinic->doctor_token_used ){
            return $this->respondValidationError(trans('api.nodoctor',[
                    'first_name'=>$clinic->first_name
                    ]));
        }



        $user = User::create([
            "last_name"         => $request->last_name,
            "full_name"         => $request->first_name.' '.$request->last_name,
            "first_name"        => $request->first_name,
            "email"             => $request->email,
            "username"          => $request->username,
            "contact_number"    => $request->phone,
            "password"          => $password
        ]);
        
        // delete image from local
        //unlink(storage_path('public/user/doctor/'.$request->image));

        $user->assignRole('Doctor');
        $clinic->givePermissionToDoctor($user);

        $this->updateDoctor($clinic,'doctor_token_used',1);
        
        $clinic     = User::with('clinicDetail')->find($clinicId)->toarray();
        $clinic_name = $clinic['clinic_detail']['clinic_name'];


        $mailuser =  (object)[
        'first_name' => $request->first_name,
        'clinicname' => $clinic_name,
        'password' => $password,
        'email' => $request->email,
        'phonenumber' => $request->phone,
        "username"          => $request->username,
        ];

        Mail::to($request->email)->send(new DoctorCreate($mailuser));

         #mprd($request->all());

        return response([
                "message"        => 'Doctor created successfully',
                "status_code" => 201
            ],201);
        


    }

    public function image(Request $request){
        
        
        $imageData = $request->file;

        $path = $request->file->path();

        $extension = $request->file->extension();


        $fileName  = Carbon::now()->timestamp . '_' . uniqid() . '.' .$extension;
       // $this->createDir('public/user/doctor/');

        $image = $this->quickUpload($request->file, DOCTOR_PATH);

        //$image = Image::make($request->file)->save(storage_path('app/public/user/doctor/').$fileName);

        return response([
                "data"        => $image,
                "status_code" => 200
            ],200);
       
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $clinicId   =  auth()->user()->id;
        $clinic=
                $this->user
                ->where('d.clinic_id',$clinicId)
                ->where('d.doctor_id',$id)
                ->join('clinic_doctor as d', 'users.id' ,'=','d.doctor_id')

                ->select('users.*','d.status as doctorstatus')->first();

        return response([
                   // "message" => "Doctor status updated successfully",
                    "data"        =>$this->doctorsTransformer->transform($clinic),
                    "status_code" =>200
                ],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('clinic.doctors.edit',['defaultImg'=>$this->getFileUrl('user/avatar.png'),'doctor_id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $clinicId   =  auth()->user()->id;
       $clinic=
                $this->user
                ->where('d.clinic_id',$clinicId)
                ->where('d.doctor_id',$id)
                ->join('clinic_doctor as d', 'users.id' ,'=','d.doctor_id')

                ->select('users.*','d.status as doctorstatus')->first();

       $clinic->first_name = $request->first_name;         
       $clinic->last_name  = $request->last_name;
       $clinic->full_name  = $request->first_name.' '.$request->last_name;
       $clinic->contact_number = $request->contact_number;
       $clinic->save();
       $clinic=
                $this->user
                ->where('d.clinic_id',$clinicId)
                ->where('d.doctor_id',$request->id)
                ->join('clinic_doctor as d', 'users.id' ,'=','d.doctor_id')

                ->select('users.*','d.status as doctorstatus')->first();

        return response([
                   // "message" => "Doctor status updated successfully",
                    "data"        =>$this->doctorsTransformer->transform($clinic),
                    "message"     => 'Detail updated successfully',
                    "status_code" =>200
                ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function switchStatus(Request $request){
        
        $currentStatus = $request->doctorstatus;

        $clinicId   =  auth()->user()->id;

        $doctor =\DB::table('clinic_doctor')
        ->where('clinic_doctor.clinic_id','=',$clinicId)
        ->where('clinic_doctor.doctor_id','=',$request->id)
        ->get();

        $doctor_status = $currentStatus == '1' ? '0' : '1';
       
        \DB::update('update clinic_doctor set status = '.$doctor_status.' where clinic_id = ? and doctor_id = ? ', [$clinicId,$request->id]);

        $clinic=
                $this->user
                ->where('d.clinic_id',$clinicId)
                ->where('d.doctor_id',$request->id)
                ->join('clinic_doctor as d', 'users.id' ,'=','d.doctor_id')

                ->select('users.*','d.status as doctorstatus')->first();

        return response([
                   // "message" => "Doctor status updated successfully",
                    "data"        =>$this->doctorsTransformer->transform($clinic),
                    "message"     =>trans('messages.clinic-status',["status"=>$doctor_status]),
                    "status_code" =>200
                ],200);
    }

    public function purchase()
    {
        
        $clinicData   =  auth()->user();
        $stripe = Stripe::make(env('STRIPE_SECRET'));
        if($clinicData->stripe_id == ''){
            
            $customer = $stripe->customers()->create([
                'email' => $clinicData->email,
            ]);

            $stripe_id  =  $customer['id'];
            $clinicData->stripe_id   = $stripe_id;
            $clinicData->save();            
        }
        
        $stripe_id = $clinicData->stripe_id;


        $cards = $stripe->cards()->all($stripe_id);

        if(!empty($cards['data'])){
            $is_card = 'yes';
        }
        else{
            $is_card = 'no';
        }
        
        return view('clinic.doctors.purchase',compact('is_card'));
    }

     public function buy_doctor(Request $request){
        $clinicData   =  auth()->user();
        $stripe_id = $clinicData->stripe_id;
        $stripe = Stripe::make(env('STRIPE_SECRET'));
        $cards = $stripe->cards()->all($stripe_id);

        if(isset($cards['data']) && !empty($cards['data'])){ 
            $card_id = $cards['data'][0]['id'];
        }else{
            try{
            
                
                $token = $stripe->tokens()->create([
                    'card' => [
                        'number'    => '4242424242424242',
                        'exp_month' => 10,
                        'cvc'       => 314,
                        'exp_year'  => 2020,
                    ],
                ]);

                $card = $stripe->cards()->create($stripe_id, $token['id']);
                $card_id = $card['id'];

                //return $token['id'];
            } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) { 
                flash('Invalid card details','danger');
                return back()->withErrors($validator)->withInput();
            } catch (Exception $e) {
                flash('Something went wrong with your card details.','danger');
                return back()->withErrors($validator)->withInput();
            }    
        }
        
        $charge = $stripe->charges()->create([
            'customer' => $stripe_id,
            'currency' => 'USD',
            'amount'   => '10',
            'card'     => $card_id
        ]);
        
         
        $message = 'Buy in clinic web';

        $remark =  $charge['id'];

        $this->addDoctor($clinicData,2,'clinic','paid',$message,'web',0,$remark,'10',"$");

        $this->updateDoctor($clinicData,'doctor_token_purchased',2);

        /*set purchase message*/

        return redirect('/clinic/doctor');


     }

    public function purchase_token()
    {
        
        $clinicData   =  auth()->user();
        $stripe = Stripe::make(env('STRIPE_SECRET'));
        if($clinicData->stripe_id == ''){
            
            $customer = $stripe->customers()->create([
                'email' => $clinicData->email,
            ]);

            $stripe_id  =  $customer['id'];
            $clinicData->stripe_id   = $stripe_id;
            $clinicData->save();            
        }
        
        $stripe_id = $clinicData->stripe_id;


        $cards = $stripe->cards()->all($stripe_id);

        if(!empty($cards['data'])){
            $is_card = 'yes';
        }
        else{
            $is_card = 'no';
        }
        
        return view('clinic.token.purchase',compact('is_card'));
    }


    public function buy_token(Request $request){
        $clinicData   =  auth()->user();
        $stripe_id = $clinicData->stripe_id;
        $stripe = Stripe::make(env('STRIPE_SECRET'));
        $cards = $stripe->cards()->all($stripe_id);

        if(isset($cards['data']) && !empty($cards['data'])){ 
            $card_id = $cards['data'][0]['id'];
        }else{
            try{            
                $token = $stripe->tokens()->create([
                    'card' => [
                        'number'    => '4242424242424242',
                        'exp_month' => 10,
                        'cvc'       => 314,
                        'exp_year'  => 2020,
                    ],
                ]);

                $card = $stripe->cards()->create($stripe_id, $token['id']);
                $card_id = $card['id'];

                //return $token['id'];
            } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) { 
                flash('Invalid card details','danger');
                return back()->withErrors($validator)->withInput();
            } catch (Exception $e) {
                flash('Something went wrong with your card details.','danger');
                return back()->withErrors($validator)->withInput();
            }    
        }
        
        $charge = $stripe->charges()->create([
            'customer' => $stripe_id,
            'currency' => 'USD',
            'amount'   => '10',
            'card'     => $card_id
        ]);
        
         
        $message = 'Buy in clinic web';

        $remark =  $charge['id'];

        $this->addToken($clinicData,2,'clinic','paid',$message,'web',0,$remark,'10',"$");

        $this->updateToken($clinicData,'case_token_purchased',2);

        /*set purchase message*/

        return redirect('/clinic/doctor');


     }
}
