<?php

namespace App\Http\Controllers\Clinic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cases;
use App\Transformers\CaseCalenderTransformer;
use App\Transformers\CaseWebTransformer;

class DashController extends Controller
{

    public function __construct()
    {
        $this->caseCalenderTransformer = new CaseCalenderTransformer;
        $this->casesWebTransformer      = new CaseWebTransformer;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function checkislogin(){ 
         //$labId = auth()->user()->id;
         $is_login = isset(auth()->user()->id) && auth()->user()->id != '' ? auth()->user()->id : 0;
         return response([
                "is_login"    => $is_login,
                "id"          => auth()->user()->id,
                "status_code" => 200
            ],200);
    }

    public function index()
    {   
        $labId = auth()->user()->id;
        $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
            ->with('images')            
            ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
            ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
            ->where('cases.clinic_id',$labId)
            ->where('cases.status','!=','Repetition')
            ->orderBy('id','desc')->paginate(5);

        $allcase = $this->casesWebTransformer->transformCollection($cases->all());
        $casecount = count($cases);
        #mpr($allcase);
        return view('clinic.home',compact('allcase','pendingcasecount'));
    }

    public function calender()
    {
        return view('clinic.calender');
    }

    public function caselibrary(){
        $clinicId = auth()->user()->id;
         if(request()->ajax() || request()->wantsJson()){
            

            
        }
        return view('clinic.labs.caselist');   
    }

    public function calendercaselist (){
        $clinicId = auth()->user()->id;
         $cases = Cases::select('cases.case_code','cases.status','cases.end_date')
            ->where('cases.clinic_id',$clinicId)
            ->where('cases.status','!=','Repetition')
            ->get();
            

            
            return response([
                "data"        => $this->caseCalenderTransformer->transformCollection($cases->all()),
                "status_code" => 200
            ],200);
        
    }
}
