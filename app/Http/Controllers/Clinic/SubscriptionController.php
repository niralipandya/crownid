<?php

namespace App\Http\Controllers\Clinic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use App\Traits\Subscriptiontrait;
use App\Traits\Purchasetrait;

class SubscriptionController extends Controller
{
    use Subscriptiontrait,Purchasetrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function current()
    {
        $is_card        = 'yes';

        $clinicData     =  auth()->user();
        $subscription   =  $this->getSubescriptionDetail($clinicData->id);

        $avaliblePackages = [];

        if($subscription['subscription']['plan'] == 'Basic'){

            $avaliblePackages[0]['name'] = 'Pro - Monthly';
            $avaliblePackages[0]['id'] = 'Upgrade_PRO_Monthly';
            $avaliblePackages[1]['name'] = 'Pro - Yearly';
            $avaliblePackages[1]['id'] = 'Upgrade_PRO_Yearly';
            $avaliblePackages[2]['name'] = 'Premium - Yearly';
            $avaliblePackages[2]['id'] = 'Upgrade_PREMIUM_Yearly';

        }
        else if($subscription['subscription']['plan'] == 'Pro'){

            if($subscription['subscription']['type'] == 'Month' ){
                $avaliblePackages[0]['name'] = 'Pro - Yearly';
                $avaliblePackages[0]['id'] = 'Upgrade_PRO_Yearly';
                $avaliblePackages[1]['name'] = 'Premium - Yearly';
                $avaliblePackages[1]['id'] = 'Upgrade_PREMIUM_Yearly';
            }
            else{
                $avaliblePackages[0]['name'] = 'Premium - Yearly';
                $avaliblePackages[0]['id'] = 'Upgrade_PREMIUM_Yearly';
            }
        }

        #mprd($subscription);
        return view('clinic.subscription.upgrade',compact('is_card','subscription','avaliblePackages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function upgrade(Request $request)
    {

        $clinicData     =  auth()->user();
        $subscription   =  $this->getSubescriptionDetail($clinicData->id);


        $plan_id    =   $request->plan_name;
        if($plan_id == 'Upgrade_PRO_Monthly'){
            $plan = 'Pro'; 
            $request_type = 'Month';
            $request_amount = '24.99';
        }
        else if ($plan_id == 'Upgrade_PRO_Yearly') {
            $plan = 'Pro';
            $request_type = 'Year';
            $request_amount = '239.88';
        }
        else if ($plan_id == 'Upgrade_PREMIUM_Yearly'){
            $plan = 'Premium';
            $request_type = 'Year';
            $request_amount = '299.88';
        }
        $receiptData = [];
        $stripe_id = $clinicData->stripe_id;
        $stripe = Stripe::make(env('STRIPE_SECRET'));
        $subscription = $stripe->subscriptions()->update(
        $stripe_id,'sub_DHwUEZZiqrFIz2', [
            'plan' => $plan_id,
        ]);

        $returnData['auto_renew_status']        = 'true';
        $returnData['transaction_id']           = $subscription['id'];
        $returnData['original_transaction_id']  = $subscription['id'];
        $returnData['is_trial_period']          = 'false';
        $returnData['expires_date']             = date('Y-m-d H:i:00', $subscription['current_period_end']);
        $returnData['next_bill_date']           = date('Y-m-d H:i:00', $subscription['current_period_end']);
        $returnData['product_id']               = $plan_id;

        $this->updateSubscription($clinicData->id,$plan,'clinic',$request_type,$request_amount,'$','web',0,$returnData,0,$returnData);
                    $paid_from = $request->device_type;
                    $this->giveToken($request->plan,'Update subscription','Update','web',$clinicData,'clinic','Web'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
