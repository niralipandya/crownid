<?php

namespace App\Http\Controllers\Clinic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\CaseWebTransformer;
use App\Transformers\LabWebTransformer;
use App\Cases;
use App\User;
use App\Traits\CaseLogT;

class LabController extends Controller
{
    protected $labWebTransformer;
    protected $casesWebTransformer;

    use CaseLogT;

    public function __construct()
    {
        $this->casesWebTransformer = new CaseWebTransformer;
        $this->labWebTransformer = new LabWebTransformer;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(request()->ajax() || request()->wantsJson()){
            $clinicId = auth()->user()->id;

            $sort= request()->has('sort')?request()->get('sort'):'id';
            $order= request()->has('order')?request()->get('order'):'desc';
            $search= request()->has('searchQuery')?request()->get('searchQuery'):'';

           
            if(request()->has('sort') && request()->get('sort') == 'Name'){
                $sort = "full_name";
            }

            if(request()->has('sort') && request()->get('sort') == 'Phone'){
                $sort = "contact_number";
            }
            
            $clinic=
                User::where('r.role_id','5')
                ->where('l.clinic_id',$clinicId)
                ->join('role_user as r', 'users.id' ,'=','r.user_id')
                ->join('clinic_labs as l', 'users.id' ,'=','l.lab_id')

                ->whereHas('roles',function($query) use ($search)
                {
                    $query->where('name','Lab');
                    if ($search) {
                        $query->where('users.first_name','like',"$search%")
                            ->orWhere('users.last_name','like',"$search%")
                            ->orWhere('users.email','like',"$search%")
                            ->orWhere('users.full_name','like',"$search%");
                    }
                    
                })

                ->select('users.*')->orderBy("$sort", "$order")->paginate(10);

               
       
            $paginator=[
                'total_count'  => $clinic->total(),
                'total_pages'  => $clinic->lastPage(),
                'current_page' => $clinic->currentPage(),
                'limit'        => $clinic->perPage()
            ];
            return response([
                "data"        => $this->labWebTransformer->transformCollection($clinic->all()),
                "paginator"   => $paginator,
                "status_code" => 200
            ],200);

            
        }
        return view('clinic.labs.list');
    }

    public function labcaselibrary($id){
        $clinicId = auth()->user()->id;

         if(request()->ajax() || request()->wantsJson()){
            $sort= request()->has('sort')?request()->get('sort'):'id';
            $order= request()->has('order')?request()->get('order'):'desc';
            $search= request()->has('searchQuery')?request()->get('searchQuery'):'';
            
            if($sort == 'patient_Name'){
                $sort = 'first_name';
            }

            $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
            ->with('images')
            ->where(function ($query) use($search){
                
                $query->where('d.first_name','like',"$search%")
                      ->orWhere('l.full_name','like',"$search%")
                      ->orWhere('cases.first_name','like',"$search%")
                      ->orWhere('cases.last_name','like',"$search%")
                      ->orWhere('cases.case_code','like',"$search%")
                      ->orWhere('cases.status','like',"$search%");
            })
            ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
            ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
            ->where('cases.clinic_id',$clinicId)
            ->where('cases.lab_id',$id)
            ->orderBy($sort,$order)->get();
            

            
            return response([
                "data"        => $this->casesWebTransformer->transformCollection($cases->all()),
                "status_code" => 200
            ],200);

            
        }
        $lab     = User::find($id)->toarray();
        $lab_name = $lab['full_name'];
        return view('clinic.labs.labcaselist',compact('id','lab_name'));
    }

    public function caselibrary(){
        $clinicId = auth()->user()->id;
         if(request()->ajax() || request()->wantsJson()){
            $sort= request()->has('sort')?request()->get('sort'):'id';
            $order= request()->has('order')?request()->get('order'):'desc';
            $search= request()->has('searchQuery')?request()->get('searchQuery'):'';
            
            if($sort == 'patient_Name'){
                $sort = 'first_name';
            }

            $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
            ->with('images')
            ->where(function ($query) use($search){
                
                $query->where('d.first_name','like',"$search%")
                      ->orWhere('l.full_name','like',"$search%")
                      ->orWhere('cases.first_name','like',"$search%")
                      ->orWhere('cases.last_name','like',"$search%")
                      ->orWhere('cases.case_code','like',"$search%")
                      ->orWhere('cases.status','like',"$search%");
            })
            ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
            ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
            ->where('cases.clinic_id',$clinicId)
            ->orderBy($sort,$order)->get();
            

            
            return response([
                "data"        => $this->casesWebTransformer->transformCollection($cases->all()),
                "status_code" => 200
            ],200);

            
        }
        return view('clinic.labs.caselist');   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function caseshow($id)
    {
        $clinicId = auth()->user()->id;
        $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
                ->with('images')                
                ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
                ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
                ->where('cases.id',$id)->first();
                
        return response([
            "data"        => $this->casesWebTransformer->transform($cases),
            "status_code" => 200
        ],200);
    }

    public function casefilter(Request $request)
    {
        $clinicId = auth()->user()->id;
        if(request()->ajax() || request()->wantsJson()){

            $sort= request()->has('sort')?request()->get('sort'):'id';
            $order= request()->has('order')?request()->get('order'):'desc';
            $search = request()->has('searchQuery')?request()->get('searchQuery'):'';
            $labid  = request()->has('labid')?request()->get('labid'):'';
            $doctorid  = request()->has('doctorid')?request()->get('doctorid'):'';
            if($search == "Pick-up pending"){
                $search = 'Pending';
            }
            if($search == "Delayed cases"){
                $search = 'Delayed';
            }

            $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
            ->with('images')
            ->where(function ($query) use($search,$labid,$doctorid){
                if($search != 'All'){
                    $query->where('cases.status','like',"$search%");
                }
                
                if($labid != '' ){
                    $query->where('cases.lab_id','=',"$labid");
                }

                if($doctorid != ''){
                    $query->where('cases.doctor_id','=',"$doctorid");
                }
             })
            ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
            ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
            ->where('cases.clinic_id',$clinicId)
            ->orderBy($sort,$order)
            ->get();
            

            
            return response([
                "data"        => $this->casesWebTransformer->transformCollection($cases->all()),
                "status_code" => 200
            ],200);

            
        }
    }

     public function updateStatus(Request $request)
    {
       $clinicId = auth()->user()->id; 
       $cases = Cases::where('cases.id',$request->id)->where('cases.case_code',$request->case_code)->where('cases.clinic_id',$clinicId)->first();
       
       $cases->status = 'Delivered to patient';
       $cases->save();


       $clinic = User::with('clinicDetail')->find($clinicId)->toarray();
       $clinic_name = $clinic['clinic_detail']['clinic_name'];

       $this->addCaseLog($cases->id,9,'user',$clinic_name);

       return response([
                 "status_code" => 200
            ],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
