<?php

namespace App\Http\Controllers\Clinic;
use App\Cases;
use App\Delaybox;
use App\ClinicDetail;
use App\User;
use App\Image;
use Illuminate\Http\Request;
use App\Transformers\CaseWebTransformer;
use App\Http\Controllers\Controller;
use App\Transformers\CaseTransformer;
use App\Caselog;

class CaseController extends Controller
{
    protected $delaycasesTransformer;
    protected $casesWebTransformer;

    public function __construct()
    {
        $this->middleware('auth:clinic');
        $this->casesWebTransformer   =  new CaseWebTransformer;
        $this->delaycasesTransformer =  new CaseTransformer;
    }

    public function delayBoxCase()
    { 

        if(request()->ajax() || request()->wantsJson()){
            $clinicId = auth()->user()->id;

            $cases = Delaybox::select('c.*','delayboxes.remarks','delayboxes.expeted_date','delayboxes.due_date','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
                    ->where('delayboxes.clinic_id', $clinicId)
                    ->leftJoin('cases as c', 'c.case_code', '=', 'delayboxes.case_code')
                    ->leftJoin('users as d', 'c.doctor_id', '=', 'd.id')
                    ->leftJoin('users as l', 'c.lab_id', '=', 'l.id')
                    ->orderBy('id','desc')->paginate(15);

        #mprd($cases->toArray());
            
            $paginator =[
                    'total_count'  => $cases->total(),
                    'total_pages'  => $cases->lastPage(),
                    'current_page' => $cases->currentPage(),
                    'limit'        => $cases->perPage()
                ];

            if($cases)
            {
                return response([
                    "data"          =>  $this->delaycasesTransformer->transformCollection($cases->all()),
                    "paginator"     =>  $paginator ,               
                    "message"       =>  trans('api.case_list'),
                    "status_code"   =>  200,
                ],200);
            }
            else{
              return response ([
                    "message" => trans('api.no_Data'),
                    "status" => 202,
                ],202);
            }
        }
        return view('clinic.labs.delaybox');
    }



    public function doctorcaselibrary($id){
        
        $clinicId = auth()->user()->id;

         if(request()->ajax() || request()->wantsJson()){
            $sort= request()->has('sort')?request()->get('sort'):'first_name';
            $order= request()->has('order')?request()->get('order'):'asc';
            $search= request()->has('searchQuery')?request()->get('searchQuery'):'';
            
            if($sort == 'patient_Name'){
                $sort = 'first_name';
            }

            $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
            ->with('images')
            ->where(function ($query) use($search){
                
                $query->where('d.first_name','like',"$search%")
                      ->orWhere('l.full_name','like',"$search%")
                      ->orWhere('cases.first_name','like',"$search%")
                      ->orWhere('cases.last_name','like',"$search%")
                      ->orWhere('cases.case_code','like',"$search%")
                      ->orWhere('cases.status','like',"$search%");
            })
            ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
            ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
            ->where('cases.clinic_id',$clinicId)
            ->where('cases.doctor_id',$id)
            ->orderBy($sort,$order)->get();
            

            
            return response([
                "data"        => $this->casesWebTransformer->transformCollection($cases->all()),
                "status_code" => 200
            ],200);

            
        }
        $lab     = User::find($id)->toarray();
        $lab_name = $lab['full_name'];
        return view('clinic.doctors.caselist',compact('id','lab_name'));
    }

    public function caselogs($id){

        $caseLog = Caselog::select('*')
                ->where('case_id', $id)->get();                
       
        return response([
                    "data"          =>  $caseLog,               
                    "message"       =>  trans('api.case_list'),
                    "status_code"   =>  200,
                ],200);

    }
    
    public function updateStatus(Request $request)
    {
       $labId = auth()->user()->id;
       $cases = Cases::where('cases.id',$request->id)->where('cases.case_code',$request->case_code)->where('cases.lab_id',$labId)->first();

       $cases->status = $request->status;
       $cases->save();

       if($request->status == 'In Progress' ){
            $this->addCaseLog($cases->id,6,'user',$clinic_name,$labName);
       }
       else if ($request->status == 'Ready for delivery'){
            $this->addCaseLog($cases->id,8,'user',$clinic_name,$labName);
       }


       return response([
                 "status_code" => 200
            ],200);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addtodelaybox(Request $request)
    {
        $clinicId = auth()->user()->id;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
