<?php

namespace App\Http\Controllers\Clinic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications;
use App\Transformers\NotificationTransformer;


class NotificationController extends Controller
{
   public function __construct()
    {
       $this->notificationTransformer = new NotificationTransformer;
    }

    public function index()
    {
        $clinic = auth()->user();

        

        $filedname  = "message_en";
        
        $list = Notifications::where('user_id',$clinic->id)
        ->select('user_id','case_id','case_code','push_type',"message_en as message",'id','sender_type','reciver_type','sender_id','send_from','created_at')->orderBy('id','desc')
        ->paginate(5);
       
        return response([
            "list" => $this->notificationTransformer->transformCollection($list->all()),
            "count" => $clinic->badge_count,
            "message" => trans('api.notification_list'),
            "status_code" => 200,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function badgeUpdate()
    {
        $clinic = auth()->user();
        $clinic->badge_count = 0;
        $clinic->save();
        return response([
            "status_code" => 200,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
