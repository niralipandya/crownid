<?php

namespace App\Http\Controllers\Clinic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Cases;
use Illuminate\Support\Facades\Storage;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($case_code = '')
    {   
        $clinicId = auth()->user()->id;
        $clinic = User::with('clinicDetail')->find($clinicId)->toarray();
        $clinic_name = $clinic['clinic_detail']['clinic_name'];
        
        if (!empty($case_code)) {
            $modelCases = Cases::where('clinic_id', $clinicId)->where('case_code', $case_code)->first();
            if (empty($modelCases)) {
                $case_code = '';
            }
        } else {
            $case_code = '';
        }

        $otherdata = [
            'caselist_image' => asset('/images/Image_from_Skype.png'),
            'chat_clinic_image' => asset('/images/11.png')
        ];

        return view('clinic.chat.list', compact('clinicId', 'clinic_name', 'case_code', 'otherdata'));
    }

    public function listAjax(Request $request)
    {
        $data = $request->all();
        
        $file_path = 'public/chat/'.$data['showchat'].'.json';
        $contents = "";
        if (Storage::exists($file_path)) {
            $contents = Storage::get($file_path);
        }
        
        return response([
            "data"        => $contents,
            "status_code" => 200
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cases(Request $request, $id)
    {
        $clinicId = auth()->user()->id;
        $clinic = User::with('clinicDetail')->find($clinicId)->toarray();
        $clinic_name = $clinic['clinic_detail']['clinic_name'];
        $cases = Cases::select('cases.*', 'd.first_name as doctor_name', 'l.full_name as lab_name', 'l.image as lab_image')
                ->with('images')
                ->where('cases.case_code', $id)
                ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
                ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
                ->orderBy('id', 'desc')->first()->toarray();

        $data = $request->all();
        $data['showchat'] = $id;

        $file_path = 'public/chat/'.$data['showchat'].'.json';
        $contents = "";
        if (Storage::exists($file_path)) {
            $contents = Storage::get($file_path);
        }
        
        return response([
            "doctor_id"   => $cases['doctor_id'],
            "doctor_name" => $cases['doctor_name'],
            "lab_id"      => $cases['lab_id'],
            "lab_name"    => $cases['lab_name'],
            "case_code"   => $cases['case_code'],
            "end_date"    => $cases['end_date'],
            "id"   => $cases['id'],
            "status"      => $cases['status'],
            "data"        => $contents,
            "status_code" => 200
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();   

        $file_path = 'public/chat/'.$data['showchat'].'.json';

        $obj1 = [];
        if (Storage::exists($file_path)) {
            $obj1 = json_decode(Storage::get($file_path));
        }

        $obj2 = $data['snapshot'];

        $contents = json_encode((object) array_merge((array) $obj1, (array) $obj2));
        
        Storage::put($file_path, $contents);

        return response([
            "status_code" => 200
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
