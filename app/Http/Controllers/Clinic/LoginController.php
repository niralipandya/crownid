<?php

namespace App\Http\Controllers\Clinic;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Hesto\MultiAuth\Traits\LogsoutGuard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'username'  => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->respondValidationError(
                trans('api.fail_validation'), $validator->messages());
        }

        // check user auth and send response
        if (auth()->attempt(['username' => $request->username, 'password' => $request->password, 'status' => 1])) 
        {
            $clinic = auth()->guard()->user();
            $item = User::find($clinic->id);
            $userRole = $item->roles()->first();
            if($userRole->name == 'Clinic'){
                
                $clinic->api_token = str_random(60);
                $clinic->save();
                
                return response([
                   "message" => trans('api.clinic_authenticated'),
                    "status"  => 200,
                ],200);
            }
        }

        return $this->respond([
            "message" => trans('api.clinic_unauthorise'),
            "status"  => 400,
        ]);
        
    }

    public function resetPassword(Request $request){
        $validator = validator()->make($request->all(), [
            'otp' => 'required',
            'username' => 'required',
            'password'  => 'required',
        ]);
        if ($validator->fails()) {
            return response([
                "message"     => trans('api.fail_validation'),$validator->messages(),
                "status_code" => 422
            ],422);
            
        }

        // check user auth and send response
        if ($user = User::where('username', $request->username)->first())
        {
            if($user->otp == $request->otp)
            {
                $user->otp = null;
                $user->password = $request->password;

                $user->save();
                return response([
                    "message"        =>trans('api.clinic_password_reset'),
                    "status_code" => 200
                ],200);
            }
            else
            {
                return response([
                    "message" => trans('api.clinic_otp_invalid'),
                    "status"  => 500,
                ],500);
            }
        }

        return response([
            "message" => trans('api.clinic_not_register'),
            "status"  => 400,
        ],400);
    }
}
