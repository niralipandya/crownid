<?php

namespace App\Http\Controllers\Clinic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Subscription;
use App\Traits\Subscriptiontrait;
use App\Traits\Purchasetrait;
use Cartalyst\Stripe\Laravel\Facades\Stripe;

class HomeController extends Controller
{
    use Subscriptiontrait,Purchasetrait;

    public function showHomePage()
    {
        return view('clinic.welcome');
    }

    public function checkislogin(){
        mprd(54); exit;
    }

    public function passwordresetabc(){
        mprd(4);
    }

    public function upgreadplans($who,$plan,$type){
        $price          = $this->getPriceDetail();
        $allFacility    = $this->getFacilityDetail();

        if(!isset($price[$who][$plan][$type])) {
            return redirect('/clinic');
        }

        $typesend = 'Month';
        $paymentdurationtext = 'mo';
        $duration            = 1;
        $amount =  $price[$who][$plan][$type];
        if($type == 'annual'){
          $amount =  $price[$who][$plan][$type] * 12;
          $duration       = 12;
          $paymentdurationtext = 'yr';
          $typesend = 'Year';
        }
        $date           = date('m/d') ; 
        $trialEnd       = date("m/d", strtotime("+1 month", strtotime($date)));
        
        $planname       = $who.' '.$plan.' '.$type;
        $fullPlanname   = $plan.' '.$type.' - $'.$price[$who][$plan][$type].'/mo  '.$amount.' billed '.$paymentdurationtext; 

        $facilityArray  = $allFacility[$who][$plan][$type] ;
        
        return view('clinic.upgreadplans',compact('amount','date','trialEnd','duration','planname','fullPlanname','facilityArray','plan','type','who','typesend'));  
    }

    public function upgreaddoctorplan($who,$plan,$type){ 
        $price          = $this->getPriceDetail();
        $allFacility    = $this->getFacilityDetail();

        if(!isset($price[$who][$plan][$type])) {
            return redirect('/clinic');
        }

        $typesend = 'Month';
        $paymentdurationtext = 'mo';
        $duration            = 1;
        $amount =  $price[$who][$plan][$type];
        if($type == 'annual'){
          $amount =  $price[$who][$plan][$type] * 12;
          $duration       = 12;
          $paymentdurationtext = 'yr';
          $typesend = 'Year';
        }
        $date           = date('m/d') ; 
        $trialEnd       = date("m/d", strtotime("+1 month", strtotime($date)));
        
        $planname       = $who.' '.$plan.' '.$type;
        $fullPlanname   = $plan.' '.$type.' - $'.$price[$who][$plan][$type].'/mo  '.$amount.' billed '.$paymentdurationtext; 

        $facilityArray  = $allFacility[$who][$plan][$type] ;
        
        return view('clinic.upgreaddoctorplan',compact('amount','date','trialEnd','duration','planname','fullPlanname','facilityArray','plan','type','who','typesend'));  
    }

    public function doctor_pro($who,$plan,$type){
        
        $price          = $this->getPriceDetail();
        $allFacility    = $this->getFacilityDetail();

        if(!isset($price[$who][$plan][$type])) {
            return redirect('/clinic');
        }

        $typesend = 'Month';
        $paymentdurationtext = 'mo';
        $duration            = 1;
        $amount =  $price[$who][$plan][$type];
        if($type == 'annual'){
          $amount =  $price[$who][$plan][$type] * 12;
          $duration       = 12;
          $paymentdurationtext = 'yr';
          $typesend = 'Year';
        }
        $date           = date('m/d') ; 
        $trialEnd       = date("m/d", strtotime("+1 month", strtotime($date)));
        
        $planname       = $who.' '.$plan.' '.$type;
        $fullPlanname   = $plan.' '.$type.' - $'.$price[$who][$plan][$type].'/mo  '.$amount.' billed '.$paymentdurationtext; 

        $facilityArray  = $allFacility[$who][$plan][$type] ;
        #mprd($planname = $this->getPlanName($who,$plan,$request->type));
        return view('clinic.purchase',compact('amount','date','trialEnd','duration','planname','fullPlanname','facilityArray','plan','type','who','typesend'));
    }

    public function getPriceDetail(){


        $priceData = [];

        $priceData['clinic']['basic']['free']    = '0';
        $priceData['clinic']['pro']['monthly']   = '24.99';
        $priceData['clinic']['pro']['annual']    = '19.99';
        $priceData['clinic']['premium']['annual']= '24.99';

        $priceData['doctor']['basic']['free']    = '0';
        $priceData['doctor']['pro']['monthly']   = '13.99';
        $priceData['doctor']['pro']['annual']    = '9.99';

        if(request()->ajax() || request()->wantsJson()){
            return response([
                "data"        => $priceData,
                "status_code" => 200
            ],200);
        }

        return $priceData;

    }

    public function getFacilityDetail(){


        $facilityData = [];

        $facilityData['clinic']['basic']['free']    = [
                        'apply'      =>  "1 user/doctor*",
                        'apply1'     =>  "3 credits/month to use with 3 impressions*",
                        'apply2'     =>  "Unlimited labs",
                        'apply3'     =>  "Access to all functionalities",
                        'not-apply'  =>  "Access to default technical values*"
                          ];
        $facilityData['clinic']['pro']['monthly']   = [
                        'apply'      =>  "Up to 3 users/doctors*",
                        'apply1'     =>  "10 credits/month to use with 10 impressions*",
                        'apply2'     =>  "Unlimited labs",
                        'apply3'     =>  "Access to all functionalities",
                        'apply4'     =>  "Access to default technical values*",
                        'apply5'     =>  "Can purchase extra credits in bundles"
                          ];
        $facilityData['clinic']['pro']['annual']    = [
                        'apply'      =>  "Up to 3 users/doctors*",
                        'apply1'     =>  "10 credits/month to use with 10 impressions*",
                        'apply2'     =>  "Unlimited labs",
                        'apply3'     =>  "Access to all functionalities",
                        'apply4'     =>  "Access to default technical values*",
                        'apply5'     =>  "Can purchase extra credits in bundles"
                          ];
        $facilityData['clinic']['premium']['annual']= [
                        'apply'      =>  "Unlimited users/doctors*",
                        'apply1'     =>  "30 credits/month to use with 30 impressions*",
                        'apply2'     =>  "Unlimited labs",
                        'apply3'     =>  "Access to all functionalities",
                        'apply4'   =>  "Access to default technical values*",
                        'apply5'     =>  "Can purchase extra credits in bundles"
                          ];

        $facilityData['doctor']['basic']['free']    = [
                        'apply'      =>  "Active on Dr's personal device",
                        'apply1'     =>  "Unlimited clinics*",
                        'apply2'     =>  "Unlimited access to cases through chat interface",
                        'apply3'     =>  'Access to group chats with clinics and labs',
                        'not-apply'  =>  'Can start a chat',
                        'not-apply1' =>  'Can see all Dr. cases details'
                          ];
        $facilityData['doctor']['pro']['monthly']   = [
                        'apply'      =>  "Active on Dr's personal device",
                        'apply1'     =>  "Unlimited clinics*",
                        'apply2'     =>  "Unlimited access to cases through chat interface",
                        'apply3'     =>  'Access to group chats with clinics and labs',
                        'apply4'     =>  'Can start a chat',
                        'apply5'    =>  'Can see all Dr. cases details'
                          ];
        $facilityData['doctor']['pro']['annual']    = [
                            "Active on Dr's personal device",
                            "Unlimited clinics*",
                            "Unlimited access to cases through chat interface",
                            'Access to group chats with clinics and labs',
                            'Can start a chat',
                            'Can see all Dr. cases details'
                          ];

        if(request()->ajax() || request()->wantsJson()){
            return response([
                "data"        => $facilityData,
                "status_code" => 200
            ],200);
        }

        return $facilityData;

    }

    public function check_email(Request $request){
        $validator = validator()->make($request->all(), [
            'email' => 'required|email',
        ]);

        // if error than response error with its message
        if($validator->fails()) 
        {
            return 'false';
        }

        // check email is registered or not
        $clinic = User::where('email', $request->email)->first();

        if($clinic)
        {
           return 'false';
        }
        else
        {
            return 'true';
        }
    }

    public function check_username(Request $request){ 
        $validator = validator()->make($request->all(), [
            'username' => 'required',
        ]);

        // if error than response error with its message
        if($validator->fails()) 
        {
            return 'false';
        }

        // check email is registered or not
        $clinic = User::where('username', $request->username)->first();

        if($clinic)
        {
           return 'false';
        }
        else
        {
            return 'true';
        }
    }

    public function create_clinic(Request $request){
        #mprd($request->all());

        try{
            
            $stripe = Stripe::make(env('STRIPE_SECRET'));
            $token = $stripe->tokens()->create([
                'card' => [
                    'number'    => '4242424242424242',
                    'exp_month' => 10,
                    'cvc'       => 314,
                    'exp_year'  => 2020,
                ],
            ]);
            //return $token['id'];
        } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) { 
            flash('Invalid card details','danger');
            return back()->withErrors($validator)->withInput();
        } catch (Exception $e) {
            flash('Something went wrong with your card details.','danger');
            return back()->withErrors($validator)->withInput();
        }

        $validator = validator()->make($request->all(), [
            'last_name'   =>'required|max:255',
            'first_name'   =>'required|max:255',
            'clinic_name' => 'required',
            'password' => 'required',
            'address' => 'required',
            'city' => 'required',
            'country' => 'required',
            'contact_number' => 'required',
        ]);
      
        if ($validator->fails()) {
            flash(trans("messages.parameters-fail-validation"),'danger');
            return back()->withErrors($validator)->withInput();
        }

        $clinicemail = User::where('email', $request->email)->first();

        if($clinicemail)
        {
           flash(trans("api.email_registered"),'danger');
            return back()->withErrors($validator)->withInput();
        }

        $clinicusername = User::where('username', $request->username)->first();
        if($clinicusername)
        {
           flash('Username already exist.','danger');
            return back()->withErrors($validator)->withInput();
        }

        $price          = $this->getPriceDetail();
     // mprd($price[$request->user_type][$request->plan][$request->type]);
        if(!isset($price[$request->user_type][$request->plan][$request->pricetype])) {
            flash('Invaid Plan.','danger');
            return back()->withErrors($validator)->withInput();
        }

        $amount  = $price[$request->user_type][$request->plan][$request->pricetype];
        //check payment part

        $stripe = Stripe::make(env('STRIPE_SECRET'));
        $customer = $stripe->customers()->create([
            'email' => $request->email,
        ]);

        $stripe_id  =  $customer['id'];
        $card = $stripe->cards()->create($stripe_id, $token['id']);
        /**/

        $receiptData = array();

        $clinic = User::create([
            "email"        => $request->email,
            "is_purchased" => 1,
        ]);
        
        $clinic->assignRole('Clinic');
        $planname = $this->getPlanName($request->user_type,$request->plan,$request->pricetype);
        if($planname != false){ 
        $subscription = $stripe->subscriptions()->create(
            $stripe_id, [
                'plan' => $planname,
            ]);

            $returnData['auto_renew_status']        = true;
            $returnData['transaction_id']           = $subscription['id'];
            $returnData['original_transaction_id']  = 456;
            $returnData['is_trial_period']          = true;
            $returnData['expires_date']             = date('Y-m-d H:i:00', $subscription['current_period_end']);
            $returnData['next_bill_date']           = date('Y-m-d H:i:00', $subscription['current_period_end']);
            $returnData['product_id']               = $planname;
        }



        $this->newUser($clinic->id,$request->plan,$request->user_type,@$request->type,$amount,'$','web',0,$request->receipt,0,$returnData);

        $token_count = $this->get_signup_token($request->plan);
        $doctor_count = $this->get_signup_doctor($request->plan);
        $message = 'Free Trial';
        $remark  = 'Free Trial';
        $paid_from = 'create-web';
        $this->addToken($clinic,$token_count,$request->user_type,'free',$message,$paid_from,0,$remark,0,"$");
        $this->addDoctor($clinic,$doctor_count,$request->user_type,'free',$message,$paid_from,0,$remark,0,"$");

        $clinic->case_token_free    = $clinic->case_token_free + $token_count;
        $clinic->case_token_total   = $clinic->case_token_bylab + $clinic->case_token_free + $clinic->case_token_purchased;

        $clinic->doctor_token_free  = $clinic->doctor_token_free + $token_count;
        $clinic->doctor_token_total = $clinic->doctor_token_free + $clinic->doctor_token_purchased;

        $clinic->first_name     = $request->first_name;
        $clinic->last_name      = $request->last_name;
        $clinic->username       = $request->username;
        $clinic->contact_number = $request->contact_number;
        $clinic->password       = $request->password;
        $clinic->stripe_id       = $stripe_id;
        $clinic->save();

        $clinicDetail = new \App\ClinicDetail;
        $clinicDetail->clinic_name    = $request->clinic_name;
        $clinicDetail->address        = $request->address;
        $clinicDetail->city           = $request->city;
        $clinicDetail->country        = $request->country;
        $clinic->saveClinicDetail($clinicDetail);

        return redirect('/clinic/success');
    }

    function success(){ 
        return view('clinic.success');
    }

    /*$planname = $this->getPlanName($who,$plan,$type);
        if($planname != false){
            try{
                


                $stripe = Stripe::make(env('STRIPE_SECRET'));


                $subscription = $stripe->subscriptions()->find('cus_DEsi3EBLbSYCU4', 'sub_DG1FIk7mB7OWJ3'); sub_DG1FIk7mB7OWJ3*

                mpr( 'billing_cycle_anchor------'.date('Y-m-d H:i:00', $subscription['billing_cycle_anchor']) );
                mpr( 'created------'.date('Y-m-d H:i:00', $subscription['created']));
                mpr( 'current_period_end------'.date('Y-m-d H:i:00', $subscription['current_period_end']));
                mpr( 'current_period_start------'.date('Y-m-d H:i:00', $subscription['current_period_start']));
                mpr( 'start------'.date('Y-m-d H:i:00', $subscription['start']));
               
                mprd($subscription);

                $subscription = $stripe->subscriptions()->create(
                    'cus_DEsi3EBLbSYCU4', [
                        'plan' => $planname,
                        'trial_period_days' => '30'
                    ]);

                $returnData['auto_renew_status']        = true;
                $returnData['transaction_id']           = $subscription['id'];
                $returnData['original_transaction_id']  = 456;
                $returnData['is_trial_period']          = true;
                $returnData['expires_date']             = date('Y-m-d H:i:00', $subscription['current_period_end']);
                $returnData['next_bill_date']           = date('Y-m-d H:i:00', $subscription['current_period_end']);
                $returnData['product_id']               = $planname;

                echo date('Y-m-d H:i:00', $subscription['trial_end']);
                mprd($subscription);

            } catch (\Stripe\Error\Base $e) {
                return $e->getMessage();
            } catch (Exception $e) {
                return "Server error!";
            }

        }
        

        exit;
*/
}
