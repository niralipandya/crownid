<?php

namespace App\Http\Controllers\ClinicAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use App\User;
use App\Mail\UserResetPassword;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('clinic.guest');
    }


    public function sendResetLinkEmail(Request $request){
       $validator = validator()->make($request->all(), [
            'username'  => 'required'
        ]);
        if ($validator->fails()) {
            return $this->respondValidationError(
                trans('api.fail_validation'),$validator->messages());
        }

        // check user auth and send response
        if ($user = User::where('username', $request->username)->first())
        {
            // save otp in user table
            $user->otp = rand(11111, 99999);
            $user->save();

            Mail::to($user)->send(new UserResetPassword($user));

            return response([
                "data" => [
                    "username" => $request->username,
                ],
                "message" => trans('api.clinic_forgot_password'),
                "status_code" => 200
            ],200);


        }

        return response([
            "message" => trans('api.clinic_not_register'),
            "status_code"  => 202,
        ],202);
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('clinic.auth.passwords.email');
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('clinics');
    }
}
