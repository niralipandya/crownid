<?php

namespace App\Http\Controllers\lab;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Cases;
use App\ClinicDetail;
use App\Transformers\ClinicAdminTransformer;
use App\Transformers\CaseWebTransformer;

class ClinicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct()
    {
        $this->clinicsTransformer = new ClinicAdminTransformer;
        $this->casesWebTransformer = new CaseWebTransformer;
    }

    public function cliniccaselibrary($id){
        $clinicId = auth()->user()->id;

         if(request()->ajax() || request()->wantsJson()){
            $sort= request()->has('sort')?request()->get('sort'):'id';
            $order= request()->has('order')?request()->get('order'):'desc';
            $search= request()->has('searchQuery')?request()->get('searchQuery'):'';
            
            if($sort == 'patient_Name'){
                $sort = 'first_name';
            }

            $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
            ->with('images')
            ->where(function ($query) use($search){
                
                $query->where('d.first_name','like',"$search%")
                      ->orWhere('l.full_name','like',"$search%")
                      ->orWhere('cases.first_name','like',"$search%")
                      ->orWhere('cases.last_name','like',"$search%")
                      ->orWhere('cases.case_code','like',"$search%")
                      ->orWhere('cases.status','like',"$search%");
            })
            ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
            ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
            ->where('cases.lab_id',$clinicId)
            ->where('cases.clinic_id',$id)
            ->orderBy($sort,$order)->get();
            

            
            return response([
                "data"        => $this->casesWebTransformer->transformCollection($cases->all()),
                "status_code" => 200
            ],200);

            
        }
        $clinic_detail =  ClinicDetail::where('user_id', $id)->first();
        $clinic_name   =  $clinic_detail->clinic_name;
        return view('lab.clinic.caselist',compact('id','clinic_name'));
    }

    
    public function index()
    {
        if(request()->ajax() || request()->wantsJson()){
            $clinicId = auth()->user()->id;
            $sort= request()->has('sort')?request()->get('sort'):'first_name';
            $order= request()->has('order')?request()->get('order'):'asc';
            $search= request()->has('searchQuery')?request()->get('searchQuery'):'';

           
            if(request()->has('sort') && request()->get('sort') == 'Name'){
                $sort = "full_name";
            }

            if(request()->has('sort') && request()->get('sort') == 'Phone'){
                $sort = "contact_number";
            }
            


            $clinic =\DB::table('clinic_labs')
            ->select('clinic_labs.*','users.*','clinic_details.*')
            ->where('clinic_labs.lab_id','=',$clinicId)
            ->where(function ($query) use($search){
                
                $query->where('clinic_details.clinic_name','like',"%$search%")
                      ->orWhere('users.email','like',"%$search%")
                      ->orWhere('users.full_name','like',"%$search%")
                      ->orWhere('users.first_name','like',"%$search%")
                      ->orWhere('users.last_name','like',"%$search%");
            })

            ->leftJoin('users', 'users.id', '=', 'clinic_labs.clinic_id')
            ->leftJoin('clinic_details', 'clinic_details.user_id', '=', 'users.id')
            ->orderBy('created_at', 'desc')
            ->paginate(10);


            
       
            $paginator=[
                'total_count'  => $clinic->total(),
                'total_pages'  => $clinic->lastPage(),
                'current_page' => $clinic->currentPage(),
                'limit'        => $clinic->perPage()
            ];
            return response([
                "data"        => $this->clinicsTransformer->transformCollection($clinic->all()),
                "paginator"   => $paginator,
                "status_code" => 200
            ],200);

            
            }

        return view('lab.clinic.list');    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
