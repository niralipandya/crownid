<?php

namespace App\Http\Controllers\lab;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Cases;
use App\ClinicDetail;
use App\Transformers\ClinicAdminTransformer;
use App\Transformers\CaseCalenderTransformer;
use App\Transformers\CaseWebTransformer;
use App\Traits\DateFormate;
use App\Traits\CaseLogT;

class DashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use DateFormate,CaseLogT;
    
    public function __construct()
    {
        $this->clinicsTransformer       = new ClinicAdminTransformer;
        $this->caseCalenderTransformer  = new CaseCalenderTransformer;
        $this->casesWebTransformer      = new CaseWebTransformer;
    }

    public function index()
    {    
        $labId = auth()->user()->id;
        $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
            ->with('images')            
            ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
            ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
            ->where('cases.lab_id',$labId)
            ->where('cases.status','!=','Repetition')
            ->orderBy('id','desc')->get();

        $allcase = $this->casesWebTransformer->transformCollection($cases->all());

        $pendingcasecount = Cases::where('cases.lab_id',$labId)
            ->where('cases.status','=','Pending')->count();

        #$pendingcasecount = count($cases);
        #mprd($pendingcasecount);
        return view('lab.home',compact('allcase','pendingcasecount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pendingCaseList()
    {
        $labId = auth()->user()->id;
        $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
            ->with('images')            
            ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
            ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
            ->where('cases.lab_id',$labId)
            ->where('cases.status','Pending')
            ->orderBy('id','desc')->get();

        $allcase = $this->casesWebTransformer->transformCollection($cases->all());
        return response([
                    "data"          =>  $allcase,             
                    "message"       =>  trans('api.case_list'),
                    "status_code"   =>  200,
                ],200);
    }


    public function calender()
    {


        return view('lab.calender');
    }

    public function caselibrary(){
        $clinicId = auth()->user()->id;
         if(request()->ajax() || request()->wantsJson()){
            

            
        }
        return view('labs.caselist');   
    }

    public function calendercaselist (){
        $clinicId = auth()->user()->id;
        $cases = Cases::select('cases.case_code','cases.status','cases.end_date')
            ->where('cases.lab_id',$clinicId)
            ->where('cases.status','!=','Repetition')
            ->get();
            

            
        return response([
            "data"        => $this->caseCalenderTransformer->transformCollection($cases->all()),
            "status_code" => 200
        ],200);
        
    }
}
