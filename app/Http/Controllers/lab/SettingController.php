<?php

namespace App\Http\Controllers\lab;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('lab.settings.list');
    }

    public function getData()
    {
        $clinicId = auth()->user()->id;
        $clinic     = User::with('clinicDetail')->find($clinicId)->toarray();
        #mprd($clinic);

        $date_format = $clinic['date_format'];

        switch ($date_format) {
            case "dd-MM-yyyy":
                $dateformat = "option1";
                break;
            case "MM-dd-yyyy":
                $dateformat = "option2";
                break;
            case "dd-MM-yy":
                $dateformat = "option3";
                break;
            default:
                $dateformat = "option4";
        }



        #$dateformat = $clinic['date_format']  == 'dd-MM-yyyy' ? 'option1' : 'option2';
        #mprd($clinic['date_format']);
        return response([
                "preferred_metal"       => $clinic['preferred_metal'],
                "prosthetic_margins"    => $clinic['prosthetic_margins'],
                'delivery'              => $clinic['delivery'],
                'language'              => 'english',
                'dateformat'            => $dateformat,
                "status_code" => 200
            ],200);
    }

     public function setDateFormat(Request $request)
    {
        $clinicId = auth()->user()->id;
        $clinic     = User::where('id',$clinicId)->first();
        #$dateformat = $request->dateformat  == 'option1' ? 'dd-MM-yyyy' : 'MM-dd-yy';

        $date_format = $request->dateformat;

        switch ($date_format) {
            case "option1":
                $dateformat = "dd-MM-yyyy";
                break;
            case "option2":
                $dateformat = "MM-dd-yyyy";
                break;
            case "option3":
                $dateformat = "dd-MM-yy";
                break;
            default:
                $dateformat = "MM-dd-yy";
        }


        $clinic->date_format = $dateformat;
        $clinic->save();

        

        return response([
                'dd'    =>  $dateformat,
                'dateformat'            => $request->dateformat,
                'date_format'     => $clinic->date_format,
                "status_code" => 200
            ],200);
    }

    public function setProfileData(Request $request){
        $clinicId   = auth()->user()->id;
        $clinic     = User::where('id',$clinicId)->first();

        $clinic->first_name     = $request->first_name;
        $clinic->last_name      = $request->last_name;
        $clinic->contact_number = $request->contact_number;
        $clinic->full_name      = $request->first_name.' '.$request->last_name;
        $clinic->save();

        return response([
                "first_name"        => $clinic->first_name,
                "last_name"         => $clinic->last_name,
                'contact_number'    => $clinic->contact_number,
                "status_code"       => 200,
                "message"           => 'Profile detail updated sucessfully.'
            ],200);

    }

    public function getProfileData()
    {
        $clinicId = auth()->user()->id;
        $clinic     = User::find($clinicId)->toarray();
        
        return response([
                "first_name"        => $clinic['first_name'],
                "last_name"         => $clinic['last_name'],
                'contact_number'    => $clinic['contact_number'],
                "status_code" => 200
            ],200);
    }

    public function changepasseordpage(){
        return view('lab.password');
    }

    public function checkpassword(Request $request){ 
        $password = auth()->user()->password;
        if (Hash::check($request->old_password, $password))
        {
            return response([

                "status_code" => 200
            ],200);
        }
        return response([
            "message" => trans('api.clinic_password_invalid'),            
            "status_code" => 201
        ],201);
    }

    public function changePassword(Request $request){ #mprd(2);
        $clinicId   = auth()->user()->id;
        $clinic     = User::where('id',$clinicId)->first();
        $password = auth()->user()->password;
        if (Hash::check($request->old_password, $password))
        {
            $clinic->password     = $request->password;
            $clinic->save();
            return response([
                "message" => 'Password changed successfully.',
                "status_code" => 200
            ],200);
        }
        return response([
                "message" => trans('api.clinic_password_invalid'),            
                "status_code" => 205
            ],205);
    }
}
