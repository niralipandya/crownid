<?php

namespace App\Http\Controllers\lab;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cases;
use App\Delaybox;
use App\ClinicDetail;
use App\User;
use App\Image;
use App\Transformers\CaseWebTransformer;
use App\Transformers\CaseTransformer;

class DelayboxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    protected $delaycasesTransformer;
    protected $casesWebTransformer;

    public function __construct()
    {
        $this->middleware('auth:lab');
        $this->casesWebTransformer = new CaseWebTransformer;
        $this->delaycasesTransformer =  new CaseTransformer;
    }


    public function index()
    {
        return view('lab.delaybox.list');
    }


    public function delayBoxCase()
    { 

        if(request()->ajax() || request()->wantsJson()){
            $clinicId = auth()->user()->id;

            $cases = Delaybox::select('c.*','delayboxes.remarks','delayboxes.expeted_date','delayboxes.due_date','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
                    ->where('delayboxes.lab_id', $clinicId)
                    ->leftJoin('cases as c', 'c.case_code', '=', 'delayboxes.case_code')
                    ->leftJoin('users as d', 'c.doctor_id', '=', 'd.id')
                    ->leftJoin('users as l', 'c.lab_id', '=', 'l.id')
                    ->orderBy('id','desc')->paginate(15);

        #mprd($cases->toArray());
            
            $paginator =[
                    'total_count'  => $cases->total(),
                    'total_pages'  => $cases->lastPage(),
                    'current_page' => $cases->currentPage(),
                    'limit'        => $cases->perPage()
                ];

            if($cases)
            {
                return response([
                    "data"          =>  $this->delaycasesTransformer->transformCollection($cases->all()),
                    "paginator"     =>  $paginator ,               
                    "message"       =>  trans('api.case_list'),
                    "status_code"   =>  200,
                ],200);
            }
            else{
              return response ([
                    "message" => trans('api.no_Data'),
                    "status" => 202,
                ],202);
            }
        }
        return view('lab.delaybox.list');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
