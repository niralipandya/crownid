<?php

namespace App\Http\Controllers\lab;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\CaseWebTransformer;
use App\Transformers\LabWebTransformer;
use App\Cases;
use App\User;
use App\Caselog;
use App\Delaybox;
use App\ClinicDetail;
use App\Traits\DateFormate;
use App\Traits\CaseLogT;

class LabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use DateFormate,CaseLogT;

    protected $labWebTransformer;
    protected $casesWebTransformer;

    public function __construct()
    {
        $this->casesWebTransformer = new CaseWebTransformer;
        $this->labWebTransformer = new LabWebTransformer;
    }

    
    public function profile(){
      return view('lab.profile');
    }


    public function index()
    {
       // phpinfo();
        return view('lab.caselist.list');
    }

    public function caselibrary(){
        $clinicId = auth()->user()->id;
         if(request()->ajax() || request()->wantsJson()){
            $sort= request()->has('sort')?request()->get('sort'):'id';
            $order= request()->has('order')?request()->get('order'):'desc';
            $search= request()->has('searchQuery')?request()->get('searchQuery'):'';
            
            if($sort == 'patient_Name'){
                $sort = 'first_name';
            }

            $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
            ->with('images')
            ->where(function ($query) use($search){
                
                $query->where('d.first_name','like',"$search%")
                      ->orWhere('l.full_name','like',"$search%")
                      ->orWhere('cases.first_name','like',"$search%")
                      ->orWhere('cases.last_name','like',"$search%")
                      ->orWhere('cases.case_code','like',"$search%")
                      ->orWhere('cases.status','like',"$search%");
            })
            ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
            ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
            ->leftJoin('users as c', 'cases.clinic_id', '=', 'c.id')
            ->where('cases.lab_id',$clinicId)
            ->orderBy($sort,$order)->get();
            

            
            return response([
                "data"        => $this->casesWebTransformer->transformCollection($cases->all()),
                "status_code" => 200
            ],200);

            
        }
        return view('lab.caselist.list');   
    }

    public function caseshow($id)
    {
        $clinicId = auth()->user()->id;
        $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
                ->with('images')                
                ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
                ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
                ->where('cases.id',$id)->first();
                
        return response([
            "data"        => $this->casesWebTransformer->transform($cases),
            "status_code" => 200
        ],200);
    }

    public function casefilter(Request $request)
    {
        $clinicId = auth()->user()->id;
        if(request()->ajax() || request()->wantsJson()){

            $sort= request()->has('sort')?request()->get('sort'):'id';
            $order= request()->has('order')?request()->get('order'):'desc';
            $search = request()->has('searchQuery')?request()->get('searchQuery'):'';
            $labid  = request()->has('labid')?request()->get('labid'):'';
            $doctorid  = request()->has('doctorid')?request()->get('doctorid'):'';
            $clinicid  = request()->has('clinicid')?request()->get('clinicid'):'';
            if($search == "Pick-up pending"){
                $search = 'Pending';
            }

            $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
            ->with('images')
            ->where(function ($query) use($search,$labid,$doctorid,$clinicid){
                if($search != 'All'){
                    $query->where('cases.status','like',"$search%");
                }
                
                if($labid != '' ){
                    $query->where('cases.lab_id','=',"$labid");
                }

                if($clinicid != ''){
                    $query->where('cases.clinic_id','=',"$clinicid");
                }

                if($doctorid != ''){
                    $query->where('cases.doctor_id','=',"$doctorid");
                }
             })
            ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
            ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
            ->where('cases.lab_id',$clinicId)
            ->orderBy($sort,$order)
            ->get();
            

            
            return response([
                "data"        => $this->casesWebTransformer->transformCollection($cases->all()),
                "status_code" => 200
            ],200);

            
        }
    }

    public function receiveStatus(Request $request)
    {
       $labId = auth()->user()->id; 
       $labName = auth()->user()->full_name;
       $date  =  date("Y-m-d", strtotime($request->edd));
       $status = "Received";
       
       $cases = Cases::where('cases.id',$request->id)->where('cases.case_code',$request->case_code)->where('cases.lab_id',$labId)->first();

       $cases->end_date = $date;
       $cases->status = 'Received';
       $cases->save();

       $clinic_detail =  ClinicDetail::where('user_id', $cases->clinic_id)->first();
       $clinic_name   =  $clinic_detail->clinic_name;

       $this->addCaseLog($cases->id,5,'user',$clinic_name,$labName);

       return response([
                 "status_code" => 200
            ],200);
    }

     public function updateStatusFromDelay(Request $request)
    {
       $labId = auth()->user()->id;
       $labName = auth()->user()->full_name;
       $cases = Cases::where('cases.id',$request->id)->where('cases.case_code',$request->case_code)->where('cases.lab_id',$labId)->first();

       $cases->status = $request->status;
       $cases->save();

       $delaycases = Delaybox::where('delayboxes.lab_id', $labId)
                    ->where( 'delayboxes.case_code', '=', $request->case_code)->first();


       $clinic_detail =  ClinicDetail::where('user_id', $cases->clinic_id)->first();
       $clinic_name   =  $clinic_detail->clinic_name;

       if ($request->status == 'Ready for delivery'){
            $this->addCaseLog($cases->id,8,'user',$clinic_name,$labName);
            $delaycases = Delaybox::where('delayboxes.lab_id', $labId)
                    ->where( 'delayboxes.case_code', '=', $request->case_code)->first();

            if($delaycases){
                $delaycases->delete();
            }  
       }


       return response([
                 "status_code" => 200
            ],200);
    }

    public function updateStatus(Request $request)
    {
       $labId = auth()->user()->id;
       $labName = auth()->user()->full_name;
       $cases = Cases::where('cases.id',$request->id)->where('cases.case_code',$request->case_code)->where('cases.lab_id',$labId)->first();

       $cases->status = $request->status;
       $cases->save();

       $clinic_detail =  ClinicDetail::where('user_id', $cases->clinic_id)->first();
       $clinic_name   =  $clinic_detail->clinic_name;

       if($request->status == 'In Progress' ){
            $this->addCaseLog($cases->id,6,'user',$clinic_name,$labName);
       }
       else if ($request->status == 'Ready for delivery'){
            $this->addCaseLog($cases->id,8,'user',$clinic_name,$labName);

            $delaycases = Delaybox::where('delayboxes.lab_id', $labId)
                    ->where( 'delayboxes.case_code', '=', $request->case_code)->first();
           # mpr($delaycases);
            if($delaycases){
                $delaycases->delete();
            }        

       }


       return response([
                 "status_code" => 200
            ],200);
    }

     public function updateTrying(Request $request)
    {
       $labId = auth()->user()->id;
       $labName = auth()->user()->full_name;
       $cases = Cases::where('cases.id',$request->id)->where('cases.case_code',$request->case_code)->where('cases.lab_id',$labId)->first();
       #mprd($cases);
       
           $clinic_detail =  ClinicDetail::where('user_id', $cases->clinic_id)->first();
           $clinic_name   =  $clinic_detail->clinic_name;

           if($cases->is_trying != 1){
              $cases->is_trying = 1;
              $cases->trying_by = 'Lab';
              $this->addCaseLog($cases->id,10,'user',$clinic_name,$labName);
           }

          if($cases->trying_status == 'Pending'){
             $cases->trying_status = 'Ready for Trying';
             $cases->save();
             $this->addCaseLog($cases->id,13,'user',$clinic_name,$labName);
          }
       

       return response([
                 "status_code" => 200
            ],200);
    }

    public function delayed(Request $request)
    {
       $labId = auth()->user()->id;
       $cases = Cases::where('cases.case_code',$request->case_code)->where('cases.lab_id',$labId)->first();
       $labName = auth()->user()->full_name;
       $edd           = $cases->end_date;
       $expeted_date  =  date("Y-m-d", strtotime($request->expeted_date));

       $cases->status   = 'Delayed';
       $cases->end_date = $expeted_date;
       $cases->save();

       $delayBox = [
            'clinic_id'     => $cases->clinic_id,
            'lab_id'        => $labId,
            'doctor_id'     => $cases->doctor_id,
            'case_code'     => $cases->case_code,
            'due_date'      => $edd,
            'expeted_date'  => $expeted_date,
            'remarks'       => $request->remarks,
            'status'        =>  'Pending',
            'addedBy'       => $labId,
            'addedByType'   => 'lab',
       ];

       $delaycases = Delaybox::create($delayBox);

       /*add in delay table*/
       $clinic_detail =  ClinicDetail::where('user_id', $cases->clinic_id)->first();
       $clinic_name   =  $clinic_detail->clinic_name;
       $this->addCaseLog($cases->id,7,'user',$clinic_name,$labName,'','',$this->dateformate($expeted_date),$this->dateformate($edd)) ;

       return response([
                 "status_code" => 200
            ],200);
    }
     
    public function caselogs($id){

        $caseLog = Caselog::select('*')
                ->where('case_id', $id)->get();                
       
        return response([
                    "data"          =>  $caseLog,               
                    "message"       =>  trans('api.case_list'),
                    "status_code"   =>  200,
                ],200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
