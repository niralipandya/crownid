<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\ClinicDetail;
use App\Transformers\DoctorAdminTransformer;
use App\Traits\FileManipulationTrait;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Image;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use FileManipulationTrait;
    protected $user;
    protected $doctorsTransformer;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->doctorsTransformer = new DoctorAdminTransformer;
    }

    public function index()
    {
        if (\Gate::denies('developerOnly') && \Gate::denies('doctor')) {
            return back();
        }
        // If there is an Ajax request or any request wants json data
        if(request()->ajax() || request()->wantsJson()){
            $is_clinic = request()->has('is_clinic')?request()->get('is_clinic'):'no';
            $sort= request()->has('sort')?request()->get('sort'):'first_name';
            $order= request()->has('order')?request()->get('order'):'asc';
            $search= request()->has('searchQuery')?request()->get('searchQuery'):'';

           
            if($is_clinic  == 'yes'){
                $clinic=
                $this->user->where('r.role_id','4')
                ->where('d.clinic_id',$search)
                ->join('role_user as r', 'users.id' ,'=','r.user_id')
                ->join('clinic_doctor as d', 'users.id' ,'=','d.doctor_id')
                ->select('users.*')->orderBy("$sort", "$order")->paginate(10);
            }
            else{
                 $clinic= 
                $this->user->whereHas('roles',function($query) use ($search)
                {
                    $query->where('name','Doctor');
                    if ($search) {
                        $query->where('users.first_name','like',"$search%")
                            ->orWhere('users.last_name','like',"$search%")
                            ->orWhere('users.email','like',"$search%")
                            ->orWhere('users.username','like',"$search%");
                    }
                    
                })

                ->select('users.*')->orderBy("$sort", "$order")->paginate(10);
            }
       
            $paginator=[
                'total_count'  => $clinic->total(),
                'total_pages'  => $clinic->lastPage(),
                'current_page' => $clinic->currentPage(),
                'limit'        => $clinic->perPage()
            ];
            return response([
                "data"        => $this->doctorsTransformer->transformCollection($clinic->all()),
                "paginator"   => $paginator,
                "status_code" => 200
            ],200);

            
        }
        return view('admin.doctors.list');
        
    }

    public function byClinic($id)
    {
        $clinic_count =$this->user->whereHas('roles',function($query) 
            {   
                $query->where('name','Clinic');
                
            })->where('users.id',$id)->count();
        if($clinic_count == 0){
            return back();
        }
        $clinic_detail =  ClinicDetail::where('user_id', $id)->first();
        if (\Gate::denies('developerOnly') && \Gate::denies('clinic') && \Gate::denies('doctor')) {
            return back();
        }
        // If there is an Ajax request or any request wants json data
        if(request()->ajax() || request()->wantsJson()){
            $is_clinic = request()->has('is_clinic')?request()->get('is_clinic'):'no';
            $sort= request()->has('sort')?request()->get('sort'):'first_name';
            $order= request()->has('order')?request()->get('order'):'asc';
            $search= request()->has('searchQuery')?request()->get('searchQuery'):'';

           
            $clinic=
                $this->user->where('r.role_id','4')
                ->where('d.clinic_id',$id)
                ->join('role_user as r', 'users.id' ,'=','r.user_id')
                ->join('clinic_doctor as d', 'users.id' ,'=','d.doctor_id')
                ->select('users.*')->orderBy("$sort", "$order")->paginate(10);
      
            $paginator=[
                'total_count'  => $clinic->total(),
                'total_pages'  => $clinic->lastPage(),
                'current_page' => $clinic->currentPage(),
                'limit'        => $clinic->perPage()
            ];
            return response([
                "data"        => $this->doctorsTransformer->transformCollection($clinic->all()),
                "paginator"   => $paginator,
                "status_code" => 200
            ],200);

            
        }
        return view('admin.doctors.clinicdoctorlist',['clinic_id' => $id ,'clinic_name' => $clinic_detail->clinic_name ]);
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (\Gate::denies('developerOnly') && \Gate::denies('doctor')) {
            return back();
        }

        return view('admin.doctors.add',['defaultImg'=>$this->getFileUrl('user/avatar.png')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (\Gate::denies('developerOnly') && \Gate::denies('clinic')) {
            return back();
        }

        $clinic=$this->user->with('clinicDetail')->find($id);

       // mprd($clinic->clinicDetail);
        if(!$clinic){
            flash(trans("messages.clinic-not-found"),'info');
            return back();
        }



        $picture=($clinic->image==NULL)?$this->getFileUrl('user/clinics/avatar.png'):$this->getFileUrl('user/clinics/'.$id.'/'.$clinic->image);

        $userId = $id;
       // echo $picture; exit;
        return view('admin.doctors.edit',compact('clinic','picture','userId'));
    }

    public function fetchData($id)
    {
        
        $clinic=$this->user->with('clinicDetail')->find($id);

        if(!$clinic){
            flash(trans("messages.clinic-not-found"),'info');
            return back();
        }

        return response([
            "data"        => $clinic,
            "status_code" =>200
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (\Gate::denies('developerOnly') && \Gate::denies('doctor')) {
            return back();
        }

        $clinic=$this->user->find($id);
        
        if($clinic->image!=NULL)
            $this->destoryFile('user/clinics/'.$clinic->id.'/'.$clinic->image);


        $clinic->delete();
        return response([
            "data"=>[],
            "message"=>trans('messages.clinic-distroy'),
            "status_code"=>200
        ],200);
    }

    public function destroyBulk(Request $request){
        if (\Gate::denies('developerOnly') && \Gate::denies('doctor')) {
            return back();
        }

        $deleteArr = $request->all();
        for($i=0;$i<count($deleteArr);$i++)
        {
                $clinic=$this->user->find($deleteArr[$i]);
        
                if($clinic->image!=NULL)
                    $this->destoryFile('user/clinics/'.$clinic->id.'/'.$clinic->image);

                $clinic->delete();
        }

        return response([
            "data"=>[],
            "message"=>trans('messages.clinic-distroy'),
            "status_code"=>200
        ],200);
    }

    public function switchStatus(Request $request){
        if (\Gate::denies('developerOnly') && \Gate::denies('doctor')) {
            return back();
        }
        $validator = validator()->make($request->all(), [
            'id'   =>'required'
        ]);
        if ($validator->fails()) {
            return response(["error"=>trans('messages.parameters-fail-validation')],422);
        }
        extract($request->all());
        $clinic= $this->user->find($id);
        if($clinic){
            $newStatus = ($clinic->status==1)?0:1;
            $clinic->status=$newStatus;
            $clinic->save();
            // Get New updated Object of User
            $updated = $clinic;

            if($request->wantsJson()){
                return response([
                    "data"        =>$this->doctorsTransformer->transform($updated),
                    "message"     =>trans('messages.clinic-status',["status"=>$newStatus]),
                    "status_code" =>200
                ],200);
            }
            flash(trans('messages.clinic-status'),'success');
            return back();
        }
        flash(trans('messages.clinic-update-fail'),'error');
        return back();
    }

    public function switchStatusBulk(Request $request){
        if (\Gate::denies('developerOnly') && \Gate::denies('doctor')) {
            return back();
        }
        $input= $request->all();
        if (count($input)==0) {
            return response(["error"=>trans('messages.parameters-fail-validation')],422);
        }
        $clinics= $this->user->whereIn('id',$request->all())->get();
        if($clinics->count() > 0){
            foreach ($clinics as $clinic) {
                $newStatus = ($clinic->status==1)?0:1;
                $clinic->status=$newStatus;
                $clinic->save();
            }

            if($request->wantsJson()){
                return response([
                    "data"=>[],
                    "message"     =>trans('messages.clinic-status',["status"=>"updated"]),
                    "status_code" =>200
                ],200);
            }
            flash(trans('messages.clinic-status'),'success');
            return back();
        }
        flash(trans('messages.clinic-update-fail'),'error');
        return back();
    }

    public function all(){
        $clinic=$this->user->whereHas('roles',function($query)
            {   
                $query->where('name','Doctor');
                
            })->select('users.id','users.full_name')->get();  
        
        return response([
            "data"        =>$clinic->all(),
            "status_code" =>200
        ],200);
    }  
}
