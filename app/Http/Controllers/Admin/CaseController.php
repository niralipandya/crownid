<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Cases;
use App\ClinicDetail;
use App\Transformers\CaseTransformer;
use App\Traits\FileManipulationTrait;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Image;
use App\Caselog;
class CaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use FileManipulationTrait;
    protected $user;
    protected $cases;
    protected $casesTransformer;

    public function __construct(User $user,Cases $cases)
    {
        $this->user = $user;
        $this->cases = $cases;
        $this->casesTransformer = new CaseTransformer;
    }

    public function logesDetail($id)
    { 

        $cases = Cases::select('cases.*')
                ->where('cases.id', $id)->first();
        $caseLog = Caselog::select('*')
                ->where('case_id', $id)->get();                
        $clinic_detail =  ClinicDetail::where('user_id', $cases->clinic_id)->first();
        return view('admin.cases.caselog',['clinic_id' => $id ,'clinic_name' => $clinic_detail->clinic_name ,'cases' => $cases,'caseLog' => $caseLog]);
        
    }


    public function index()
    {
        if (\Gate::denies('developerOnly') && \Gate::denies('case')) {
            return back();
        }
        // If there is an Ajax request or any request wants json data
        if(request()->ajax() || request()->wantsJson()){
            $is_clinic = request()->has('is_clinic')?request()->get('is_clinic'):'no';
            $sort= request()->has('sort')?request()->get('sort'):'first_name';
            $order= request()->has('order')?request()->get('order'):'asc';
            $search= request()->has('searchQuery')?request()->get('searchQuery'):'';
            $clinic= request()->has('clinic') ?request()->get('clinic'):'';
            $doctor= request()->has('doctor')?request()->get('doctor'):'';
            $lab   = request()->has('lab')?request()->get('lab'):'';
\DB::enableQueryLog();
           
            if($is_clinic  == 'yes' && $clinic != 'undefined' && $clinic != 'undefined' && $clinic != 'undefined' && $clinic != '' && $clinic != '' && $clinic != ''){
                
                
                $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
                ->with('images')
                ->where(function ($query) use($clinic,$doctor,$lab) {
                    if($clinic != 'undefined' ){
                        $query->orWhere('cases.clinic_id',$clinic);
                    }
                    if($doctor != 'undefined' ){
                        $query->orWhere('cases.doctor_id',$doctor);
                    }
                    if($lab != 'undefined' ){
                        $query->orWhere('cases.lab_id',$lab);
                    }
                   /* $query->where('cases.clinic_id',$clinic)
                          ->orWhere('cases.doctor_id',$doctor)
                          ->orWhere('cases.lab_id',$lab);*/
                })
                
                ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
                ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
                ->orderBy($sort,$order)->paginate(10);
            }
            else{
                if($sort == 'patient_Name'){
                        $sort = 'first_name';
                    }
                 $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
                ->with('images')
                ->where(function ($query) use($search){
                    
                    $query->where('d.first_name','like',"$search%")
                          ->orWhere('l.full_name','like',"$search%")
                          ->orWhere('cases.first_name','like',"$search%")
                          ->orWhere('cases.last_name','like',"$search%")
                          ->orWhere('cases.case_code','like',"$search%")
                          ->orWhere('cases.status','like',"$search%");
                })
                ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
                ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
                ->orderBy($sort,$order)->paginate(10);
            }
            

            $paginator=[
                'total_count'  => $cases->total(),
                'total_pages'  => $cases->lastPage(),
                'current_page' => $cases->currentPage(),
                'limit'        => $cases->perPage()
            ];
            return response([
                "data"        => $this->casesTransformer->transformCollection($cases->all()),
                "paginator"   => $paginator,
                "status_code" => 200
            ],200);

            
        }
        return view('admin.cases.list');
        
    }

    public function byClinicajax($id)
    {
            if(request()->ajax() || request()->wantsJson()){ 
            $is_clinic = request()->has('is_clinic')?request()->get('is_clinic'):'no';
            $sort= request()->has('sort')?request()->get('sort'):'first_name';
            $order= request()->has('order')?request()->get('order'):'asc';
            $search= request()->has('searchQuery')?request()->get('searchQuery'):'';

           
            $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
                ->with('images')
                ->where('cases.clinic_id',$id)
                ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
                ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
                ->orderBy('id','desc')->paginate(10);
            if($is_clinic == 'no'){
                $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
                ->with('images')
                ->where(function ($query) use($search){
                    $query->where('d.first_name','like',"$search%")
                          ->orWhere('l.full_name','like',"$search%")
                          ->orWhere('cases.first_name','like',"$search%")
                          ->orWhere('cases.last_name','like',"$search%")
                          ->orWhere('cases.case_code','like',"$search%")
                          ->orWhere('cases.status','like',"$search%");
                })
                ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
                ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
                ->orderBy('id','desc')->paginate(10);
            }
            $paginator=[
                'total_count'  => $cases->total(),
                'total_pages'  => $cases->lastPage(),
                'current_page' => $cases->currentPage(),
                'limit'        => $cases->perPage()
            ];

            return response([
                "data"        => $this->casesTransformer->transformCollection($cases->all()),
                "paginator"   => $paginator,
                "status_code" => 200
            ],200);

        }
    }

    public function byClinic($id)
    {

        $clinic_count =$this->user->whereHas('roles',function($query) 
            {   
                $query->where('name','Clinic');
                
            })->where('users.id',$id)->count();

        if($clinic_count == 0){
            return back();
        }
        $clinic_detail =  ClinicDetail::where('user_id', $id)->first();
        if (\Gate::denies('developerOnly') && \Gate::denies('case') && \Gate::denies('clinic')) {
            return back();
        }
        // If there is an Ajax request or any request wants json data
        

        return view('admin.cases.cliniccaselist',['clinic_id' => $id ,'clinic_name' => $clinic_detail->clinic_name ]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (\Gate::denies('developerOnly') && \Gate::denies('case')) {
            return back();
        }

        return view('admin.cases.add',['defaultImg'=>$this->getFileUrl('user/avatar.png')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    public function fetchData($id)
    {
        
        $cases = Cases::select('cases.*','d.first_name as doctor_name','l.full_name as lab_name','l.image as lab_image')
                ->with('images')
                ->where('cases.id', $id)
                ->leftJoin('users as d', 'cases.doctor_id', '=', 'd.id')
                ->leftJoin('users as l', 'cases.lab_id', '=', 'l.id')
                ->orderBy('id','desc')->paginate(10);
        mprd($this->casesTransformer->transformCollection($cases->all()));        
        if(!$cases){
            flash(trans("messages.clinic-not-found"),'info');
            return back();
        }

        return response([
            "data"        => $cases,
            "status_code" =>200
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (\Gate::denies('developerOnly') && \Gate::denies('case')) {
            return back();
        }

        $clinic=$this->user->find($id);
        
        if($clinic->image!=NULL)
            $this->destoryFile('user/clinics/'.$clinic->id.'/'.$clinic->image);


        $clinic->delete();
        return response([
            "data"=>[],
            "message"=>trans('messages.clinic-distroy'),
            "status_code"=>200
        ],200);
    }

    public function destroyBulk(Request $request){
        if (\Gate::denies('developerOnly') && \Gate::denies('case')) {
            return back();
        }

        $deleteArr = $request->all();
        for($i=0;$i<count($deleteArr);$i++)
        {
                $clinic=$this->user->find($deleteArr[$i]);
        
                if($clinic->image!=NULL)
                    $this->destoryFile('user/clinics/'.$clinic->id.'/'.$clinic->image);

                $clinic->delete();
        }

        return response([
            "data"=>[],
            "message"=>trans('messages.clinic-distroy'),
            "status_code"=>200
        ],200);
    }

    public function switchStatus(Request $request){
        if (\Gate::denies('developerOnly') && \Gate::denies('case')) {
            return back();
        }
        $validator = validator()->make($request->all(), [
            'id'   =>'required'
        ]);
        if ($validator->fails()) {
            return response(["error"=>trans('messages.parameters-fail-validation')],422);
        }
        extract($request->all());
        $clinic= $this->user->find($id);
        if($clinic){
            $newStatus = ($clinic->status==1)?0:1;
            $clinic->status=$newStatus;
            $clinic->save();
            // Get New updated Object of User
            $updated = $clinic;

            if($request->wantsJson()){
                return response([
                    "data"        =>$this->clinicsTransformer->transform($updated),
                    "message"     =>trans('messages.clinic-status',["status"=>$newStatus]),
                    "status_code" =>200
                ],200);
            }
            flash(trans('messages.clinic-status'),'success');
            return back();
        }
        flash(trans('messages.clinic-update-fail'),'error');
        return back();
    }

    public function switchStatusBulk(Request $request){
        if (\Gate::denies('developerOnly') && \Gate::denies('case')) {
            return back();
        }
        $input= $request->all();
        if (count($input)==0) {
            return response(["error"=>trans('messages.parameters-fail-validation')],422);
        }
        $clinics= $this->user->whereIn('id',$request->all())->get();
        if($clinics->count() > 0){
            foreach ($clinics as $clinic) {
                $newStatus = ($clinic->status==1)?0:1;
                $clinic->status=$newStatus;
                $clinic->save();
            }

            if($request->wantsJson()){
                return response([
                    "data"=>[],
                    "message"     =>trans('messages.clinic-status',["status"=>"updated"]),
                    "status_code" =>200
                ],200);
            }
            flash(trans('messages.clinic-status'),'success');
            return back();
        }
        flash(trans('messages.clinic-update-fail'),'error');
        return back();
    }
}
