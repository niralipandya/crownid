<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Transformers\ClinicAdminTransformer;
use App\Traits\FileManipulationTrait;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Image;

class ClinicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use FileManipulationTrait;
    protected $user;
    protected $clinicsTransformer;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->clinicsTransformer = new ClinicAdminTransformer;
    }

    public function index()
    {
        if (\Gate::denies('developerOnly') && \Gate::denies('clinic')) {
            return back();
        }
        // If there is an Ajax request or any request wants json data
        if(request()->ajax() || request()->wantsJson()){
            $sort= request()->has('sort')?request()->get('sort'):'clinic_name';
            $order= request()->has('order')?request()->get('order'):'asc';
            $search= request()->has('searchQuery')?request()->get('searchQuery'):'';
            $clinic=$this->user->whereHas('roles',function($query) use ($search)
            {   
                $query->where('name','Clinic');
                if ($search) {
                    $query->where('cd.clinic_name','like',"$search%")
                        ->orWhere('user.email','like',"$search%")
                        ->orWhere('user.username','like',"$search%");
                }
                
            })->join('clinic_details as cd', 'cd.user_id', '=', 'users.id')
                ->select('users.*','cd.user_id as clinic_detail_id','cd.*')->orderBy("$sort", "$order")->paginate(10);
               
            $paginator=[
                'total_count'  => $clinic->total(),
                'total_pages'  => $clinic->lastPage(),
                'current_page' => $clinic->currentPage(),
                'limit'        => $clinic->perPage()
            ];
            return response([
                "data"        => $this->clinicsTransformer->transformCollection($clinic->all()),
                "paginator"   => $paginator,
                "status_code" => 200
            ],200);

            
        }
        return view('admin.clinics.list');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (\Gate::denies('developerOnly') && \Gate::denies('clinic')) {
            return back();
        }

        return view('admin.clinics.add',['defaultImg'=>$this->getFileUrl('user/avatar.png')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (\Gate::denies('developerOnly') && \Gate::denies('clinic')) {
            return back();
        }

        $this->validate($request, [
            'clinic_name'   => 'required',
            'first_name'    => 'required',
            'last_name'     => 'required',
            'username'      => 'required|unique:users',
            'password'      => 'required',
            'pic'           => 'required',
            'contact_number'=> 'required',
            'address'       => 'required',
            'city'          => 'required',
            'email'         => 'required',
            'country'       => 'required'
        ]);

     
        
        $clinic['first_name']     = $request->first_name;
        $clinic['last_name']      = $request->last_name;
        $clinic['email']          = $request->email;
        $clinic['username']       = $request->username;
        $clinic['password']       = $request->password;
        $clinic['contact_number'] = $request->contact_number;

        $clinic = $this->user->create($clinic);

        if($request->has('pic') && $request->get('ispicchange') == true){

            $imageData = $request->get('pic');
            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
            $this->createDir('public/user/clinics/'.$clinic->id);
            Image::make($request->get('pic'))->save(storage_path('app/public/user/clinics/'.$clinic->id.'/').$fileName);
            $clinic->image=$fileName;
            $clinic->image_thumb=$fileName;
            $clinic->save();
        }
        $clinic->assignRole('Clinic');

        $clinicDetail = new \App\ClinicDetail;

        $clinicDetail->clinic_name    = $request->clinic_name;
        $clinicDetail->address        = $request->address;
        $clinicDetail->city           = $request->city;
        $clinicDetail->country        = $request->country;

        $clinic->saveClinicDetail($clinicDetail);


        return response(['message' => trans('messages.clinic-add')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (\Gate::denies('developerOnly') && \Gate::denies('clinic')) {
            return back();
        }

        $clinic=$this->user->with('clinicDetail')->find($id);

       // mprd($clinic->clinicDetail);
        if(!$clinic){
            flash(trans("messages.clinic-not-found"),'info');
            return back();
        }



        $picture=($clinic->image==NULL)?$this->getFileUrl('user/clinics/avatar.png'):$this->getFileUrl('user/clinics/'.$id.'/'.$clinic->image);

        $userId = $id;
       // echo $picture; exit;
        return view('admin.clinics.edit',compact('clinic','picture','userId'));
    }

    public function fetchData($id)
    {
        
        $clinic=$this->user->with('clinicDetail')->find($id);

        if(!$clinic){
            flash(trans("messages.clinic-not-found"),'info');
            return back();
        }

        return response([
            "data"        => $clinic,
            "status_code" =>200
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (\Gate::denies('developerOnly') && \Gate::denies('clinic')) {
            return back();
        }

        $clinic=$this->user->find($id);

        if(!$clinic){
            flash(trans("messages.clinic-not-found"),'info');
            return back();
        }
        
        // VALIDATION OF INPUT
        $this->validate($request, [
            'clinic_name'   => 'required',
            'first_name'    => 'required',
            'last_name'     => 'required',
            'username'      => 'required|unique:users,username,'.$clinic->id,
            'pic'           => 'required',
            'contact_number'=> 'required',
            'address'       => 'required',
            'city'          => 'required',
            'email'         => 'required',
            'country'       => 'required'
        ]);

        
        if($request->has('pic')){

            if($request->get('ispicchange'))
            {
                $imageData = $request->get('pic');
                $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];

                $this->createDir('public/user/clinics/'.$clinic->id);

                Image::make($request->get('pic'))->save(storage_path('app/public/user/clinics'.$clinic->id.'/').$fileName);

                $this->destoryFile('user/'.$clinic->id.'/'.$clinic->image);
                
                $clinic->image=$fileName;
                $clinic->image_thumb=$fileName;

            }
            
            
        }
        // If has pic then update new pic
       
       // $clinic->clinic_name    = $request->clinic_name;
        $clinic->first_name     = $request->first_name;
        $clinic->last_name      = $request->last_name;
       // $clinic->address        = $request->address;
        //$clinic->city           = $request->city;
        $clinic->email          = $request->email;
        //$clinic->country        = $request->country;
        $clinic->username       = $request->username;
        $clinic->contact_number = $request->contact_number;

        if(!empty($request->get('password'))){
            $clinic->password = $request->get('password');           
        }
     
        $clinic->save();

        $clinicDetail = new \App\ClinicDetail;

        $clinicDetail->clinic_name    = $request->clinic_name;
        $clinicDetail->address        = $request->address;
        $clinicDetail->city           = $request->city;
        $clinicDetail->country        = $request->country;

        $clinic->saveClinicDetail($clinicDetail);
        
        # Respond in JSON
        return response(['message' => trans('messages.clinic-update')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (\Gate::denies('developerOnly') && \Gate::denies('clinic')) {
            return back();
        }

        $clinic=$this->user->find($id);
        
        if($clinic->image!=NULL)
            $this->destoryFile('user/clinics/'.$clinic->id.'/'.$clinic->image);


        $clinic->delete();
        return response([
            "data"=>[],
            "message"=>trans('messages.clinic-distroy'),
            "status_code"=>200
        ],200);
    }

    public function destroyBulk(Request $request){
        if (\Gate::denies('developerOnly') && \Gate::denies('clinic')) {
            return back();
        }

        $deleteArr = $request->all();
        for($i=0;$i<count($deleteArr);$i++)
        {
                $clinic=$this->user->find($deleteArr[$i]);
        
                if($clinic->image!=NULL)
                    $this->destoryFile('user/clinics/'.$clinic->id.'/'.$clinic->image);

                $clinic->delete();
        }

        return response([
            "data"=>[],
            "message"=>trans('messages.clinic-distroy'),
            "status_code"=>200
        ],200);
    }

    public function switchStatus(Request $request){
        if (\Gate::denies('developerOnly') && \Gate::denies('clinic')) {
            return back();
        }
        $validator = validator()->make($request->all(), [
            'id'   =>'required'
        ]);
        if ($validator->fails()) {
            return response(["error"=>trans('messages.parameters-fail-validation')],422);
        }
        extract($request->all());
        $clinic= $this->user->find($id);
        if($clinic){
            $newStatus = ($clinic->status==1)?0:1;
            $clinic->status=$newStatus;
            $clinic->save();
            // Get New updated Object of User
            $updated = $clinic;

            if($request->wantsJson()){
                return response([
                    "data"        =>$this->clinicsTransformer->transform($updated),
                    "message"     =>trans('messages.clinic-status',["status"=>$newStatus]),
                    "status_code" =>200
                ],200);
            }
            flash(trans('messages.clinic-status'),'success');
            return back();
        }
        flash(trans('messages.clinic-update-fail'),'error');
        return back();
    }

    public function switchStatusBulk(Request $request){
        if (\Gate::denies('developerOnly') && \Gate::denies('clinic')) {
            return back();
        }
        $input= $request->all();
        if (count($input)==0) {
            return response(["error"=>trans('messages.parameters-fail-validation')],422);
        }
        $clinics= $this->user->whereIn('id',$request->all())->get();
        if($clinics->count() > 0){
            foreach ($clinics as $clinic) {
                $newStatus = ($clinic->status==1)?0:1;
                $clinic->status=$newStatus;
                $clinic->save();
            }

            if($request->wantsJson()){
                return response([
                    "data"=>[],
                    "message"     =>trans('messages.clinic-status',["status"=>"updated"]),
                    "status_code" =>200
                ],200);
            }
            flash(trans('messages.clinic-status'),'success');
            return back();
        }
        flash(trans('messages.clinic-update-fail'),'error');
        return back();
    }

    public function all(){
        $clinic=$this->user->whereHas('roles',function($query)
            {   
                $query->where('name','Clinic');
                
            })->join('clinic_details as cd', 'cd.user_id', '=', 'users.id')
                ->select('users.id','cd.user_id as clinic_detail_id','cd.clinic_name')->get();  
        
        return response([
            "data"        =>$clinic->all(),
            "status_code" =>200
        ],200);
    }  
}
