<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
   protected $table = 'subscriptions';
    protected $fillable = [
           'user_id', 'user_type', 'plan', 'type', 'amount', 'currency', 'bill_date', 'payment_date', 'next_bill_date', 'next_token_date', 'paid_from', 'history_id', 'renew_id', 'payment_id', 'created_at', 'updated_at'
    ];
}
