<?php 

namespace App\Traits;
use App\Notifications;
use App\Cases;
use App\User;
trait FcmTrait 
{
	protected $apiKey = null;
	protected $deviceTokens = [];
	
	// get api key from settings
	protected function getApiKey()
	{
		return config('services.fcm.key');
	}

	public function testhk(){
		return 'hk';
	}

	public function sendPush($cases_id,$type,$clinicname,$labName){
        
        $user 			= auth()->user();
 		$userRole 		= $user->roles()->first();
        $userRoleName   = $userRole->name;
 		$cases 			= Cases::where('id',$cases_id)->first();

 		$user_id 		= $this->getUserId($type,$cases);

 		$userData	 	= User::where('id',$user_id)->first();

 		$reciver_type 	= $userData->roles()->first();
        $reciverTypeName= $reciver_type->name;
        $send_from      = $this->getFromName($type,$clinicname,$labName);
 		$push_type 		= $this->getPushType($type);
 		$message_es 	= $this->getMessageES($type,$clinicname,$labName,$cases->case_code);
 		$message_en 	= $this->getMessageEN($type,$clinicname,$labName,$cases->case_code);

 		$message 		= $userData->language == 'es' ? $message_es : $message_en ;

		$adddata = [
			'user_id'		=>	$user_id,
			'case_id'		=>	$cases_id,
			'case_code'		=>	$cases->case_code,
			'reciver_type'	=>	$reciverTypeName,
			'push_type'		=>	$push_type,
			'message_es'	=>	$message_es,
			'message_en'	=>	$message_en,
			'send_from'		=>	$send_from,
			'sender_id'		=>	$user->id,
			'sender_type'	=>	$userRoleName,
			'is_send'		=>	0,
			'is_read'		=>	0,
		];

		$addedNotification = Notifications::create($adddata);
		
        $userData->badge_count    = $userData->badge_count + 1 ;
        $userData->save();
        

		$to  		  = $userData->device_token;
		$device_type  = $userData->device_type;
		$data 		  = ['type' => $push_type,'case_code' => $cases->case_code ,'case_id' => $cases_id,"body" => $message,'title' => 
        'CrownId'];
        $notification = [ "body" => $message ];


		$this->notify($to, $notification, $data ,$addedNotification->id,$device_type, $isSilent = false);
		
    }

     public function getMessageES($type,$clinic_name,$lab_name,$case_code){
    	switch ($type) {
            case "1":
                $return = "Nuevo caso es creado por $clinic_name";
                break;
            case "2":
                $return = "El nuevo caso es asignado por $clinic_name";
                break;
            case "3":
                $return =  "#$case_code está marcado como repetido por $clinic_name";
                break;
            case "4":
                $return = "#$case_code el detalle es editado por $clinic_name";
                break;
            case "5":
                $return = "#$case_code está marcado como recibido por $lab_name";
                break;
            case "6":
                $return = "#$case_code está marcado como en progreso por $lab_name";  
                break;
            case "7":
                $return = "#$case_code está marcado como en retrasado por $lab_name";
                break;
            case "8":
                $return = "#$case_code está marcado como listo para la entrega por $lab_name";
                break;
            case "9":
                $return = "#$case_code se entrega a la patente $clinic_name";
                break;
            case "9":
                $return = "#$case_code se entrega a la patente $clinic_name";
                break;
            case "12":
                $return = "#$case_code está marcado como intentar por $lab_name";
                break;
            case "11":
                $return = "#$case_code tratando detalle de estado actualizado por $clinic_name";
                break; 
            case "13":
                $return = "Case is ready for trying, marked by $lab_name";    
                break;       
            default:
                $return = "";
        }
        return $return;
    }



     public function getMessageEN($type,$clinic_name,$lab_name,$case_code){
    	switch ($type) {
            case "1":
                $return = "New case is created by $clinic_name";
                break;
            case "2":
                $return = "New case is assigned by $clinic_name";
                break;
            case "3":
                $return = "#$case_code is marked as repeated by $clinic_name";
                break;
            case "4":
                $return = "#$case_code detail is edited by $clinic_name";
                break;
            case "5":
                $return = "#$case_code is marked as received by $lab_name";
                break;
            case "6":
                $return = "#$case_code is marked as in progress by $lab_name";
                break;
            case "7":
                $return = "#$case_code is marked as delayed by $lab_name";
                break;
            case "8":
                $return = "#$case_code is marked as in ready for delivery by $lab_name";   
                break;
            case "9":
                $return = "#$case_code is delivered to patent by $clinic_name";
                break;
            case "12":
                $return = "#$case_code is marked as trying by  $lab_name";
                break;
            case "11":
                $return = "#$case_code trying status detail updated by  $clinic_name";
                break;
            case "13":
                $return = "Case is ready for trying, marked by $lab_name";    
                break; 
            default:
                $return = "";
        }
        return $return;
    }

    public function getFromName($type,$clinic_name,$lab_name){
        switch ($type) {
            case "1":
                $return = $clinic_name;
                break;
            case "2":
                $return = $clinic_name;
                break;
            case "3":
                $return = $clinic_name;
                break;
            case "4":
                $return = $clinic_name;
                break;
            case "5":
                $return = $lab_name;
                break;
            case "6":
                $return = $lab_name;  
                break;
            case "7":
                $return = $lab_name; 
                break;
            case "8":
                $return = $lab_name;    
                break;
            case "9":
                $return = $clinic_name;  
                break;
            case "12":
                $return = $lab_name;  
                break;
            case "11":
                $return = $clinic_name;  
                break;
            case "13":
                $return = $lab_name;  
                break;
            default:
                $return = "";
        }
        return $return;
    }

    public function getUserId($type,$cases){
    	switch ($type) {
            case "1":
                $return = $cases->lab_id;
                break;
            case "2":
                $return = $cases->lab_id;
                break;
            case "3":
                $return = $cases->lab_id;
                break;
            case "4":
                $return = $cases->lab_id;
                break;
            case "5":
                $return = $cases->clinic_id;
                break;
            case "6":
                $return = $cases->clinic_id;  
                break;
            case "7":
                $return = $cases->clinic_id; 
                break;
            case "8":
                $return = $cases->clinic_id;    
                break;
            case "9":
                $return = $cases->lab_id;  
                break;
            case "12":
                $return = $cases->clinic_id;  
                break;
            case "11":
                $return = $cases->lab_id;  
                break;
            case "13":
                $return = $cases->clinic_id;  
                break;
            default:
                $return = "";
        }
        return $return;
    }

    public function getPushType($type){

    	//'create','receive','process','delayed','ready','delivered','repetition'
    	switch ($type) {
            case "1":
                $return = "create";
                break;
            case "2":
                $return = "assigned";
                break;
            case "3":
                $return = "repetition";
                break;
            case "4":
                $return = "edit";    
                break;
            case "5":
                $return = "receive";    
                break;
            case "6":
                $return = "process";    
                break;
            case "7":
                $return = "delayed";    
                break;
            case "8":
                $return = "ready";    
                break;
            case "9":
                $return = "delivered";    
                break;
            case "12":
                $return = "trying";    
                break;
            case "11":
                $return = "tryingupdate";    
                break;
            case "13":
                $return = "readyfortrying";
                break;
            default:
                $return = "";
        }
        return $return;
    }

	// set device token in array
	public function setDeviceToken($tokens)
	{
		if(is_array($tokens))
		{
			$this->deviceTokens = '';
			$this->deviceTokens = $tokens;
		}
		else if(!empty($tokens))
		{
			$this->deviceTokens = [];
			$this->deviceTokens[] = $tokens;
		}
	}

	// send notification to users
	public function notify($to, $notification, $data = [],$notification_id,$device_type, $isSilent = false)
	{
		#mprd($device_type);
		$this->setDeviceToken($to);
		$fields['registration_ids'] = $this->deviceTokens;
		
		if($device_type == 'android'){
			if(count($data) > 0)
			{	
				$data['notification'] = $notification;
				$fields['data'] = $data;
			}	
		}
		else{
			if(count($notification) > 0)
			{
				$fields['notification'] = $notification;
			}
			if(count($data) > 0)
			{
				$fields['data'] = $data;
			}	
		}
		if($isSilent)
		{
			$fields['content-available'] = $isSilent;
		}
        //header with content_type api key
        $headers = [
        	'Content-Type:application/json',
    		'Authorization:key=' . $this->getApiKey(),
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        #print_r($result);die;
        if ($result === FALSE) 
        {
        	die('FCM Send Error: ' . curl_error($ch));
        }
        else{
        	Notifications::where('id', $notification_id)
            ->update(['is_send' => 1]);
        }
        curl_close($ch);
        return $result;
	}



}