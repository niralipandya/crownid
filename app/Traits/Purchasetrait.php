<?php 

namespace App\Traits;
use App\Subscription;
use App\Subscriptionhistory;
use App\Tokenhistory;
use App\Doctortokenhistory;
use App\User;

trait Purchasetrait {

	public function getPlanName($who,$plan,$type){ 
		if($who == 'clinic'){
			if($plan == 'pro'){
				if($type == 'annual'){
					return 'PRO_Yearly';
				}
				else {
					return 'PRO_Monthly';
				}
			}
			else if($type == 'premium'){
				if($type == 'annual'){
					return 'PREMIUM_Yearly';
				}
			}
		}
		else {
			if($plan == 'pro'){
				if($type == 'annual'){
					return 'Doctor_pro_yearly';
				}
				else {
					return 'Docto_PRO_Monthly';
				}
			}
		}
		return false;
	}

	public function addToken($user,$token_count,$user_type,$type,$message,$paid_from,$payment_id,$remark,$amount = 0,$currency = "$"){
		#($user);
		$add_data = [
			'user_id' 		=> $user->id,
			'amount'  		=> $amount, 
			'currency'		=> $currency, 
			'remark'  		=> $remark, 
			'message' 		=> $message, 
			'token_count' 	=> $token_count,
			'user_type'		=> $user_type, 
			'type'			=> $type, 
			'paid_from'		=> $paid_from,
			'payment_id' 	=> $payment_id
		];
		#mprd($add_data);
		$addToken = Tokenhistory::create($add_data);

	}

	public function get_signup_token($plan){
        return 3;
	}

	public function get_signup_doctor($plan){
        return 1;
	}

	public function get_free_token($plan){
        switch ($plan) {
            case "Basic":
                $token = "3";
                break;
            case "Pro":
                $token = "10";
                break;
            case "Premium":
                $token = "30";
                break;
            case "basic":
                $token = "3";
                break;
            case "pro":
                $token = "10";
                break;
            case "premium":
                $token = "30";
                break;
            default:
                $token = "0";
        }
        return $token;
	}

	public function updateToken($user,$filedname,$new_token){
		$user->$filedname = $user->$filedname + $new_token;
		$user->case_token_total = $user->case_token_bylab + $user->case_token_free + $user->case_token_purchased;
		$user->save(); 
	}


	public function lessToken($user,$filedname,$new_token){
		$user->$filedname = $user->$filedname + $new_token;
		$user->case_token_total = $user->case_token_bylab + $user->case_token_free + $user->case_token_purchased;
		$user->save(); 
	}

	public function addDoctor($user,$token_count,$user_type,$type,$message,$paid_from,$payment_id,$remark,$amount = 0,$currency = "$"){

		$add_data = [
			'user_id' 		=> $user->id,
			'amount'  		=> $amount, 
			'currency'		=> $currency, 
			'remark'  		=> $remark, 
			'message' 		=> $message, 
			'doctor_count' 	=> $token_count,
			'user_type'		=> $user_type, 
			'type'			=> $type, 
			'paid_from'		=> $paid_from,
			'payment_id' 	=> $payment_id
		];

		$addDoctor = Doctortokenhistory::create($add_data);

	}

	public function updateDoctor($user,$filedname,$new_token){
		$user->$filedname = $user->$filedname + $new_token;
		$user->doctor_token_total = $user->doctor_token_free + $user->doctor_token_purchased;
		$user->save(); 
	}

	
}