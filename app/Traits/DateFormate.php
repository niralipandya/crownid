<?php 

namespace App\Traits;


trait DateFormate{

	function dateformate($date){

        $clinic      = auth()->user();  
        $date_format = $clinic->date_format;

        switch ($date_format) {
            case "dd-MM-yyyy":
                $dateformat = "d-m-Y";
                break;
            case "MM-dd-yyyy":
                $dateformat = "m-d-Y";
                break;
            case "dd-MM-yy":
                $dateformat = "d-m-y";
                break;
            default:
                $dateformat = "m-d-y";
        }
        $newDate = date($dateformat, strtotime($date));
        return $newDate;
    }

    function dateTimeformate($date){

        $clinic      = auth()->user();  
        $date_format = $clinic->date_format;
 
        switch ($date_format) {
            case "dd-MM-yyyy":
                $dateformat = "d-m-Y H:i:s";
                break;
            case "MM-dd-yyyy":
                $dateformat = "m-d-Y H:i:s";
                break;
            case "dd-MM-yy":
                $dateformat = "d-m-y H:i:s";
                break;
            default:
                $dateformat = "m-d-y H:i:s";
        }
        $newDate = date($dateformat, strtotime($date));
        return $newDate;
    }

}