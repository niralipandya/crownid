<?php 

namespace App\Traits;
use App\Subscription;
use App\Subscriptionhistory;
use App\Tokenhistory;
use App\Doctortokenhistory;
use Google_Client;
use Google_Auth_AssertionCredentials;
use Google_Service_AndroidPublisher;

use \Carbon\Carbon;
use DateTime;
use DateInterval;
use DatePeriod;

trait Subscriptiontrait {

	public function updateSubscription($clinic_id,$plan,$user_type,$type,$amount,$currency,$paid_from,$payment_id,$receipt,$renew_id = 0,$receiptData,$subscription=''){
		
		if($paid_from != 'web'){
			$subscription  = Subscription::where('original_transaction_id',$receiptData['original_transaction_id'])->where('user_id',$clinic_id)->first();
		}
		else{
			$subscription  = Subscription::where('user_id',$clinic_id)->first();
		}

        
        $date 		= strtotime(date('Y-m-d H:i:00'));
		$next_token_date = date("Y-m-d H:i:00", strtotime("+1 month", $date));
		$payment_date = date("Y-m-d H:i:00", strtotime("+1 month", $date));
		if($plan == 'Pro'){
			if($type == 'Month'){
				$next_token_date = null;
				$next_bill  = date("Y-m-d H:i:00", strtotime("+1 month", $date));
			}
			else{
				$next_bill  = date("Y-m-d H:i:00", strtotime("+1 year", $date));	
			}	
		}
		else if($plan == 'Basic'){
			$next_bill  = null;
		}
		else{
			$next_token_date = null;
			$next_bill  = date("Y-m-d H:i:00", strtotime("+1 year", $date));	
		}

		if($next_token_date != null){
			$subscription->next_token_date 			= $next_token_date;
		}

		if($receipt != ''){
			
			$subscription->auto_renew_status		= '1';
			if($paid_from != 'web'){
				$subscription->receipt 					= $receipt;
			}
			$subscription->transaction_id 			= $receiptData['transaction_id'];
			$subscription->original_transaction_id 	= $receiptData['original_transaction_id'];
			$subscription->is_trial_period 			= '0';
			$subscription->expires_date 			= date("Y-m-d H:i:00", strtotime($receiptData['expires_date']));
			$subscription->product_id 				= $receiptData['product_id'];
			
		}
		$subscription->plan 					= $plan;
		$subscription->type 					= $type;
		
		$subscription->history_id 				= 0;

		$subscription->save();


	}

	public function updateSubscriptioncron($sub_id,$history_id,$returnData){
		$subscriptionhistory  = Subscriptionhistory::where('id',$history_id)->first();
		mprd($subscriptionhistory);
	}

	public function newUser($clinic_id,$plan,$user_type,$type,$amount,$currency,$paid_from,$payment_id,$receipt,$renew_id = 0,$receiptData){
		$date 		= strtotime(date('Y-m-d H:i:00'));
		$next_token_date = date("Y-m-d H:i:00", strtotime("+1 month", $date));
		$payment_date = date("Y-m-d H:i:00", strtotime("+1 month", $date));
		if($plan == 'Pro' || $plan == 'pro'){
			if($type == 'Month'){
				$next_token_date = null;
				$next_bill  = date("Y-m-d H:i:00", strtotime("+1 month", $date));
			}
			else{
				$next_bill  = date("Y-m-d H:i:00", strtotime("+13 month", $date));	
			}	
		}
		else if($plan == 'Basic' || $plan == 'basic' ){
			$next_bill  = null;
		}
		else{
			$next_token_date = null;
			$next_bill  = date("Y-m-d H:i:00", strtotime("+13 month", $date));	
		}

		$add_data = [ 
			'user_id' 	=> $clinic_id,
			'user_type' => $user_type,
			'plan'      => $plan,
			'type'		=> $type,
			'amount'	=> $amount,
			'currency'	=> $currency,
			//'payment_date'  => $payment_date,
			'bill_date' 	=> date('Y-m-d H:i:00'),
			'next_bill_date' => $next_bill,
			'paid_from'		 => $paid_from,
			'payment_id' 	 => $payment_id,
			'renew_id' 	 	 => $renew_id,

		];

		$subscription = Subscription::create($add_data);		
		if($receiptData != ''){
			
			$subscription->receipt 					= $receipt;
			$subscription->transaction_id 			= $receiptData['transaction_id'];
			$subscription->original_transaction_id 	= $receiptData['original_transaction_id'];
			$subscription->expires_date 			= date("Y-m-d H:i:00", strtotime($receiptData['expires_date']));
			$subscription->product_id 				= $receiptData['product_id'];
			
		}
		$subscription->auto_renew_status		= 1;
		$subscription->is_trial_period 			= true;
		$subscription->next_token_date 			= $next_token_date;
		$subscription->history_id 				= 0;
		$subscription->save();

		//$subscriptionhistory = Subscriptionhistory::create($add_data);

		/*$subscriptionhistory->next_token_date = $next_token_date;
		$subscriptionhistory->receipt = $receipt;
		$subscriptionhistory->save();*/


		/*$subscriptionhistory->transaction_id 			= $receiptData['transaction_id'];
		$subscriptionhistory->original_transaction_id 	= $receiptData['original_transaction_id'];
		$subscriptionhistory->is_trial_period 			= $receiptData['is_trial_period'];
		$subscriptionhistory->expires_date 				= date("Y-m-d H:i:s", strtotime($receiptData['expires_date']));
		$subscriptionhistory->product_id 				= $receiptData['product_id'];
		$subscriptionhistory->save();*/
		return true;
	}

	public function checkSubescriptionExist($original_transaction_id){
		$returnData = [];
		$subscription  = Subscription::where('original_transaction_id',$original_transaction_id)->first();
		if($subscription){
			return true;
		}else{
			return false;
		}
	}

	public function checkSubescriptionOldExist($original_transaction_id,$clinic_id){
		$returnData = [];
		$subscription  = Subscription::where('original_transaction_id',$original_transaction_id)->where('user_id',$clinic_id)->first();
		if($subscription){
			return true;
		}else{
			return false;
		}
	}

	public function getSubescriptionDetail($clinic_id){
		$returnData = [];
		$subscription  = Subscription::where('user_id',$clinic_id)->first();
		if($subscription){
			$next_bill_date = $subscription->next_bill_date;
			$current_date   = date('Y-m-d H:i:00');
			#mpr($subscription->payment_date);mpr($current_date);
			$freeTrail 	= $subscription->is_trial_period;
			$is_paid 	= false;
			
			
			$plan = $subscription->plan;
			if($plan != 'Basic' )
			{
				if($subscription->payment_date != ''){
					$is_paid = true;
				}

				if($next_bill_date > $current_date){
					$returnData['is_subscribe'] = 'yes';
					$returnData['subscription']['plan'] = $plan;
					$returnData['subscription']['type'] = $subscription->type;
					$returnData['subscription']['next_bill_date'] = $subscription->next_bill_date;	
					$returnData['is_free_trial']	= $freeTrail;
					$returnData['is_paid']			=	$is_paid;
				}
				else{
					// check on apple and andrid and visa varsa on cross check on both device.
					$returnData['is_subscribe'] 	= 'no';
					$returnData['is_free_trial']	= $freeTrail;
					$returnData['is_paid']			= $is_paid;
					$returnData['subscription'] 	= [];
				}
				
			}
			else{
				$returnData['is_subscribe'] = 'yes';
				$returnData['subscription']['plan'] = $plan;
				$returnData['subscription']['type'] = $subscription->type;
				$returnData['subscription']['next_bill_date'] = $subscription->next_bill_date;	
			}
		}else{
			$returnData['is_subscribe'] = 'no';
			$returnData['subscription'] = [];
		}
		return $returnData;
	}
	
	public function verifyReceipt($receipt,$device_type){
       # mprd($request->all());
		if($device_type == 'ios'){
			$arr = array(
            'receipt-data'  => $receipt,
            'password'      => "7df9570b7d954500aeb28082f0f0148d" );

	        $data_string = json_encode($arr);
	        $ch = curl_init('https://sandbox.itunes.apple.com/verifyReceipt');        
	        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                      
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                  
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                          
	        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                               
	            'Content-Type: application/json',      
	            'Content-Length: ' . strlen($data_string))                  
	        );
	        $result = curl_exec($ch);
	        $data = json_decode($result);
	        #mprd($data);
	        $receiptArray = (array)$data->latest_receipt_info;
	        $total_count = count($receiptArray) ;
	        
	        $latest_receipt = $receiptArray[$total_count - 1];
	        $latest_receipt = $receiptArray[0];
	        $pending_Array = (array)$data->pending_renewal_info;

	        $returnData['auto_renew_status']		= @$pending_Array[0]->auto_renew_status;
	        $returnData['transaction_id']           = @$latest_receipt->transaction_id;
	        $returnData['original_transaction_id']  = @$latest_receipt->original_transaction_id;
	        $returnData['is_trial_period']          = @$latest_receipt->is_trial_period;
	        $returnData['expires_date']             = @$latest_receipt->expires_date;
	        $returnData['product_id']               = @$latest_receipt->product_id;
	        #mprd($latest_receipt);

	        return $returnData;
		}
        else{

        	$receipt11 = json_decode($receipt);
        	mprd($this->getsubscriptionAndroid($receipt));
        //$purchaseInfo = json_decode($receipt11->purchaseInfo->responseData);
        	mprd($receipt11);
        	$returnData['auto_renew_status']		= true;
	        $returnData['transaction_id']           = 123;
	        $returnData['original_transaction_id']  = 456;
	        $returnData['is_trial_period']          = true;
	        $returnData['expires_date']             = null;
	        $returnData['product_id']               = 456;
	        #mprd($latest_receipt);

	        return $returnData;
        }
    }

    public function getsubscriptionAndroid($receipts){
       
        $encode_receipt = json_encode($receipts, true);
        $decode_receiptData = json_decode($encode_receipt, true);
       
            $receiptData = json_decode($decode_receiptData, true); 
            $purchaseToken = $receiptData['purchaseToken'];
            $packageName =$receiptData['packageName'];
            $subscriptionId = $receiptData['productId'];
            try {
                // create new Google Client
                $client = new Google_Client();
                // set Application Name to the name of the mobile app
                $client->setApplicationName("crownid");
                // get p12 key file
#mprd(public_path());
                $key = file_get_contents(public_path().'/keys/crownid-e6f21-c8919fc2363d.p12');
                // create assertion credentials class and pass in:
                // - service account email address
                // - query scope as an array (which APIs the call will access)
                // - the contents of the key file
                $cred = new Google_Auth_AssertionCredentials(
                    'crownidbilling@crownid-e6f21.iam.gserviceaccount.com',
                    ['https://www.googleapis.com/auth/androidpublisher'],
                    $key
                );

                // add the credentials to the client
                $client->setAssertionCredentials($cred);

                // create a new Android Publisher service class
                $service = new Google_Service_AndroidPublisher($client);
                
                // use the purchase token to make a call to Google to get the subscription info
                $subscription = $service->purchases_subscriptions->get($packageName, $subscriptionId, $purchaseToken);
                if (is_null($subscription)) {
                    // query returned no data
                    return $this->setStatuscode(500)
                        ->respond([
                            "data"        => "",
                            "message"     => trans('apimessages.subscriptionStatus'),
                            "status_code" => $this->getStatuscode(),
                        ]
                    );
                } elseif (isset($subscription['error']['code'])) {
                    // query returned an error
                    return $this->setStatuscode(500)
                            ->respond([
                                "data"        => "",
                                "message"     => trans('apimessages.subscriptionStatus'),
                                "status_code" => $this->getStatuscode(),
                            ]
                        );
                } elseif (!isset($subscription['expiryTimeMillis'])) {
                // query did not return a subscription expiration time
                    return $this->setStatuscode(500)
                        ->respond([
                            "data"        => "",
                            "message"     => trans('apimessages.subscriptionStatus'),
                            "status_code" => $this->getStatuscode(),
                        ]
                    );
                }

                // convert expiration time milliseconds since Epoch to seconds since Epoch
                $seconds = $subscription['expiryTimeMillis'] / 1000;
                // format seconds as a datetime string and create a new UTC Carbon time object from the string
                $date = date("d-m-Y H:i:s", $seconds);
                $datetime = new Carbon($date);
                // dd($datetime);
                // check if the expiration date is in the past
                if (Carbon::now()->gt($datetime)) {
                     
                     // Subscription::where('user_id', $user_login->id)->latest()->first()->delete();
                      Subscription::where('user_id', $user_login->id)->latest()->first()->update(['end_date' => $datetime,'duration' => 0]);
                      User::where('id', $user_login->id)->update(["is_subscribe" => "no","subscription_end_date" =>$datetime]);
                     return $this->setStatuscode(200)
                        ->respond([
                            "data"        =>  $datetime,
                            "message"     => trans('apimessages.subscriptionExpired'),
                            "status_code" => $this->getStatuscode(),
                        ]
                    );
                }
                else
                {
                    Subscription::where('user_id', $user_login->id)->latest()->first()->update(['end_date' => $datetime]);
                  
                    User::where('id', $user_login->id)->update(["is_subscribe" => "yes","subscription_end_date" =>$datetime ]);
                     return $this->setStatuscode(200)
                        ->respond([
                            "data"        =>  $datetime,
                            "message"     => trans('apimessages.subscriptionRenewed'),
                            "status_code" => $this->getStatuscode(),
                        ]
                    );
                   
                }
            } catch (Google_Auth_Exception $e) {
                // if the call to Google fails, throw an exception
                 return $this->setStatuscode(500)
                        ->respond([
                            "data"        => "",
                            "message"     => trans('apimessages.subscriptionStatus'),
                            "status_code" => $this->getStatuscode(),
                        ]
                    );
            }
        
      
    }


    public function verifyReceiptCron($receipt,$expires_date){
       # mprd($request->all());
        $arr = array(
            'receipt-data'  => $receipt,
            'password'      => "7df9570b7d954500aeb28082f0f0148d" );

        $data_string = json_encode($arr);
        $ch = curl_init('https://sandbox.itunes.apple.com/verifyReceipt');        
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                      
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                          
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                               
            'Content-Type: application/json',      
            'Content-Length: ' . strlen($data_string))                  
        );
        $result = curl_exec($ch);
        $data = json_decode($result);
        mprd($data);
        $receiptArray = (array)$data->latest_receipt_info;
        $total_count = count($receiptArray) ;
        
        foreach ($receiptArray as $key => $value) {
        	mpr($value['purchase_date']);
        }



        $latest_receipt = $receiptArray[$total_count - 1];
        $latest_receipt = $receiptArray[0];
        $pending_Array  = (array)$data->pending_renewal_info;

        $returnData['purchase_date']            = @$latest_receipt->purchase_date;
        $returnData['transaction_id']           = @$latest_receipt->transaction_id;
        $returnData['original_transaction_id']  = @$latest_receipt->original_transaction_id;
        $returnData['is_trial_period']          = @$latest_receipt->is_trial_period;
        $returnData['expires_date']             = @$latest_receipt->expires_date;
        $returnData['product_id']               = @$latest_receipt->product_id;
        mprd($latest_receipt);

        return $returnData;
    }


};