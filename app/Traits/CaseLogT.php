<?php 

namespace App\Traits;
use App\Caselog;
use App\Traits\FcmTrait;

trait CaseLogT{

    use FcmTrait;

	public function addCaseLog($cases_id,$type,$created_by_type,$lgoin_name,$labName = '',$case_code = '',$retake_text='',$update_date='' ,$edd='')
    {

        date_default_timezone_set('Asia/Kolkata');

        $timestamp = time();
        $display_date = date("Y-m-d H:i:s", $timestamp);

        $text                       = $this->text_type($type,$lgoin_name,$labName,$case_code,$retake_text,$update_date ,$edd);
        $class                      = $this->class_text($type);
        $type_text                  = $this->type_text($type);
        $input['case_id']           =  $cases_id;
        $input['text']              =  $text;
        $input['type']              =  $type;
        $input['type_display']      =  $type_text;
        $input['class']             =  $class;
        $input['created_by_id']     =  auth()->user()->id;
        $input['created_by_name']   =  $lgoin_name;
        $input['created_by_type']   =  $created_by_type;
        $input['display_date']      =  $display_date;
        $cases                      = Caselog::create($input);
        #mpr($input);
        if($type != 10){
            $notification   = $this->sendPush($cases_id,$type,$lgoin_name,$labName);
        }
        
        return true;
    }



    public function text_type($text_type,$lgoin_name,$labName = '',$case_code = '',$retake_text='',$update_date='' ,$edd=''){

        switch ($text_type) {
            case "1":
                $return = "New case is created by $lgoin_name";
                break;
            case "2":
                $return = "Case is assigned to $labName lab by $lgoin_name ";
                break;
            case "3":
                $var = isset($retake_text) && $retake_text != '' ? 'with reason ('.($retake_text.')') : '' ;
                $return = "Case is retake by $case_code $var ";
                break;
            case "4":
                $return = "Case is edited by $lgoin_name";    
                break;
            case "5":
                $return = "Case is received by $labName";    
                break;
            case "6":
                $return = "Case is marked as in progress by $labName";    
                break;
            case "7":
                $return = "Case status updated as delayed by $labName. EDD update to $update_date from $edd";    
                break;
            case "8":
                $return = "Case is Ready for delivery";    
                break;
            case "9":
                $return = "Case is Delivered to patient by $lgoin_name";    
                break;
            case "10":
                $return = "Case needs trying, marked by $lgoin_name";    
                break;
            case "11":
                $return = "Trying status comment as $retake_text by $lgoin_name";    
                break;
            case "12":
                $return = "Case needs trying, marked by $labName";    
                break;
            case "13":
                $return = "Case is ready for trying, marked by $labName";    
                break;    
            default:
                $return = "";
        }
        return $return;
    }

    public function class_text($type){

        switch ($type) {
            case "1":
                $return = "fa fa-folder-open-o bg-blue";
                break;
            case "2":
                $return = "fa fa-medkit bg-aqua";
                break;
            case "3":
                $return = "fa fa-folder-open-o bg-yellow";
                break;
            case "4":
                $return = "fa fa-edit bg-yellow";    
                break;
            case "5":
                $return = "fa fa-reorder bg-orange";
                break;
            case "6":
                $return = "fa fa-recycle bg-olive";
                break;
            case "7":
                $return = "fa fa-warning bg-red";
                break;
            case "8":
                $return = "fa fa-thumbs-o-up bg-green";
                break;
            case "9":
                $return = "fa  fa-upload bg-gray";
                break;
            case "10":
                $return = "fa  fa-lemon-o bg-olive";
                break;
            case "11":
                $return = "fa  fa-lemon-o bg-olive";
                break;
            case "12":
                $return = "fa  fa-lemon-o bg-olive";
                break;
            case "13":
                $return = "fa  fa-warning-o bg-red";
                break;
            default:
                $return = "fa fa-video-camera bg-maroon";
        }
        return $return;
    }

    public function type_text($type){

        switch ($type) {
            case "1":
                $return = "Create";
                break;
            case "2":
                $return = "Lab Assigned";
                break;
            case "3":
                $return = "Repetition";
                break;
            case "4":
                $return = "Edit";    
                break;
            case "5":
                $return = "Received";    
                break;
            case "6":
                $return = "In Progress";    
                break;
            case "7":
                $return = "Delayed";    
                break;
            case "8":
                $return = "Ready for delivery";    
                break;
            case "9":
                $return = "Delivered to patient";    
                break;
            case "10":
                $return = "Trying";    
                break;
            case "11":
                $return = "Trying Update";    
                break;
            case "12":
                $return = "Trying";    
                break;
            case "13":
                $return = "Ready for trying";    
                break;
            default:
                $return = "";
        }
        return $return;
    }

    /*'Pending','Received','In Progress','Delayed','Ready for delivery','Delivered to patient','Repetition'*/
}