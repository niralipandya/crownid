<?php

namespace App\Exceptions;
use Illuminate\Support\Facades\Route;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if($exception instanceof NotFoundHttpException)
        { 
            if ($request->is('clinic/*')) {
                return redirect('/clinic/home');
            }
            return redirect('/admin');
        }
        else if($exception instanceof \BadMethodCallException)
        {
            return back();
        }
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json([
                'error' => 'Unauthenticated.',
                "success"=> false,  
                "message"=> "Invalid Authentication",  
                "status"=> 405
                ], 405);
        }
        if ($request->route()->getPrefix() == 'admin' || $request->route()->getPrefix() == 'admin/')
            return redirect('/admin/login');
        else if ($request->route()->getPrefix() == 'clinic' || $request->route()->getPrefix() == 'clinic/')
            return redirect('/clinic/login');
        else if ($request->route()->getPrefix() == 'doctor' || $request->route()->getPrefix() == 'doctor/')
            return redirect('/lab/login');
        else
            return redirect('/login');
    }
}
