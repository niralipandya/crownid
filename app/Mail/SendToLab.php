<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendToLab extends Mailable
{
    use Queueable, SerializesModels;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
     public function build()
    {
        return $this->markdown('emails.app.sendtolab')
            ->with('case_code',$this->user->case_code)
            ->with('code',$this->user->case_code)
            ->with('clinicname',$this->user->clinicname)
            ->with('name', $this->user->name);
        
    }
}
