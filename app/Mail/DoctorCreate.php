<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DoctorCreate extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->markdown('emails.app.createdoctor')
            
            ->with('email',$this->user->email)
            ->with('phonenumber',$this->user->phonenumber)
            ->with('clinicname',$this->user->clinicname)
            ->with('name', $this->user->first_name)
            ->with('username', $this->user->username)
            ->with('password', $this->user->password);
    }
}
