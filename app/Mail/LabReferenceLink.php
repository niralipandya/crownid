<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LabReferenceLink extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.app.lablink')
            ->with('code',@$this->user->code)
            ->with('email',@$this->user->email)
            ->with('phonenumber',@$this->user->phonenumber)
            ->with('clinicname',@$this->user->clinicname)
            ->with('name', @$this->user->name);
        
    }
}
