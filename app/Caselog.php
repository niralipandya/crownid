<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caselog extends Model
{
    protected $table = 'caselogs';


    protected $fillable = [
        'id', 'case_id', 'text', 'type', 'type_display','created_by_id', 'created_by_name', 'created_by_type', 'display_date', 'class','created_at', 'updated_at'
    ];
    
}
