<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    protected $table = 'notifications';
    protected $fillable = [
            'user_id', 'case_id', 'case_code', 'reciver_type', 'push_type', 'message_es', 'message_en', 'send_from', 'sender_id', 'sender_type', 'is_send', 'is_read'
    ];
}
