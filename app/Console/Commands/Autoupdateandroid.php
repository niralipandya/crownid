<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Autoupdateandroid extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Autoupdateandroid:subscriptionautoupdateandroid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'subscriptionautoupdateandroid';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $current_date               = date('Y-m-d H:i:00');
        $subscription               = Subscription::where('next_token_date','=',$current_date)->get();
    }
}
