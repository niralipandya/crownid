<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

use App\Subscription;
use App\Subscriptionhistory;
use App\Tokenhistory;
use App\User;
use App\Doctortokenhistory;
use \Carbon\Carbon;

use App\Traits\Subscriptiontrait;
use App\Traits\Purchasetrait;


class MonthlyToken extends Command
{
    use Subscriptiontrait,Purchasetrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MonthlyToken:tokendistribution';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'will send token to all subscribe users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $current_date               = date('Y-m-d H:i:00');
        $subscription               = Subscription::where('next_token_date','=',$current_date)->where('auto_renew_status','1')->get();
        $subscription_array         = $subscription->toArray();
#mprd($subscription_array);
        foreach ($subscription_array as $key => $value) {
            $freeTrail = false;
            if($value['payment_date'] > $current_date){
                $freeTrail = true;
                return false;
            }
            
            $next_bill_date         = $value['next_bill_date'];
            $next_token_date        = $value['next_token_date'];

            if($next_token_date == $current_date){

                $next_token_date_update = date("Y-m-d H:i:00", strtotime("+1 month", strtotime($next_token_date)));
                
                $token_count = $this->get_free_token($value['plan']);
                $message = 'Cron Auto Tokens';
                $remark  = 'Cron Auto Tokens';
                $paid_from = 'cron';

                $clinic =  User::where('id', $value['user_id'])->first();

                $this->addToken($clinic,$token_count,$value['user_type'],'free',$message,$paid_from,0,$remark,0,"$");
                $this->updateToken($clinic,'case_token_free',$token_count);
                
                if($value['plan'] == 'Basic'){
                    $next_token_date_update = date("Y-m-d H:i:00", strtotime("+1 month", strtotime($next_token_date)));
                }
                else if($next_token_date_update == $next_bill_date){
                    $next_token_date_update = null;
                } 


                // send notification to user as well
                \DB::table('subscriptions')->where('id',  $value['id'])->update(['next_token_date' => $next_token_date_update ]);
            }    
        }


    }
}
