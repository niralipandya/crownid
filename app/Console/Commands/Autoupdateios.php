<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Subscription;
use App\Traits\Subscriptiontrait;
use App\Traits\Purchasetrait;


class Autoupdateios extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    use Subscriptiontrait,Purchasetrait;
    
    protected $signature = 'Autoupdateios:subscriptionautoupdateios';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'subscriptionautoupdateios';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currentDate   = date("Y-m-d H:i:00");
        $subscription  = Subscription::where('expires_date','=',$currentDate)->get();

        if($subscription){
           foreach ($subscription->toArray() as $key => $value) { 
               $expires_date       = $value['expires_date'];
               $is_trial_period    = $value['is_trial_period'];

               $arr = array(
                    'receipt-data'  => $value['receipt'],
                    'password'      => "7df9570b7d954500aeb28082f0f0148d" );
              

               $data_string = json_encode($arr);
                $ch = curl_init('https://sandbox.itunes.apple.com/verifyReceipt');        
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                      
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                  
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                          
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(                               
                    'Content-Type: application/json',      
                    'Content-Length: ' . strlen($data_string))                  
                );
                $result = curl_exec($ch);
                $data   = json_decode($result); 
                if($data->status != 0){
                    return 0;
                }
                $receiptArray = (array)$data->latest_receipt_info;
                #mpr($receiptArray);
                $pending_Array = (array)$data->pending_renewal_info;
                $subscriptionUpdate       = Subscription::where('id',$value['id'])->first();
                $subscriptionUpdate->auto_renew_status        = @$pending_Array[0]->auto_renew_status;
                foreach ($receiptArray as $key => $value) {
                   $purchase_date = date("Y-m-d H:i:00", strtotime($value->purchase_date));
                   $current_expires_date = date("Y-m-d H:i:00", strtotime($value->expires_date));
                   #mpr($purchase_date);
                   if($purchase_date == $expires_date){

                        if($is_trial_period == 'true'){
                             $subscriptionUpdate->payment_date   =    date("Y-m-d H:i:s", strtotime(@$value->purchase_date) );
                        }

                        $returnData['purchase_date']            = @$value->purchase_date;
                        $returnData['transaction_id']           = @$value->transaction_id;
                        $returnData['original_transaction_id']  = @$value->original_transaction_id;
                        $returnData['is_trial_period']          = @$value->is_trial_period;
                        $returnData['expires_date']             = @$value->expires_date;
                        $returnData['product_id']               = @$value->product_id;
                        
                        $subscriptionUpdate->expires_date = date("Y-m-d H:i:s", strtotime(@$value->expires_date) );
                        $subscriptionUpdate->transaction_id = $returnData['transaction_id'] ;
                        $subscriptionUpdate->is_trial_period  = @$value->is_trial_period;
                   }
                }
                $subscriptionUpdate->save();
                #mpr(date("Y-m-d H:i:s"));

            }
        }
    }
}
