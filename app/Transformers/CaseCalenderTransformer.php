<?php

namespace App\Transformers;
use App\Traits\DateFormate;


class CaseCalenderTransformer extends  Transformer
{	
	use DateFormate;

	public function transform($item)
	{ 
		switch ($item['status']) {
            case "Pending":
                $backgroundColor = "#39cccc";
                $borderColor	= '#39cccc';
                break;
            case "Received":
                $backgroundColor = "#0073b7";
                $borderColor	= '#0073b7';
                break;
            case "In Progress":
                $backgroundColor = "#00c0ef";
                $borderColor	= '#00c0ef';
                break;
            case "Delayed":
                $backgroundColor = "#e60404";    
                $borderColor	= '#e60404';
                break;
            case "Ready for delivery":
                $backgroundColor = "#605ca8";    
                $borderColor	= '#605ca8';
                break;
            case "Delivered to patient":
                $backgroundColor = "#3d9970";    
                $borderColor	= '#3d9970';
                break;
            default:
                $backgroundColor = "#E36C59";
                $borderColor	= '#E36C59';
        }
        
		
		return [
			'title'     		=> $item['case_code'],
			'start'  			=> $item['end_date'].' 00:00:00',
			"backgroundColor"   => $backgroundColor,
			'borderColor'		=> $borderColor
			];
	}
	

	
	
}