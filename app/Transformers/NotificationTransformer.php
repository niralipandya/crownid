<?php

namespace App\Transformers;
use App\Traits\DateFormate;
use App\User;

class NotificationTransformer extends  Transformer
{
	use DateFormate;
	public function transform($item)
	{  
		return [ 
			"id"             		=> $item->id,
			"user_id"		 		=> (string)$item->user_id,
			"send_from"				=> $item->send_from,
			"image"					=> $this->getImage($item->sender_id),
			"case_code"				=> (string)$item->case_code,
			"type"					=> (string)$item->push_type,
			"message"				=> (string)$item->message,
			"created_at"	 		=> $this->dateTimeformate($item->created_at->format('Y-m-d H:i:s'))
		];
	}

	public function getName(){

	}
	
	public function getImage($sender_id){
		$select 	= 'image';
		$result = \DB::table('users')->select($select)->where('id', $sender_id)->first() ;
		if($result->$select != ''){
			return AWS_URL .$result->$select;
		}
		else{
			return '';
		}
	}
}