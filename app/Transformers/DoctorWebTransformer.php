<?php

namespace App\Transformers;
class DoctorWebTransformer extends  Transformer
{
	public function transform($item) 
	{
		return [
			"id"             => $item->id,
			"first_name"     => $item->first_name,
			"email"			 => $item->email,
			"username"		 => $item->username,
			"last_name"      => $item->last_name,
			"clinic_name"    => $item->clinic_name,
			"referral_code"  => $item->referral_code,
			"address" 		 => $item->address,
			"city" 			 => $item->city,
			'status'		 => $item->status,
			"country" 		 => $item->country,
			"contact_number" => $item->contact_number,
			"date_format" 	 => $item->date_format,
			"language" 		 => $item->language,
			"api_token" 	 => $item->api_token,
			'created_at' 	 => $item->created_at->format('Y-m-d H:i:s'),
			"clinic_name"	 => $this->getClinicName($item->id),
			"case_count_pending"	 => $this->childCount($item->id,'cases','doctor_id','Completed','!='),
			"case_count_completed"	 => $this->childCount($item->id,'cases','doctor_id','Completed','='),
			"doctorstatus"   => $item->doctorstatus == 1 ,
			"doctorstatustext"   => $item->doctorstatus == 1 ? "Active" : "Inactive",
			"is_selected"   => $item->is_selected,
		];
	}

	public function getClinicName($id)
	{
		if($id != 0){
			$sql = "SELECT group_concat(clinic_details.clinic_name) as name  FROM 	clinic_doctor 
				join clinic_details on clinic_doctor.clinic_id = clinic_details.user_id
				WHERE  clinic_doctor.doctor_id = $id  " ;
			$result = collect( \DB::select($sql))->first();
			return str_replace(",,",",",$result->name); 
		}
		else{
			return '';
		}
		
	}

	public function childCount($id,$table_name ,$filed = 'clinic_id',$status='Pending',$check){
		$clinic_id = auth()->user()->id;
 		if($id != 0){
 			$count = \DB::table($table_name)
 			->where($filed, $id)
 			->where('status',$check ,$status)
 			->where('clinic_id', $clinic_id)
 			->count();
 			return $count;
 		}
 		else{
 			return '';
 		}
 	
 	}
}