<?php

namespace App\Transformers\Api;

use App\Transformers\Transformer;
use App\User;

class UsersTransformer extends  Transformer
{
	public function transform($item)
 	{
 		$item = User::find($item['id']);
 		$userRole = $item->roles()->first();
 		
 		if($userRole->name == 'Clinic')
 		{
 			$clinicDetail = $item->clinicDetail;
 			$hasDoctor = $this->childDoctorCount($item->id,'clinic_doctor');
 			return [
 				"id"             => $item->id,
 				"role"			 => strtolower($userRole->name),
 				"first_name"     => (string)$item->first_name,
 				"last_name"      => (string)$item->last_name,
 				"email" 		 => (string)$item->email,
 				"username" 		 => (string)$item->username,
 				"clinic_name"    => (string)$clinicDetail->clinic_name,
 				"image" 	 	 => !empty($item->image) ? AWS_URL . $item->image : "",
 				"image_thumb" 	 => !empty($item->image) ? AWS_URL . $item->image : "",
 				"date_format" 	 => (string)$item->date_format,
 				"language" 		 => (string)$item->language,
 				"contact_number" => (string)$item->contact_number,
 				"address" 		 => (string)$clinicDetail->address,
 				"city" 		 	 => (string)$clinicDetail->city,
 				"country" 		 => (string)$clinicDetail->country,
 				"referral_code"  => (string)$item->referral_code,
 				"api_token" 	 => (string)$item->api_token,
 				"is_purchased"   => $item->is_purchased,
 				'hasDoctor'		 => $hasDoctor,
 				'doctor_id'		 => $hasDoctor == 1 ? $this->getClinic($item->id,'doctor_id','clinic_id') : '' ,
 				'labCount'		 => $this->childCount($item->id,'clinic_labs')
 			];
 		}
 		else if($userRole->name == 'Doctor')
 		{
 			$hasDoctor = $this->childDoctorCount($item->id,'clinic_doctor','doctor_id');
 			return [
 				"id"             => $item->id,
 				"role"			 => strtolower($userRole->name),
 				"first_name"     => (string)$item->first_name,
 				"last_name"      => (string)$item->last_name,
 				"email" 		 => (string)$item->email,
 				"username" 		 => (string)$item->username,
 				"image" 	 	 => !empty($item->image) ? AWS_URL . $item->image : "",
 				"image_thumb" 	 => !empty($item->image) ? AWS_URL . $item->image : "",
 				/*"clinic_name"    => (string)$item->clinic_name,
 				"referral_code"  => (string)$item->referral_code,*/
 				"address" 		 => (string)$item->address,
 				"city" 		 	 => (string)$item->city,
 				"country" 		 => (string)$item->country,
 				"date_format" 	 => (string)$item->date_format,
 				"language" 		 => (string)$item->language,
 				"api_token" 	 => (string)$item->api_token,
 				"contact_number" => (string)$item->contact_number,
 				"is_purchased" 	 	=> $item->is_purchased,
 				"case_count_pending"  => $this->childCount($item->id,'cases','doctor_id'),
				'hasClinic'		 => $hasDoctor, 
 				"clinic_id" 	 => $hasDoctor == 1 ?  $this->getClinic($item->id) : '' ,
 			];	
 		}
 		else
 		{
 			return [
 				"id"             => $item->id,
 				"role"			 => strtolower($userRole->name),
 				"first_name"     => (string)$item->first_name,
 				"last_name"      => (string)$item->last_name,
 				"image" 	 	 => !empty($item->image) ? AWS_URL . $item->image : "",
 				"image_thumb" 	 => !empty($item->image) ? AWS_URL . $item->image : "",
 				"lab_name"    	 => (string)$item->full_name,
 				"referral_code"  => (string)$item->referral_code,
 				"address" 		 => (string)$item->address,
 				"city" 		 	 => (string)$item->city,
 				"country" 		 => (string)$item->country,
 				"date_format" 	 => (string)$item->date_format,
 				"language" 		 => (string)$item->language,
 				//"api_token" 	 => (string)$item->api_token,
 				"contact_number" => (string)$item->contact_number,
 			];
 		}
 
 	}

 	function getClinic($id,$select = 'clinic_id',$where='doctor_id'){
 		if($id != 0){
		$result = \DB::select("SELECT $select as list from clinic_doctor  WHERE $where = $id and status = 1 ") ;
		/*mpr("SELECT $select as list from clinic_doctor  WHERE $where = $id ");
		mprd($result);*/
 			return $result[0]->list;
 		}
 		else{
 			return '';
 		} 		
 	}

 	public function childDoctorCount($id,$table_name ,$filed = 'clinic_id'){
 		if($id != 0){
 			$count = \DB::table($table_name)->where($filed, $id)->where('status','1')->count();
 			return $count;
 		}
 		else{
 			return '';
 		}
 	
 	}

 	public function childCount($id,$table_name ,$filed = 'clinic_id'){
 		if($id != 0){
 			$count = \DB::table($table_name)->where($filed, $id)->count();
 			return $count;
 		}
 		else{
 			return '';
 		}
 	
 	}
 
 	public function transformRole($item)
 	{
 		return [
 				"id"             => $item->id,
 				"role"			 => strtolower($userRole->name),
 				"first_name"     => (string)$item->first_name,
 				"last_name"      => (string)$item->last_name,
 				"image" 	 	 => !empty($item->image) ? AWS_URL . $item->image : "",
 				"image_thumb" 	 => !empty($item->image) ? AWS_URL . $item->image : "",
 				"lab_name"    	 => (string)$item->full_name,
 				"referral_code"  => (string)$item->referral_code,
 				"address" 		 => (string)$item->address,
 				"city" 		 	 => (string)$item->city,
 				"country" 		 => (string)$item->country,
 				"date_format" 	 => (string)$item->date_format,
 				"language" 		 => (string)$item->language,
 				//"api_token" 	 => (string)$item->api_token,
 				"contact_number" => (string)$item->contact_number,
 			];
 

 	}

}