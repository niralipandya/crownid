<?php

namespace App\Transformers\Api;

use App\Transformers\Transformer;
use App\User;

class ClinicTransformer extends  Transformer
{
	public function transform($item)
 	{
 		return [
 				"id"             => $item->id,
 				"first_name"     => (string)$item->first_name,
 				"last_name"      => (string)$item->last_name,
 				"email" 		 => (string)$item->email,
 				"username" 		 => (string)$item->username,
 				"clinic_name"    => (string)$item->clinic_name,
 				"image" 	 	 => !empty($item->image) ? AWS_URL . $item->image : "",
 				"image_thumb" 	 => !empty($item->image) ? AWS_URL . $item->image : "",
 				"date_format" 	 => (string)$item->date_format,
 				"language" 		 => (string)$item->language,
 				"contact_number" => (string)$item->contact_number,
 				"address" 		 => (string)$item->address,
 				"city" 		 	 => (string)$item->city,
 				"country" 		 => (string)$item->country,
 				"referral_code"  => (string)$item->referral_code
 			];
 
 	}

 	function getClinic($id){
 		if($id != 0){
		$result = \DB::select("SELECT clinic_id as list from clinic_doctor  WHERE `doctor_id` = $id ") ;
 			return $result[0]->list;
 		}
 		else{
 			return '';
 		} 		
 	}

 	public function childCount($id,$table_name ,$filed = 'clinic_id'){
 		if($id != 0){
 			$count = \DB::table($table_name)->where($filed, $id)->count();
 			return $count;
 		}
 		else{
 			return '';
 		}
 	
 	}
 
 	public function transformRole($item)
 	{
 		return [
 				"id"             => $item->id,
 				"role"			 => strtolower($userRole->name),
 				"first_name"     => (string)$item->first_name,
 				"last_name"      => (string)$item->last_name,
 				"image" 	 	 => !empty($item->image) ? AWS_URL . $item->image : "",
 				"image_thumb" 	 => !empty($item->image) ? AWS_URL . $item->image : "",
 				"lab_name"    	 => (string)$item->full_name,
 				"referral_code"  => (string)$item->referral_code,
 				"address" 		 => (string)$item->address,
 				"city" 		 	 => (string)$item->city,
 				"country" 		 => (string)$item->country,
 				"date_format" 	 => (string)$item->date_format,
 				"language" 		 => (string)$item->language,
 				//"api_token" 	 => (string)$item->api_token,
 				"contact_number" => (string)$item->contact_number,
 			];
 

 	}

}