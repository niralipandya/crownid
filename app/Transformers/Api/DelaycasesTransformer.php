<?php

namespace App\Transformers\Api;

use App\Transformers\Transformer;
use App\User;
use App\Traits\DateFormate;

class DelaycasesTransformer extends  Transformer
{
	use DateFormate;
	public function transform($item)
	{ 
		return [
		"id"             		=> $item->id,
		"parent_id"		 		=> (string)$item->parent_id,
		"parent_case_id"		=> $this->getName($item->parent_id,'cases','case_code'),
		"first_name"			=> (string)$item->first_name,
		"case_code"				=>	$item->case_code,
		"last_name"		 		=> (string)$item->last_name,
		"lab_id"		 		=> $item->lab_id,
		"lab_name"				=> (string)$item->lab_name,
		"lab_image" 			=> (string)!empty($item->lab_image) ? AWS_URL . $item->lab_image : "",
		"doctor_id"		 		=> $item->doctor_id,
		"doctor_name"			=> (string)$item->doctor_name,
		"prosthesis_id"			=> $item->prosthesis_id,
		"status"				=> (string)$item->status,
		"end_date"				=>	$this->dateformate($item->due_date),
		"new_end_date"			=>	$this->dateformate($item->expeted_date),
		"process_date"	 		=> 	$this->dateformate($item->process_date),
		"reason"	=> ''
			];
	}
	// $case_ceramic = \DB::select("SELECT $select , IF( id =  '$ceramic',  'true',  'false' ) AS is_selected FROM case_ceramic"); 

	function getName($id,$tableName,$select){
		if($id != 0){
			$result = \DB::table($tableName)->select($select)->where('id', $id)->first() ;
			return $result->$select;
		}
		else{
			return '';
		}
		
	}

	public function getGroupName($id,$tableName)
	{
		if($id != 0){
			$sql = "SELECT group_concat(name) as name FROM $tableName WHERE id IN ($id)";
			$result = collect( \DB::select($sql))->first();
			return str_replace(",,",",",$result->name); 
		}
		else{
			return '';
		}
		
	}
}