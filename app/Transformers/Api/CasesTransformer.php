<?php

namespace App\Transformers\Api;

use App\Transformers\Transformer;
use App\User;
use App\Traits\DateFormate;


class CasesTransformer extends  Transformer
{
	use DateFormate;
	public function transform($item)
	{ 

		#mprd($item->toArray());

		$val1 = (string)$item->color_id;
		$var = strpos($val1, '3') ? $this->getName(3,'case_color','name') : '';

		$images = isset($item->images) ? $item->images->toArray() : array();
		return [
		"id"             		=> $item->id,
		"parent_id"		 		=> (string)$item->parent_id,
		"clinic_id"				=> $item->clinic_id,
		"parent_case_id"		=> $this->getName($item->parent_id,'cases','case_code'),
		"first_name"			=> (string)$item->first_name,
		"last_name"		 		=> (string)$item->last_name,
		"lab_id"		 		=> $item->lab_id,
		"lab_name"				=> (string)$item->lab_name,
		"lab_image" 			=> (string)!empty($item->lab_image) ? AWS_URL . $item->lab_image : "",
		"doctor_id"		 		=> $item->doctor_id,
		"doctor_name"			=> (string)$item->doctor_name,
		"prosthesis_id"			=> $item->prosthesis_id,
		"status"				=> (string)$item->status,
		"prosthesis_display"		=> $this->getName($item->prosthesis_id,'case_prosthesis','name'),
		
		"bite_registration_id"  => $item->bite_registration_id,
		"bite_registration_display"  => $this->getName($item->bite_registration_id,'case_bite_registration','name'),
		"problematic_teeth"		=> (string)$item->problematic_teeth,
		"end_date"				=> $item->end_date != '' ? $this->dateformate($item->end_date) : '-' ,
		"missing_teeth"			=> (string)$item->missing_teeth,

		"material_id"		 	=> $item->material_id,
		"material_other"		=> (string)$item->material_other,
		"material_display"  		=> $this->getName($item->material_id,'case_material','name'),

		"metal_ceramic_id"		=> (string)$item->metal_ceramic_id,
		"metal_ceramic_text"	=> (string)$item->metal_ceramic_text,
		"metal_ceramic_display" => $this->getName($item->metal_ceramic_id,'case_metal_ceramic','name'),
		
		"prosthetic_margins"	=> $item->prosthetic_margins,
		"prosthetic_margins_display" => $this->getName($item->prosthetic_margins,'case_prosthetic_margins','name'),

		"color_id"		 		=> $item->color_id,
		"color_text"		 	=> (string)$item->color_text,
		"color_display" 		=> $var,

		"color_neck_id"		 	=> (string)$item->color_neck_id,
		"color_neck_id_display" => $this->getGroupName($item->color_neck_id,'case_color_neck_option'),
		
		"color_body_id"		 	=> (string)$item->color_body_id,
		"color_body_id_display" => $this->getGroupName($item->color_body_id,'case_color_body_option'),

		"delivery_id"		 	=> $item->delivery_id,
		"delivery_text"		 	=> (string)$item->delivery_text,
		"delivery_display" 		=>  $this->getGroupName($item->delivery_id,'case_delivery'),
		"date_selected" 	=> (string)$item->date_selected,
		"delivery_date_text" 	=> (string)$item->delivery_date_text,
		"qr_code"	=> @$item->qr_code ? AWS_URL.$item->qr_code : '' ,
		"image"		=> @$images[0]['name'] ? AWS_URL.$images[0]['name'] : '' ,
		"bar_code"  => $item->bar_code,
		"case_code"	=> $item->case_code,
		"ceramic_id"			=> $item->ceramic_id,
		"ceramic_facial_text"	=> (string)$item->ceramic_facial_text,
		"process_date"	 		=> $this->dateformate($item->process_date),
		"ceramic_display"		=> $this->getName($item->ceramic_id,'case_ceramic','name'),
		"tryin_type"	=> (string)$item->tryin_type,
		"tryin_text"	=> (string)$item->tryin_text,
		"tryin_date"	=> (string)$item->tryin_date,
		"is_trying"				=> (string)$item->is_trying,
		"trying_by"				=> (string)$item->trying_by,
		"trying_status"			=> (string)$item->trying_status,
			];
	}
	// $case_ceramic = \DB::select("SELECT $select , IF( id =  '$ceramic',  'true',  'false' ) AS is_selected FROM case_ceramic"); 


	function dateformate($date){

        $clinic      = auth()->user();  
        $date_format = $clinic->date_format;

        switch ($date_format) {
            case "dd-MM-yyyy":
                $dateformat = "d-m-Y";
                break;
            case "MM-dd-yyyy":
                $dateformat = "m-d-Y";
                break;
            case "dd-MM-yy":
                $dateformat = "d-m-y";
                break;
            default:
                $dateformat = "m-d-y";
        }
        $newDate = date($dateformat, strtotime($date));
        return $newDate;
    }

	function getName($id,$tableName,$select){
		if($id != 0){
			$result = \DB::table($tableName)->select($select)->where('id', $id)->first() ;
			return $result->$select;
		}
		else{
			return '';
		}
		
	}

	public function getGroupName($id,$tableName)
	{
		if($id != 0){
			$sql = "SELECT group_concat(name) as name FROM $tableName WHERE id IN ($id)";
			$result = collect( \DB::select($sql))->first();
			return str_replace(",,",",",$result->name); 
		}
		else{
			return '';
		}
		
	}
}