<?php

namespace App\Transformers\Api;

use App\Transformers\Transformer;
use App\User;

class LabsTransformer extends  Transformer
{
	public function transform($item)
	{
		return [
				"id"             => $item->id,
				"contact_name"   => (string)$item->first_name,
				"email"   		 => (string)$item->email,
				"image" 	 	 => !empty($item->image) ? AWS_URL . $item->image : "",
				"image_thumb" 	 => !empty($item->image) ? AWS_URL . $item->image : "",
				"lab_name"    	 => (string)$item->full_name,
				"contact_number" => (string)$item->contact_number,
				"case_count"	 => $this->labCount($item->id,'cases')
				];
	}

	public function labCount($id,$tableName){
		if($id != 0){

			$count = \DB::table($tableName)->where('lab_id', $id)->count();
			return $count;
		}
		else{
			return '';
		}
	
	}

}