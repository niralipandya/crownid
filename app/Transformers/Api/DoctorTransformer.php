<?php

namespace App\Transformers\Api;

use App\Transformers\Transformer;
use App\User;

class DoctorTransformer extends  Transformer
{
	public function transform($item)
	{
		$clinic_id = auth()->user()->id;
		$hasDoctor = $this->childDoctorCount($clinic_id,'clinic_doctor');
		return [
				"id"             => $item->id,
				"first_name"     => (string)$item->first_name,
				"last_name"      => (string)$item->last_name,
				"email" 		 => (string)$item->email,
				"username" 		 => (string)$item->username,
				"image" 	 	 => !empty($item->image) ? AWS_URL . $item->image : "",
				"image_thumb" 	 => !empty($item->image) ? AWS_URL . $item->image : "",
				"date_format" 	 => (string)$item->date_format,
				"language" 		 => (string)$item->language,
				"contact_number" => (string)$item->contact_number,
				"doctorstatus"   => $item->doctorstatus == 1 ? "Active" : "Inactive",
				"total_case_count"	 => $this->totalCount($item->id,'cases','doctor_id',$clinic_id),
				"case_count_pending"	 => $this->childCount($item->id,'cases','doctor_id'),
				"case_count_completed"	 => $this->childCount($item->id,'cases','doctor_id','Completed'),
				"total_doctor" => $hasDoctor
			];
	}

	public function totalCount($id,$table_name ,$filed = 'clinic_id',$clinic_id){
 		if($id != 0){
 			$count = \DB::table($table_name)
 			->where($filed, $id)
 			->where('clinic_id' , $clinic_id)
 			->count();
 			return $count;
 		}
 		else{
 			return '';
 		}
 	
 	}


	public function childCount($id,$table_name ,$filed = 'clinic_id',$status='Pending'){
 		if($id != 0){
 			$count = \DB::table($table_name)
 			->where($filed, $id)
 			->where('status' ,$status)
 			->count();
 			return $count;
 		}
 		else{
 			return '';
 		}
 	
 	}

 	public function childDoctorCount($id,$table_name ,$filed = 'clinic_id'){
 		if($id != 0){
 			$count = \DB::table($table_name)->where($filed, $id)->where('status','1')->count();
 			return $count;
 		}
 		else{
 			return '';
 		}
 	
 	}

}