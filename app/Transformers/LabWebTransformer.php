<?php

namespace App\Transformers;
use \Carbon\Carbon;

class LabWebTransformer  extends  Transformer
{
	public function transform($item) 
	{
		return [
			"id"             => $item->id,
			"first_name"     => $item->first_name,
			"full_name"     => $item->full_name,
			"email"			 => $item->email,
			"username"		 => $item->username,
			"last_name"      => $item->last_name,
			"clinic_name"    => $item->clinic_name,
			"referral_code"  => $item->referral_code,
			"address" 		 => $item->address,
			"city" 			 => $item->city,
			"country" 		 => $item->country,
			"contact_number" => $item->contact_number,
			"date_format" 	 => $item->date_format,
			"language" 		 => $item->language,
			"api_token" 	 => $item->api_token,

			//'created_at' 	 => !empty($item->created_at) ? $item->created_at->format('Y-m-d H:i:s') : '' ,

			//'created_at' 	 => $item->created_at->format('Y-m-d H:i:s'),

			"clinic_name"	 => $this->getClinicName($item->id),
			"case_count_pending"	 => $this->childCount($item->id,'cases','lab_id','Completed','!='),
			"case_count_completed"	 => $this->childCount($item->id,'cases','lab_id','Completed','=')
		];
	}

	public function getClinicName($id)
	{
		if($id != 0){
			$sql = "SELECT group_concat(clinic_details.clinic_name) as name  FROM 	clinic_labs 
				join clinic_details on clinic_labs.clinic_id = clinic_details.user_id
				WHERE  clinic_labs.lab_id = $id  " ;
			$result = collect( \DB::select($sql))->first();
			return str_replace(",,",",",$result->name); 
		}
		else{
			return '';
		}
		
	}

	public function childCount($id,$table_name ,$filed = 'clinic_id',$status='Pending',$check){
		$clinic_id = auth()->user()->id;
 		if($id != 0){
 			$count = \DB::table($table_name)
 			->where($filed, $id)
 			->where('clinic_id',$clinic_id)
 			->where('status',$check ,$status)
 			->count();
 			return $count;
 		}
 		else{
 			return '';
 		}
 	
 	}
}