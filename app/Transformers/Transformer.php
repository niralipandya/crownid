<?php 

namespace App\Transformers;

abstract class Transformer{

	public function transformCollection(array $items){ 
		#mprd($this);
		return array_map([$this,'transform'],$items);
	}

	public abstract function transform($item);

	
} 