<?php

namespace App\Transformers;
class ClinicAdminTransformer extends  Transformer
{
	public function transform($item) 
	{
		return [
			"id"             => $item->id,
			"first_name"     => $item->first_name,
			"email"			 => $item->email,
			"current_package" 	=> $item->current_package,
			"current_plan" 		=> $this->getPlan($item->id),
			"username"		 => $item->username,
			"last_name"      => $item->last_name,
			"clinic_name"    => $item->clinic_name,
			"referral_code"  => $item->referral_code,
			"address" 		 => $item->address,
			"city" 			 => $item->city,
			"country" 		 => $item->country,
			"contact_number" => $item->contact_number,
			"status"		 => $item->status,
			"date_format" 	 => $item->date_format,
			"language" 		 => $item->language,
			"api_token" 	 => $item->api_token,
			'created_at' 	 => $item->created_at,
			'hasDoctor'		 => $this->childCount($item->id,'clinic_doctor'),
			"case_count_pending"	 => $this->caseCount($item->id,'cases','clinic_id','Completed','!='),
			"case_count_completed"	 => $this->caseCount($item->id,'cases','clinic_id','Completed'),
			"case_count_pending_lab"	 => $this->caseCountlab($item->id,'cases','clinic_id','Completed','!='),
			"case_count_completed_lab"	 => $this->caseCountlab($item->id,'cases','clinic_id','Completed')
		];
	}

	public function getPlan($userId){
		$subscription = \DB::table('subscriptions')->where('user_id',$userId)->first();
		if($subscription){
			$current_plan = $subscription->type != '' ? $subscription->plan." ($subscription->type)" : '' ;
		}
		else{
			$current_plan = '-';
		}
		return $current_plan;
	}


	public function childCount($id,$table_name ,$filed = 'clinic_id',$status ='Pending'){
 		if($id != 0){
 			$count = \DB::table($table_name)->where($filed, $id)
 			->count();
 			return $count;
 		}
 		else{
 			return '0';
 		}
 	
 	}

 	public function caseCountlab($id,$table_name ,$filed = 'clinic_id',$status ='Pending',$check = '='){
 		$lab_id = auth()->user()->id;
 		if($id != 0){
 			$count = \DB::table($table_name)->where($filed, $id)->where('lab_id',$lab_id)->where('status',$check, $status)->count();
 			return $count;
 		}
 		else{
 			return '0';
 		}
 	
 	}

	public function caseCount($id,$table_name ,$filed = 'clinic_id',$status ='Pending',$check = '='){
 		if($id != 0){
 			$count = \DB::table($table_name)->where($filed, $id)->where('status',$check, $status)->count();
 			return $count;
 		}
 		else{
 			return '0';
 		}
 	
 	}
}