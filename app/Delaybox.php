<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delaybox extends Model
{
    protected $table = 'delayboxes';
    protected $fillable = [
        "id", "clinic_id", "lab_id", "doctor_id", "case_code", "due_date", "expeted_date", "remarks", "status", "addedBy", "addedByType"
    ];
    
}
