<?php

namespace App\Observers;

use App\Clinic;
use App\Mail\ClinicRegister;
use Illuminate\Support\Facades\Mail;

class ClinicObserver
{
    public function creating(Clinic $clinic)
    {
        $clinic->referral_code  = str_random(10);
        $clinic->api_token      = str_random(60);

        return $clinic;
    }

    public function created(Clinic $clinic)
    {

    }

    public function saving(Clinic $clinic)
    {
        
    }

    public function saved(Clinic $clinic)
    {

    }

    public function deleting(Clinic $clinic)
    {

    }

    public function deleted(Clinic $clinic)
    {

    }

}