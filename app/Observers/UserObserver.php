<?php

namespace App\Observers;

use App\User;

class UserObserver
{
    public function creating(User $user)
    {
        $user->referral_code  = str_random(10);
        $user->api_token      = str_random(60);
        return $user;
    }

    public function created(User $user)
    {
        
    }

    public function saving(User $user)
    {
        
    }

    public function saved(User $user)
    {

    }

    public function deleting(User $user)
    {

    }

    public function deleted(User $user)
    {

    }

}