<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Cases extends Model
{
	use SoftDeletes ;
    protected $table = 'cases';
    protected $softDelete = true;
    protected $fillable = ['lab_id','doctor_id','first_name','last_name','prosthesis_id','problematic_teeth','bite_registration_id','missing_teeth','material_id','material_other','metal_ceramic_id','metal_ceramic_text','prosthetic_margins','color_id','color_text','color_neck_id','color_body_id','delivery_id','delivery_text','parent_id','have_child','clinic_id','created_by_id','created_by_name','bar_code','ceramic_id','process_date','case_code','retake_text','solder_text','tryin_type','tryin_text','ceramic_facial_text','delivery_date_text','date_selected'];

    public function images(){
        return $this->belongsToMany(Image::class);
    }

    public function givePermissionTo(Image $image){
        return $this->images()->save($image);
    }

    public function detachPermissionFrom(Image $image){
        return $this->images()->detach($image);
    }
    
}
