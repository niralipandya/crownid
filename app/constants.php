<?php

// Aws S3 default constants
define('AWS_URL', 'https://s3-ap-south-1.amazonaws.com/viragtesting/');

// User Profile Image Path
define('USER_PROFILE_PATH', 'crownid/users/profile_images');
define('CASE_QR_PATH', 'crownid/cases/qr_images');
define('CASE_PATH', 'crownid/cases/images');
define('DOCTOR_PATH', 'crownid/doctor');

function pr($arr)
{
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
}