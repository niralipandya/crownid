<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
	protected $table = 'pages';
    protected $fillable = [
        "title_en","title_es","slug",'description_en','description_es','status'
    ];
   
}
