<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tokenhistory extends Model
{
    protected $table = 'tokenhistory';
    protected $fillable = [
        'id', 'user_id', 'amount', 'currency', 'remark', 'message', 'token_count', 'user_type', 'type', 'paid_from', 'payment_id', 'created_at', 'updated_at'
    ];
}
