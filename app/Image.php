<?php
//php artisan make:migration create_clinic_schedule_table --create=clinic_schedule
namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	protected $table = 'images';

	protected $fillable = ["name"];

    public function cases(){
        return $this->belongsToMany(Cases::class);
    }

}
