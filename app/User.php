<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Observers\UserObserver;
use App\Traits\HasRoles;
use App\ClinicDetail;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    protected $table = 'users';

    

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_purchased'  => 'boolean',
        'is_registered' => 'boolean',
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at','updated_at'];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        User::observe(UserObserver::class);
    }

    // ----------------------------------------------------------------------------------------
    // Setter for Class
    // ----------------------------------------------------------------------------------------
    public function setEmailAttribute($email)
    {
        $this->attributes['email'] = strtolower($email);
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function setDateFormatAttribute($dateFormat)
    {
        $this->attributes['date_format'] = trim($dateFormat);
    }

    // ----------------------------------------------------------------------------------------
    // Relationship with "Clinic Details" table
    // ----------------------------------------------------------------------------------------
    public function clinicDetail()
    {
        return $this->hasOne(ClinicDetail::class);
    }

    // Save Clinic details
    public function saveClinicDetail(ClinicDetail $clinicDetail)
    {
        // if clinic details not available than create details
        if($this->clinicDetail == null)
        {
            $this->clinicDetail()->save($clinicDetail);
        }
        else
        {
            $this->clinicDetail()->update($clinicDetail->toArray());
        }
    }

    // ----------------------------------------------------------------------------------------
    // Relationship with "Clinic Doctors" table  // Get Doctors for clinics
    // ----------------------------------------------------------------------------------------
    public function doctors()
    {
        return $this->belongsToMany(User::class, 'clinic_doctor', 'clinic_id', 'doctor_id');
    }

    // ----------------------------------------------------------------------------------------
    // Relationship with "Clinic Doctors" table  // Get Clinics for doctor
    // ----------------------------------------------------------------------------------------
    public function doctorClinics()
    {
        return $this->belongsToMany(User::class, 'clinic_doctor', 'doctor_id', 'clinic_id');
    }

    public function givePermissionToDoctor(User $user){
        return $this->doctors()->save($user);
    }

    // ----------------------------------------------------------------------------------------
    // Relationship with "Clinic Labs" table  // Get Labs for clinics
    // ----------------------------------------------------------------------------------------


    
    public function labs()
    {
        return $this->belongsToMany(User::class, 'clinic_labs', 'clinic_id', 'lab_id');
    }

    public function givePermissionToLab(User $user){
        return $this->labs()->save($user);
    }


    // ----------------------------------------------------------------------------------------
    // Relationship with "Clinic Labs" table  // Get Clinics for Labs
    // ----------------------------------------------------------------------------------------
    public function labClinics()
    {
        return $this->belongsToMany(User::class, 'clinic_labs', 'lab_id', 'clinic_id');
    }
}
