const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('resources/assets/admin/app.scss','public/css/admin-app.css')
   .sass('resources/assets/clinic/clinic.scss','public/css/clinic-app.css')
   .sass('resources/assets/clinic/clinic-inner.scss','public/css/clinic-inner.css')
   .sass('resources/assets/lab/lab.scss','public/css/lab.css')
   .js('resources/assets/admin/js/main-buid.js','public/js/admin-main.js')
   .js('resources/assets/clinic/js/clean-blog.min.js','public/js/clinic-app.js')
   .js('resources/assets/clinic/js/custom.js','public/js/custom-clinic.js')
   .js('resources/assets/clinic/js/common.js','public/js/common-clinic.js')
   .js('resources/assets/lab/js/common.js','public/js/common-lab.js')
   .scripts([
        	'public/plugins/slimScroll/jquery.slimscroll.min.js',
        	'public/plugins/fastclick/fastclick.js',
        	'public/plugins/iCheck/icheck.min.js',
        	'resources/assets/admin/js/app.min.js'
        ],'public/js/admin-app.js');

mix.copy('node_modules/intl-tel-input', 'public/plugins/intl-tel-input');