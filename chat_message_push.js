var live = "http://52.77.117.85:8004/api/v1/chatNotification";
//var live = "http://www.portamed.org/v1/apis/chatNotification";
var Firebase = require('firebase');
var Queue = require('firebase-queue');

Firebase.initializeApp({
  databaseURL: "https://crownid-e6f21.firebaseio.com/"
});

var queueRef = Firebase.database().ref('queue');

var queue = new Queue(queueRef, function(data, progress, resolve, reject) {

  // Read and process task data
  console.log(data);
  var messageText     = data.messageText;
  var receiverId      = 0;
  var groupID         = data.groupID;
  var senderId        = data.senderId;
  var memberID        = data.memberID;
  var messageType     = data.messageType;
  var senderName      = data.senderName;


  // Update the progress state of the task
  setTimeout(function() {
    progress(50);
  }, 500);

  // Call Ajax for processing 
  var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
  var xhttp = new XMLHttpRequest();
  
  var params = "receiverId=" + receiverId + "&messageText=" + messageText + "&groupID="+groupID + "&senderId="+senderId + "&memberID="+memberID + "&messageType="+messageType + "&senderName="+senderName;

  xhttp.open("POST", live, true);
  xhttp.setRequestHeader("Content-type", "multipart/form-data");
  //xhttp.setRequestHeader("Xapi", "jwZryPtnDm5WFpmDhl2o750G5gVFVSYhpXbg7tlmO");
  xhttp.send(params);

  // get response
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      console.log(xhttp.responseText);
    }
  };

  // Finish the job asynchronously
  setTimeout(function() {
    resolve();
  }, 1000);

});
