<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Transformers\Api\UsersTransformer;
use App\User;

use App\Mail\UserResetPassword;
use Illuminate\Support\Facades\Mail;

class UsersController extends ApiController
{
    protected $usersTransformer;

    // Constructor
    public function __construct()
    {
        $this->middleware('auth:api', [
            'except' => [ 'checkEmail', 'purchase', 'login', 'forgotPassword', 'resetPassword', ]
        ]);
        
        $this->usersTransformer = new UsersTransformer;
    }

    /**
     * Login User api for doctor and clinic.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'username'  => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->respondValidationError(
                trans('api.fail_validation'), $validator->messages());
        }

        // check user auth and send response
        if (auth()->attempt(['username' => $request->username, 'password' => $request->password, 'status' => 1])) 
        {
            $clinic = auth()->guard()->user();
            if($clinic['api_token'] == ''){
                $clinic->api_token = str_random(60);
                $clinic->save();
            }
            if($clinic->is_purchased == 1)
            {
                // save device token
                $clinic->device_token = $request->device_token;
                $clinic->save();

                return $this->respond([
                    "data"    => [
                        "user" => $this->usersTransformer->transform($clinic),
                    ],
                    "message" => trans('api.clinic_authenticated'),
                    "status"  => $this->getStatuscode(),
                ]);
            }
        }

        return $this->respond([
            "message" => trans('api.clinic_unauthorise'),
            "status"  => 400,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkEmail(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'email' => 'required|email',
        ]);

        // if error than response error with its message
        if($validator->fails()) 
        {
            return $this->respondValidationError(trans('api.fail_validation'), $validator->messages());
        }

        // check email is registered or not
        $clinic = User::where('email', $request->email)->first();

        if($clinic)
        {
            if($clinic->is_purchased == 1 && $clinic->is_registered == 1)
            {
                return $this->respond([
                    "message" => trans('api.email_registered_login'),
                    "status"  => 500,
                ]);
            }

            return $this->respond([
                "message" => trans('api.email_registered'),
                "status"  => 200,
                "data"    => [
                    "is_purchased" => $clinic->is_purchased,
                    "api_token" => $clinic->api_token,
                    "is_registered" => $clinic->is_registered,
                ],
            ]);
        }
        else
        {
            return $this->respond([
                "message" => trans('api.email_available'),
                "status"  => $this->getStatuscode(),
                "data"    => [
                    "is_purchased" => false,
                    "api_token" => '',
                    "is_registered" => false,
                ],
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function purchase(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'email' => 'required|email',
        ]);

        // if error than response error with its message
        if($validator->fails()) 
        {
            return $this->respondValidationError(trans('api.fail_validation'), $validator->messages());
        }

        // check email is registered or not
        $clinic = User::where('email', $request->email)->first();

        if($clinic)
        {
            $clinic->is_purchased == 1;
            $clinic->save();

            // check role for user
            //$clinic->assignRole('Clinic');

            return $this->respond([
                "message" => trans('api.purchase_successfully'),
                "status"  => $this->getStatuscode(),
                "data"    => [
                    "id" => $clinic->id,
                    "api_token" => $clinic->api_token,
                    "is_purchased" => $clinic->is_purchased,
                ],
            ]);
        }
        else
        {
            $clinic = User::create([
                "email"        => $request->email,
                "is_purchased" => 1,
            ]);
            
            $clinic->assignRole('Clinic');

            return $this->respond([
                "message" => trans('api.register_purchase'),
                "status"  => $this->getStatuscode(),
                "data"    => [
                    "id" => $clinic->id,
                    "api_token" => $clinic->api_token,
                    "is_purchased" => $clinic->is_purchased,
                ],
            ]);
        }
    }

    /**
     * Forgot password.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function forgotPassword(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'email'  => 'required'
        ]);
        if ($validator->fails()) {
            return $this->respondValidationError(
                trans('api.fail_validation'),$validator->messages());
        }

        // check user auth and send response
        if ($user = User::where('email', $request->email)->first())
        {
            // save otp in user table
            $user->otp = rand(11111, 99999);
            $user->save();

            Mail::to($user)->send(new UserResetPassword($user));

            return $this->respond([
                "data" => [
                    "email" => $request->email,
                ],
                "message" => trans('api.clinic_forgot_password'),
                "status"  => $this->getStatuscode(),
            ]);
        }

        return $this->respond([
            "message" => trans('api.clinic_not_register'),
            "status"  => 400,
        ]);
    }

    /**
     * Reset password.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function resetPassword(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'otp' => 'required',
            'email' => 'required',
            'password'  => 'required',
        ]);
        if ($validator->fails()) {
            return $this->respondValidationError(
                trans('api.fail_validation'),$validator->messages());
        }

        // check user auth and send response
        if ($user = User::where('email', $request->email)->first())
        {
            if($user->otp == $request->otp)
            {
                $user->otp = null;
                $user->password = $request->password;

                $user->save();
                return $this->respond([
                    "message" => trans('api.clinic_password_reset'),
                    "status"  => $this->getStatuscode(),
                ]);
            }
            else
            {
                return $this->respond([
                    "message" => trans('api.clinic_otp_invalid'),
                    "status"  => 500,
                ]);
            }
        }

        return $this->respond([
            "message" => trans('api.clinic_not_register'),
            "status"  => 400,
        ]);
    }

    /**
     * Change password.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
        $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);

        $validator = validator()->make($request->all(), [
            'old_password' => 'required',
            'password'  => 'required',
        ]);
        if ($validator->fails()) {
            return $this->respondValidationError(
                trans('api.fail_validation'),$validator->messages());
        }

            
        // check user auth and send response
        if (Hash::check($request->old_password, $user->password))
        {

            $user->password = $request->password;
            $user->save();
            
            return $this->setStatuscode(200)->respond([
                "message" => trans('api.clinic_change_password'),
                "status"  => $this->getStatuscode(),
            ]);
        }
        return $this->setStatuscode(400)->respond([
            "message" => trans('api.clinic_password_invalid'),
            "status"  => $this->getStatuscode(),
        ]);
    }


    /**
     * Match password.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function matchPassword(Request $request)
    {
        $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);

        $validator = validator()->make($request->all(), [
            'current_password' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->respondValidationError(
                trans('api.fail_validation'),$validator->messages());
        }

        // check user auth and send response
        if (Hash::check($request->current_password, $user->password))
        {

            return $this->setStatuscode(200)->respond([
                "message" => trans('api.match_password'),
                "status"  => $this->getStatuscode(),
            ]);
        }
        return $this->setStatuscode(400)->respond([
            "message" => trans('api.password_invalid'),
            "status"  => $this->getStatuscode(),
        ]);
    }


    /*
     * Change Language and Date format
     */
    public function languageAndDate(Request $request)
    {
        $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);
        
        $user->language    = $request->language;
        $user->date_format = $request->date_format;
        $user->save();

        return $this->respond([
            "message" => trans('api.setting_save'),
            "status"  => $this->getStatuscode(),
            "data" => [
                "user" => $this->usersTransformer->transform($user) 
            ]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);

        return $this->respond([
            "data" => [
                "user" => $this->usersTransformer->transform($user),
            ],
            "message" => trans('api.clinic_register'),
            "status" => $this->getStatuscode(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getMetalOption(){ 
        $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);

        $selected = $user->preferred_metal;
        $name = $user->language.'_name';
        $select = " id , $name as name ";
        $case_metal_ceramic = \DB::select(
               "SELECT $select , IF( id =  '$selected',  'true',  'false' ) AS is_selected FROM case_metal_ceramic where setting  = 1 ");
        $returnData['option'] =  $case_metal_ceramic;
        
        return $this->respond([
                    "data"    => [
                        "detail" => $returnData,
                    ],
                    "message" => trans('api.option'),
                    "status"  => $this->getStatuscode(),
                ]);
    }

    public function saveMetal(Request $request){ 

        $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);

        $user->preferred_metal    = $request->preferred_metal;
        $user->save();

        return $this->respond([
            "message" => trans('api.setting_save'),
            "status"  => $this->getStatuscode(),
            "data" => [
                "user" => $this->usersTransformer->transform($user) 
            ]
        ]);
    }

    public function getProstheticOption(){ 
        $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);

        $selected = $user->prosthetic_margins;
        $name = $user->language.'_name';
        $select = " id , $name as name ";
        $case_prosthetic_margins = \DB::select(
               " SELECT $select , IF( id =  '$selected',  'true',  'false' ) AS is_selected  FROM case_prosthetic_margins where setting  = 1  ");
        $returnData['option'] =  $case_prosthetic_margins;
        return $this->respond([
                    "data"    => [
                        "detail" => $returnData,
                    ],
                    "message" => trans('api.option'),
                    "status"  => $this->getStatuscode(),
                ]);
    }

    public function saveProsthetic(Request $request){ 

        $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);
        $user->prosthetic_margins    = $request->prosthetic_margins;
        $user->save();

        return $this->respond([
            "message" => trans('api.setting_save'),
            "status"  => $this->getStatuscode(),
            "data" => [
                "user" => $this->usersTransformer->transform($user) 
            ]
        ]);
    }

    public function getDeliveryOption(){ 
        $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);
        $selected = $user->delivery;
        $name = $user->language.'_name';
        $select = " id , $name as name ";
        $case_delivery = \DB::select(
               " SELECT $select , IF( id =  '$selected',  'true',  'false' ) AS is_selected FROM case_delivery where setting = 1 ");
        $returnData['option'] =  $case_delivery;
        $returnData['selected'] =  array('id' => $selected);
        return $this->respond([
                    "data"    => [
                        "detail" => $returnData,
                    ],
                    "message" => trans('api.option'),
                    "status"  => $this->getStatuscode(),
                ]);
    }

    public function saveDelivery(Request $request){ 

        $user = auth()->user();
        $language = $user->language != '' ? $user->language : 'en';
        app()->setLocale($language);
        $user->delivery    = $request->delivery;
        $user->save();

        return $this->respond([
            "message" => trans('api.setting_save'),
            "status"  => $this->getStatuscode(),
            "data" => [
                "user" => $this->usersTransformer->transform($user) 
            ]
        ]);
    }

    public function logout(){
        
        $user_login = auth()->user();
        $language = $user_login->language != '' ? $user_login->language : 'en';
        app()->setLocale($language);
        
        $user_login->api_token = null;
        $user_login->save();
        return $this->setStatuscode(200)
            ->respond([
                "message"     => trans('api.logout',['first_name'=>$user_login->first_name]),
                "status" => $this->getStatuscode(),
            ]
        );
    }

}
