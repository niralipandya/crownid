<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Transformers\Api\UsersTransformer;
use App\User;
use App\Traits\FileManipulationTrait;

class ClinicsController extends ApiController
{
    use FileManipulationTrait;
    protected $usersTransformer;

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['forgotPassword', 'resetPassword']]);
        $this->usersTransformer = new UsersTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        //
    }

    

    /**
     * Display a record.
     *
     * @return \Illuminate\Http\Response
     */
    public function labs()
    {
        $clinic = auth()->user();
        $language = $clinic->language != '' ? $clinic->language : 'en';
        app()->setLocale($language);
        $labs = $clinic->labs;
        
        if($labs)
        {
            return $this->respond([
                "data" => [
                    "labs" => $this->usersTransformer->transformCollection($labs->toArray()),
                ],
                "message" => trans('api.lab_list'),
                "status" => $this->getStatuscode(),
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $clinic = auth()->user();
        $language = $clinic->language != '' ? $clinic->language : 'en';
        app()->setLocale($language);

        $validator = validator()->make($request->all(), [
            'clinic_name'   => 'required',
            'first_name'    => 'required',
            'last_name'     => 'required',
            'username'      => 'required|unique:users,username,'.$clinic->id,
            'password'      => 'required',
            'image'         => 'image|mimes:jpg,jpeg,png|max:15000',
            'contact_number'=> 'required',
        ]);

        // if error than response error with its message
        if($validator->fails()) {
            return $this->respondValidationError(
                trans('api.fail_validation'), $validator->messages());
        }

        
        // clinic basic details
        $clinic->first_name     = $request->first_name;
        $clinic->last_name      = $request->last_name;
        $clinic->username       = $request->username;
        $clinic->contact_number = $request->contact_number;
        $clinic->password       = $request->password;
        
        if ($request->hasFile('image')) {
            // save image in local
            $image = $this->quickUpload($request->image, USER_PROFILE_PATH);
            // transfer image to s3
            $this->transferFileToS3($image);
            // delete image from local
            unlink(storage_path('app/public/'.$image));

            // save image in database
            $clinic->image = $image;
        }
        
        // Save Clinic related details
        $clinicDetail = new \App\ClinicDetail;
        $clinicDetail->clinic_name    = $request->clinic_name;
        $clinicDetail->address        = $request->address;
        $clinicDetail->city           = $request->city;
        $clinicDetail->country        = $request->country;

        if($clinic->save())
        {
            $clinic->saveClinicDetail($clinicDetail);

            return $this->respond([
                "data" => [
                    "user" => $this->usersTransformer->transform($clinic),
                ],
                "message" => trans('api.clinic_register'),
                "status" => $this->getStatuscode(),
            ]);
        }
        else
        {
            return $this->respond([
                "message" => trans('api.register_error'),
                "status" => 500,
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $clinic = auth()->user();
        $language = $clinic->language != '' ? $clinic->language : 'en';
        app()->setLocale($language);

        $validator = validator()->make($request->all(), [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'contact_number'=> 'required',
        ]);

        // if error than response error with its message
        if($validator->fails()) {
            return $this->respondValidationError(
                trans('api.fail_validation'), $validator->messages());
        }

        // clinic basic details
        $clinic->first_name     = $request->first_name;
        $clinic->last_name      = $request->last_name;
        $clinic->contact_number = $request->contact_number;


        $userRole = $clinic->roles()->first();
        if($userRole->name == 'Clinic')
        {
            $clinicDetail = new \App\ClinicDetail;

            $clinicDetail->clinic_name    = $request->clinic_name;
            $clinicDetail->address        = $request->address;
            $clinicDetail->city           = $request->city;
            $clinicDetail->country        = $request->country;
            $clinic->saveClinicDetail($clinicDetail);
        }
        if ($request->hasFile('image')) {
            // save image in local
            $image = $this->quickUpload($request->image, USER_PROFILE_PATH);
            // transfer image to s3
            $this->transferFileToS3($image);
            // delete image from local
            unlink(storage_path('app/public/'.$image));

            // save image in database
            $clinic->image = $image;
        }
        
        if($clinic->save())
        {
            $clinic = User::find($clinic->id);
            return $this->respond([
                "data" => [
                    "user" => $this->usersTransformer->transform($clinic),
                ],
                "message" => trans('api.clinic_edit'),
                "status" => $this->getStatuscode(),
            ]);
        }
        else
        {
            return $this->respond([
                "message" => trans('api.clinic_edit_arror'),
                "status" => 500,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /* 
    * Sign up with email (check email := is email register than check purchased or not)
    */
    public function checkEmail(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'email' => 'required|email',
        ]);

        // if error than response error with its message
        if($validator->fails()) 
        {
            return $this->respondValidationError(trans('api.fail_validation'), $validator->messages());
        }

        // check email is registered or not
        $clinic = Clinic::where('email', $request->email)->first();

        if($clinic)
        {
            if($clinic->is_purchased == 1 && $clinic->is_registered == 1)
            {
                return $this->respond([
                    "message" => trans('api.email_registered_login'),
                    "status"  => 500,
                ]);
            }

            return $this->respond([
                "message" => trans('api.email_registered'),
                "status"  => 400,
                "data"    => [
                    "is_purchased" => $clinic->is_purchased,
                    "is_registered" => $clinic->is_registered,
                ],
            ]);
        }
        else
        {
            return $this->respond([
                "message" => trans('api.email_available'),
                "status"  => $this->getStatuscode(),
                "data"    => [
                    "is_purchased" => false,
                    "is_registered" => false,
                ],
            ]);
        }
    }

    public function purchase(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'email' => 'required|email',
        ]);

        // if error than response error with its message
        if($validator->fails()) 
        {
            return $this->respondValidationError(trans('api.fail_validation'), $validator->messages());
        }

        // check email is registered or not
        $clinic = Clinic::where('email', $request->email)->first();

        if($clinic)
        {
            $clinic->is_purchased == 1;
            $clinic->save();

            return $this->respond([
                "message" => trans('api.purchase_successfully'),
                "status"  => $this->getStatuscode(),
                "data"    => [
                    "id" => $clinic->id,
                    "api_token" => $clinic->api_token,
                    "is_purchased" => $clinic->is_purchased,
                ],
            ]);
        }
        else
        {
            $clinic = Clinic::create([
                "email"        => $request->email,
                "is_purchased" => 1,
            ]);
            
            return $this->respond([
                "message" => trans('api.register_purchase'),
                "status"  => $this->getStatuscode(),
                "data"    => [
                    "id" => $clinic->id,
                    "api_token" => $clinic->api_token,
                    "is_purchased" => $clinic->is_purchased,
                ],
            ]);
        }
    }

    /**
     * Register details of clinic
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $clinic = auth()->user();

        $validator = validator()->make($request->all(), [
            'clinic_name'   => 'required',
            'first_name'    => 'required',
            'last_name'     => 'required',
            'username'      => 'required|unique:clinics,username,'.$clinic->id,
            'password'      => 'required',
            'image'         => 'image|mimes:jpg,jpeg,png|max:15000',
            'contact_number'=> 'required',
        ]);

        // if error than response error with its message
        if($validator->fails()) {
            return $this->respondValidationError(
                trans('api.fail_validation'), $validator->messages());
        }
        
        $clinic->clinic_name    = $request->clinic_name;
        $clinic->first_name     = $request->first_name;
        $clinic->last_name      = $request->last_name;
        $clinic->address        = $request->address;
        $clinic->city           = $request->city;
        $clinic->country        = $request->country;
        $clinic->username       = $request->username;
        $clinic->password       = $request->password;
        $clinic->contact_number = $request->contact_number;

        if($clinic->save())
        {
            return $this->respond([
                "data" => [
                    "user" => $this->clinicsTransformer->transform($clinic),
                ],
                "message" => trans('api.clinic_register'),
                "status" => $this->getStatuscode(),
            ]);
        }
        else
        {
            return $this->respond([
                "message" => trans('api.register_error'),
                "status" => 500,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'username'  => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->respondValidationError(
                trans('api.fail_validation'), $validator->messages());
        }

        // check user auth and send response
        if (auth()->attempt(['username' => $request->username, 'password' => $request->password, 'status' => 1])) 
        {
            $clinic = auth()->guard()->user();
            
            if($clinic->is_purchased == 1)
            {
                // save device token
                $clinic->device_token = $request->device_token;
                $clinic->save();

                return $this->respond([
                    "data"    => [
                        "user" => $this->clinicsTransformer->transform($clinic),
                    ],
                    "message" => trans('api.clinic_authenticated'),
                    "status"  => $this->getStatuscode(),
                ]);
            }
        }

        return $this->respond([
            "message" => trans('api.clinic_unauthorise'),
            "status"  => 400,
        ]);
    }

    /**
     * Forgot password.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function forgotPassword(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'email'  => 'required'
        ]);
        if ($validator->fails()) {
            return $this->respondValidationError(
                trans('api.fail_validation'),$validator->messages());
        }

        // check user auth and send response
        if ($user = User::where('email', $request->email)->first())
        {
            // save device token
            $clinic->otp = rand(11111, 99999);
            $clinic->save();

            Mail::to($clinic)->send(new ClinicResetPassword($clinic));

            return $this->respond([
                "data" => [
                    "email" => $request->email,
                ],
                "message" => trans('api.clinic_forgot_password'),
                "status"  => $this->getStatuscode(),
            ]);
        }

        return $this->respond([
            "message" => trans('api.clinic_not_register'),
            "status"  => 400,
        ]);
    }

    /**
     * Reset password.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function resetPassword(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'otp' => 'required',
            'email' => 'required',
            'password'  => 'required',
        ]);
        if ($validator->fails()) {
            return $this->respondValidationError(
                trans('api.fail_validation'),$validator->messages());
        }

        // check user auth and send response
        if ($clinic = Clinic::where('email', $request->email)->first())
        {
            if($clinic->otp == $request->otp)
            {
                $clinic->otp = null;
                $clinic->password = $request->password;

                $clinic->save();
                return $this->respond([
                    "message" => trans('api.clinic_password_reset'),
                    "status"  => $this->getStatuscode(),
                ]);
            }
            else
            {
                return $this->respond([
                    "message" => trans('api.clinic_otp_invalid'),
                    "status"  => 500,
                ]);
            }
        }

        return $this->respond([
            "message" => trans('api.clinic_not_register'),
            "status"  => 400,
        ]);
    }

    /**
     * Change password.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'old_password' => 'required',
            'password'  => 'required',
        ]);
        if ($validator->fails()) {
            return $this->respondValidationError(
                trans('api.fail_validation'),$validator->messages());
        }

        $clinic = auth()->user();
       
            
        // check user auth and send response
        if (Hash::check($request->old_password, $clinic->password))
        {

            $clinic->password = $request->password;
            $clinic->save();
            
            return $this->setStatuscode(200)->respond([
                "message" => trans('api.clinic_change_password'),
                "status"  => $this->getStatuscode(),
            ]);
        }
        return $this->setStatuscode(400)->respond([
            "message" => trans('api.clinic_password_invalid'),
            "status"  => $this->getStatuscode(),
        ]);
    }

}
