<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Plan;
use App\Credittype;

class PlanController extends ApiController
{
    public function __construct()
    {
        $this->middleware('auth:api', [
            'except' => [ 'creditview', 'planview']
        ]);
    }

   function creditview(){
        $credit = Credittype::get();
        $creditArray = $credit->toArray();

        return $this->respond([
                "data" => [
                    "creditArray" => $creditArray,
                ],
                "message" => trans('api.creditview'),
                "status" => $this->getStatuscode(),
            ]);

        $i=0;
        foreach ($creditArray as $key => $value) {
            $returnData[$i] = $value;
            $returnData[$i]['text'] = $value;
            $i++;
        }
   }

   function planview(){
        $plan = Plan::get();
        $planArray = $plan->toArray();

        return $this->respond([
                "data" => [
                    "planArray" => $planArray,
                ],
                "message" => trans('api.planview'),
                "status" => $this->getStatuscode(),
            ]);
        
        $i=0;
        foreach ($planArray as $key => $value) {
            $returnData[$i] = $value;
            $returnData[$i]['text'] = $value;
            $i++;
        }
   }

}
