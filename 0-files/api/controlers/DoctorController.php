<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\Api\UsersTransformer;
use App\Transformers\Api\DoctorTransformer;
use App\User;
use App\Permission;
use Illuminate\Support\Facades\Mail;

use App\Mail\DoctorCreate;

class DoctorController extends ApiController
{

    protected $usersTransformer;

    // Constructor
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->usersTransformer = new UsersTransformer;
        $this->doctorTransformer = new DoctorTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clinicId = auth()->user()->id;
        $sort= request()->has('sort')?request()->get('sort'):'first_name';
        $order= request()->has('order')?request()->get('order'):'asc';
        $search= request()->has('searchQuery')?request()->get('searchQuery'):'';

       
        if(request()->has('sort') && request()->get('sort') == 'Name'){
            $sort = "full_name";
        }

        if(request()->has('sort') && request()->get('sort') == 'Phone'){
            $sort = "contact_number";
        }
        
        $clinic=
            User::where('r.role_id','4')
            ->where('d.clinic_id',$clinicId)
            ->join('role_user as r', 'users.id' ,'=','r.user_id')
            ->join('clinic_doctor as d', 'users.id' ,'=','d.doctor_id')

            ->whereHas('roles',function($query) use ($search)
            {
                $query->where('name','Doctor');
                if ($search) {
                    $query->where('users.first_name','like',"$search%")
                        ->orWhere('users.last_name','like',"$search%")
                        ->orWhere('users.email','like',"$search%")
                        ->orWhere('users.full_name','like',"$search%");
                }
                
            })

            ->select('users.*','d.status as doctorstatus')->orderBy("$sort", "$order")->paginate(10);

            
   
        $paginator=[
            'total_count'  => $clinic->total(),
            'total_pages'  => $clinic->lastPage(),
            'current_page' => $clinic->currentPage(),
            'limit'        => $clinic->perPage()
        ];
        return response([
            "data"        => ['doctor' => $this->doctorTransformer->transformCollection($clinic->all()),"paginator"   => $paginator],
            "message"   => 'case list',
            "status" => 200
        ],200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkEmail(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'email' => 'required|email',
        ]);

        // if error than response error with its message
        if($validator->fails()) 
        {
            return $this->respond([
                "message" => trans('api.fail_validation'),
                "status"  => 401,
            ]);
            #return $this->respondValidationError(trans('api.fail_validation'), $validator->messages());
        }

        // check email is registered or not
        $clinic = User::where('email', $request->email)->first();
    

        if($clinic)
        {           
            $role       = \DB::table('role_user')->where('role_user.user_id', $clinic->id)->leftJoin('roles as r', 'r.id', '=', 'role_user.role_id')->first();
            $roleName   =  $role->name;
            if($roleName == 'Doctor'){
                return $this->respond([
                    "message" => trans('api.email_registered'),
                    "status"  => 200,
                    "data"    => [
                        "can_link" => true,
                        "detail" => $this->usersTransformer->transform($clinic),
                        
                    ],
                ]);
            }
            return $this->respond([
                "message" => trans('api.email_registered_other',['name'=>$roleName]),
                "status"  => 200,
                "data"    => [
                    "id" => $clinic->id,
                    "can_link" => false
                ],
            ]);
        }
        else
        {
            return $this->respond([
                "message" => trans('api.email_available'),
                "status"  => $this->getStatuscode(),
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate= validate_parameter($request->all(),['first_name','username','last_name','email','contact_number','can_link','doctor_id']);
        if(!$validate){
            return $this->respond([
                "message" => trans('api.fail_validation'),
                "status"  => 401,
            ]);
        }

        $clinicId   =  auth()->user()->id;
        $clinic     =  User::where('id', $clinicId)->first();
        
        if($request->can_link == 'true'){

            $doctor =\DB::table('clinic_doctor')
            ->where('clinic_doctor.clinic_id','=',$clinicId)
            ->where('clinic_doctor.doctor_id','=',$request->doctor_id)
            ->count();

            if($doctor == 1){
                return $this->respond([
                    "message" => 'Doctor already linked',
                    "status"  => 200,
                ]);
            }

            $user       =  User::where('id', $request->doctor_id)->first();
            $clinic->givePermissionToDoctor($user);
            return $this->respond([
                "message" => 'Doctor linked successfully',
                "status"  => 200,
            ]);
        }
        else{
            $validator = validator()->make($request->all(), [
                'username'      => 'required|unique:users,username'
            ]);

            // if error than response error with its message
            if($validator->fails()) {
                return $this->respond([
                    "message" => trans('api.username_exist'),
                    "status"  => 401,
                ]);
            }
            

            $password   =  rand(111111, 999999);
            $hashed     =  \Hash::make($password);

            $user       =  User::create([
                "last_name"         => $request->last_name,
                "full_name"         => $request->first_name.' '.$request->last_name,
                "first_name"        => $request->first_name,
                "username"          => $request->username,
                "email"             => $request->email,
                "contact_number"    => $request->contact_number,
                "password"          => $hashed
            ]);

            $user->assignRole('Doctor');
            $clinic->givePermissionToDoctor($user);


            $clinic     = User::with('clinicDetail')->find($clinicId)->toarray();
            $clinic_name = $clinic['clinic_detail']['clinic_name'];


            $mailuser =  (object)[
                'first_name'    => $request->first_name,
                'clinicname'    => $clinic_name,
                'password'      => $password,
                'email'         => $request->email,
                'phonenumber'   => $request->phone,
                "username"      => $request->username,
            ];

            Mail::to($request->email)->send(new DoctorCreate($mailuser));

             #mprd($request->all());

            return $this->respond([
                    "message" => trans('api.doctor_add'),
                    "status"  => 200,
                ]);
        }

        


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function statusUpdate(Request $request)
    {
        $validate= validate_parameter($request->all(),['status','doctor_id']);
        if(!$validate){
            return $this->respond([
                "message" => trans('api.fail_validation'),
                "status"  => 401,
            ]);
        }

        $clinicId   =  auth()->user()->id;

        $doctor =\DB::table('clinic_doctor')
        ->where('clinic_doctor.clinic_id','=',$clinicId)
        ->where('clinic_doctor.doctor_id','=',$request->doctor_id)
        ->get();

        $doctor_status = $request->status == 'Inactive' ? '0' : '1';
        #mpr($clinicId); mpr($request->doctor_id);
        \DB::update('update clinic_doctor set status = '.$doctor_status.' where clinic_id = ? and doctor_id = ? ', [$clinicId,$request->doctor_id]);

        return $this->respond([
                    "message" => "Doctor status updated successfully",
                    "status"  => 200,
                ]);
    }
}
