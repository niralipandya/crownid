@extends('clinic.layout.auth')
@section('content')
	<section class="content" id="app">
	      <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Reports</a></li>
            <li class="breadcrumb-item active">Cases</li>
          </ol>
        </div>

          <div class="col-12">
              <div class="dashboard-box">
                  <div class="row bs-example-report-detail">
                     <div class="col-xl-8 col-lg-12 col-sm-12">
                        <ul class="nav nav-pills">
                            <li class="nav-item navigation right-padding" id="ongoingcase">
                                <a class="nav-link active" href="#" data-toggle="tab">ON GOING CASES</a>
                            </li>
                            <li class="nav-item navigation right-padding" id="labs">
                                <a class="nav-link" href="#" data-toggle="tab">DELAY CASES</a>
                            </li>
                            <li class="nav-item navigation" id="companion">
                                <a class="nav-link" href="#" data-toggle="tab">PENDING DELIVERY</a>
                            </li>
                        </ul>
                      </div>
                      <div class="col-xl-4 col-lg-12 col-sm-12">
                        <div class="buttonchrticon">
                         <a href="#"> <i class="fa fa-line-chart" aria-hidden="true"></i></a>
                         <a href="#" class="active"> <i class="fa fa-list-alt" aria-hidden="true"></i> </a>
                        </div>   
                        <button class="btn btn-yellow btn-report">Filter</button>
                        <a href="#"><i class="fa fa-print print-report" aria-hidden="true"></i></a>
                      </div>
                </div>
              </div>
          </div>
          
          <div class="col-12">
                <div class="dashboard-box-no-margin" id="ongoingcase">
                  <div class="row bs-example-report-table">
                    <div class="box-body content-table">
                      <table class="table table-bordered">
                        <tbody>
                          <tr>
                            <th>Case Id</th>
                            <th>Lab Name</th>
                            <th>EDD</th>
                            <th>Current Status</th>
                          </tr>
                          <tr>
                            <td>#jospau1234</td>
                            <td>Dentech Labs</td>
                            <td>May 3,2017</td>
                            <td class="bggreen">Pickup Pending</td>
                          </tr>
                          <tr>
                            <td>#jospau1234</td>
                            <td>Dentech Labs</td>
                            <td>May 3,2017</td>
                            <td class="bggray">No status</td>
                          </tr>
                           <tr>
                            <td>#jospau1234</td>
                            <td>Dentech Labs</td>
                            <td>May 3,2017</td>
                            <td class="bggreen">Pickup Pending</td>
                          </tr>
                          <tr>
                            <td>#jospau1234</td>
                            <td>Dentech Labs</td>
                            <td>May 3,2017</td>
                            <td class="bggray">No status</td>
                          </tr>
                           <tr>
                            <td>#jospau1234</td>
                            <td>Dentech Labs</td>
                            <td>May 3,2017</td>
                            <td class="bggreen">Pickup Pending</td>
                          </tr>
                          <tr>
                            <td>#jospau1234</td>
                            <td>Dentech Labs</td>
                            <td>May 3,2017</td>
                            <td class="bggray">No status</td>
                          </tr> 
                          <tr>
                            <td>#jospau1234</td>
                            <td>Dentech Labs</td>
                            <td>May 3,2017</td>
                            <td class="bggreen">Pickup Pending</td>
                          </tr>
                          <tr>
                            <td>#jospau1234</td>
                            <td>Dentech Labs</td>
                            <td>May 3,2017</td>
                            <td class="bggray">No status</td>
                          </tr>
                        
                        </tbody>
                      </table>
                    </div>
                    <div class="col-12 box-footer clearfix">
                      <ul class="pagination pagination-sm center-pagination pull-right">
                        <li><a href="#">Previous</a></li>
                        <li class="bordered"><a href="#">1</a></li>
                        <li class="bordered"><a href="#">2</a></li>
                        <li class="bordered"><a href="#">3</a></li>
                        <li><a href="#">Next</a></li>
                      </ul>
                    </div>
                     
                  </div>
                </div>
          </div>

	       
        
     </section>
@endsection
