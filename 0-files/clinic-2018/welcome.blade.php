@extends('clinic.layout.master')

@section('js')
<script type="text/javascript">
  $('#contctUS').submit(function(e){
    e.preventDefault();
   /* $.each($(e).find('.form-group'), function(){
      // Long but clear version
      if ( $(e).hasClass('has-error') ) {
        $(e).find('.help-block').show();
      } else {
        $(e).find('.help-block').hide();
      }
    });*/
  });
 
  $('#loginform').submit(function(e){
    e.preventDefault();
    $.each($(e).find('.form-group'), function(){
      // Long but clear version
      if ( $(e).hasClass('has-error') ) {
        $(e).find('.help-block').show();
      } else {
        $(e).find('.help-block').hide();
      }
    });
  });  

  $('#loginRform').submit(function(e){
    e.preventDefault();
    $.each($(e).find('.form-group'), function(){
      // Long but clear version
      if ( $(e).hasClass('has-error') ) {
        $(e).find('.help-block').show();
      } else {
        $(e).find('.help-block').hide();
      }
    });
  });
</script>
@endsection

@section('content')
   

    <!-- Main Content -->
  <div class="container">
      <div class="row" id="feature">
        <div class="col-lg-8 col-md-10 mx-auto">
          <h4 class="section-heading">Application Features</h4>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-10 col-md-10 mx-auto">
          <p class="subheading">Duis bibendum diam non erat facilaisis tincidunt. Fusce leo neque, lacinia at tempor vitae, porta at arcu. Vestibulum varius non dui at pulvinar. Ut egestas orci in quam sollicitudin aliquet. </p>
        </div>
      </div>

      <hr class="hr">

      <div class="row connector">
        <div class="col-lg-4">
            <div class="img1"><img src="{{ asset('/images/Shape2.png') }}" alt="Responsive image" /></div>
            
            <h4 class="sub-section-heading"><b>Feature title goes here</b></h4>
            <p class="small">Duis bibendum diam non erat facilaisis tincidunt. Fusce leo neque, lacinia at tempor vitae, porta at arcu.</p>
        </div>
     
        <div class="col-lg-4">
            <div class="img1"><img src="{{ asset('/images/Shape3.png') }}" alt="Responsive image" /></div>

            <h4 class="sub-section-heading"><b>Feature title goes here</b></h4>
            <p class="small">Duis bibendum diam non erat facilaisis tincidunt. Fusce leo neque, lacinia at tempor vitae, porta at arcu.</p>
        </div>
     
        <div class="col-lg-4">
            <div class="img1"><img src="{{ asset('/images/Shape4.png') }}" alt="Responsive image"/></div>

            <h4 class="sub-section-heading"><b>Feature title goes here</b></h4>
            <p class="small">Duis bibendum diam non erat facilaisis tincidunt. Fusce leo neque, lacinia at tempor vitae, porta at arcu.</p>
        </div>
       
      </div>   
  </div>

  <div class="customback">
      <div class="col-lg-10 col-md-10 mx-auto">
        <h4 class="section-heading">Our Latest screenshots gallery</h4>
      </div>
    
      <div class="col-lg-7 col-md-10 mx-auto">
        <p class="subheading">Duis bibendum diam non erat facilaisis tincidunt. Fusce leo neque, lacinia at tempor vitae, porta at arcu. Vestibulum varius non dui at pulvinar. Ut egestas orci in quam sollicitudin aliquet.</p>
      </div>
      
      <hr class="hr">

      <div class="col-lg-8 col-md-10 mx-auto">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>
             
          <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
              <div class="row removeRow">
               <img class="d-block col-4 img-fluid" src="{{ asset('/images/mobile.png') }}" alt="First slide">
               <img class="d-block col-4 img-fluid" src="{{ asset('/images/mobile1.png') }}" alt="First slide">
               <img class="d-block col-4 img-fluid" src="{{ asset('/images/mobile2.png') }}" alt="First slide">
              </div>
            </div>
            <div class="carousel-item">
              <div class="row removeRow">
                 <img class="d-block col-4 img-fluid" src="{{ asset('/images/mobile.png') }}" alt="First slide">
               <img class="d-block col-4 img-fluid" src="{{ asset('/images/mobile1.png') }}" alt="First slide">
               <img class="d-block col-4 img-fluid" src="{{ asset('/images/mobile2.png') }}" alt="First slide">
               </div>
            </div>
            <div class="carousel-item">
              <div class="row removeRow">
                <img class="d-block col-4 img-fluid" src="{{ asset('/images/mobile.png') }}" alt="First slide">
               <img class="d-block col-4 img-fluid" src="{{ asset('/images/mobile1.png') }}" alt="First slide">
               <img class="d-block col-4 img-fluid" src="{{ asset('/images/mobile2.png') }}" alt="First slide">
              </div>
            </div>
          </div>
           <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true" style="background: url('../images/4arrow.png') no-repeat; background-size: contain;"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true" style="background: url('../images/5arrow.png') no-repeat; background-size: contain;"></span>
                <span class="sr-only">Next</span>
            </a>

             
        </div>
      </div>

      <div class="row removeRow">&nbsp;</div>     
  </div>

  <div class="customback2" id="pricing">
      <div class="col-lg-10 col-md-10 mx-auto">
        <h4 class="section-heading-white">Our Plans</h4>
      </div>
    
      <div class="col-lg-7 col-md-10 mx-auto">
        <p class="subheading-white">Duis bibendum diam non erat facilaisis tincidunt. Fusce leo neque, lacinia at tempor vitae, porta at arcu. Vestibulum varius non dui at pulvinar. Ut egestas orci in quam sollicitudin aliquet.</p>
      </div>
      
      <hr class="hr">

      <div class="row removeRow">
        <div class="col-7 mx-auto">
         <ul class="nav nav-pills nav-justified">
            <li class="nav-item navigation" id="practice">
                <a class="nav-link active" href="#" data-toggle="tab">DENTAL PRACTICE</a>
            </li>
            <li class="nav-item navigation" id="labs">
                <a class="nav-link" href="#" data-toggle="tab">DENTAL LABS</a>
            </li>
            <li class="nav-item navigation" id="companion">
                <a class="nav-link" href="#" data-toggle="tab">DOCTOR COMPANION</a>
            </li>
        </ul>
        </div>
      </div>

      <div class="row box" id="practicebox">
        <div class="col-lg-4 mx-auto">
            <div class="box1">
                <div class="box-heading">BASIC</div>
                <div class="box-options">
                  <button type="button" class="btn btn-deselected" id="monthlyPlan">Monthly</button>
                  <button type="button" class="btn btn-selected" id="annualPlan">Annual</button>
                </div>
                <div class="amount-display"><span class="doller"><i class="fa fa-usd" aria-hidden="true"></i></span><span class="amount">249.99</span><span class="indication">/yr</span></div>
                <div class="userDisplay">1 user</div>
                <hr class="boxhr">
                <div class="dataDisplay">Additional Users $3/mo</div>
                <div class="box-options"><button type="button" class="btn btn-custom1" data-toggle="modal" onclick="openPurchaseModal()">BUY NOW</button>
                </div>
            </div>
        </div>
     
        <div class="col-lg-4 mx-auto">
            <div class="box1">
                <div class="box-heading">PRO</div>
                <div class="box-options">
                  <button type="button" class="btn btn-deselected" id="monthlyPlan1">Monthly</button>
                  <button type="button" class="btn btn-selected" id="annualPlan1">Annual</button>
                </div>
                <div class="amount-display"><span class="doller"><i class="fa fa-usd" aria-hidden="true"></i></span><span class="amount">299.99</span><span class="indication">/yr</span></div>
                <div class="userDisplay">3 users</div>
                <hr class="boxhr">
                <div class="dataDisplay">Additional Users $3/mo</div>
                <div class="box-options"><button type="button" class="btn btn-custom1" data-toggle="modal" onclick="openPurchaseModal()">BUY NOW</button>
                </div>
            </div>
        </div>

        <div class="col-lg-4 mx-auto">
            <div class="box1">
                <div class="box-heading">PREMIUM</div>
                <div class="box-options">
                  <button type="button" class="btn btn-deselected" id="monthlyPlan2">Monthly</button>
                  <button type="button" class="btn btn-selected" id="annualPlan2">Annual</button>
                </div>
                <div class="amount-display"><span class="doller"><i class="fa fa-usd" aria-hidden="true"></i></span><span class="amount">399.99</span><span class="indication">/yr</span></div>
                <div class="nodataDisplay">Unlimited users</div>
                <div class="box-options"><button type="button" class="btn btn-custom1" data-toggle="modal" onclick="openPurchaseModal()">BUY NOW</button>
                </div>
            </div>
        </div>
      </div>

      <div class="row box" id="labsbox" style="display: none">
        <div class="col-lg-4">
            <div class="box1">
                <div class="box-heading">BASIC</div>
                <div class="box-options">
                  <button type="button" class="btn btn-deselected">Monthly</button>
                  <button type="button" class="btn btn-selected">Annual</button>
                </div>
                <div class="amount-display"><span class="doller"><i class="fa fa-usd" aria-hidden="true"></i></span><span class="amount">249.99</span><span class="indication">/yr</span></div>
                <div class="userDisplay">1 user</div>
                <hr class="boxhr">
                <div class="dataDisplay">Additional Users $3/mo</div>
                <div class="box-options"><button type="button" class="btn btn-custom1" data-toggle="modal" onclick="openPurchaseModal()">BUY NOW</button>
                </div>
            </div>
        </div>
     
        <div class="col-lg-4">
            <div class="box1">
                <div class="box-heading">PRO</div>
                <div class="box-options">
                  <button type="button" class="btn btn-deselected">Monthly</button>
                  <button type="button" class="btn btn-selected">Annual</button>
                </div>
                <div class="amount-display"><span class="doller"><i class="fa fa-usd" aria-hidden="true"></i></span><span class="amount">299.99</span><span class="indication">/yr</span></div>
                <div class="userDisplay">3 users</div>
                <hr class="boxhr">
                <div class="dataDisplay">Additional Users $3/mo</div>
                <div class="box-options"><button type="button" class="btn btn-custom1" data-toggle="modal" onclick="openPurchaseModal()">BUY NOW</button>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="box1">
                <div class="box-heading">PREMIUM</div>
                <div class="box-options">
                  <button type="button" class="btn btn-deselected">Monthly</button>
                  <button type="button" class="btn btn-selected">Annual</button>
                </div>
                <div class="amount-display"><span class="doller"><i class="fa fa-usd" aria-hidden="true"></i></span><span class="amount">399.99</span><span class="indication">/yr</span></div>
                <div class="nodataDisplay">Unlimited users</div>
                <div class="box-options"><button type="button" class="btn btn-custom1" data-toggle="modal" onclick="openPurchaseModal()">BUY NOW</button>
                </div>
            </div>
        </div>
      </div>

      <div class="row box" id="companionbox" style="display: none">
        <div class="col-lg-4">
            <div class="box1">
                <div class="box-heading">BASIC</div>
                <div class="box-options">
                  <button type="button" class="btn btn-deselected">Monthly</button>
                  <button type="button" class="btn btn-selected">Annual</button>
                </div>
                <div class="amount-display"><span class="doller"><i class="fa fa-usd" aria-hidden="true"></i></span><span class="amount">249.99</span><span class="indication">/yr</span></div>
                <div class="userDisplay">1 user</div>
                <hr class="boxhr">
                <div class="dataDisplay">Additional Users $3/mo</div>
                <div class="box-options"><button type="button" class="btn btn-custom1" data-toggle="modal" onclick="openPurchaseModal()">BUY NOW</button>
                </div>
            </div>
        </div>
     
        <div class="col-lg-4">
            <div class="box1">
                <div class="box-heading">PRO</div>
                <div class="box-options">
                  <button type="button" class="btn btn-deselected">Monthly</button>
                  <button type="button" class="btn btn-selected">Annual</button>
                </div>
                <div class="amount-display"><span class="doller"><i class="fa fa-usd" aria-hidden="true"></i></span><span class="amount">299.99</span><span class="indication">/yr</span></div>
                <div class="userDisplay">3 users</div>
                <hr class="boxhr">
                <div class="dataDisplay">Additional Users $3/mo</div>
                <div class="box-options"><button type="button" class="btn btn-custom1" data-toggle="modal" onclick="openPurchaseModal()">BUY NOW</button>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="box1">
                <div class="box-heading">PREMIUM</div>
                <div class="box-options">
                  <button type="button" class="btn btn-deselected">Monthly</button>
                  <button type="button" class="btn btn-selected">Annual</button>
                </div>
                <div class="amount-display"><span class="doller"><i class="fa fa-usd" aria-hidden="true"></i></span><span class="amount">399.99</span><span class="indication">/yr</span></div>
                <div class="nodataDisplay">Unlimited users</div>
                <div class="box-options"><button type="button" class="btn btn-custom1" data-toggle="modal" onclick="openPurchaseModal()">BUY NOW</button>
                </div>
            </div>
        </div>
      </div>
  </div>
  
  <div class="customback-nopadding" id="aboutus">
      <div class="col-lg-10 col-md-10 mx-auto">
        <h4 class="black-section-heading">About Us</h4>
      </div>

      <hr class="hr1">
    
      <div class="col-lg-10 col-md-10 mx-auto">
        <p class="subheading">Duis bibendum diam non erat facilaisis tincidunt. Fusce leo neque, lacinia at tempor vitae, porta at arcu. Vestibulum varius non dui at pulvinar. Ut egestas orci in quam sollicitudin aliquet. Duis bibendum diam non erat facilaisis tincidunt. Fusce leo neque, lacinia at tempor vitae, porta at arcu. Vestibulum varius non dui at pulvinar. Ut egestas orci in quam sollicitudin aliquet. Duis bibendum diam non erat facilaisis tincidunt. Fusce leo neque, lacinia at tempor vitae, porta at arcu. Vestibulum varius non dui at pulvinar. Ut egestas orci in quam sollicitudin aliquet. Duis bibendum diam non erat facilaisis tincidunt. Fusce leo neque, lacinia at tempor vitae, porta at arcu. Vestibulum varius non dui at pulvinar. Ut egestas orci in quam sollicitudin aliquet.</p>
      </div>
  </div>

  <div class="contentcustom" id="contactus">
      <div class="col-lg-10 col-md-10 mx-auto">
        <h4 class="black-section-heading">Contact Us</h4>
      </div>

      <hr class="hr1">
      <section class="content">
          <contact></contact>
      </section>
  </div>

   <component is='purchase' inline-template>
      <div class="modal fade" id="purchase" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                    <span class="whitebg">DENTAL PRACTICE</span>
                    
                </div>
                <div class="modal-body">
                  <form id="loginform" name="loginform" method="POST" enctype="multipart/form-data">
                      <div class="col-lg-10 mx-auto form-group" v-bind:class="{ 'has-error': $v.email.$error }">
                          <label class="form-label">Enter Your Email Address</label>
                          <input type="email" class="form-control" name="email" id="email" value="" v-model="email" @input="$v.email.$touch()" placeholder="(eg) smith.s@company.com" required>
                          <span class="form-error-message" v-bind:class="{ 'help-block': $v.email.$error }" v-if="!$v.email.required">Email Field is required</span>
                      </div>
                      <div class="col-lg-10 mx-auto">
                          <div class="information">
                            <span class="information-label">Plan</span><br/>
                            <span class="information-info">Basic</span>
                          </div>

                           <div class="information">
                            <span class="information-label">Subscription</span><br/>
                            <span class="information-info">Yearly</span>
                          </div>

                           <div class="information">
                            <span class="information-label">Price</span><br/>
                            <span class="information-info">$ 249.99/mo</span>
                          </div>

                           <div class="information">
                            <span class="information-label">Free User</span><br/>
                            <span class="information-info">1 User</span>
                          </div>
                      </div>

                        <div class="col-lg-10">
                          <button type="submit" class="btn btn-custom4" @click="submitpurchase" disabled="disabled">CONTINUE PAYMENT</button>
                        </div>

                  </form>
                </div>
                
              </div>
            </div>
        </div>
    </component>


@endsection

