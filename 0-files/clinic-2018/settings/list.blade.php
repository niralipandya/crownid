@extends('clinic.layout.auth')
@section('content')
	<section class="content" id="app">
	      <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Settings</a></li>
            <li class="breadcrumb-item active">General</li>
          </ol>
        </div>

          <div class="col-12">
              <div class="dashboard-box">
                  <div class="row bs-example-support-detail">
                      <div class="col-7">
                        <span class="inner-box-support">GENERAL SETTINGS</span>
                      </div>
                  </div>
              </div>
          </div>
          
          <div class="col-12">
                <div class="dashboard-box-no-margin" id="ongoingcase">
                  <div class="row bs-example-report-table">
                    <div class="box-body content-table">
                      <table class="table table-bordered">
                        <tbody>
                          <tr>
                            <td>
                              <div class="row sub-classes-tikets">
                                 <div class="col-2">
                                  <span class="support-name">Language:</span> 
                                 </div> 
                                 <div class="col-10">
                                   <label class="form-check-label">
                                  <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked><span class="table-span">English</span></label>
                                   <label class="form-check-label">
                                  <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2"><span class="table-span">Spanish</span></label>
                                 </div>
                              </div>
                            </td>
                          </tr>

                          <tr>
                            <td>
                              <div class="row sub-classes-tikets">
                                 <div class="col-2">
                                  <span class="support-name">Date format:</span> 
                                 </div> 
                                 <div class="col-10">
                                    <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="gridRadiosdate" id="gridRadios1" value="option1" checked><span class="table-span">dd/mm/yy</span></label>
                                     <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="gridRadiosdate" id="gridRadios2" value="option2"><span class="table-span">mm/dd/yy</span></label>
                                  
                                 </div>
                              </div>
                            </td>
                          </tr>

                           
                        </tbody>
                      </table>
                    </div>
                  
                  </div>
                </div>
          </div>

	       
        
     </section>
@endsection
