@extends('clinic.layout.auth')
@section('content')
	<section class="content" id="app">
	      <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Support</li>
          </ol>
        </div>

          <div class="col-12">
              <div class="dashboard-box">
                  <div class="row bs-example-support-detail">
                      <div class="col-xl-7 col-lg-12 col-sm-12">
                        <span class="inner-box-support">SUPPORT TICKETS</span>
                      </div>
                      <div class="col-xl-5 col-lg-12 col-sm-12">
                          <div class="box-tools">
                            <form class="form-inline" @submit.prevent="searchInput">
                              <div class="form-group">
                                <div class="input-group">
                                  <label class="searchlabel">Search:</label>
                                  <input type="text" v-model="searchQuery" class="form-control" id="exampleInputAmount" placeholder="Search" @keyup.delete="searchChanges">
                                </div>
                              </div>
                              <transition name="custom-classes-transition" enter-active-class="animated tada" leave-active-class="animated bounceOutRight">
                                <button class="btn btn-yellow"> <i class="fa fa-plus"></i>&nbsp;Create New</button>
                              </transition>
                            </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          
          <div class="col-12">
                <div class="dashboard-box-no-margin" id="ongoingcase">
                  <div class="row bs-example-report-table">
                    <div class="box-body content-table">
                      <table class="table table-bordered">
                        <tbody>
                          <tr>
                            <td>
                              <ul class="support-table"><li>How do you get a medical claim form to print?</li></ul>
                              <span class="blue-class">Answer:</span> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                              <div class="row sub-classes-tikets">
                                 <div class="col-3">
                                  <span class="support-name">Ticket Number</span> : 2488851
                                 </div> 
                                 <div class="col-3">
                                  <span class="support-name">Ticket Date</span> : 03 July,2017 15:32:16
                                 </div> 
                                 <div class="col-3">
                                  <span class="support-name">Status</span> : <span class="bggreen">Open</span>
                                 </div>
                                 <div class="col-3">
                                  <button class="btn btn-default">Replay</button> 
                                  <a type="button" class="btn btn-default btn-round">
                                   <i class='fa fa-trash-o'></i>
                                  </a>
                                 </div>  
                              </div>
                            </td>
                          </tr>

                           <tr>
                            <td>
                              <ul class="support-table"><li>How do you get a medical claim form to print?</li></ul>
                              <span class="blue-class">Answer:</span> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                              <div class="row sub-classes-tikets">
                                 <div class="col-3">
                                  <span class="support-name">Ticket Number</span> : 2488851
                                 </div> 
                                 <div class="col-3">
                                  <span class="support-name">Ticket Date</span> : 03 July,2017 15:32:16
                                 </div> 
                                 <div class="col-3">
                                  <span class="support-name">Status</span> : <span class="red">Closed</span>
                                 </div>
                                 <div class="col-3">
                                  <button class="btn btn-default">Replay</button> 
                                  <a type="button" class="btn btn-default btn-round">
                                   <i class='fa fa-trash-o'></i>
                                  </a>
                                 </div>  
                              </div>
                            </td>
                          </tr>

                           <tr>
                            <td>
                              <ul class="support-table"><li>How do you get a medical claim form to print?</li></ul>
                              <span class="blue-class">Answer:</span> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                              <div class="row sub-classes-tikets">
                                 <div class="col-3">
                                  <span class="support-name">Ticket Number</span> : 2488851
                                 </div> 
                                 <div class="col-3">
                                  <span class="support-name">Ticket Date</span> : 03 July,2017 15:32:16
                                 </div> 
                                 <div class="col-3">
                                  <span class="support-name">Status</span> : <span class="bggreen">Open</span>
                                 </div>
                                 <div class="col-3">
                                  <button class="btn btn-default">Replay</button> 
                                  <a type="button" class="btn btn-default btn-round">
                                   <i class='fa fa-trash-o'></i>
                                  </a>
                                 </div>  
                              </div>
                            </td>
                          </tr>
                        
                        </tbody>
                      </table>
                    </div>
                    <div class="col-12 box-footer clearfix">
                      <ul class="pagination pagination-sm center-pagination pull-right">
                        <li><a href="#">Previous</a></li>
                        <li class="bordered"><a href="#">1</a></li>
                        <li class="bordered"><a href="#">2</a></li>
                        <li class="bordered"><a href="#">3</a></li>
                        <li><a href="#">Next</a></li>
                      </ul>
                    </div>
                     
                  </div>
                </div>
          </div>

	       
        
     </section>
@endsection
