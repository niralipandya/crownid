

<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index.html"><img src="{!! url('/images/logo1.png') !!}" class="img-responsive hidden-sm hidden-xs img" alt="Responsive image"/></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>

        
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/clinic">HOME</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#feature">FEATURES</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#pricing">PRICING</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#aboutus">ABOUT US</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#contactus">CONTACT</a>
                  </li>
                  <li class="nav-item">
                  
                   <a class="nav-link js-scroll-trigger" href="javascript:void(0)">
                    <button type="button" class="btn btn-custom" data-toggle="modal" onclick="openLoginRModal()">
                       Login
                    </button>
                   </a>
                   
                  </li>
          </ul>
        </div>
        
          
      </div>
</nav>

    <component is='auth-register' inline-template>
      <div class="modal fade" id="xyz" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                   <img :src="'/images/logo1.png'" class="img-responsive imageContainer" alt="Responsive image" align="center" /><!-- 
                                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                                     </button>
                                     -->
                </div>
                <div class="modal-body">
                  <div class="modal-title">Login Information</div>
                   <form id="loginform" name="loginform" method="POST" enctype="multipart/form-data">
                      
                        <div class="col-lg-10 mx-auto form-group loginformgrp" v-bind:class="{ 'has-error': $v.email.$error }">
                          
                          <input type="email" class="form-control loginfrm" name="email" id="email" value="" v-model="email" @input="$v.email.$touch()" placeholder="Email">
                          <span class="form-error-message" v-bind:class="{ 'help-block': $v.email.$error }" v-if="!$v.email.required">Email Field is required</span>
                        </div>
                      
                      
                      
                        <div class="col-lg-10 mx-auto form-group loginformgrp" v-bind:class="{ 'has-error': $v.password.$error }">
                          
                          <input type="password" class="form-control loginfrm" id="password" name="password" value="" v-model = "password"  @input="$v.password.$touch()" placeholder="Password">
                            <span class="form-error-message" v-bind:class="{ 'help-block': $v.password.$error }">Password Field is required</span>

                        </div>

                        <div class="col-lg-10 mx-auto form-group loginformgrp" v-bind:class="{ 'has-error': $v.cpassword.$error }">
                          <input type="password" class="form-control loginfrm" id="cpassword" name="cpassword" value="" v-model = "cpassword"  @input="$v.cpassword.$touch()" placeholder="Confirm Password">
                            <span class="form-error-message" v-bind:class="{ 'help-block': $v.password.$error }">Confirm Password Field is required</span>
                            <span class="help-block" v-if="!$v.cpassword.sameAsPassword">Passwords must be identical.</span>

                        </div>

                         <div class="col-lg-10 mx-auto form-group loginformgrp">
                          <input type="text" class="form-control loginfrm" id="ref" name="ref" value="" v-model = "ref" placeholder="Referral Code(Optional)" />

                        </div>
                        <div class="col-lg-10 mx-auto">
                          <button type="submit" class="btn btn-custom3" @click="submitform">SUBMIT</button>
                        </div>
                     
                </form>
                </div>
                
              </div>
            </div>
        </div>
    </component>

    <component is='auth-login' inline-template>
      <div class="modal fade" id="register" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                   <img :src="'/images/logo1.png'" class="img-responsive imageContainer" alt="Responsive image" align="center" /><!-- 
                                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                                     </button>
                                     -->
                </div>
                <div class="modal-body">
                  <div class="modal-title">Login with</div>
                  <form id="loginRform" name="loginRform" method="POST" enctype="multipart/form-data">
                        <div class="col-lg-10 mx-auto form-group radiobutton">
                          <label class="form-check-label">
                          <input class="form-check-input" type="radio" id="picked" value="clinic" v-model="picked" checked><span class="table-span">Clinic</span></label>
                           <label class="form-check-label">
                          <input class="form-check-input" type="radio" id="gridRadios2" value="lab" v-model="picked"><span class="table-span">Lab</span></label>
                        </div>


                        <div class="col-lg-10 mx-auto form-group loginformgrp" v-bind:class="{ 'has-error': $v.email.$error }">
                          
                          <input type="email" class="form-control loginfrm" name="email" id="email" value="" v-model="email" @input="$v.email.$touch()" placeholder="Email">
                          <span class="form-error-message" v-bind:class="{ 'help-block': $v.email.$error }" v-if="!$v.email.required">Email Field is required</span>
                        </div>
                      
                      
                      
                        <div class="col-lg-10 mx-auto form-group loginformgrp" v-bind:class="{ 'has-error': $v.password.$error }">
                          
                          <input type="password" class="form-control loginfrm" id="password" name="password" value="" v-model = "password"  @input="$v.password.$touch()" placeholder="Password">
                            <span class="form-error-message" v-bind:class="{ 'help-block': $v.password.$error }">Password Field is required</span>

                        </div>

                        <div class="col-lg-10 col-sm-12 mx-auto form-group">
                         <div class="checkbox pull-left">
                              
                                  <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> 
                                  <span class="chckboxlab">Remember Me</span>
                              
                          </div>
                          <a class="" href="#">
                                     <span class="forgotclass pull-right"> Forgot Password?</span>
                          </a>
                        </div>

                      
                        <div class="col-lg-10 mx-auto">
                          <button type="submit" class="btn btn-custom3" @click="submitloginform">LOGIN</button>
                        </div>

                        
                        
                        
                     
                  </form>
                </div>
              
              </div>
               <div class="customfooter">
                   Don't have an account? <a href="#" class="footer-anchor" data-toggle="modal" onclick="openLoginModal()">Sign Up &nbsp;<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
</a>
               </div>
            </div>
        </div>
    </component>


 