<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
  <ul class="sidebar-menu">
    <li class="sidebar-li {{ $current_route_name=='clinic.home'?'active':'' }}">
    <a href="{{ url('/clinic/home') }}">
        <i class="fa fa-home fa-lg"></i> <span>HOME</span>
    </a></li>
    <li class="sidebar-li {{ $current_route_name=='clinic.doctor.index'?'active':'' }}">
    <a href="{{ url('clinic/doctor') }}">
        <i class="fa fa-user-md fa-lg"></i> <span>DOCTORS</span>
    </a></li>
    <li class="sidebar-li {{ $current_route_name=='clinic.chat.index'?'active':'' }}">
    <a href="{{ url('clinic/chat') }}">
        <i class="fa fa-comment fa-lg"></i> <span>CHATS<span class="label label-danger">10</span></span> 
    </a></li>
    <li class="sidebar-li {{ $current_route_name=='clinic.labs.index'?'active':'' }}">
    <a href="{{ url('clinic/labs') }}">
        <i class="fa fa-flask fa-lg"></i> <span>LABS</span>
    </a></li>
    <li class="sidebar-li {{ $current_route_name=='clinic.caselibrary'?'active':'' }}">
    <a href="{{ url('clinic/caselibrary') }}">
        <i class="fa fa-file-code-o fa-lg"></i> <span>CASE LIBRARY</span>
    </a></li>
    <li class="sidebar-li"><a href="#">
        <i class="fa fa-tags fa-lg"></i> <span>PURCHASES</span>
    </a></li>
    <li class="treeview sidebar-li {{ in_array($current_route_name,['clinic.report.index','clinic.report.labs'])?'active':'' }}">
      <a href="{{ url('clinic/report') }}">
        <i class="fa fa-medkit fa-lg"></i> <span>REPORTS</span>
         <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
      </a>
      <ul class="treeview-menu">
            <li class="sidebar-li {{ $current_route_name=='clinic.report.index'?'active':'' }}">
              <a href="{{ url('clinic/report') }}">CASE REPORTS</a>
            </li>
            <li class="sidebar-li {{ $current_route_name=='clinic.report.labs'?'active':'' }}">
              <a href="#">LABS REPORTS</a>
            </li>
      </ul>
    </li>
    <li class="treeview sidebar-li  {{ in_array($current_route_name,['clinic.support.index','clinic.support.faq','clinic.support.contactus'])?'active':'' }}">
      <a href="{{ url('/') }}">
          <i class="fa fa-question-circle fa-lg"></i> <span>SUPPORT</span>

            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
      </a>

      <ul class="treeview-menu">
            <li class="sidebar-li {{ $current_route_name=='clinic.support.index'?'active':'' }}">
              <a href="{{ url('clinic/support') }}">SUPPORT TICKET</a>
            </li>
            <li class="sidebar-li {{ $current_route_name=='clinic.support.faq'?'active':'' }}">
              <a href="#">FAQ</a>
            </li>

            <li class="sidebar-li {{ $current_route_name=='clinic.support.contactus'?'active':'' }}">
              <a href="#">CONTACT US</a>
            </li>
      </ul>
    </li>
    <li class="treeview sidebar-li {{ in_array($current_route_name,['clinic.settings.index','clinic.settings.default','clinic.aboutus'])?'active':'' }}">
      <a href="{{ url('/') }}">
        <i class="fa fa-cog fa-lg"></i> <span>SETTINGS</span>
         <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
          </span>
      </a>

      <ul class="treeview-menu">
            <li class="sidebar-li {{ $current_route_name=='clinic.settings.index'?'active':'' }}">
              <a href="{{ url('clinic/settings') }}">GENERAL</a>
            </li>
           <li class="treeview sidebar-li {{ in_array($current_route_name,['clinic.aboutus','clinic.contactus'])?'active':'' }}">
              <a href="#"> <span>DEFAULT</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
               <ul class="treeview-menu">
                <li class="sidebar-li {{ $current_route_name=='clinic.aboutus'?'active':'' }}">
                  <a href="{{ url('clinic/settings/aboutus') }}">About Us</a>
                </li>
              
               </ul>
            </li>
      </ul>
    </li>
  
  </ul>
  </section>
  <!-- /.sidebar -->
</aside>