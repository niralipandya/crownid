<header class="masthead" style="background-image: url('/images/banner.png')">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10">
            <div class="site-heading">
                    <div class="intro-lead-in">Welcome To CrownID</div>
                    <div class="intro-heading">Duis bibendum diam</div>
                    <div class="paragraph">
                    Donec elementum mollis magna id aliquet. Etiam eleifend urna eget sem 
                    sagittis feugiat. Pellentesque habitant morbi tristique senectus et 
                    netus et malesuada fames ac turpis egestas.</div>
                    <div class="intro-heading">
                        <a href="#" class=""><img src="{!! url('/images/app_store.png') !!}" class="img-responsive
                        hidden-sm hidden-xs image" alt="Responsive image"/> </a>
                        <a href="#" class=""><img src="{!! url('/images/google_play.png') !!}" class="img-responsive hidden-sm hidden-xs" alt="Responsive image"/></a>
                    </div>
                
            </div>
          </div>
        </div>
      </div>
</header>