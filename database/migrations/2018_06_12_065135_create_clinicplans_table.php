<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClinicplansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinicplans', function (Blueprint $table) {
            $table->increments('id');
            $table->float('price', 8, 2); 
            $table->string('name', 100);   
            $table->string('currency', 100);   
            $table->string('doctor_text', 100)->nullable(); 
            $table->string('token_text', 100)->nullable(); 
            $table->enum('type', ['Month', 'Year'])->default('Month');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinicplans');
    }
}
