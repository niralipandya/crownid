<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration
{
    /**
     * Run the migrations.
     *ALTER TABLE `cases` CHANGE `status` `status` ENUM('Pending','Received','In Process','Delayed','Ready for delivery','Delivered to patient') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Pending';
     * @return void
     */
    public function up()
    {
        Schema::create('cases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('case_code')->nullable();
            $table->integer('parent_id')->default(0)->nullable();
            $table->boolean('have_child')->default(0)->nullable();
            $table->integer('clinic_id');
            $table->integer('lab_id')->nullable();
            $table->integer('doctor_id')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('prosthesis_id')->nullable()->default(0);
            $table->string('problematic_teeth')->nullable()->default(0);
            $table->string('missing_teeth')->nullable()->default(0);
            $table->string('bite_registration_id')->nullable()->default(0);
            $table->string('material_id')->nullable()->default(0);
            $table->string('material_other')->nullable();
            $table->string('metal_ceramic_id')->nullable()->default(0);
            $table->string('metal_ceramic_text')->nullable();
            $table->string('prosthetic_margins')->nullable()->default(0);
            $table->string('ceramic_id')->nullable()->default(0);
            $table->string('ceramic_facial_text')->nullable();
            $table->string('color_id')->nullable()->default(0);
            $table->string('color_text')->nullable();
            $table->string('color_neck_id')->nullable()->default(0);
            $table->string('color_body_id')->nullable()->default(0);
            $table->string('delivery_id')->nullable()->default(0);
            $table->string('delivery_text')->nullable();
            $table->string('date_selected')->nullable()->default(0);
            $table->string('delivery_date_text')->nullable();
            $table->text('comment')->nullable();
            $table->date('end_date')->nullable();
            
            $table->string('qr_code')->nullable()->default(0);
            $table->string('bar_code')->nullable()->default(0);
            $table->enum('status', ['Pending','Received','In Progress','Delayed','Ready for delivery','Delivered to patient'])->default('Pending');
            $table->integer('created_by_id')->default(0);
            $table->string('created_by_name');
            $table->string('retake_text')->nullable();
            $table->string('solder_text')->nullable();
            $table->string('tryin_type')->nullable();
            $table->string('tryin_text')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });

        Schema::create('cases_image', function (Blueprint $table) {
            $table->integer('cases_id')->unsigned();
            $table->foreign('cases_id')->references('id')->on('cases')->onDelete('cascade');
            
            $table->integer('image_id')->unsigned();
            $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');

            $table->primary(['cases_id','image_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cases');
        Schema::dropIfExists('images');
        Schema::dropIfExists('case_image');
    }
}
