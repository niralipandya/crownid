<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaselogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caselogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('case_id')->unsigned();
            $table->text('text')->nullable();
            $table->integer('type')->nullable();
            $table->string('type_display')->default('')->nullable();
            $table->string('class')->default('');
            $table->integer('created_by_id')->default(0);
            $table->string('created_by_name')->default('');
            $table->string('created_by_type')->default('');
            $table->date('display_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caselogs');
    }
}
