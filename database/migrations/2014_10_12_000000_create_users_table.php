<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     ALTER TABLE `users` ADD `is_approved` ENUM('Approved','Pending Approval') NULL DEFAULT 'Pending Approval' AFTER `is_registered`;
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->unique();
            $table->string('username')->nullable();
            $table->string('password')->nullable();
            $table->string('contact_number', 15)->nullable();
            
            $table->string('image')->nullable();
            $table->string('image_thumb')->nullable();

            $table->string('referral_code', 10)->unique()->nullable();
            $table->enum('language', ['en', 'es'])->default('en')->comment('en = English, es = Spanish');
            $table->string('date_format', 15)->default('dd-mm-yyyy');

            $table->boolean('status')->default(1);
            $table->integer('case_tokens')->default(0);
            $table->integer('doctor_tokens')->default(0);
            $table->boolean('is_purchased')->default(0);
            $table->boolean('is_registered')->default(0)->comment("clinic is fully register or not");
            $table->enum('is_approved', ['Approved', 'Pending Approval'])->default('Pending Approval')->nullable();
            $table->boolean('verified')->default(1);
            $table->text('api_token')->nullable();
            $table->string('otp', 10)->nullable();

            $table->enum('device_type', ['ios', 'android'])->default('ios');
            $table->string('device_token')->nullable();
            
            $table->timestamps();
        });

        Schema::create('clinic_details', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('clinic_name')->nullable();
            $table->string('current_package')->nullable();
            $table->text('address')->nullable();
            $table->text('city')->nullable();
            $table->text('country')->nullable();

            $table->string('image')->nullable();
            $table->string('image_thumb')->nullable();

            $table->integer('prosthetic_id')->default(0);
            $table->integer('metal_id')->default(0);
            $table->integer('delivery_id')->default(0);
            
            $table->integer('total_credits')->default(0);
            $table->integer('used_credits')->default(0);

            $table->boolean('is_purchased')->default(0);
        });

        Schema::create('clinic_doctor',function(Blueprint $table){
            $table->integer('clinic_id')->unsigned();
            $table->foreign('clinic_id')->references('id')->on('users')->onDelete('cascade');
            
            $table->integer('doctor_id')->unsigned();
            $table->foreign('doctor_id')->references('id')->on('users')->onDelete('cascade');

            $table->boolean('is_selected')->default(0);
            $table->boolean('is_purchased')->default(0);
            $table->boolean('status')->default(1);
            
            $table->primary(['clinic_id','doctor_id']);
        });

        Schema::create('clinic_labs',function(Blueprint $table){
            $table->integer('clinic_id')->unsigned();
            $table->foreign('clinic_id')->references('id')->on('users')->onDelete('cascade');
            
            $table->integer('lab_id')->unsigned();
            $table->foreign('lab_id')->references('id')->on('users')->onDelete('cascade');

            $table->primary(['clinic_id','lab_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('clinic_details');
        Schema::dropIfExists('clinic_doctor');
        Schema::dropIfExists('clinic_labs');
    }
}
