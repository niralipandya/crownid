<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokenhistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tokenhistory', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->float('amount', 8, 2)->nullable(); 
            $table->string('currency', 100)->nullable(); 
            $table->string('remark')->nullable(); 
            $table->text('message')->nullable(); 
            $table->integer('token_count');
            $table->enum('user_type', ['clinic','doctor']);
            $table->enum('type', ['free','paid']);
            $table->enum('paid_from', ['web','android','ios','cron','create-web','create-android','create-ios'])->nullable();
            $table->integer('payment_id')->references('id')->on('payments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tokenhistories');
    }
}
