<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDelayboxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delayboxes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clinic_id');
            $table->integer('lab_id')->nullable();
            $table->integer('doctor_id')->nullable();
            $table->string('case_code')->nullable();
            $table->date('due_date')->nullable();
            $table->date('expeted_date')->nullable();
            $table->text('remarks')->nullable();
            $table->enum('status', ['Pending','In Progress','Completed'])->default('Pending');
            $table->integer('addedBy')->nullable();
            $table->enum('addedByType', ['','lab', 'clinic','doctor','admin'])->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delayboxes');
    }
}
