<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');
            $table->float('price', 8, 2); 
            $table->string('name', 100);   
            $table->string('currency', 100);   
            $table->string('user', 100)->nullable(); 
            $table->string('additionalprice', 100)->nullable(); 
            $table->enum('additionaltype', ['Monthly', 'Yearly'])->default('Monthly')->nullable();
            $table->enum('type', ['Monthly', 'Yearly'])->default('Monthly');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
