<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('case_id');
            $table->string('case_code')->nullable();           
            $table->enum('reciver_type',['','Clinic','Lab','Doctor'])->default('');
            $table->enum('push_type',['create','receive','process','delayed','ready','delivered','repetition','edit','assigned']);
            $table->text('message_es')->nullable();
            $table->text('message_en')->nullable();
            $table->string('send_from')->nullable();
            $table->integer('sender_id');
            $table->enum('sender_type',['Lab','Clinic','Doctor','Admin','Cron'])->default('Lab');
            $table->tinyInteger('is_send')->default(0);
            $table->tinyInteger('is_read')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
