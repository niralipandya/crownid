<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferencecodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referencecode', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phonenumber')->nullable();
            $table->string('code')->nullable();
            $table->enum('used', ['yes', 'no'])->default('no');
            $table->enum('for', ['','lab', 'clinic','doctor'])->default('');
            $table->integer('sendBy')->nullable();
            $table->enum('sendByType', ['','lab', 'clinic','doctor'])->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('referencecode');
    }
}
