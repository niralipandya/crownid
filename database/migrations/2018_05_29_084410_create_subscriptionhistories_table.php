<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionhistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptionhistory', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->references('id')->on('users');
            $table->enum('user_type', ['clinic','doctor']);
            $table->enum('plan', ['Basic','Pro','Premium']);
            $table->enum('type', ['Month','Year'])->nullable();
            $table->float('amount', 8, 2)->nullable(); 
            $table->string('currency', 100)->nullable();  
            $table->datetime('bill_date')->nullable();
            $table->datetime('next_bill_date')->nullable();
            $table->datetime('next_token_date')->nullable();
            $table->enum('paid_from', ['web','android','ios']);
            $table->binary('receipt');
            $table->integer('payment_id')->references('id')->on('payments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptionhistory');
    }
}
