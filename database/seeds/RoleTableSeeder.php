<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name'     => "developer",
                'label'    => "Developer"
            ],
            [
                'name'     => "Super",
                'label'    => "Super"
            ],
            [
                'name'     => "Clinic",
                'label'    => "Clinic"
            ],
            [
                'name'     => "Doctor",
                'label'    => "Doctor"
            ],
            [
                'name'     => "Lab",
                'label'    => "Lab"
            ],
        ]);
    }
}
