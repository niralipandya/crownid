<?php

use Illuminate\Database\Seeder;

class CreditsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Credittype::create([
            'price'     => "10",
            'currency'    => "$",
            'credits' => "5",
        ]);
        App\Credittype::create([
            'price'     => "20",
            'currency'    => "$",
            'credits' => "10",
        ]);
        App\Credittype::create([
            'price'     => "25",
            'currency'    => "$",
            'credits' => "15",
        ]);
    }
}
