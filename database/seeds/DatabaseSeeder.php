<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        Model::unguard();
        $this->call(AdminSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(RolePermissionTableSeeder::class);
        $this->call(AdminRoleTableSeeder::class);
        $this->call(CreditsTableSeeder::class);
        $this->call(PlanTableSeeder::class);
       
    }
}
