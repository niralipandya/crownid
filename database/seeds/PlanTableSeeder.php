<?php

use Illuminate\Database\Seeder;

class PlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Plan::create([
            'price'     => "249.99",
            'name'     => "Basic",
            'currency'    => "$",
            'user' => "1",
            'additionalprice' => "3",
            'additionaltype' => "Monthly",
            'type' => "Yearly",
        ]);
        App\Plan::create([
            'price'     => "299.99",
            'name'     => "Pro",
            'currency'    => "$",
            'user' => "3",
            'additionalprice' => "3",
            'additionaltype' => "Monthly",
            'type' => "Yearly",
        ]);
        App\Plan::create([
            'price'     => "399.99",
            'name'     => "Premimum",
            'currency'    => "$",
            'user' => "Unlimited",
            'additionalprice' => "3",
            'additionaltype' => "Monthly",
            'type' => "Yearly",
        ]);
        App\Plan::create([
            'price'     => "29.99",
            'name'     => "Basic",
            'currency'    => "$",
            'user' => "1",
            'additionalprice' => "3",
            'additionaltype' => "Monthly",
            'type' => "Monthly",
        ]);
        App\Plan::create([
            'price'     => "39.99",
            'name'     => "Pro",
            'currency'    => "$",
            'user' => "3",
            'additionalprice' => "3",
            'additionaltype' => "Monthly",
            'type' => "Monthly",
        ]);
        App\Plan::create([
            'price'     => "59.99",
            'name'     => "Premimum",
            'currency'    => "$",
            'user' => "Unlimited",
            'additionalprice' => "3",
            'additionaltype' => "Monthly",
            'type' => "Monthly",
        ]);
    }
}
